<?php

namespace App\Observers;

use App\Models\Department;
use App\Models\Role;
use App\Models\Audit\Enum\Events;

class DepartmentObserver
{

  public function created(Department $model)
  {
		if (Role::isAdmin()) {
			$model->newAuditEvent(Events::ADMIN_DEPARTMENT_CREATE);
		}
  }

  public function deleted(Department $model)
  {
		if (Role::isAdmin()) {
			$model->newAuditEvent(Events::ADMIN_DEPARTMENT_SOFT_DELETE);
		}
  }
}
