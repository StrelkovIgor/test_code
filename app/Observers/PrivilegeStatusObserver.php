<?php

namespace App\Observers;

use App\Models\PrivilegeStatus;
use App\Models\Role;
use App\Models\Audit\Enum\Events;

class PrivilegeStatusObserver
{

  public function updated(PrivilegeStatus $model)
  {
		if (Role::isAdmin()) {
			$model->newAuditEvent(Events::ADMIN_STATUS_EDIT);
		}
  }

  public function created(PrivilegeStatus $model)
  {
		if (Role::isAdmin()) {
			$model->newAuditEvent(Events::ADMIN_STATUS_CREATE);
		}
  }

  public function deleted(PrivilegeStatus $model)
  {
		if (Role::isAdmin()) {
			$model->newAuditEvent(Events::ADMIN_STATUS_SOFT_DELETE);
		}
  }
}
