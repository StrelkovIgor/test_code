<?php

namespace App\Observers;

use App\Models\ControlPost;
use App\Models\Role;
use App\Models\Audit\Enum\Events;

class ControlPostObserver
{

  public function created(ControlPost $model)
  {
		if (Role::isAdmin()) {
			$model->newAuditEvent(Events::ADMIN_POST_CREATE);
		}
  }

  public function deleted(ControlPost $model)
  {
		if (Role::isAdmin()) {
			$model->newAuditEvent(Events::ADMIN_POST_SOFT_DELETE);
		}
  }
}
