<?php

namespace App\Observers;

use App\Models\Coefficient;
use App\Models\Role;
use App\Models\Audit\Enum\Events;

class CoefficientObserver
{

  public function updated(Coefficient $model)
  {
		if (Role::isAdmin()) {
			$model->newAuditEvent(Events::ADMIN_COEFF_EDIT);
		}
  }
}
