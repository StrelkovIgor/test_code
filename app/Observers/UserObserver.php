<?php

namespace App\Observers;

use App\Models\Role;
use App\Models\User;
use App\Models\Audit\Enum\Events;
use Illuminate\Support\Facades\Auth;

class UserObserver
{

	public function created(User $model)
	{
		/** @var User $admin */
		$admin = Auth::user();
		if(
			$admin &&
				(
					$admin->role_id === Role::ROLE_ADMIN ||
					$admin->role_id === ROLE::ROLE_OFFICER
				)
		) {
			$model->newAuditEvent(Events::USER_CREATE, $model);
		}
	}
}
