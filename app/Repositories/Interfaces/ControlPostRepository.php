<?php

namespace App\Repositories\Interfaces;

use App\Models\ControlPost;

/**
 * Interface ControlPostRepository
 * @package App\Repositories\Interfaces
 */
interface ControlPostRepository
{
	/**
	 * @return mixed
	 */
	public function list();
	
	/**
	 * @param int $perPage
	 * @return mixed
	 */
	public function page($perPage);
	
	/**
	 * @param $data
	 * @return mixed
	 */
	public function create(array $data);
	
	/**
	 * @param $data
	 * @param ControlPost $controlPost
	 * @return mixed
	 */
	public function update(array $data, ControlPost $controlPost);
	
	/**
	 * @param ControlPost $controlPost
	 * @return boolean|null
	 */
	public function delete(ControlPost $controlPost);
}