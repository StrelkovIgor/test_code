<?php


namespace App\Repositories;

use App\Models\ControlPost;
use App\Repositories\Interfaces\ControlPostRepository as ControlPostRepositoryInterface;

/**
 * Class ControlPostRepository
 * @package App\Repositories
 */
class ControlPostRepository implements ControlPostRepositoryInterface
{
	/**
	 * @return mixed
	 */
	public function list()
	{
		return ControlPost::orderBy('title', 'asc')->get();
	}
	
	/**
	 * @param int $perPage
	 * @return mixed
	 */
	public function page($perPage)
	{
		return ControlPost::orderBy('title', 'desc')
			->paginate($perPage)
			->appends('paged');
	}
	
	/**
	 * @param array $data
	 * @return ControlPost
	 */
	public function create(array $data)
	{
		$controlPost = ControlPost::create([
			'title' => array_get($data, 'title'),
		]);
		
		return $controlPost;
	}
	
	/**
	 * @param array $data
	 * @param ControlPost $controlPost
	 * @return ControlPost|mixed
	 */
	public function update(array $data, ControlPost $controlPost)
	{
		$controlPost->update([
			'title' => array_get($data, 'title'),
		]);
		
		return $controlPost;
	}
	
	/**
	 * @param ControlPost $controlPost
	 * @return boolean|null
	 * @throws \Exception
	 */
	public function delete(ControlPost $controlPost)
	{
		return $controlPost->delete();
	}
}
