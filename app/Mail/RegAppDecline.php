<?php

namespace App\Mail;

use App\Models\RegApp;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class RegAppDecline extends Mailable
{
    use Queueable, SerializesModels;

    /** @var User $user */
    public $user;

    /** @var string $note */
    public $note;

    /**
     * Create a new password restore instance.
     *
     * @param User $user
     * @param string $note
     */
    public function __construct(User $user, $note)
    {
        $this->user = $user;
        $this->note = $note;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->subject('Заявка отклонена!')
            ->view('emails.reg-app.reg-app-decline');
    }
}
