<?php

namespace App\Mail;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class RegAppAccept extends Mailable
{
    use Queueable, SerializesModels;

    /** @var User $user */
    public $user;

    /**
     * Create a new password restore instance.
     *
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->subject('Заявка активирована!')
            ->view('emails.reg-app.reg-app-accept');
    }
}
