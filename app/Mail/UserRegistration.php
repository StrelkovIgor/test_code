<?php

namespace App\Mail;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class UserRegistration extends Mailable
{
	use Queueable, SerializesModels;
	
	/** @var User $user */
	public $user;
	
	/** @var string $password */
	public $password;
	
	/**
	 * Registration of new user
	 *
	 * @param User $user
	 * @param string $password
	 */
	public function __construct(User $user, string $password)
	{
		$this->user = $user;
		$this->password = $password;
	}
	
	/**
	 * Build the message.
	 *
	 * @return $this
	 */
	public function build()
	{
		return $this
			->subject('Подтверждение email учетной записи АИС КТГ!')
			->view('emails.user.registration');
	}
}
