<?php

namespace App\Mail;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class PasswordRestore extends Mailable
{
    use Queueable, SerializesModels;

    /** @var User $user */
    public $user;

    /** @var  string $token */
    public $token;

    /**
     * Create a new password restore instance.
     *
     * @param User $user
     * @param string $token
     * @return void
     */
    public function __construct(User $user, $token)
    {
        $this->user = $user;
        $this->token = $token;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->subject('Восстановление пароля')
            ->view('emails.auth.password-restore');
    }
}
