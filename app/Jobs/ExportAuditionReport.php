<?php

namespace App\Jobs;

use App\Models\Audit\Audit;
use App\Models\Audit\Enum\Events;
use App\Models\Role;

class ExportAuditionReport extends AbstractPortionsExport
{
	protected function getExportHeaders()
	{
		return [
			'Дата и время события',
			'Фио и роль пользователя',
			'Тип действия',
			'Объект действия'
		];
	}
	protected function getTableName()
	{
		return 'audits';
	}

	protected function getFilePath()
	{
		return [
			'logs'
		];
	}

	protected function getFileName()
	{
		return 'audit_' . time() . '.csv';
	}


	protected function initBaseQuery()
	{
		$audit = new Audit();
		$this->baseQuery = $audit
			->setFilter($this->filterData)
			->getList()
			->with(['auditable']);
	}

	/**
	 * Get portion data and conver it to array by columns
	 */
	protected function getPortionData()
	{
		/** @var Audit[] $audits */
		$audits = $this->portionQuery->get();

		$result = [];
		foreach($audits as $audit) {
			$eventCode = Events::getEventCodeByKey($audit->event_key);
			$row = [
				$audit->created_at,
				$audit->user->name . ' (' . Role::getNameById($audit->user->role_id) . ')',
				Events::getEventTitle($eventCode),
				$audit->getObjectInfo()
			];

			$result[] = $row;
		}
		return $result;
	}
}
