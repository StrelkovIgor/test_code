<?php

namespace App\Jobs;

use App\Helpers\CSVHelper;
use App\Models\Process;
use Illuminate\Bus\Queueable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

abstract class AbstractPortionsExport implements ShouldQueue
{
	use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

	/** @var int rows count in 1 query portion */
	protected $portionSize = 2000;

	/** @var int|null  */
	protected $userId = null;
	/** @var array  */
	protected $filterData = [];
	/** @var int  */
	protected $processId;

	/** @var int  */
	protected $minId = 0;
	/** @var int  */
	protected $maxId = 0;
	/** @var Builder|null  */

	/** @var int  */
	protected $portionFirstId = 0;
	/** @var int  */
	protected $portionLastId = 0;
	/** @var Builder|null  */

	protected $baseQuery = null;
	/** @var Builder|null  */
	protected $portionQuery = null;

	/** @var CSVHelper|null  */
	protected $exportHelper = null;
	/** @var array  */
	protected $pathInfo = [];

	protected $times = [];

	/**
	 * Base table name which we split to portions
	 * @return string
	 */
	protected abstract function getTableName();

	/**
	 * Headers column titles
	 * @return array
	 */
	protected abstract function getExportHeaders();

	/**
	 * Current portion data as array
	 * @return array
	 */
	protected abstract function getPortionData();

	/**
	 * Path to directory for file with exported data
	 * @return string
	 */
	protected abstract function getFilePath();

	/**
	 * Filename with exported data
	 * @return string
	 */
	protected abstract function getFileName();

	/**
	 * Init base query to get export data. Need to define $this->baseQuery
	 * @return
	 */
	protected abstract function initBaseQuery();


	/**
	 * Create a new job instance.
	 *
	 * @param int $userId
	 * @param array $filterData
	 * @param int $processId
	 *
	 * @return void
	 */
	public function __construct($userId, $filterData, $processId)
	{
		$this->userId = $userId;
		$this->filterData= $filterData;
		$this->processId = $processId;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$process = $this->initProcess();

		$result = null;

		try {
			if($process) {
				$initStart = microtime(true);

				$this->init();
				$this->initBaseQuery();
				$this->initStartPortionFrames();
				$this->initExportHelper();

				$initFinish = microtime(true);
				$this->times['init'] = $initFinish - $initStart;

				while($this->hasQueryData()) {
					$startQueryTime = microtime(true);
					$this->nextPortionQuery();
					$portionData = $this->getPortionData();
					$finishQueryTime = microtime(true);
					$queryTime = $finishQueryTime - $startQueryTime;

					$startWriteTime = microtime(true);
					$this->addExportData($portionData);

					$this->refreshProgress($process);

					$finishWriteTime = microtime(true);
					$writeTime = $finishWriteTime - $startWriteTime;
					$this->refreshPortionFrames();

					$this->times['cycles'][] = [
						'query' => $queryTime,
						'write' => $writeTime
					];
				}

				$result = $this->completeExportProcess();
			}
		} catch (\Exception $e) {
			$process->result = ['error' => $e->getMessage()];
		}

		if($result) {
			$process->status = Process::STATUS_SUCCEED;
			$process->result = [
				'url' => $this->pathInfo['url'],
				'times' => $this->times
			];
		}else{
			$process->status = Process::STATUS_FAILED;
		}

		$process->save();
	}
	
	/**
	 * Init export process
	 */
	protected function init()
	{
		$this->times = [
			'cycles' => []
		];
	}

	/**
	 * @return Process|null
	 */
	protected function initProcess()
	{
		ini_set('max_execution_time', 0);

		/** @var Process $process */
		$process = Process::find($this->processId);
		if(!$process) {
			return null;
		}

		$process->status = Process::STATUS_PROCESSING;

		$saved = $process->save();

		if($saved) {
			return $process;
		}

		return null;
	}



	/**
	 * @return CSVHelper
	 */
	protected function initExportHelper()
	{
		$this->pathInfo =  $this->getResultFilePath();
		$filename = $this->pathInfo['path'];

		$csvExporter = new CSVHelper();
		$csvExporter->initExport($filename, $this->getExportHeaders());

		$this->exportHelper = $csvExporter;
	}

	/**
	 * @return array
	 */
	protected function getResultFilePath()
	{
		$path = $this->getFilePath();
		$filename = $this->getFileName();

		$url = env('APP_URL') . '/storage/' . implode('/', $path) . '/' . $filename;

		$dirPath = 'public/' . implode('/', $path);
		\Storage::makeDirectory($dirPath);
		$path = storage_path('app/' . $dirPath . '/' . $filename);

		return [
			'path' => $path,
			'url' => $url
		];
	}

	/**
	 * @param $data
	 */
	protected function addExportData($data)
	{
		$this->exportHelper->appendRows($data);
	}
	
	/**
	 * Complete export process, store data to file
	 * @return string
	 */
	protected function completeExportProcess()
	{
		return $this->exportHelper->completeExport();
	}
	
	
	/**
	 * Check if we have next portion of data
	 * @return bool
	 */
	protected function hasQueryData()
	{
		return $this->portionFirstId <= $this->maxId;
	}
	
	/**
	 * Get next portion of data
	 */
	protected function nextPortionQuery()
	{
		$this->portionQuery = clone $this->baseQuery;

		$this->portionQuery
			->where($this->getTableName() . '.id', '>', $this->portionFirstId)
			->where($this->getTableName() . '.id', '<=', $this->portionLastId);
	}
	
	/**
	 * Get portion data
	 */
	protected function initStartPortionFrames()
	{
		$this->minId = 0;
		$this->maxId = $this->baseQuery->max($this->getTableName() . '.id');

		$this->portionFirstId = $this->minId;
		$this->portionLastId = $this->portionFirstId + $this->portionSize;
	}
	
	/**
	 * Refresh min, max id of portion
	 */
	protected function refreshPortionFrames()
	{
		$this->portionFirstId += $this->portionSize;
		$this->portionLastId += $this->portionSize;
	}

	/**
	 * @param Process $process
	 */
	protected function refreshProgress($process)
	{
		$progress = $this->calculateProgress();
		$process->progress = $progress;
		$process->save();
	}

	/**
	 * return progress in percents
	 * @return int
	 */
	private function calculateProgress()
	{
		$totalPortions = ceil($this->maxId / $this->portionSize);
        if ($totalPortions == 0) {
            return 100;
        }
		$completePortions = $this->portionLastId / $this->portionSize;

		$progress = intval(100 * $completePortions / $totalPortions);
		$progress = max(0, min($progress, 100));

		return $progress;
	}
}
