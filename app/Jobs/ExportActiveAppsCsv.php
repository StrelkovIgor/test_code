<?php

namespace App\Jobs;

use App\Models\Application;
use App\Models\ApplicationAgreement;
use App\Models\Audit\Audit;
use App\Models\Audit\Enum\Events;
use App\Models\Coefficient;
use App\Models\User;

class ExportActiveAppsCsv extends AbstractPortionsExport
{
	/** @var int */
	private $currentRow;

	/** @var Coefficient */
	private $activeReportKoeff;

	/** @var int  */
	protected $portionSize = 3000;

	protected function getExportHeaders()
	{
		return [
			'Порядковый номер',
			'Номер разрешения',
			'Номер бланка',
			'Территориальное управление',
			'Дата начала действия разрешения',
			'Дата окончания действия разрешения',
			'Организация / ФИО заявителя',
			'ИНН заявителя',
			'ГРЗ ТС (основное)',
			'Стоимость разрешения (руб)',
			'Стоимость вреда',
			'Дата и время подачи заявления',
			'Ведомства для согласования',
			'Дата и время согласования разрешения',
			'Сотрудник, согласовавший разрешение',
			'Дата и время выдачи разрешения',
			'Сотрудник, выдавший (активировавший) разрешение'
		];
	}
	protected function getTableName()
	{
		return 'applications';
	}

	protected function getFilePath()
	{
		return [
			'active-report'
		];
	}

	protected function getFileName()
	{
		return 'audit_' . time() . '.csv';
	}

	protected function init()
	{
		$this->currentRow = 1;
		$this->activeReportKoeff = Coefficient::find(Coefficient::ID_GP);
	}

	protected function initBaseQuery()
	{
	    $user = null;
	    if ($this->userId) {
            $user = User::find($this->userId);
        }


		$query = Application::acivatedStatusQuery();

		//filter by form data
		$data = $this->filterData;

		$query = Application::adminDataFilter($query, $data, $user);

		$query = Application::activeReportsFilter($query, $data);

		$query->select([
			'applications.*',
			'applications.status as status'
		])
		->with([
//			'user',
			'vehicle',
			'admin',
			'application_dates.issue_place',
			'application_agreements.department',
			'application_agreements.user',
			'application_agreements.audits_active.user',
			//'audits_report'
			'audits.user',
			//'fast_application'
		]);

		$this->baseQuery = $query;
	}

	/**
	 * Get portion data and conver it to array by columns
	 */
	protected function getPortionData()
	{
		/** @var Application[] $applications */
		$applications = $this->portionQuery->get();

		$result = [];
		foreach($applications as $application) {
			$koeff = $this->activeReportKoeff;

			$blankPrice = 0;
			if($application->is_spring) {
				$blankPrice = $koeff->spring_value;
			}else{
				$blankPrice = $koeff->value;
			}
			$damage = $application->getRealPrice() - $blankPrice;
			$damage = max(0, $damage);


			//events
			/** @var Audit $toReviewEvent */
			$toReviewEvent = null;
			/** @var Audit $acceptEvent */
			$acceptEvent = null;
			/** @var Audit $activateEvent */
			$activateEvent = null;
			foreach($application->audits as $audit) {
				if($audit->event_key === Events::APPLICANT_APP_TOREVIEW_KEY) {
					$toReviewEvent = $audit;
				}
				if($audit->event_key === Events::ADMINGBU_APP_ACCEPT_KEY) {
					$acceptEvent = $audit;
				}
				if($audit->event_key === Events::ADMINGBU_APP_ACTIVATE_KEY) {
					$activateEvent = $audit;
				}
			}

			$toReviewTime = $toReviewEvent ? $toReviewEvent->created_at : '';


			$acceptTime = $acceptEvent ? $acceptEvent->created_at : $application->accept_date;
			$acceptUsername = $acceptEvent && $acceptEvent->user ? $acceptEvent->user->name : '';
			if(!$acceptUsername){
				if($application->admin) {
					$acceptUsername = $application->admin->name;
				}
			}

			$activateTime = $activateEvent ? $activateEvent->created_at : $application->activate_date;
			$activateUsername = $activateEvent && $activateEvent->user ? $activateEvent->user->name : '';
			if(!$activateUsername) {
				if($application->admin) {
					$activateUsername = $application->admin->name;
				}
			}

			$row = [
				$this->currentRow,						//A
				$application->getFormattedId(),			//B
				$application->form_id,					//C
				$application->getIssuePlaceTitle(),		//D
				$application->startDateFormatted(),		//E
				$application->finishDateFormatted(),	//F

				$application->username,					//G
				$application->getUserInn(),				//H
				$application->getVehicleRealNumber(),	//I
				number_format($application->getRealPrice(), 2, ',', ''),			//J
				number_format($damage, 2, ',', ''),								//K
				$toReviewTime,							//L
				$this->getAppAgreementsOptimized($application),//M
				$acceptTime,							//N
				$acceptUsername,						//O
				$activateTime,							//P
				$activateUsername,						//Q
			];
			$result[] = $row;
			$this->currentRow++;
		}
		return $result;
	}

	/**
	 * @param Application $application
	 * @return string
	 */
	private function getAppAgreementsOptimized($application)
	{
		$result = "";

		if(!empty($application->application_agreements)) {
			foreach($application->application_agreements as $agreement) {
				if($agreement->status !== ApplicationAgreement::STATUS_ACCEPTED) {
					continue;
				}

				$acceptEvent = null;
				foreach($agreement->audits_active as $audit) {
					if($audit->event_key === Events::DEPAGENT_APP_ACCEPT_KEY) {
						$acceptEvent = $audit;
					}
				}

				$username = '';
				if($acceptEvent && $acceptEvent->user) {
					$username = $acceptEvent->user->name;
				}
				if(!$username) {
					if($agreement->user) {
						$username = $agreement->user->name;
					}
				}
				$agreementRow =
					(isset($agreement->department) ? $agreement->department->title : '') . '/' .
					(isset($agreement->created_at) ? $agreement->created_at->toDateTimeString() : '') . '/' .
					($acceptEvent ? $acceptEvent->created_at : $agreement->accept_date) . '/' .
					$username;

				if($result) {
					$result .= ", ";
				}
				$result .= $agreementRow;
			}
		}

		return $result;
	}
}
