<?php

namespace App\Jobs;

use App\Helpers\ExcelHelper;
use App\Models\Application;
use App\Models\Process;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ExportAllAppsReport implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    const PORTION_SIZE = 2000;

    protected $userId = null;
    protected $filterData = [];
    protected $processId;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($userId, $filterData, $processId)
    {
        $this->userId = $userId;
        $this->filterData= $filterData;
        $this->processId = $processId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        ini_set('max_execution_time', 0);

        /** @var Process $process */
        $process = Process::find($this->processId);
        $process->status = Process::STATUS_PROCESSING;
        $process->save();

        //filter by status
        $query = Application::notTemplateQuery()
            ->where('is_draft', 0);

        $excelExporter = new ExcelHelper();
        $excelExporter->initAllReportExport();

        $maxId = Application::max('id');

        $firstId = 0;
        $lastId = $firstId + self::PORTION_SIZE;

        $start = microtime(true);
        $times = [
            'cycles' => []
        ];

        while ($firstId < $maxId) {
            $queryStart = microtime(true);

            $queryPart = clone $query;
            $queryPart
                ->where('id', '>', $firstId)
                ->where('id', '<=', $lastId);

            $dataPart = $queryPart
                ->select([
                    'applications.*',
                    'applications.status as status'
                ])
                ->with([
//					'user',
                    'vehicle',
                    'admin',
                    'application_dates.issue_place',
                    'application_agreements.department',
                    'application_agreements.user',
                    'application_agreements.audits_active.user',
                    //'audits_report'
                    'audits.user',
                    //'fast_application'
                ])
                ->get();
            $queryInt = microtime(true) - $queryStart;

            $excelStart = microtime(true);
            $excelExporter->addAllReportData($dataPart);
            $excelInt = microtime(true) - $excelStart;

            $times['cycles'][] = [
                'sql' => $queryInt,
                'excel' => $excelInt,
                'info' => 'ids: ' . $firstId . ' - ' . $lastId,
                'applications' => count($dataPart)
            ];

            $firstId += self::PORTION_SIZE;
            $lastId += self::PORTION_SIZE;
        }

        $url = $excelExporter->finishActiveExport();

        $times['all'] = microtime(true) - $start;

        $process->status = Process::STATUS_SUCCEED;
        $process->result = \GuzzleHttp\json_encode([
            'url' => $url
        ]);
        $process->save();
    }
}
