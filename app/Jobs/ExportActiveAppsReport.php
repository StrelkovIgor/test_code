<?php

namespace App\Jobs;

use App\Helpers\ExcelHelper;
use App\Models\Application;
use App\Models\Process;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ExportActiveAppsReport implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

	const PORTION_SIZE = 2000;

	protected $userId = null;
	protected $filterData = [];
	protected $processId;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($userId, $filterData, $processId)
    {
        $this->userId = $userId;
        $this->filterData= $filterData;
        $this->processId = $processId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
		ini_set('max_execution_time', 0);

		/** @var Process $process */
		$process = Process::find($this->processId);
		$process->status = Process::STATUS_PROCESSING;
		$process->save();

		/** @var User $user */
		$user = User::find($this->userId);

		//filter by status
		$query = Application::acivatedStatusQuery();

		//filter by form data
		$data = $this->filterData;

		$query = Application::adminDataFilter($query, $data);

		$query = Application::activeReportsFilter($query, $data);


		$excelExporter = new ExcelHelper();
		$excelExporter->initActiveReportExport();

		//$maxId = Application::max('id');
		$maxId = 4000;

		$firstId = 0;
		$lastId = $firstId + self::PORTION_SIZE;

		$start = microtime(true);
		$times = [
			'cycles' => []
		];
		//$data = new Collection();
		while ($firstId < $maxId) {
			$queryStart = microtime(true);

//			$firstId=98;
//			$lastId=99;
			$queryPart = clone $query;
			$queryPart
				->where('id', '>', $firstId)
				->where('id', '<=', $lastId);

			$dataPart = $queryPart
				->select([
					'applications.*',
					'applications.status as status'
				])
				->with([
//					'user',
					'vehicle',
					'admin',
					'application_dates.issue_place',
					'application_agreements.department',
					'application_agreements.user',
					'application_agreements.audits_active.user',
					//'audits_report'
					'audits.user',
					//'fast_application'
				])
				->get();
			$queryInt = microtime(true) - $queryStart;

			$excelStart = microtime(true);
			$excelExporter->addActiveReportData($dataPart);
			$excelInt = microtime(true) - $excelStart;
			//$data = $data->concat($dataPart);

			$times['cycles'][] = [
				'sql' => $queryInt,
				'excel' => $excelInt,
				'info' => 'ids: ' . $firstId . ' - ' . $lastId,
				'applications' => count($dataPart)
			];

			$firstId += self::PORTION_SIZE;
			$lastId += self::PORTION_SIZE;

//			$firstId = $maxId+1;
		}
		$url = $excelExporter->finishActiveExport();

		$times['all'] = microtime(true) - $start;

		$process->status = Process::STATUS_SUCCEED;
		$process->result = \GuzzleHttp\json_encode([
			'url' => $url
		]);
		$process->save();
    }
}
