<?php

namespace App\Console\Commands;

use App\Models\VehicleAxle;
use Illuminate\Console\Command;

class VehicleAxlesAddWheels extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'vehicle_axles:add_wheels';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add wheels field for vehicle axles';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
		$changeCount = 0;
		/** @var VehicleAxle[] $vehicleAxles */
		$vehicleAxles = VehicleAxle::all();
		foreach($vehicleAxles as $vehicleAxle) {
			$vehicleAxle->wheels =
				$vehicleAxle->wheel_count == 1 || $vehicleAxle->wheel_count ==2 ?
					$vehicleAxle->wheels = 2 :
					$vehicleAxle->wheels = null;

			$success = $vehicleAxle->save();
			if($success) {
				$changeCount++;
			}
		}
		echo 'Success! ' . $changeCount . ' vehicle axles changed!';
    }
}
