<?php

namespace App\Console\Commands;

use App\Models\Application;
use Illuminate\Console\Command;

class ApplicationAddRealNumber extends Command
{
	const PORTION_SIZE = 500;

	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'application:add_real_number';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Copy real numbers from vehicles to application';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		$maxId = Application::max('id');

		$firstId = 0;
		$lastId = $firstId + self::PORTION_SIZE;

		while ($firstId < $maxId) {
			$updated = 0;

			/** @var Application[] $applications */
			$applications = Application
				::withTrashed()
				->where('id', '>', $firstId)
				->where('id', '<=', $lastId)
				->whereNull('real_number')
				->with(['vehicle'])
				->get();

			if(!empty($applications)) {
				foreach ($applications as $application) {
					$application->real_number = $application->vehicle ? $application->vehicle->real_number : null;
					$application->timestamps = false;
					$success = $application->save();
					if($success){
						$updated++;
					}
				}
			}

			echo "ids: $firstId - $lastId, updated: $updated\r\n";

			$firstId += self::PORTION_SIZE;
			$lastId += self::PORTION_SIZE;
		}
	}
}
