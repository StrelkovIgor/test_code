<?php

namespace App\Console\Commands;

use App\Models\Application;
use Illuminate\Console\Command;

class ApplicationFirmUserUsernameFix extends Command
{
	const PORTION_SIZE = 500;

	/**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'application:firm_username_fix';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fixes wrong username for apps, created by firm users';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
	{
		$maxId = Application::max('id');

		$firstId = 0;
		$lastId = $firstId + self::PORTION_SIZE;

		while ($firstId < $maxId) {
			$updated = 0;

			/** @var Application[] $applications */
			$applications = Application
				::withTrashed()
				->where('id', '>', $firstId)
				->where('id', '<=', $lastId)
				->whereNotNull('employee_id')
				->with(['user'])
				->get();

			if(!empty($applications)) {
				foreach ($applications as $application) {
					$username = null;
					$oldUsername = $application->username;
					if($application->isFast()) {
						$username = $application->fast_application ? $application->fast_application->name : null;
					}else{
						$username = $application->user ? $application->user->name : null;
					}

					if($username && $username !== $oldUsername) {
						$application->username = $username;
						$application->timestamps = false;
						$success = $application->save();
						if($success){
							$updated++;
						}
					}
				}
			}

			echo "ids: $firstId - $lastId, updated: $updated\r\n";

			$firstId += self::PORTION_SIZE;
			$lastId += self::PORTION_SIZE;
		}
	}
}
