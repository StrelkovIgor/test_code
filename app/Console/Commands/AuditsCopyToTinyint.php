<?php

namespace App\Console\Commands;

use App\Models\Audit\Audit;
use App\Models\Audit\Enum\Events;
use Illuminate\Console\Command;

class AuditsCopyToTinyint extends Command
{
	const PORTION_SIZE = 500;
	
	/**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'audit:add_tinyints';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Copy event, auditable_type to tinyiny columns';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
		$maxId = Audit::max('id');

		$firstId = 0;
		$lastId = $firstId + self::PORTION_SIZE;

		while ($firstId < $maxId) {
			$updated = 0;
			$insertData = [];

			/** @var Audit[] $audits */
			$audits = Audit
				::where('id', '>', $firstId)
				->where('id', '<=', $lastId)
				->whereNull('auditable_type_key')
				->get();

			if(!empty($audits)) {
				foreach ($audits as $audit) {

					$audit->auditable_type_key = Audit::getAuditObjectKey($audit->auditable_type);
					$audit->event_key = Events::getEventKey($audit->event);
					$audit->timestamps = false;
					$success = $audit->save();

					if($success){
						$updated++;
					}
				}
			}

			echo "ids: $firstId - $lastId, updated: $updated\r\n";

			$firstId += self::PORTION_SIZE;
			$lastId += self::PORTION_SIZE;
		}
    }
}
