<?php

namespace App\Console\Commands;

use App\Models\Department;
use Illuminate\Console\Command;

class DeparmentFixTitle extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'departments:fix_title';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fix title for department with id = 1';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
    	/** @var Department $department */
        $department = Department::find(1);
        if($department) {
        	$department->title = 'Управление ГИБДД МВД по Республике Татарстан';
        	$department->timestamps = false;
        	$department->save();
		}
    }
}
