<?php

namespace App\Console\Commands;

use App\Models\File;
use App\Models\Vehicle;
use Illuminate\Console\Command;

class FixFileSrc extends Command
{
	const OLD_DOMAIN = 'https://api.aisktg.ru',
		NEW_DOMAIN = 'https://aisktg.ru/api';

	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'files:fix_src';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Fix file src values from old domain to new';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		$changeCount = 0;

		/** @var Vehicle[] $vehicles */
		$files = File::all();

		foreach($files as $file) {
			$success = false;
			$url = $file->url;
			$pos = strpos($url, self::OLD_DOMAIN);
			if($pos !== false) {
				$file->url = str_replace(self::OLD_DOMAIN, self::NEW_DOMAIN, $url);
				$success = $file->save();

				if($success) {
					$changeCount++;
				}
			}
		}


		echo 'Success! ' . $changeCount . ' files changed!';
	}
}
