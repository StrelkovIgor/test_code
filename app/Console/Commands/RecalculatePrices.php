<?php

namespace App\Console\Commands;

use App\Helpers\PriceCalculator;
use App\Models\Application;
use Illuminate\Console\Command;

class RecalculatePrices extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'application:recalculatePrices';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Recalculate prices for all apps';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        /** @var Application[] $applications */
    	$applications = Application::notTemplateQuery()
			->where('is_draft', 0)
			->get();
     
    	$successCount = 0;
    	$priceInfo = [];
        foreach($applications as $application){
			$priceCalculator = new PriceCalculator($application);
			$application->price = $priceCalculator->calculatePrice();
			//$params = $priceCalculator->getLastCalculationInfo();
        	
        	//$application->price = $application->calculatePrice($priceInfo);
        	$success = $application->save();
        	if($success){
        		$successCount++;
			}
		}
		
		echo 'Success! ' . $successCount . ' prices recalculated';
    }
}
