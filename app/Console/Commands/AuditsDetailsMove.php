<?php

namespace App\Console\Commands;

use App\Models\Audit\Audit;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class AuditsDetailsMove extends Command
{
	const PORTION_SIZE = 500;

	/**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'audit:move_details';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Move url, ip, browser info to audit_details table';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
		$maxId = Audit::max('id');

		$firstId = 0;
		$lastId = $firstId + self::PORTION_SIZE;

		while ($firstId < $maxId) {
			$updated = 0;
			$insertData = [];

			/** @var Audit[] $audits */
			$audits = Audit
				::where('id', '>', $firstId)
				->where('id', '<=', $lastId)
				->get();

			if(!empty($audits)) {
				foreach ($audits as $audit) {
					$insertData[] = [
						'audit_id' => $audit->id,
						'ip_address' => $audit->ip_address,
						'user_agent' => $audit->user_agent,
						'url' => $audit->url,
					];
				}
			}

			if(!empty($insertData)){
				$updated = DB::table('audit_details')->insert($insertData);
			}

			echo "ids: $firstId - $lastId, updated: $updated\r\n";

			$firstId += self::PORTION_SIZE;
			$lastId += self::PORTION_SIZE;
		}
    }
}
