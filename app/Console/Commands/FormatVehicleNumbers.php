<?php

namespace App\Console\Commands;

use App\Helpers\VehicleNumberHelper;
use App\Models\PrivilegeVehicle;
use App\Models\Vehicle;
use Illuminate\Console\Command;

class FormatVehicleNumbers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'vehicle:format_numbers';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Handle vehicle, privilege vehicle numbers. Replace letters to upper case russian symbols';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $replaces = 0;
    	$vehicles = Vehicle::all();
        foreach($vehicles as $vehicle) {
        	$success = $this->replaceVehicle($vehicle);
        	if($success){
        		$replaces++;
			}
		}

		$privilegeVehicles = PrivilegeVehicle::all();
		foreach($privilegeVehicles as $vehicle) {
			$success = $this->replaceVehicle($vehicle);
			if($success){
				$replaces++;
			}
		}

        echo 'Success, total replaces: ' . $replaces;
    }

    public function replaceVehicle($vehicle)
	{
		$success = false;
		if($vehicle->number){
			$vehicle->number = VehicleNumberHelper::formatNumber($vehicle->number);
			$success = $vehicle->save();
		}

		return $success;
	}
}
