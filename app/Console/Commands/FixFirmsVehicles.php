<?php

namespace App\Console\Commands;

use App\Models\Role;
use App\Models\Vehicle;
use Illuminate\Console\Command;

class FixFirmsVehicles extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'vehicles:fix_owners';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Return vehicles, which was moved to firm users - to firms';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		$changeCount = 0;

		/** @var Vehicle[] $vehicles */
		$vehicles =
			Vehicle
				::select('vehicles.*')
				->leftjoin('users', 'users.id', '=', 'vehicles.user_id')
				->where('users.role_id', Role::ROLE_FIRM_USER)
				->with('users')
				->get();

		foreach($vehicles as $vehicle) {
			if($vehicle->user && $vehicle->user->role_id === Role::ROLE_FIRM_USER && $vehicle->user->owner_id){
				$vehicle->user_id = $vehicle->user->owner_id;
				$success = $vehicle->save();
				if($success){
					$changeCount++;
				}
			}
		}


		echo 'Success! ' . $changeCount . ' vehicles changed!';
	}
}
