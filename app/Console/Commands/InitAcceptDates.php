<?php

namespace App\Console\Commands;

use App\Models\Application;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\App;

class InitAcceptDates extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'application:initAcceptDates';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Move updated_at to accept_date as value';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
		/** @var Application[] $applications */
		$applications = Application::notTemplateQuery()
			->whereIn('status', [Application::STATUS_ACCEPTED, Application::STATUS_ACCEPTED_WITH_CHANGES, Application::STATUS_ACTIVE])
			->get();
	
		$successCount = 0;
		foreach($applications as $application){
			$success = false;
			if(!$application->accept_date){
				$application->accept_date = $application->updated_at;
				$success = $application->save();
			}
			
			if($success){
				$successCount++;
			}
		}
	
		echo 'Success! ' . $successCount . ' accept dates refreshed';
    }
}
