<?php

namespace App\Console\Commands;

use App\Models\PrivilegeVehicle;
use App\Models\Vehicle;
use Illuminate\Console\Command;

class FixPrivilegeNumbers extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'vehicles:fix_privileges';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Set real numbers for vehicles and privilege vehicles';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		$changeCount = 0;
		/** @var Vehicle[] $vehicles */
		$vehicles = Vehicle::all();
		foreach($vehicles as $vehicle) {
			$vehicle->real_number = $vehicle->is_non_standard_number ? $vehicle->non_standard_number : $vehicle->number . ' ' . $vehicle->regions;
			$vehicle->timestamps = false;
			$success = $vehicle->save();
			if($success) {
				$changeCount++;
			}
		}
		echo 'Success! ' . $changeCount . ' vehicles changed!';


		$changeCount = 0;
		/** @var PrivilegeVehicle[] $privilegeVehicles */
		$privilegeVehicles = PrivilegeVehicle::all();
		foreach($privilegeVehicles as $vehicle) {
			$vehicle->real_number = $vehicle->is_non_standard_number ? $vehicle->non_standard_number : $vehicle->number . ' ' . $vehicle->region;
			$vehicle->timestamps = false;
			$success = $vehicle->save();
			if($success) {
				$changeCount++;
			}
		}

		echo 'Success! ' . $changeCount . ' privilege vehicles changed!';
	}
}
