<?php

namespace App\Console\Commands;

use App\Models\ApplicationLoad;
use Illuminate\Console\Command;

class ApplicationAxlesInfoAddWheels extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'application_load:add_wheels';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add wheels field for application_loads vehicle axles';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
		$changeCount = 0;
		/** @var ApplicationLoad[] $applicationLoads */
		$applicationLoads = ApplicationLoad::all();
		foreach($applicationLoads as $applicationLoad) {
			if($applicationLoad->axles_info){
				$axlesInfo = \GuzzleHttp\json_decode($applicationLoad->axles_info, true);
				if(!empty($axlesInfo)){
					for($i=0;$i<count($axlesInfo);$i++){
						if($axlesInfo[$i]['wheel_count'] == 1 || $axlesInfo[$i]['wheel_count'] == 2) {
							$axlesInfo[$i]['wheels'] = 2;
						}else{
							$axlesInfo[$i]['wheels'] = '';
						}
					}
				}
				$applicationLoad->axles_info = json_encode($axlesInfo);

				$success = $applicationLoad->save();
				if($success) {
					$changeCount++;
				}
			}
		}
		echo 'Success! ' . $changeCount . ' applications changed!';
    }
}
