<?php

namespace App\Console\Commands;

use App\Models\Application;
use Illuminate\Console\Command;

class ApplicationAddUsername extends Command
{
    const PORTION_SIZE = 500;

	/**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'application:add_username';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Copy username from user, fast app to application table';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
		$maxId = Application::max('id');

		$firstId = 0;
		$lastId = $firstId + self::PORTION_SIZE;

		while ($firstId < $maxId) {
			$updated = 0;
			$insertData = [];

			/** @var Application[] $applications */
			$applications = Application
				::withTrashed()
				->where('id', '>', $firstId)
				->where('id', '<=', $lastId)
				->whereNull('username')
				->with(['user', 'fast_application'])
				->get();

			if(!empty($applications)) {
				foreach ($applications as $application) {
					$username = null;
					if($application->isFast()) {
						$username = $application->fast_application ? $application->fast_application->name : null;
					}else{
						$username = $application->user ? $application->user->name : null;
					}
					$application->username = $username;
					$application->timestamps = false;
					$success = $application->save();
					if($success){
						$updated++;
					}
				}
			}

			echo "ids: $firstId - $lastId, updated: $updated\r\n";

			$firstId += self::PORTION_SIZE;
			$lastId += self::PORTION_SIZE;
		}
    }
}
