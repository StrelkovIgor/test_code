<?php

namespace App\Console\Commands;

use App\Models\Role;
use App\Models\User;
use Illuminate\Console\Command;

class CopyInns extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'users:copy_inns';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Copy inns from individuals, firms to users';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $changeCount = 0;
    	$users = User::withTrashed()->get();
        
        /** @var User $user */
		foreach($users as $user){
			$success = false;
			
        	if($user->role_id === Role::ROLE_INDIVIDUAL && $user->individual){
        		$user->inn = $user->individual->inn;
				$success = $user->save();
			}
			if($user->role_id === Role::ROLE_FIRM && $user->firm){
				$user->inn = $user->firm->inn_org;
				$success = $user->save();
			}
			
			if($success){
				$changeCount++;
			}
		}
		
		echo 'Success! ' . $changeCount . ' users changed!';
    }
}
