<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 07 Jul 2018 12:57:09 +0000.
 */

namespace App\Models;

use App\Helpers\VehicleNumberHelper;
use App\Models\Interfaces\ObjectInfoInterface;
use DateTime;
use Illuminate\Database\Eloquent\Model as Eloquent;
use App\Models\Role;
use App\Models\Audit\AuditableTrait;
use App\Models\Audit\Enum\Events;

/**
 * Class Vehicle
 *
 * @property int $id
 * @property int $is_trailer
 * @property int $user_id
 * @property int $brand_id
 * @property int $model_id
 * @proterty string $title
 * @property int $vehicle_type_id
 * @property bool $is_owner
 * @property string $owner_name
 * @property string $number
 * @property string $regions
 * @property string $pts_number
 * @property string $sts_number
 * @property float $pts_weight
 * @property string $pts_max_weight
 * @property float $height
 * @property float $length
 * @property float $width
 * @property int $axle_count
 * @property int $confirmation_status
 * @property int $admin_id
 * @property int $is_non_standard_number
 * @property string $non_standard_number
 * @property string $real_number
 * @property integer $is_fast
 * @property integer $is_smev
 * @property string $deleted_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @property VehicleBrand $vehicle_brand
 * @property VehicleModel $vehicle_model
 * @property PrivilegeVehicle $privilege
 * @property User $user
 * @property User $admin
 * @property \Illuminate\Database\Eloquent\Collection $vehicle_apps
 * @property VehicleApp $vehicle_app
 * @property VehicleAxle[] $vehicle_axles
 * @property File[] $files
 *
 * @package App\Models
 */
class Vehicle extends Eloquent implements ObjectInfoInterface
{

	use \Illuminate\Database\Eloquent\SoftDeletes;
	use AuditableTrait;

    const TYPE_CAR_AXLE_2 = 1,
        TYPE_CAR_AXLE_3 = 2,
        TYPE_CAR_AXLE_4_2_2 = 3,
        TYPE_CAR_AXLE_4_1_3 = 4,
        TYPE_CAR_AXLE_5 = 5,

        TYPE_TRACTOR_1 = 6,
        TYPE_TRACTOR_2 = 7,
        TYPE_TRACTOR_3 = 8,
        TYPE_TRACTOR_4 = 9,
		TYPE_OTHER = 19;

    const TYPE_TRAILER_AXLE_1 = 10,
        TYPE_TRAILER_AXLE_2 = 11,
        TYPE_TRAILER_AXLE_2_SEPARATED = 12,
        TYPE_TRAILER_AXLE_3 = 13,
        TYPE_TRAILER_AXLE_4 = 14,
        TYPE_SEMITRAILER_AXLE_1 = 15,
        TYPE_SEMITRAILER_AXLE_2 = 16,
        TYPE_SEMITRAILER_AXLE_3 = 17,
        TYPE_SPEC = 18,
        TYPE_TRAILER_OTHER = 20,
        TYPE_SEMI_TRAILER_OTHER = 21;

    const CONFIRM_STATUS_NEW = 0,
        CONFIRM_STATUS_DECLINE = 1,
        CONFIRM_STATUS_REVIEW = 2,
        CONFIRM_STATUS_ACCEPT = 3,
        CONFIRM_STATUS_ALL = -1;


    protected $casts = [
        'user_id' => 'int',
        'brand_id' => 'int',
        'model_id' => 'int',
        'vehicle_type_id' => 'int',
        'is_owner' => 'bool',
        'pts_weight' => 'float',
        'height' => 'float',
        'length' => 'float',
        'width' => 'float',
        'axle_count' => 'int',
        'confirmation_status' => 'int',
        'admin_id' => 'int',
		'is_fast' => 'int',
		'is_smev' => 'int'
    ];

    protected $fillable = [
        'is_trailer',
        'user_id',
        'brand_id',
        'model_id',
		'title',
        'vehicle_type_id',
        'is_owner',
        'owner_name',
        'number',
        'regions',
        'pts_number',
        'sts_number',
        'pts_weight',
        'pts_max_weight',
        'height',
        'length',
        'width',
        'axle_count',
        'confirmation_status',
        'is_non_standard_number',
        'non_standard_number',
		'real_number',
		'is_fast',
		'is_smev'
    ];

    public function vehicle_brand()
    {
        return $this->belongsTo(VehicleBrand::class, 'brand_id');
    }

    public function vehicle_model()
    {
        return $this->belongsTo(VehicleModel::class, 'model_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

	public function admin()
	{
		return $this->belongsTo(User::class, 'admin_id');
	}

    public function privilege()
    {
        $date = date('Y-m-d');
    	return $this->belongsTo(PrivilegeVehicle::class, 'real_number', 'real_number')
			->leftJoin('privilege_statuses', 'privilege_vehicles.privilege_status_id', '=', 'privilege_statuses.id')
			->where('privilege_statuses.start_date', '<=', $date)
			->where('privilege_statuses.finish_date', '>=', $date)
            ->whereNull('privilege_statuses.deleted_at');
    }

    public function files()
    {
        return $this->hasMany(File::class, 'vehicle_id');
    }

    public function users()
    {
        return $this->belongsToMany(User::class, 'user_to_vehicle')
            ->withPivot('id', 'can_edit')
            ->withTimestamps();
    }

    public function vehicle_apps()
    {
        return $this->hasMany(VehicleApp::class);
    }
    public function vehicle_app()
    {
        return $this->hasOne(VehicleApp::class);
    }

    public function vehicle_axles()
    {
        return $this->hasMany(VehicleAxle::class);
    }

    /**
     * @return array
     */
    public static function getCarTypesList(){
        return [
            self::TYPE_CAR_AXLE_2 => 'Одиночный грузовой двухосный автомобиль',
            self::TYPE_CAR_AXLE_3 => 'Одиночный грузовой трехосный автомобиль',
            self::TYPE_CAR_AXLE_4_2_2 => 'Одиночный грузовой четырехосный автомобиль (2+2)',
            self::TYPE_CAR_AXLE_4_1_3 => 'Одиночный грузовой четырехосный автомобиль (1 + 3)',
            self::TYPE_CAR_AXLE_5 => 'Одиночный грузовой пятиосный автомобиль',

            self::TYPE_TRACTOR_1 => 'Тягач одноосный',
            self::TYPE_TRACTOR_2 => 'Тягач двухосный',
            self::TYPE_TRACTOR_3 => 'Тягач трехосный',
            self::TYPE_TRACTOR_4 => 'Тягач четрырехосный',
			self::TYPE_SPEC => 'Спецтехника',
			self::TYPE_OTHER => 'Другое',
        ];
    }

    /**
     * @return array
     */
    public static function getTrailerTypesList(){
        return [
            self::TYPE_TRAILER_AXLE_1 => 'Прицеп одноосный',
            self::TYPE_TRAILER_AXLE_2 => 'Прицеп двухосный (оси рядом)',
            self::TYPE_TRAILER_AXLE_2_SEPARATED => 'Прицеп двухосный (оси разделены)',
            self::TYPE_TRAILER_AXLE_3 =>'Прицеп трехосный',
            self::TYPE_TRAILER_AXLE_4 =>'Прицеп четырехосный',
            self::TYPE_SEMITRAILER_AXLE_1 => 'Полуприцеп одноосный',
            self::TYPE_SEMITRAILER_AXLE_2 => 'Полуприцеп двухосный',
            self::TYPE_SEMITRAILER_AXLE_3 => 'Полуприцеп трехосный',
            self::TYPE_TRAILER_OTHER => 'Другое (Прицеп)',
            self::TYPE_SEMI_TRAILER_OTHER => 'Другое (Полуприцеп)',
        ];
    }

    /**
     * @param bool $isTrailer
     * @return array
     */
    public static function getApiTypeList($isTrailer = null){
        $list = [];
        if($isTrailer === null){
            $list = self::getCarTypesList() + self::getTrailerTypesList();
        }else{
            $list = $isTrailer ? self::getCarTypesList() : self::getTrailerTypesList();
        }

        $result = [];
        foreach($list as $id => $title){
            $result[] = ['id' => $id, 'title' => $title];
        }
        return $result;
    }

    public function realNumber($separator = ' '){
        return $this->real_number;
        //return !$this->is_non_standard_number ? $this->number . $separator . $this->regions : $this->non_standard_number;
    }
    
    public function getTrailerType(){

    	if(
    		$this->vehicle_type_id === self::TYPE_TRAILER_AXLE_1 ||
    		$this->vehicle_type_id === self::TYPE_TRAILER_AXLE_2 ||
    		$this->vehicle_type_id === self::TYPE_TRAILER_AXLE_2_SEPARATED ||
    		$this->vehicle_type_id === self::TYPE_TRAILER_AXLE_3 ||
    		$this->vehicle_type_id === self::TYPE_TRAILER_AXLE_4
		){
    		return 'Прицеп';
		}
    	if(
			$this->vehicle_type_id === self::TYPE_SEMITRAILER_AXLE_1 ||
			$this->vehicle_type_id === self::TYPE_SEMITRAILER_AXLE_2 ||
			$this->vehicle_type_id === self::TYPE_SEMITRAILER_AXLE_3
		){
    		return 'Полуприцеп';
		}
		if($this->vehicle_type_id === self::TYPE_SPEC){
    		return 'Спецтехника';
		}
		if(
			$this->vehicle_type_id === self::TYPE_OTHER

		){
			return 'Другое';
		}
		if(
			$this->vehicle_type_id === self::TYPE_TRAILER_OTHER

		){
			return 'Другое (Прицеп)';
		}
		if(
			$this->vehicle_type_id === self::TYPE_SEMI_TRAILER_OTHER

		){
			return 'Другое (Полуприцеп)';
		}
		return '';
	}
	
	public static function statusQuery($status)
	{
		$status = intval($status);
	    if($status !== Vehicle::CONFIRM_STATUS_ALL) {
            return self::where([
                ['confirmation_status', $status],
            ]);
        }
        return self::where([
            ['confirmation_status', '>', -1],
        ]);
	}

	/**
	 * @param int $tab
	 * @param User $user
	 * @return mixed
	 */
	public static function adminStatusQuery($tab, $user)
	{
		$status = self::mapAdminStatus($tab);

		$query = self::where('vehicles.confirmation_status', $status);

		if (
			$status === self::CONFIRM_STATUS_REVIEW &&
			$user->role_id === Role::ROLE_OFFICER
		) {
			$query->where('admin_id', $user->id);
		}

		$query->where('vehicles.is_fast', '<>', 1);
		$query->where('vehicles.is_smev', '<>', 1);

		return $query;
	}
	
	/**
	 * @param $query
	 * @param array $data
	 * @param User|null $user
	 * @return mixed
	 */
	public static function dataFilter($query, $data, $user = null)
	{
		$isAdmin = $user && $user->role_id === Role::ROLE_ADMIN;
		$isClient = $user && (
			$user->role_id === Role::ROLE_INDIVIDUAL ||
			$user->role_id === Role::ROLE_FIRM ||
			$user->role_id === Role::ROLE_FIRM_USER
		);

		if ($dateStr = array_get($data, 'date')) {
			$dateStart = (new DateTime($dateStr))->format('Y-m-d');
			$dateFinish =(new DateTime($dateStr))->modify('+1 day')->format('Y-m-d');
			$query->where('vehicles.updated_at', '>=', $dateStart);
			$query->where('vehicles.updated_at', '<', $dateFinish);
		}

		if($isAdmin) {
			$name = array_get($data, 'user_name');
			if ($name !== null) {
				$query->leftjoin('users', 'users.id', '=', 'vehicles.user_id');
				$query->where('users.name', 'like', '%' . $name . '%');
			}
		}

		$type = array_get($data, 'type');
		if ($type !== null) {
			$query->where('vehicles.is_trailer', $type);
		}
		
		if ($brand = array_get($data, 'brand')) {
			$query->leftjoin('vehicle_brands', 'vehicle_brands.id', '=', 'vehicles.brand_id');
			$query->where('vehicle_brands.title', 'like', '%' . $brand . '%');
		}
		
		if ($model = array_get($data, 'model')) {
			$query->leftjoin('vehicle_models', 'vehicle_models.id', '=', 'vehicles.model_id');
			$query->where('vehicle_models.title', 'like', '%' . $model . '%');
		}
		
		if ($number = array_get($data, 'number')) {
			$query->where('vehicles.real_number', 'like', '%' . $number . '%');
		}

		if($isAdmin) {
			$admin_name = array_get($data, 'admin_name');
			if ($admin_name !== null) {
				$query->leftjoin('users as admins', 'admins.id', '=', 'vehicles.admin_id');
				$query->where('admins.name', 'like', '%' . $admin_name . '%');
			}
		}

		if($isClient) {
			//@todo sort by privilege status
		}
		
		return $query;
	}
	
	/**
	 * Add user condition to query
	 * @param mixed $query
	 * @param User $user
	 * @return mixed
	 */
	public static function userQuery($query, $user)
	{
		if ($user->role_id === Role::ROLE_FIRM_USER) {
			$query
				->select('vehicles.*', 'user_to_vehicle.can_edit as can_edit')
				->leftJoin('user_to_vehicle', 'user_to_vehicle.vehicle_id', '=', 'vehicles.id')
				->where('user_to_vehicle.user_id', $user->id);
		} else {
			$query
				->where('user_id', $user->id);
		}
		
		return $query;
	}
	
	/**
	 * @param User $user
	 * @param int $status
	 * @return mixed
	 */
	public static function firmUserQuery($user, $status)
	{
		$query = Vehicle::select('vehicles.*', 'user_to_vehicle.can_edit as can_edit')
			->leftJoin('user_to_vehicle', 'user_to_vehicle.vehicle_id', '=', 'vehicles.id')
			->where('user_to_vehicle.user_id', $user->id)
			->where('vehicles.confirmation_status', $status);
		
		return $query;
	}

	/**
	 * Get to work by admin or department agent
	 * @param User $user
	 * @return boolean
	 */
	public function toWork($user)
	{
		$this->confirmation_status = self::CONFIRM_STATUS_REVIEW;
		$this->admin_id = $user->id;
		if ($this->save()) {
			if (Role::isAdminOrGbu()) {
				$this->newAuditEvent(Events::ADMINGBU_VEHICLE_TOWORK);
			}
			return true;
		}
		return false;
	}

	/**
	 * @param $admin
	 * @return bool
	 */
	public function accept($admin)
	{
		$this->confirmation_status = Vehicle::CONFIRM_STATUS_ACCEPT;
		$this->admin_id = $admin->id;
		
		VehicleApp::accept($this->id, $admin);
		
		if ($this->save()) {
			if (Role::isAdminOrGbu()) {
				$this->newAuditEvent(Events::ADMINGBU_VEHICLE_ACCEPT);
			}
			return true;
		}
		return false;
	}
	
	
	/**
	 * @param User $admin
	 * @param string $note
	 * @return bool
	 */
	public function decline($admin, $note){
		$this->confirmation_status = Vehicle::CONFIRM_STATUS_DECLINE;
		$this->admin_id = $admin->id;
		
		VehicleApp::decline($this->id, $admin, $note);
		
		if ($this->save()) {
			if (Role::isAdminOrGbu()) {
				$this->newAuditEvent(Events::ADMINGBU_VEHICLE_DECLINE);
			}
			return true;
		}
		return false;
	}
	
	/**
	 * @param array $data
	 * @param User $user
	 */
	public static function createByUser($data, $user)
	{
		$vehicleData = $data;
		
		$vehicleData['user_id'] = $user->id;
		if(array_get($data, 'is_owner', false)){
			$vehicleData['owner_name'] = $user->name;
		}
		
		//handle new brand or model
		self::handleNewBrandModel($vehicleData, $data, $user);
		
		$vehicle = self::create($vehicleData);
		
		VehicleAxle::createByVehicle($vehicle, $data);
		
		return $vehicle;
	}

	/**
	 * Create by fast app
	 * @param array $data
	 * @return static|null
	 */
	public static function createFast($data, $appData, $isTrailer)
	{
		$additionalData = [
			'is_trailer' => $isTrailer,
			'vehicle_type_id' => self::TYPE_TRAILER_OTHER,
			'owner_name' => array_get($appData, 'name'),
			'length' => 0,
			'width' => 0,
			'height' => 0,
			'is_fast' => 1,
            'is_smev' => 0
		];
		$vehicleData = array_merge($data, $additionalData);
		$vehicle = self::create($vehicleData);

		return $vehicle;
	}
	
	/**
	 * Create by smev app
	 * @param array $data
	 * @return static|null
	 */
	public static function createSmev($data, $appData, $isTrailer)
	{
		$additionalData = [
			'is_trailer' => $isTrailer,
			'vehicle_type_id' => self::TYPE_TRAILER_OTHER,
			'length' => 0,
			'width' => 0,
			'height' => 0,
			'is_fast' => 0,
			'is_smev' => 1,

			'pts_max_weight' => 0,
			'axle_count' => 0,
		];
		$vehicleData = array_merge($data, $additionalData);
//		print_r($vehicleData);die;
		$vehicle = self::create($vehicleData);
		
		return $vehicle;
	}
	
	/**
	 * @param array $data
	 * @param User $user
	 */
	public function updateByUser($data, $user)
	{
		$vehicleData = $data;

		$user_id = $user->id;
		if($user->role_id === Role::ROLE_FIRM_USER) {
			$user_id = $user->owner_id;
		}
		$vehicleData['user_id'] = $user_id;
		if(array_get($data, 'is_owner', false)){
			$vehicleData['owner_name'] = $this->user->getNameWithOwner();
		}
		
		//handle new brand or model
		self::handleNewBrandModel($vehicleData, $data, $user);
		
		
		//change status
		if ($this->confirmation_status !== Vehicle::CONFIRM_STATUS_NEW) {
			$vehicleData['confirmation_status'] = Vehicle::CONFIRM_STATUS_REVIEW;
		}
		
		$this->update($vehicleData);
		
		VehicleAxle::createByVehicle($this, $data);
	}
	
	public static function handleNewBrandModel(&$vehicleData, $data, $user)
	{
		//create brand and model if necessary
		$newBrand = null;
		if($needCreateBrand = array_get($data, 'is_new_brand')){
			$newBrand = VehicleBrand::createByUser($data, $user);
		}
		
		//create model
		$newModel = null;
		$needCreateModel = array_get($data, 'is_new_model', false);
		if($newBrand || $needCreateModel){
			$brandId = $newBrand ? $newBrand->id : array_get($data, 'brand_id');
			$newModel = VehicleModel::createByUser($data, $user, $brandId);
		}
		
		//set brand id
		if($newBrand){
			$vehicleData['brand_id'] = $newBrand->id;
		}
		
		//set model id
		if($newModel){
			$vehicleData['model_id'] = $newModel->id;
		}
	}
	
	/**
	 * @param array $filesData
	 * @param int $fileType
	 */
	public function updateFiles($filesData, $fileType)
	{
		//@todo optimize
		$ids = [];
		if(!empty($filesData)){
			foreach($filesData as $fileData){
				$ids[] = $fileData['id'];
			}
		}
		
		File::where('vehicle_id', $this->id)
			->where('type_id', $fileType)
			->whereNotIn('id', $ids)
			->delete();
		
		if(!empty($ids)){
			foreach($ids as $id){
				if(intval($id)){
					$file = File::find($id);
					if($file){
						$file->user_id = $this->user_id;
						$file->vehicle_id = $this->id;
						$file->type_id = $fileType;
						$file->save();
					}
				}
			}
		}
	}
	
	/**
	 * @return string
	 */
	public function getPtsWeightFormatted(){
		return number_format($this->pts_weight, 1);
	}
	
	/**
	 * Split number with region to associative array with number, region
	 * @param string $fullNumber
	 * @return array
	 */
	public static function parseNumberRegion($fullNumber)
	{
		$parts = explode(' ', $fullNumber);
		
		$numberInfo = [
			'number' => $parts[0],
			'region' => null
		];
		if(!empty($parts[1])){
			$numberInfo['region'] = $parts[1];
		}
		return $numberInfo;
	}

	protected static function mapAdminStatus($clientStatus)
	{
		return intval($clientStatus);
	}

	/**
	 * @return string
	 */
	public function getBrandModelTitle()
	{
		if($this->is_smev) {
			return $this->title;
		}
		return $this->getBrandTitle() . ' ' . $this->getModelTitle();
	}

	/**
	 * @return string
	 */
	public function getBrandTitle()
	{
		if($this->is_smev) {
			return '';
		}
		return $this->vehicle_brand->title ?? '';
	}

	/**
	 * @return string
	 */
	public function getModelTitle()
	{
		if($this->is_smev) {
			return $this->title;
		}
		return $this->vehicle_model->title ?? '';
	}

	/**
	 * @return string
	 */
	public function getFullInfo()
	{
		return $this->getBrandTitle() . ' ' . $this->getModelTitle() . ' ' . $this->real_number;
	}


	/**
	 * @return array
	 */
	public function getAxlesInfo()
	{
		$result = [];
		foreach($this->vehicle_axles as $vehicleAxle) {
			$result[] = [
				'vehicle_id' => $vehicleAxle->id,
				'num' => $vehicleAxle->num,
				'wheel_count' => $vehicleAxle->wheel_count,
				'wheels' => $vehicleAxle->wheels,
				'type_id' => $vehicleAxle->type_id,
				'is_lifting' => $vehicleAxle->is_lifting,
				'distance' => $vehicleAxle->distance
			];
		}

		if(empty($result)) {
			return null;
		}

		return $result;
	}

	public function save(array $options = [])
	{
		$this->number = VehicleNumberHelper::formatNumber($this->number);
		$this->non_standard_number = VehicleNumberHelper::formatNumber($this->non_standard_number);
		$this->real_number = $this->is_non_standard_number ? $this->non_standard_number : $this->number . ' ' . $this->regions;
		return parent::save();
	}
	public function getObjectInfo()
	{
		return $this->real_number;
	}
}
