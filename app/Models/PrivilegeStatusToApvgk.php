<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PrivilegeStatusToApvgk extends Model
{
    protected $table = 'privilege_status_to_apvgk';

    protected $casts = [
        'apvgk_id' => 'int',
        'privilege_status_id' => 'int'
    ];

    protected $fillable = [
        'apvgk_id',
        'privilege_status_id'
    ];

    public function apvgk()
    {
        return $this->belongsTo(\App\Models\APVGK::class);
    }

    public function privilege_status()
    {
        return $this->belongsTo(\App\Models\PrivilegeStatus::class);
    }

    /**
     * @param $apvgk object|array
     * @param $privilege_status object|array
     * @retrun null
     */
    public static function createPole($privilege_status, $apvgk){
        $data = [];
        if(is_object($apvgk)) {
            foreach ($privilege_status as $status_id) $data[] = ['apvgk_id' => $apvgk->id, 'privilege_status_id' => $status_id];
            self::where('apvgk_id', $apvgk->id)->delete();
        }else{
            foreach ($apvgk as $apvgk_id) $data[] = ['apvgk_id' => $apvgk_id, 'privilege_status_id' => $privilege_status->id];
            self::where('privilege_status_id', $privilege_status->id)->delete();
        }
        self::insert($data);
    }
}
