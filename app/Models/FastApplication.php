<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 06 Jun 2019 17:30:33 +0300.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class FastApplication
 * 
 * @property int $id
 * @property int $application_id
 * @property int $admin_id
 * @property string $name
 * @property string $address
 * @property string $inn
 * @property int $role_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\User $user
 * @property \App\Models\Application $application
 *
 * @package App\Models
 */
class FastApplication extends Eloquent
{
	protected $casts = [
		'application_id' => 'int',
		'admin_id' => 'int',
		'role_id' => 'int'
	];

	protected $fillable = [
		'application_id',
		'admin_id',
		'name',
		'address',
		'inn',
		'role_id'
	];

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class, 'admin_id');
	}

	public function admin()
	{
		return $this->belongsTo(\App\Models\User::class, 'admin_id');
	}

	public function application()
	{
		return $this->belongsTo(\App\Models\Application::class);
	}

	/**
	 * @param Application $application
	 * @param array $data
	 * @param User $admin
	 *
	 * @return static|null
	 */
	public static function updateByData($application, $data, $admin)
	{
		/** @var ApplicationDate $fastApplication */
		$fastApplication = static::where('application_id', $application->id)->first();
		if (!$fastApplication) {
			$fastApplication = new static(['application_id' => $application->id]);
		}

		$address = array_get($data, 'address', '');
		if(!$address) {
			$address = '';
		}
		$applicationData = [
			'application_id' => $application['id'],
			'name' => array_get($data, 'name'),
			'address' => $address,
			'inn' => array_get($data, 'inn'),
			'admin_id' => $admin->id,
			'role_id' => array_get($data, 'role_id')
		];

		//update request
		if ($fastApplication->id) {
			$fastApplication->update($applicationData);
		} else {
			$fastApplication = FastApplication::create($applicationData);
		}

		return $fastApplication;
	}
}
