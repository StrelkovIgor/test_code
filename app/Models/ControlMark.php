<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 05 Nov 2018 14:41:25 +0000.
 */

namespace App\Models;

use DateTime;
use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class ControlMark
 * 
 * @property int $id
 * @property int $application_id
 * @property int $user_id
 * @property int $control_post_id
 * @property int $reverse
 * @property int $active
 * @property int $run_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\Application $application
 * @property \App\Models\ControlPost $control_post
 * @property \App\Models\User $user
 * @property \App\Models\Run $run
 *
 * @package App\Models
 */
class ControlMark extends Eloquent
{
	protected $casts = [
		'application_id' => 'int',
		'user_id' => 'int',
		'control_post_id' => 'int',
		'reverse' => 'int',
		'active' => 'int',
		'run_id' => 'int',
	];

	protected $fillable = [
		'application_id',
		'user_id',
		'control_post_id',
		'reverse',
		'active',
		'run_id',
	];

	public function application()
	{
		return $this->belongsTo(\App\Models\Application::class);
	}

	public function control_post()
	{
		return $this->belongsTo(\App\Models\ControlPost::class);
	}

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class);
	}
	
	public function run()
	{
		return $this->belongsTo(\App\Models\Run::class);
	}
	
	/**
	 * Create by admin
	 * @param User $user
	 * @param Application $application
	 * @param array $data
	 * @return static
	 */
	public static function addByAdmin($user, $application, $data)
	{
		$controlMark = self::create([
			'application_id' => $application->id,
			'user_id' => $user->id,
			'control_post_id' => array_get($data, 'post_id'),
			'reverse' => array_get($data, 'reverse', 0)
		]);
		
		return $controlMark;
	}
	
	/**
	 * Find control mark by run and control post
	 * @param int $runId
	 * @param int $controlPostId
	 * @return static|null
	 */
	public static function getPostMarkByRun($runId, $controlPostId)
	{
		$controlMark = static
			::where('run_id', $runId)
			->where('control_post_id', $controlPostId)
			->first();
		
		return $controlMark;
	}
	
	/**
	 * @param $runId
	 * @return mixed
	 */
	public static function getMarksByRun($runId)
	{
		$controlMarks = static
			::where('run_id', $runId)
			->get();
		
		return $controlMarks;
	}
	
	/**
	 * Get marks count added last 30 minutes
	 * @param int $applicationId
	 * @return int
	 */
	public static function find10MinutesMarks($applicationId)
	{
		$oldestDate = new DateTime();
		$oldestDate->modify('-10 minutes');
		$oldestDateStr = $oldestDate->format('Y-m-d H:i:s');
		
		$marksIn30Minutes = ControlMark
			::where('application_id', $applicationId)
			->where('created_at', '>=', $oldestDateStr)
			->count();
		
		return $marksIn30Minutes;
	}
}
