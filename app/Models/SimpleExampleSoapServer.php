<?php

namespace App\Models;


class SimpleExampleSoapServer
{
    /**
     * @WebMethod
     * @param string $name
     * @param int $age
     * @return string $nameWithAge
     */
    public function getNameWithAge($name, $age)
    {
        return 'Your name is: ' . $name . ' and you have ' . $age . ' years old';
    }

    /**
     * @WebMethod
     * @param string[] $names
     * @return string $userNames
     */
    public function getNameForUsers($names)
    {
        return 'User names: ' . implode(', ', $names);
    }

    /**
     * @WebMethod
     * @param int $max
     * @return string[] $count
     */
    public function countTo($max)
    {
        $array = array();
        for ($i = 0; $i < $max; $i++) {
            $array[] = 'Number: ' . ($i + 1);
        }
        return $array;
    }
}