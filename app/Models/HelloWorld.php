<?php

namespace App\Models;

/**
 * Created by PhpStorm.
 * User: gserg13
 * Date: 02.03.2019
 * Time: 13:48
 */

/**
 * Class HelloWorld
 * @package App\Models
 * @WebService(
 *     targetNamespace="http://foo.bar/servicewithnotwebmethods",
 *     location="http://localhost/wsdl-creator/service.php",
 *     ns="http://foo.bar/servicewithnotwebmethods/types"
 * )
 */
class HelloWorld
{
	/**
	 * ServiceWebMethods
	 *
	 * @WebService(
	 *     targetNamespace="http://foo.bar/servicewithnotwebmethods",
	 *     location="http://localhost/wsdl-creator/service.php",
	 *     ns="http://foo.bar/servicewithnotwebmethods/types"
	 * )
	 */
	public function hello() {
		return 1;
	}
}