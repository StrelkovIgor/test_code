<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class apvgk extends Model
{
    use \Illuminate\Database\Eloquent\SoftDeletes;

    protected $table = "apvgk";

    protected $fillable = [
        "number","name","direction"
    ];

    public function applications(){
        return $this->belongsToMany(Application::class,"application_apvgk","apvgk_id", "application_id")->withTimestamps();
    }

    public function privilege_status()
    {
        return $this->belongsTo(\App\Models\PrivilegeStatus::class);
    }
}
