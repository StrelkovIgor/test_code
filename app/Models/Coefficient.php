<?php

namespace App\Models;

use App\Models\Interfaces\ObjectInfoInterface;
use Illuminate\Database\Eloquent\Model as Eloquent;
use App\Models\Audit\AuditableTrait;
use App\Observers\CoefficientObserver;

/**
 * Class Coefficient
 * 
 * @property int $id
 * @property int $key
 * @property int $road_type
 * @property string $title
 * @property string $short_title
 * @property float $value
 * @property float $spring_value
 * @property string $deleted_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class Coefficient extends Eloquent implements ObjectInfoInterface
{
    use \Illuminate\Database\Eloquent\SoftDeletes;
	use AuditableTrait;

    const
        ID_GP = 1,//Госпошлина
        ID_PB = 2,//Стоимость бланка
        ID_TPG = 3,//Базовый компенсационный индекс предыдущего года
        ID_ITG = 4,//Индекс-дефлятор инвестиций
        ID_KDKZ = 5,//Коэффициент, учитывающий условия дорожно-климатических зон (Кдкз)
        ID_KAP_REM = 6,//Кап ремонт
        ID_KSEZ = 7,//коэффициент, учитывающий природно-климатические условия (Ксез)
        ID_RISH = 8,//Рисх
        ID_KPM = 9,//Рисх
        ID_A = 10,//Рисх
        ID_B = 11,//Рисх
        ID_C = 12,//Рисх
        ID_D = 13,//Рисх
        ID_H = 14,//H
        ID_KLOW = 15; //коэффициент при превышении значений допустимой массы от  2% до 15% включительно (Klow)

	/**
	 * @var array
	 */
	protected $casts = [
        'value' => 'float',
        'spring_value' => 'float'
    ];

	/**
	 * @var array
	 */
	protected $fillable = [
	    'key',
        'road_type',
        'title',
        'short_title',
        'value',
		'spring_value'
    ];

	protected static $cachedValues = [];

	public static function boot()
	{
			self::observe(new CoefficientObserver); 
			parent::boot();
	}

	public function getObjectInfo()
	{
		return $this->title;
	}

    /**
     * Используем только для коэффициентов не зависящих от типов дорог
     * @param string $key
     * @return static|null
     */
	public static function getCachedKoefficient($key) {
	    if (!isset(self::$cachedValues[$key])) {
	        $coeff = self::where('key', $key)->first();
	        if ($coeff) {
	            self::$cachedValues[$key] = $coeff;
            }
        }

	    return self::$cachedValues[$key];
    }
}
