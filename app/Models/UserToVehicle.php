<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 23 Jul 2018 14:47:22 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class UserToVehicle
 * 
 * @property int $id
 * @property int $user_id
 * @property int $vehicle_id
 * @property bool $can_edit
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\User $user
 * @property \App\Models\Vehicle $vehicle
 *
 * @package App\Models
 */
class UserToVehicle extends Eloquent
{
	protected $table = 'user_to_vehicle';

	protected $casts = [
		'user_id' => 'int',
		'vehicle_id' => 'int',
		'can_edit' => 'bool'
	];

	protected $fillable = [
		'user_id',
		'vehicle_id',
		'can_edit'
	];

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class);
	}

	public function vehicle()
	{
		return $this->belongsTo(\App\Models\Vehicle::class);
	}
	
	/**
	 * @param array $data
	 * @param User $user
	 * @param Vehicle $vehicle
	 */
	public static function createUsers($data, $user, $vehicle)
	{
		//remove all old bind @todo avoid this
		self::where('vehicle_id', $vehicle->id)->delete();
		
		$usersData = array_get($data, 'userform', []);
		
		if (!empty($usersData)) {
			foreach ($usersData as $userData) {
				if(!empty($userData)) {
					$user_id = null;
					if (
						!isset($userData['is_existing_user']) ||
						!filter_var($userData['is_existing_user'], FILTER_VALIDATE_BOOLEAN)
					) {
						//create new user
						//@todo remove it

						$formUser = null;
						if($email = array_get($userData, 'email')){
							$formUser = User::where('email',$email)->first();
						}
						if(!$formUser){
							$formUser = User::create([
								'name' => $userData['email'],
								'email' => $userData['email'],
								'role_id' => Role::ROLE_FIRM_USER,
								'password' => bcrypt($userData['password']),
								'owner_id' =>$user->id
							]);
						}

						$user_id = $formUser ? $formUser->id : null;
					} else {
						$user_id = intval($userData['existing_user_id'] ?? 0);
					}

					if ($user_id) {
						$userToVehicle = UserToVehicle::create([
							'user_id' => $user_id,
							'vehicle_id' => $vehicle->id,
							'can_edit' => isset($userData['user_edit_access']) ? filter_var($userData['user_edit_access'],
								FILTER_VALIDATE_BOOLEAN) : 0
						]);
					}
				}
			}
		}
	}
}
