<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 25 Aug 2019 11:21:04 +0300.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class RequestLog
 * 
 * @property int $id
 * @property float $duration
 * @property string $ip_address
 * @property string $url
 * @property string $method
 * @property string $request
 * @property string $response
 * @property string $number
 * @property \Carbon\Carbon $event_datetime
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class RequestLog extends Eloquent
{
	protected $casts = [
		'duration' => 'float',
		'request' => 'array',
		'response' => 'array',
	];

	protected $dates = [
		'event_datetime'
	];

	protected $fillable = [
		'duration',
		'ip_address',
		'url',
		'method',
		'request',
		'response',
		'number',
		'event_datetime'
	];
}
