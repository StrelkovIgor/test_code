<?php

namespace App\Models;

use App\Helpers\GradoserviceDistanceHelper;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class ApplicationRoute
 *
 * @property int $id
 * @property int $application_id
 * @property int $type_id
 * @property string $points
 * @property string $legs
 * @property string $text_route
 * @property float $distance
 * @property string $distance_info
 * @property float $federal_distance
 * @property float $regional_distance
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $tracks
 * @property string $description
 *
 * @property \App\Models\Application $application
 *
 * @package App\Models
 */
class ApplicationRoute extends Eloquent
{
    const TYPE_BOTH_WITH_LOAD = 2,
        TYPE_BOTH_NO_LOAD = 5;

    const ROAD_TYPE_REGIONAL = 0, //региональная
        ROAD_TYPE_FEDERAL = 1,  //федиральная
        ROAD_TYPE_LOCAL = 2;//муниципальная

    const DEFAULT_ROAD_TYPE = 0;//REGIONAL

    const ROAD_TO_LOAD = [
        0 => 10,
        1 => 11.5,
        2 => 6
    ];

    protected $casts = [
        'application_id' => 'int',
        'type_id' => 'int',
        'distance_info' => 'array'
    ];

    protected $fillable = [
        'application_id',
        'type_id',
        'points',
        'legs',
        'text_route',
        'distance',
        'distance_info',
        'federal_distance',
        'regional_distance',
        'tracks',
        'description'
    ];

    private static $routeTypes = [
        'По маршруту с грузом, обратно без груза',
        'По маршруту с грузом, обратно с грузом',
        'По маршруту без груза, обратно с грузом',
        'По маршруту с грузом',
        'По маршруту без груза, обратно без груза',
        'По маршруту (без груза)',
    ];

    private static $privilegeRouteInfo = 'По маршруту с грузом, обратно с грузом';

    public static function getFullRouteDistance($distance, $type_id)
    {
        if ($type_id == self::TYPE_BOTH_WITH_LOAD || $type_id == self::TYPE_BOTH_NO_LOAD) {
            $distance *= 2;
        }
        return $distance;
    }

    /**
     * @return array|mixed
     */
    public function getTracks(){
        return $this->tracks ? \GuzzleHttp\json_decode($this->tracks, true) : [];
    }

    /**
     * @param $track
     * @return int
     */
    public function setTracks($track){
        $track['timeId'] = time();
        $tracks = $this->getTracks();

        array_push($tracks,$track);
        $this->tracks = \GuzzleHttp\json_encode($tracks);

        $this->save();
        return $track['timeId'];
    }

    public function setTracksByTimeId($timeId, $trackSet){
        $tracks = $this->getTracks();
        foreach ($tracks as &$track)
            if($track['timeId'] == $timeId){
                $track = array_merge($track,$trackSet);
                break;
            }
        $this->tracks = \GuzzleHttp\json_encode($tracks);
        $this->save();
    }

    /**
     * @param Application $application
     * @param array $data
     * @return ApplicationRoute
     * @throws GuzzleException
     */
    public static function updateByData($application, $data)
    {
        //get route object or create
        /** @var ApplicationRoute $route */
        $route = ApplicationRoute::where('application_id', $application->id)->first();
        if (!$route) {
            $route = new ApplicationRoute(['application_id' => $application->id]);
        }

        $applicationData = $data;
        $applicationData['application_id'] = $application['id'];

        if (!isset($data['markers'])) {
            $data['markers'] = [];
        }
        $applicationData['points'] = json_encode($data['markers']);

        if (isset($data['text_route'])) {
            $applicationData['text_route'] = $data['text_route'];
        }

        //calculate distance
        $coords = [];
        if (!empty($data['markers'])) {
            foreach ($data['markers'] as $marker) {
                $coords[] = $marker['coords'];
            }
        }
        if (!empty($coords)) {
            $distanceHelper = new GradoserviceDistanceHelper();
            $routeInfo = $distanceHelper->getRouteInfo($coords);
            $distance = array_get($routeInfo, 'distance');
            $distanceInfo = array_get($routeInfo, 'distance_info');
            $legs = array_get($routeInfo, 'legs');

            $applicationData['distance'] = $distance;
            $applicationData['legs'] = \GuzzleHttp\json_encode($legs);
            $applicationData['distance_info'] = $distanceInfo;
        }

        //update request
        if ($route->id) {
            $route->update($applicationData);
        } else {
            $route = ApplicationRoute::create($applicationData);
        }

        return $route;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function application()
    {
        return $this->belongsTo(\App\Models\Application::class);
    }

    /**
     * @param array $changedPoints
     * @param float $federalDistance
     * @param float $regionalDistance
     * @throws GuzzleException
     */
    public function applyRouteChange($changedPoints = [], $federalDistance = 0.0, $regionalDistance = 0.0)
    {
//		$distanceHelper = new MapBoxDistanceHelper();
        $distanceHelper = new GradoserviceDistanceHelper();
        $routeInfo = $distanceHelper->getRouteInfo($changedPoints);
        $distance = array_get($routeInfo, 'distance');
        $legs = array_get($routeInfo, 'legs');


        $this->distance = $distance;
        $this->legs = json_encode($legs);

        $this->federal_distance = $federalDistance;
        $this->regional_distance = $regionalDistance;

        $formattedPoints = [];
        foreach ($changedPoints as $point) {
            $formattedPoints[] = [
                'coords' => [
                    'lat' => $point['lat'],
                    'lon' => $point['lon'],
                ],
                'text' => $point['text']
            ];
        }

        $this->points = \GuzzleHttp\json_encode($formattedPoints);
        $this->save();
    }

    public function track($points, $save, $data){
        if(count($points)){
            $distanceHelper = new GradoserviceDistanceHelper();
            $routeInfo = $distanceHelper->getRouteInfo($points);
        }
        if($save){
            $newTrack = [];
            $newTrack['name'] = array_get($data,'name', null);
            $timeId = array_get($data,'timeId', null);
            $newTrack['points'] = $points;
            $newTrack['resultInfo'] = $routeInfo;
            if($timeId)
                $this->setTracksByTimeId($timeId, $newTrack);
            else
                $this->setTracks($newTrack);
        }
        return $routeInfo;
    }

    public function trackRemove($request){
        $timeId = $request->route('timeId');
        $tracks = $this->getTracks();
        foreach ($tracks as $key => $track)
            if($track['timeId'] == $timeId){
                unset($tracks[$key]);
                $this->tracks = \GuzzleHttp\json_encode(array_values($tracks));
                $this->save();
                return true;
                break;
            }
        return false;
    }

    public function setDistanceInfo($distance){
        if(is_array($distance)){
            $this->distance = array_sum($distance);
            $this->distance_info = json_decode(json_encode($distance), FALSE);;
            $this->save();
        }
    }

    /**
     * @return string
     */
    public function getRouteText()
    {
        $texts = [];
        $points = \GuzzleHttp\json_decode($this->points, true);
        if (!empty($points)) {
            foreach ($points as $point) {
                $texts[] = $point['text'];
            }
        }

        return implode(' - ', $texts);
    }

    /**
     * @return mixed
     */
    public function getRouteTypeInfo()
    {
        return array_get(self::$routeTypes, $this->type_id - 1, '');
    }

    public static function getPrivilegeTypeInfo()
    {
        return self::$privilegeRouteInfo;
    }

    public static function getRoadTypes() {
        return [
            self::ROAD_TYPE_REGIONAL,
            self::ROAD_TYPE_FEDERAL,
            self::ROAD_TYPE_LOCAL
        ];
    }

    public function pointParse(){
        $points = \GuzzleHttp\json_decode($this->points, true);
        $result = ['legs' => [], 'distance' => 0, 'distance_info' => [0,0,0],'description' =>[]];

        if(is_array($points))
            foreach ($points as $key => $point){
                $distance_info = null;
                if(isset($point['coords'])){
                    $coords = [];
                    $description = [];

                    foreach ($points as $p) {
                        $coords[] = $p['coords'];
                        $description[] = array_get($p,'text');
                    }

                    $distanceHelper = new GradoserviceDistanceHelper();
                    $routeInfo = $distanceHelper->getRouteInfo($coords);

                    if ($routeInfo['distance']) {
                        $result['legs'][] = $routeInfo['legs'];
                        $result['distance'] += $routeInfo['distance'];
                        $distance_info = $routeInfo['distance_info'];
                    }

                    foreach ($result['distance_info'] as $type => &$value){
                        if($distance_info[$type]) $value += $distance_info[$type];
                    }

                    array_push($result['description'], $description);

                    break;

                }elseif(isset($point['id_list'])){
                    $data = RouteList::getListUpdate($point['id_list']);
                    if($data){
                        $result['legs'][] = $data['legs'];
                        $result['distance'] += $data['distance'];
                        $distance_info = $data['distance_info'];
                        array_push($result['description'], $data['description']);
                    }
                }elseif(is_array($point) && isset($point[0]['coords'])){
                    $coords = [];
                    $description = [];

                    foreach ($point as $p) {
                        $coords[] = $p['coords'];
                        $description[] = array_get($p,'name');
                    }

                    $distanceHelper = new GradoserviceDistanceHelper();
                    $routeInfo = $distanceHelper->getRouteInfo($coords);

                    if ($routeInfo['distance']) {
                        $result['legs'][] = $routeInfo['legs'];
                        $result['distance'] += $routeInfo['distance'];
                        $distance_info = $routeInfo['distance_info'];
                        array_push($result['description'], $description);
                    }

                }

                foreach ($result['distance_info'] as $type => &$value){
                    if($distance_info && $distance_info[$type]) $value += $distance_info[$type];
                }

            }

        return $result;
    }

    public function getLegsByPoints($is_save = true){
        $result = $this->pointParse();

        if($is_save){
            $this->legs = \GuzzleHttp\json_encode(array_get($result, 'legs', []));
            $this->distance = array_get($result, 'distance', []);
            $this->distance_info = array_get($result, 'distance_info', []);

            $description = array_get($result, 'description',[]);
            $this->description = self::descriptionFormat($description);

            $this->save();
        }
        return $result;
    }

    public static function descriptionFormat(array $description){
        foreach($description as &$d)
            $d = is_array($d) ? implode(' - ',$d) : $d ;

        return preg_replace('/^ +| +$|( ) +/m', '$1', implode(' ; ',$description));
    }

    public function descriptionSave(){
        $result = $this->pointParse();
        $description = array_get($result, 'description',[]);

        $this->description = self::descriptionFormat($description);
        $this->save();
    }

}
