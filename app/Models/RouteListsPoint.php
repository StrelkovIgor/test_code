<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RouteListsPoint extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'route_list_id',
        'name',
        'lat',
        'lon'
    ];

    const STEP = 0.00000000001;

    public function RouteLists(){
        return $this->belongsTo(RouteList::class);
    }

    public static function search(&$points){

        foreach ($points as &$point){
            $point['s'] = self::where(function($column) use ($point){
                $column->where('lat','<=', $point['lat'] + self::STEP)
                    ->where('lat','>=', $point['lat'] - self::STEP)
                    ->where('lon','<=', $point['lon'] + self::STEP)
                    ->where('lon','>=', $point['lon'] - self::STEP);
            })->get();
        }
    }

    public static function isCheck(&$points, &$route_list_id = 0){
        $routes = [];
        foreach ($points as $point){
            if(!$point['s']->count()) return true;
            foreach ($point['s'] as $point)
                $routes[$point->route_list_id] = isset($routes[$point->route_list_id])?$routes[$point->route_list_id] + 1:1;

        }
        foreach ($routes as $id => $countPoints)
            if($countPoints == count($points)){
                $route_list_id = $id;
                return false;
            }
        return true;
    }


}
