<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Role extends Model
{
    const
        ROLE_ADMIN = 1, // админ
        ROLE_OFFICER = 2, // сотрудник ГБУ БДД
        ROLE_INDIVIDUAL = 3, // заявитель, физ лицо
        ROLE_FIRM = 4, // заявитель, юр лицо
        ROLE_FIRM_USER = 5, // сотрудник юр лица, заявитель
        ROLE_DEPARTMENT_AGENT = 6, // сотрудник ведомства
        ROLE_WEIGHT_CONTROL = 7; // сотрудник ПВК

    public static function isApplicant()
    {
        return in_array(Auth::user()->role_id, [
            self::ROLE_INDIVIDUAL,
            self::ROLE_FIRM,
            self::ROLE_FIRM_USER,
        ]);
    }

    public static function isAdmin()
    {
        return Auth::user()->role_id === self::ROLE_ADMIN;
    }

    public static function isAdminOrGbu()
    {
        return in_array(Auth::user()->role_id, [
            self::ROLE_ADMIN,
            self::ROLE_OFFICER,
        ]);
    }

    public static function isDepAgent()
    {
        return Auth::user()->role_id === self::ROLE_DEPARTMENT_AGENT;
    }

    public static function isWeightControl()
    {
        return Auth::user()->role_id === self::ROLE_WEIGHT_CONTROL;
    }

	/**
	 * @param int $role_id
	 * @return string
	 */
    public static function getNameById($role_id)
	{
		switch($role_id) {
			case self::ROLE_ADMIN:
				return 'Администратор';
			case self::ROLE_OFFICER:
				return 'Сотрудник ГБУ БДД';
			case self::ROLE_INDIVIDUAL:
				return 'заявитель, физ лицо';
			case self::ROLE_FIRM:
				return 'заявитель, юр лицо';
			case self::ROLE_DEPARTMENT_AGENT:
				return 'Сотрудник ведомства';
			case self::ROLE_WEIGHT_CONTROL:
				return 'Сотрудник ПВК';
		}

		return '';
	}
}
