<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 14 Dec 2018 08:54:57 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class IssuePlace
 * 
 * @property int $id
 * @property string $title
 * @property string $fio
 * @property string $position
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class IssuePlace extends Eloquent
{
	protected $fillable = [
		'title',
		'fio',
		'position'
	];
}
