<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 18 Nov 2018 05:41:03 +0000.
 */

namespace App\Models;

use App\Models\Interfaces\ObjectInfoInterface;
use Illuminate\Database\Eloquent\Model as Eloquent;
use App\Models\Audit\AuditableTrait;
use App\Observers\DepartmentObserver;

/**
 * Class Department
 *
 * @property int $id
 * @property string $title
 * @property string $short_title
 * @property string $deleted_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @property \Illuminate\Database\Eloquent\Collection $application_agreements
 * @property \Illuminate\Database\Eloquent\Collection $applications
 * @property \Illuminate\Database\Eloquent\Collection $users
 *
 * @package App\Models
 */
class Department extends Eloquent implements ObjectInfoInterface
{

	use \Illuminate\Database\Eloquent\SoftDeletes;
	use AuditableTrait;

	const DEPARTMENT_GIBDD = 1,
		DEPARTMENT_GDS = 2;
	
	protected $fillable = [
		'title',
		'short_title'
	];
		
	public static function boot()
	{
			self::observe(new DepartmentObserver); 
			parent::boot();
	}

	public function application_agreements()
	{
		return $this->hasMany(\App\Models\ApplicationAgreement::class);
	}
	
	public function applications()
	{
		return $this->hasMany(\App\Models\Application::class);
	}
	
	public function users()
	{
		return $this->hasMany(\App\Models\User::class);
	}

	public function getObjectInfo()
	{
		return $this->title;
	}
}
