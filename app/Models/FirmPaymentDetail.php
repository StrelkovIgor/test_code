<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 06 Jun 2018 09:53:47 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class FirmPaymentDetail
 * 
 * @property int $id
 * @property int $firm_id
 * @property string $bank_name
 * @property string $bank_inn
 * @property string $correspondent_account
 * @property string $bank_bik
 * @property string $account
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\Firm $firm
 *
 * @package App\Models
 */
class FirmPaymentDetail extends Eloquent
{
	protected $casts = [
		'firm_id' => 'int'
	];

	protected $fillable = [
		'firm_id',
		'bank_name',
		'bank_inn',
		'correspondent_account',
		'bank_bik',
		'account'
	];

	public function firm()
	{
		return $this->belongsTo(\App\Models\Firm::class);
	}

	/**
	 * @param array $data
	 */
	public function updateCabinet($data)
	{
		if ($this->firm) {
			$this->bank_name = array_get($data, 'bank_name');
			$this->bank_inn = array_get($data, 'bank_inn');
			$this->correspondent_account = array_get($data, 'correspondent_account');
			$this->bank_bik = array_get($data, 'bank_bik');
			$this->account = array_get($data, 'account');
			
			$this->save();
		}
	}

	/**
	 * @param int $firmId
	 * @param array $data
	 */
	public static function createByData($firmId, $data)
	{
		$details = new static([
			'firm_id' => $firmId
		]);

		$details->bank_name = array_get($data, 'bank_name');
		$details->bank_inn = array_get($data, 'bank_inn');
		$details->correspondent_account = array_get($data, 'correspondent_account');
		$details->bank_bik = array_get($data, 'bank_bik');
		$details->account = array_get($data, 'account');

		$details->save();
	}
}
