<?php

namespace App\Models\Audit;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class AuditDetail
 * 
 * @property int $id
 * @property int $audit_id
 * @property string $ip_address
 * @property string $user_agent
 * @property string $url
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\Audit\Audit $audit
 *
 * @package App\Models
 */
class AuditDetail extends Eloquent
{
	protected $casts = [
		'audit_id' => 'int'
	];

	protected $fillable = [
		'audit_id',
		'ip_address',
		'user_agent',
		'url'
	];

	public function audit()
	{
		return $this->belongsTo(\App\Models\Audit\Audit::class);
	}
}
