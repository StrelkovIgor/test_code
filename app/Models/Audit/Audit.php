<?php

namespace App\Models\Audit;

use App\Models\ApplicationAgreement;
use App\Models\Coefficient;
use App\Models\ControlPost;
use App\Models\Department;
use App\Models\PrivilegeStatus;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use App\Models\Application;
use App\Models\Vehicle;
use App\Models\Audit\Enum\Events;

/**
 * Class Audit
 *
 * @property int $id
 * @property int $user_id
 * @property int $auditable_id
 * @property int $auditable_type_key
 * @property int $event_key
 * @property int $control_post_id
 * @property string $created_at
 * @property string $updated_at
 *
 * @property User $user
 * @property Application|User|Vehicle|ApplicationAgreement $auditable
 * @property ControlPost $control_post
 * @property AuditDetail $audit_details
 *
 * @package App\Models\Audit
 *
 */
class Audit extends Model
{
	const KEY_APPLICATION = 0,
		KEY_USER = 1,
		KEY_VEHICLE = 2,
		KEY_CONTROL_POST = 3,
		KEY_DEPARTMENT = 4,
		KEY_PRIVILEGE_STATUS = 5,
		KEY_COEFFICIENT = 6,
		KEY_APPLICATION_AGREEMENT = 7;

	protected static $keyInfo = [
		self::KEY_APPLICATION => [
			'className' => Application::class,
			'title' => 'Заявление'
		],
		self::KEY_USER => [
			'className' => User::class,
			'title' => 'Пользователь'
		],
		self::KEY_VEHICLE => [
			'className' => Vehicle::class,
			'title' => 'ТС'
		],
		self::KEY_CONTROL_POST => [
			'className' => ControlPost::class,
			'title' => 'Контрольный пост'
		],
		self::KEY_DEPARTMENT => [
			'className' => Department::class,
			'title' => 'Ведомство'
		],
		self::KEY_PRIVILEGE_STATUS => [
			'className' => PrivilegeStatus::class,
			'title' => 'Статус'
		],
		self::KEY_COEFFICIENT => [
			'className' => Coefficient::class,
			'title' => 'Коэффициент'
		],
		self::KEY_APPLICATION_AGREEMENT => [
			'className' => ApplicationAgreement::class,
			'title' => 'Согласование'
		],
	];

	protected static $objectNames = [
		'application' => 'Заявление',
		'user' => 'Пользователь',
		'vehicle' => 'ТС',
		'controlpost' => 'Контрольный пост',
		'department' => 'Ведомство',
		'privilegestatus' => 'Статус',
		'coefficient' => 'Коэффициент',
		'applicationagreement' => 'Согласование'
	];

	protected static $objectKeys = [
		'application' => self::KEY_APPLICATION,
		'user' => self::KEY_USER,
		'vehicle' => self::KEY_VEHICLE,
		'controlpost' => self::KEY_CONTROL_POST,
		'department' => self::KEY_DEPARTMENT,
		'privilegestatus' => self::KEY_PRIVILEGE_STATUS,
		'coefficient' => self::KEY_COEFFICIENT,
		'applicationagreement' => self::KEY_APPLICATION_AGREEMENT
	];

	protected $casts = [
		'auditable_type_key' => 'int',
		'event_key' => 'int',
	];
	protected $fillable = [
		'user_id',
		'auditable_id',
		'auditable_type',
		'auditable_type_key',
		'event_key',
		'control_post_id'
	];
	protected $_filter = [];
	private $_filterLegal = [
		'date_from',
		'date_to',
		'fio',
		'type',
		'real_number',
		'app_id',
		'event',
		'posts'
	];

	public function auditable()
	{
		return $this->morphTo()->withTrashed();
	}

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class);
	}

	public function control_post()
	{
		return $this->belongsTo(\App\Models\ControlPost::class);
	}

	public function audit_details()
	{
		return $this->hasOne(\App\Models\Audit\AuditDetail::class);
	}

	public function setFilter($_arrParams = [])
	{
		// список фильтров которые может применить клиент
		if (empty($this->_filterLegal)) {
			return $this;
		}
		// накладывание входных параметров с клиента на дефолтный фильтр
		$_arrParams = array_merge($this->_filter, $_arrParams);
		foreach ($_arrParams as $k => $v) {
			// отсеивание только разрешённых значений
			if (!in_array($k, $this->_filterLegal)) {
				continue;
			}
			$this->_filter[$k] = $v;
		}

		return $this;
	}

	public function getList()
	{
		$builder = $this->with('auditable', 'user');
		$builder->select('audits.*');

		// event
		$eventsExceptions = [
			Events::APPLICANT_APP_INVOICE,
			Events::APPLICANT_APP_SOFT_DELETE,
			Events::APPLICANT_APP_TOREVIEW,
			Events::APPLICANT_APP_WITHDRAW,
		];
		if(!empty($this->_filter['event']) && is_array($this->_filter['event'])) {
			$this->_filter['event'] = array_diff($this->_filter['event'], $eventsExceptions);
		}
		if (!empty($this->_filter['event'])) {
			//replace events codes to event keys
			$keys = [];
			foreach($this->_filter['event'] as $eventCode) {
				$keys[] = Events::getEventKey($eventCode);
			}
			if(count($keys) === 1) {
				$builder->where('event_key', $keys[0]);
			}else{
				$builder->whereIn('event_key', $keys);
			}
		}else{
			$keys = [];
			foreach($eventsExceptions as $eventCode) {
				$keys[] = Events::getEventKey($eventCode);
			}
			$builder->whereNotIn('event_key', $keys);
		}


		// date filter
		if (!empty($this->_filter['date_from'])) {
			$builder->where('audits.created_at', '>=', $this->_filter['date_from'] . ' 00:00:00');
		}
		if (!empty($this->_filter['date_to'])) {
			$builder->where('audits.created_at', '<=', $this->_filter['date_to'] . ' 23:59:59');
		}


		// user fio filter
		if ($fio = array_get($this->_filter,'fio')) {
			$builder->leftjoin('users', 'users.id', '=', 'audits.user_id');
			$builder->where('users.name', 'like', '%' . $fio . '%');
		}

		//type
		$auditable_type_key = null;
		if($formattedAppId = array_get($this->_filter, 'app_id')) {
			$appId = Application::formattedIdToId($formattedAppId);
			$builder->leftjoin('application_agreements', 'application_agreements.id', '=', 'auditable_id');

			$builder->where(function ($q) use ($appId) {
				$q->where('auditable_type_key', Audit::KEY_APPLICATION)
					->where('auditable_id', $appId)
					->orWhere(function ($q1) use ($appId) {
						$q1->where('auditable_type_key', Audit::KEY_APPLICATION_AGREEMENT)
							->where('application_agreements.application_id', $appId);
					});
			});
		}
		if($number = array_get($this->_filter, 'real_number')) {
			$builder->leftjoin('vehicles', 'vehicles.id', '=', 'audits.auditable_id');
			$builder->where('vehicles.real_number', 'like', '%' . $number . '%');
			$auditable_type_key = Audit::KEY_VEHICLE;
		}
		if($auditable_type_key !== null) {
			$builder->where('auditable_type_key', $auditable_type_key);
		}

		$posts = array_get($this->_filter, 'posts');
		if(!empty($posts)) {
			$builder->whereIn('control_post_id', $posts);
		}

		return $builder;
	}

	public static function getObjectTitleByKey($key)
	{
		return isset(self::$keyInfo[$key]) && isset(self::$keyInfo[$key]['title']) ? self::$keyInfo[$key]['title'] : '';
	}
	public static function getObjectClassNameByKey($key)
	{
		return isset(self::$keyInfo[$key]) && isset(self::$keyInfo[$key]['className']) ? self::$keyInfo[$key]['className'] : '';
	}

	/**
	 * @param string $className
	 * @return string|null
	 */
	public static function getAuditObjectTitle($className)
	{
		$modelName = static::simplifyClassName($className);

		if($modelName) {
			return self::$objectNames[$modelName] ?? null;
		}

		return null;
	}

	/**
	 * @param string $className
	 * @return mixed|null
	 */
	public static function getAuditObjectKey($className)
	{
		$modelName = static::simplifyClassName($className);

		if($modelName) {
			return self::$objectKeys[$modelName] ?? null;
		}

		return null;
	}

	/**
	 * @param string $className
	 * @return null|string
	 */
	private static function simplifyClassName($className)
	{
		$parts = explode('\\', $className);
		$modelName = $parts[count($parts) - 1];

		if($modelName) {
			return strtolower($modelName);
		}

		return null;
	}

	/**
	 * @return string
	 */
	public function getObjectClassName()
	{
		return self::getObjectClassNameByKey($this->auditable_type_key);
	}

	/**
	 * Get object info for report
	 * @return string
	 */
	public function getObjectInfo()
	{
		$objectInfo = '';
		if($this->auditable && method_exists($this->auditable, 'getObjectInfo')) {
			$objectInfo = $this->auditable->getObjectInfo();
		}
		$auditName = self::getObjectTitleByKey($this->auditable_type_key);
		return $auditName . ': ' . $objectInfo;
	}

	public static function createWithDetails($data)
	{
		/** @var static|null $result */
		$result = static::create($data);

		if($result) {
			$detailsData = [
				'audit_id' => $result->id,
				'ip_address' => $data['ip_address'],
				'user_agent' => $data['user_agent'],
				'url' => $data['url'],
			];
			AuditDetail::create($detailsData);
		}

		return $result;
	}
}
