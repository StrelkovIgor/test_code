<?php

namespace App\Models\Audit\Enum;

use App\Helpers\AbstractEnum;

// audits.event
class Events extends AbstractEnum
{
    // http://tracker.namisoft.ru/projects/ais-ktg/work_packages/4546/activity
    public const APPLICANT_APP_TOREVIEW = 'APPLICANT_APP_TOREVIEW';
    public const APPLICANT_APP_WITHDRAW = 'APPLICANT_APP_WITHDRAW';
    public const APPLICANT_APP_SOFT_DELETE = 'APPLICANT_APP_SOFT_DELETE';
    public const APPLICANT_APP_INVOICE = 'APPLICANT_APP_INVOICE';
    public const ADMINGBU_APP_TOWORK = 'ADMINGBU_APP_TOWORK';
    public const DEPAGENT_APP_TOWORK = 'DEPAGENT_APP_TOWORK';
    public const ADMINGBU_APP_TODEPARTMENT = 'ADMINGBU_APP_TODEPARTMENT';
    public const ADMINGBU_CHANGE_PATH = 'ADMINGBU_CHANGE_PATH';
    public const ADMINGBU_APP_ACCEPT = 'ADMINGBU_APP_ACCEPT';
    public const ADMINGBU_APP_DECLINE = 'ADMINGBU_APP_DECLINE';
    public const ADMINGBU_APP_ACTIVATE = 'ADMINGBU_APP_ACTIVATE';
    public const ADMINGBU_APP_PDF = 'ADMINGBU_APP_PDF';
    public const ADMIN_APP_RESTORE = 'ADMIN_APP_RESTORE';
    public const ADMIN_APP_LOCK = 'ADMIN_APP_LOCK';
    public const ADMIN_APP_UNLOCK = 'ADMIN_APP_UNLOCK';
    public const DEPAGENT_APP_ACCEPT = 'DEPAGENT_APP_ACCEPT';
    public const DEPAGENT_APP_DECLINE = 'DEPAGENT_APP_DECLINE';
    public const WCONTROL_APP_ADD_CONTROL_MARK = 'WCONTROL_APP_ADD_CONTROL_MARK';

    // http://tracker.namisoft.ru/projects/ais-ktg/work_packages/4504/activity
    public const USER_CREATE = 'USER_CREATE';
    public const USER_LOGIN_EXEPT_APPLICANT = 'USER_LOGIN_EXEPT_APPLICANT';
    public const ADMINGBU_USERREG_ACCEPT = 'ADMINGBU_USERREG_ACCEPT';
    public const ADMINGBU_USERREG_DECLINE = 'ADMINGBU_USERREG_DECLINE';
    public const ADMINGBU_USER_LOCK = 'ADMINGBU_USER_LOCK';
    public const ADMINGBU_USER_UNLOCK = 'ADMINGBU_USER_UNLOCK';
    public const ADMINGBU_VEHICLE_TOWORK = 'ADMINGBU_VEHICLE_TOWORK';
    public const ADMINGBU_VEHICLE_ACCEPT = 'ADMINGBU_VEHICLE_ACCEPT';
    public const ADMINGBU_VEHICLE_DECLINE = 'ADMINGBU_VEHICLE_DECLINE';
    public const ADMIN_USER_SOFT_DELETE = 'ADMIN_USER_SOFT_DELETE';
    public const ADMIN_USER_EDIT = 'ADMIN_USER_EDIT';
    public const ADMIN_DEPARTMENT_CREATE = 'ADMIN_DEPARTMENT_CREATE';
    public const ADMIN_DEPARTMENT_SOFT_DELETE = 'ADMIN_DEPARTMENT_SOFT_DELETE';
    public const ADMIN_POST_CREATE = 'ADMIN_POST_CREATE';
    public const ADMIN_POST_SOFT_DELETE = 'ADMIN_POST_SOFT_DELETE';
    public const ADMIN_STATUS_CREATE = 'ADMIN_STATUS_CREATE';
    public const ADMIN_STATUS_EDIT = 'ADMIN_STATUS_EDIT';
    public const ADMIN_STATUS_SOFT_DELETE = 'ADMIN_STATUS_SOFT_DELETE';
    public const ADMIN_COEFF_EDIT = 'ADMIN_COEFF_EDIT';

    const ADMINGBU_CANCEL_STATUS_NEW= 'ADMINGBU_CANCEL_STATUS_NEW';

    //keys
	// http://tracker.namisoft.ru/projects/ais-ktg/work_packages/4546/activity
	public const APPLICANT_APP_TOREVIEW_KEY = 0;
	public const APPLICANT_APP_WITHDRAW_KEY = 1;
	public const APPLICANT_APP_SOFT_DELETE_KEY = 2;
	public const APPLICANT_APP_INVOICE_KEY = 3;
	public const ADMINGBU_APP_TOWORK_KEY = 4;
	public const DEPAGENT_APP_TOWORK_KEY = 5;
	public const ADMINGBU_APP_TODEPARTMENT_KEY = 6;
	public const ADMINGBU_CHANGE_PATH_KEY =7;
	public const ADMINGBU_APP_ACCEPT_KEY = 8;
	public const ADMINGBU_APP_DECLINE_KEY = 9;
	public const ADMINGBU_APP_ACTIVATE_KEY = 10;
	public const ADMINGBU_APP_PDF_KEY = 11;
	public const ADMIN_APP_RESTORE_KEY = 12;
	public const ADMIN_APP_LOCK_KEY = 13;
	public const ADMIN_APP_UNLOCK_KEY = 14;
	public const DEPAGENT_APP_ACCEPT_KEY = 15;
	public const DEPAGENT_APP_DECLINE_KEY = 16;
	public const WCONTROL_APP_ADD_CONTROL_MARK_KEY = 17;

	// http://tracker.namisoft.ru/projects/ais-ktg/work_packages/4504/activity
	public const USER_CREATE_KEY = 18;
	public const USER_LOGIN_EXEPT_APPLICANT_KEY = 19;
	public const ADMINGBU_USERREG_ACCEPT_KEY = 20;
	public const ADMINGBU_USERREG_DECLINE_KEY = 21;
	public const ADMINGBU_USER_LOCK_KEY = 22;
	public const ADMINGBU_USER_UNLOCK_KEY = 23;
	public const ADMINGBU_VEHICLE_TOWORK_KEY = 24;
	public const ADMINGBU_VEHICLE_ACCEPT_KEY = 25;
	public const ADMINGBU_VEHICLE_DECLINE_KEY = 26;
	public const ADMIN_USER_SOFT_DELETE_KEY = 27;
	public const ADMIN_USER_EDIT_KEY = 28;
	public const ADMIN_DEPARTMENT_CREATE_KEY = 29;
	public const ADMIN_DEPARTMENT_SOFT_DELETE_KEY = 30;
	public const ADMIN_POST_CREATE_KEY = 31;
	public const ADMIN_POST_SOFT_DELETE_KEY = 32;
	public const ADMIN_STATUS_CREATE_KEY = 33;
	public const ADMIN_STATUS_EDIT_KEY = 34;
	public const ADMIN_STATUS_SOFT_DELETE_KEY = 35;
	public const ADMIN_COEFF_EDIT_KEY = 36;

    const ADMINGBU_CANCEL_STATUS_NEW_KEY= 37;


    public static $toSelect = [
        self::ADMINGBU_APP_TOWORK => 'Взял в работу заявление',
        self::ADMINGBU_APP_TODEPARTMENT => 'Отправил  на согласование заявление',
        self::ADMINGBU_CHANGE_PATH => 'Изменил маршрут заявления',
        self::ADMINGBU_APP_ACCEPT => 'Одобрил заявление',
        self::ADMINGBU_APP_DECLINE => 'Отклонил заявление',
        self::ADMINGBU_APP_ACTIVATE => 'Активировал разрешение',
        self::ADMINGBU_APP_PDF => 'Распечатал разрешение',
        self::ADMIN_APP_RESTORE => 'Восстановил разрешение',
        self::ADMIN_APP_LOCK => 'Заблокировал разрешение',
        self::ADMIN_APP_UNLOCK => 'Разблокировал разрешение',
        self::DEPAGENT_APP_TOWORK => 'Взял заявление в работу',
        self::DEPAGENT_APP_ACCEPT => 'Cогласовал заявление',
        self::DEPAGENT_APP_DECLINE => 'Отклонил согласование заявления',
        self::WCONTROL_APP_ADD_CONTROL_MARK => 'Добавил отметку о проезде в разрешении',
        self::USER_CREATE => 'Создал пользователя',
        self::USER_LOGIN_EXEPT_APPLICANT => 'Авторизовался в системе',
        self::ADMINGBU_USERREG_ACCEPT => 'Принял заявку на регистрацию пользователя',
        self::ADMINGBU_USERREG_DECLINE => 'Отклонил заявку на регистрацию пользователя',
        self::ADMINGBU_USER_LOCK => 'Заблокировал пользователя',
        self::ADMINGBU_USER_UNLOCK => 'Разблокировал пользователя',
        self::ADMINGBU_VEHICLE_TOWORK => 'Взял в работу заявку на регистрацию ТС',
        self::ADMINGBU_VEHICLE_ACCEPT => 'Принял заявку на регистрацию ТС',
        self::ADMINGBU_VEHICLE_DECLINE => 'Отклонил заявку на регистрацию ТС',
        self::ADMIN_USER_SOFT_DELETE => 'Удалил пользователя',
        self::ADMIN_USER_EDIT => 'Изменил данные пользователя',
        self::ADMIN_DEPARTMENT_CREATE => 'Создал ведомство',
        self::ADMIN_DEPARTMENT_SOFT_DELETE => 'Удалил ведомство',
        self::ADMIN_POST_CREATE => 'Создал ПВК',
        self::ADMIN_POST_SOFT_DELETE => 'Удалил ПВК',
        self::ADMIN_STATUS_CREATE => 'Создал статус',
        self::ADMIN_STATUS_EDIT => 'Изменил статус',
        self::ADMIN_STATUS_SOFT_DELETE => 'Удалил статус',
        self::ADMIN_COEFF_EDIT => 'Изменил значение коэффициента',
        self::ADMINGBU_CANCEL_STATUS_NEW => 'Прекратил работу с разрешением',
    ];

    private static $keys = [
		self::APPLICANT_APP_TOREVIEW => self::APPLICANT_APP_TOREVIEW_KEY,
		self::APPLICANT_APP_WITHDRAW => self::APPLICANT_APP_WITHDRAW_KEY,
		self::APPLICANT_APP_SOFT_DELETE => self::APPLICANT_APP_SOFT_DELETE_KEY,
		self::APPLICANT_APP_INVOICE => self::APPLICANT_APP_INVOICE_KEY,
		self::ADMINGBU_APP_TOWORK => self::ADMINGBU_APP_TOWORK_KEY,
		self::DEPAGENT_APP_TOWORK => self::DEPAGENT_APP_TOWORK_KEY,
		self::ADMINGBU_APP_TODEPARTMENT => self::ADMINGBU_APP_TODEPARTMENT_KEY,
		self::ADMINGBU_CHANGE_PATH => self::ADMINGBU_CHANGE_PATH_KEY,
		self::ADMINGBU_APP_ACCEPT => self::ADMINGBU_APP_ACCEPT_KEY,
		self::ADMINGBU_APP_DECLINE => self::ADMINGBU_APP_DECLINE_KEY,
		self::ADMINGBU_APP_ACTIVATE => self::ADMINGBU_APP_ACTIVATE_KEY,
		self::ADMINGBU_APP_PDF => self::ADMINGBU_APP_PDF_KEY,
		self::ADMIN_APP_RESTORE => self::ADMIN_APP_RESTORE_KEY,
		self::ADMIN_APP_LOCK => self::ADMIN_APP_LOCK_KEY,
		self::ADMIN_APP_UNLOCK => self::ADMIN_APP_UNLOCK_KEY,
		self::DEPAGENT_APP_ACCEPT => self::DEPAGENT_APP_ACCEPT_KEY,
		self::DEPAGENT_APP_DECLINE => self::DEPAGENT_APP_DECLINE_KEY,
		self::WCONTROL_APP_ADD_CONTROL_MARK => self::WCONTROL_APP_ADD_CONTROL_MARK_KEY,
		self::USER_CREATE => self::USER_CREATE_KEY,
		self::USER_LOGIN_EXEPT_APPLICANT => self::USER_LOGIN_EXEPT_APPLICANT_KEY,
		self::ADMINGBU_USERREG_ACCEPT => self::ADMINGBU_USERREG_ACCEPT_KEY,
		self::ADMINGBU_USERREG_DECLINE => self::ADMINGBU_USERREG_DECLINE_KEY,
		self::ADMINGBU_USER_LOCK => self::ADMINGBU_USER_LOCK_KEY,
		self::ADMINGBU_USER_UNLOCK => self::ADMINGBU_USER_UNLOCK_KEY,
		self::ADMINGBU_VEHICLE_TOWORK => self::ADMINGBU_VEHICLE_TOWORK_KEY,
		self::ADMINGBU_VEHICLE_ACCEPT => self::ADMINGBU_VEHICLE_ACCEPT_KEY,
		self::ADMINGBU_VEHICLE_DECLINE => self::ADMINGBU_VEHICLE_DECLINE_KEY,
		self::ADMIN_USER_SOFT_DELETE => self::ADMIN_USER_SOFT_DELETE_KEY,
		self::ADMIN_USER_EDIT => self::ADMIN_USER_EDIT_KEY,
		self::ADMIN_DEPARTMENT_CREATE => self::ADMIN_DEPARTMENT_CREATE_KEY,
		self::ADMIN_DEPARTMENT_SOFT_DELETE => self::ADMIN_DEPARTMENT_SOFT_DELETE_KEY,
		self::ADMIN_POST_CREATE => self::ADMIN_POST_CREATE_KEY,
		self::ADMIN_POST_SOFT_DELETE => self::ADMIN_POST_SOFT_DELETE_KEY,
		self::ADMIN_STATUS_CREATE => self::ADMIN_STATUS_CREATE_KEY,
		self::ADMIN_STATUS_EDIT => self::ADMIN_STATUS_EDIT_KEY,
		self::ADMIN_STATUS_SOFT_DELETE => self::ADMIN_STATUS_SOFT_DELETE_KEY,
		self::ADMIN_COEFF_EDIT => self::ADMIN_COEFF_EDIT_KEY,
        self::ADMINGBU_CANCEL_STATUS_NEW => self::ADMINGBU_CANCEL_STATUS_NEW_KEY
	];

    /** @var array|null  */
    private static $keyToCode = null;

	/**
	 * @return array|bool|null
	 */
    private static function getKeyToCode()
	{
		if(!self::$keyToCode) {
			self::$keyToCode = array_flip(self::$keys);
		}

		return self::$keyToCode;
	}

    public static function getEventKey($code)
	{
		return self::$keys[$code] ?? null;
	}

	/**
	 * @param string $code
	 * @return mixed|null
	 */
    public static function getEventTitle($code)
	{
		return self::$toSelect[$code] ?? null;
	}

	/**
	 * @param int $key
	 * @return string|null
	 */
	public static function getEventCodeByKey($key)
	{
		$codes = self::getKeyToCode();
		return $codes[$key] ?? null;
	}

	/**
	 * Get application events
	 * @return array
	 */
	public static function getApplicationEvents()
	{
		return [
			self::APPLICANT_APP_TOREVIEW_KEY,
			self::ADMINGBU_APP_TOWORK_KEY,
			self::ADMINGBU_APP_DECLINE_KEY,
			self::ADMINGBU_APP_ACCEPT_KEY,
			self::ADMINGBU_APP_ACTIVATE_KEY,
			self::APPLICANT_APP_SOFT_DELETE_KEY,
			self::ADMIN_APP_RESTORE_KEY,

			self::ADMINGBU_APP_TODEPARTMENT_KEY,
			self::DEPAGENT_APP_TOWORK_KEY,
			self::DEPAGENT_APP_ACCEPT_KEY,
			self::DEPAGENT_APP_DECLINE_KEY
		];
	}

	public static function getAgreementEvents()
	{
		return [
			self::ADMINGBU_APP_TODEPARTMENT_KEY,
			self::DEPAGENT_APP_TOWORK_KEY,
			self::DEPAGENT_APP_ACCEPT_KEY,
			self::DEPAGENT_APP_DECLINE_KEY
		];
	}
}
