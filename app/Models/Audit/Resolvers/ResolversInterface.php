<?php

namespace App\Models\Audit\Resolvers;

interface ResolversInterface
{
    /**
     * Resolve
     *
     * @return mixed|null
     */
    public static function resolve();
}
