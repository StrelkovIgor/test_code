<?php

namespace App\Models\Audit\Resolvers;

use Illuminate\Support\Facades\Request;

class UserAgentResolver implements ResolversInterface
{
    public static function resolve()
    {
        return Request::header('User-Agent');
    }
}
