<?php

namespace App\Models\Audit\Resolvers;

use Illuminate\Support\Facades\Request;

class IpAddressResolver implements ResolversInterface
{
    /**
     * {@inheritdoc}
     */
    public static function resolve(): string
    {
        return Request::ip();
    }
}
