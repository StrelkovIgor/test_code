<?php

namespace App\Models\Audit\Resolvers;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Request;

class UrlResolver implements ResolversInterface
{
    public static function resolve(): string
    {
        if (App::runningInConsole()) {
            return 'console';
        }
        return Request::fullUrl();
    }
}
