<?php

namespace App\Models\Audit\Resolvers;

use Illuminate\Support\Facades\Auth;

class UserResolver implements ResolversInterface
{
    public static function resolve()
    {
        $guards = [
            'web',
            'api',
        ];

        foreach ($guards as $guard) {
            if (Auth::guard($guard)->check()) {
                return Auth::guard($guard)->user();
            }
        }
    }
}
