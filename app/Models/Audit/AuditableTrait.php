<?php

namespace App\Models\Audit;

use App\Models\Audit\Resolvers\IpAddressResolver;
use App\Models\Audit\Resolvers\UrlResolver;
use App\Models\Audit\Resolvers\UserAgentResolver;
use App\Models\Audit\Resolvers\UserResolver;
use App\Models\Audit\Enum\Events;
use App\Models\User;

trait AuditableTrait
{
	/**
	 * @param string $event
	 * @param User|null $user
	 * @param int $control_post_id
	 * @return bool
	 */
	public function newAuditEvent($event, $user = null, $control_post_id = null)
	{
		$lengthLimit = 190;

		if (!Events::isValidValue($event)) {
			return false;
		}
		if (!$user) {
			$user = UserResolver::resolve();
		}
		$userAgent = UserAgentResolver::resolve();
		$agentLength = strlen($userAgent);
		if($agentLength >= $lengthLimit) {
			$userAgent = substr($userAgent, 0, $lengthLimit);
		}
		$record = [
			'user_id' => $user ? $user->getAuthIdentifier() : null,
			'auditable_id' => $this->getKey(),
			'auditable_type' => $this->getMorphClass(),
			'auditable_type_key' => Audit::getAuditObjectKey($this->getMorphClass()),
			'event_key' => Events::getEventKey($event),
			'ip_address' => IpAddressResolver::resolve(),
			'user_agent' => $userAgent,
			'url' => UrlResolver::resolve(),
			'control_post_id' => $control_post_id
		];
		$audit = Audit::createWithDetails($record);

		return !empty($audit->id);
	}
}
