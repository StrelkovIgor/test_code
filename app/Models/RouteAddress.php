<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 08 Jan 2019 12:23:16 +0300.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class RouteAddress
 * 
 * @property int $id
 * @property string $title
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class RouteAddress extends Eloquent
{
	protected $fillable = [
		'title'
	];

	/**
	 * Save route addresses
	 * @param $points
	 */
	public static function addNewAddresses ($points) {
		if (!empty($points)) {
			foreach($points as $point) {
				$title = $point['text'] ?? null;
				if(!empty($title)) {
					$address = self::where('title', $title)->first();
					if(!$address){
						$address = new RouteAddress(['title' => $title]);
						$success = $address->save();
					}
				}
			}
		}
	}
}
