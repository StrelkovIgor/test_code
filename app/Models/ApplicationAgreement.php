<?php

namespace App\Models;

use App\Models\Audit\Audit;
use App\Models\Audit\AuditableTrait;
use App\Models\Audit\Enum\Events;
use App\Models\Interfaces\ObjectInfoInterface;
use DateTime;
use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class ApplicationAgreement
 * 
 * @property int $id
 * @property int $application_id
 * @property int $department_id
 * @property int $status
 * @property string $comment
 * @property int $user_id
 * @property string $accept_date
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\Application $application
 * @property \App\Models\Department $department
 * @property \App\Models\User $user
 * @property  \App\Models\Audit\Audit[] $audits
 * @property  \App\Models\Audit\Audit[] $audits_agreements
 * @property  \App\Models\Audit\Audit[] $audits_active
 *
 * @package App\Models
 */
class ApplicationAgreement extends Eloquent implements ObjectInfoInterface
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	use AuditableTrait;

	const STATUS_NEW = 0,
		STATUS_REVIEW = 1,
		STATUS_DECLINED = 2,
		STATUS_ACCEPTED = 4;
	
	protected $casts = [
		'application_id' => 'int',
		'department_id' => 'int',
		'status' => 'int',
		'user_id' => 'int'
	];

	protected $fillable = [
		'application_id',
		'department_id',
		'status',
		'comment',
		'user_id',
		'accepted_date'
	];
	protected $myDateFormat = 'd.m.Y';
	
	public function application()
	{
		return $this->belongsTo(\App\Models\Application::class)->withTrashed();
	}

	public function department()
	{
		return $this->belongsTo(\App\Models\Department::class);
	}

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class);
	}

	public function audits()
	{
		return $this
			->hasMany(\App\Models\Audit\Audit::class, 'auditable_id')
			->where('auditable_type_key', Audit::KEY_APPLICATION_AGREEMENT);
	}

	public function audits_active()
	{
		return $this->audits()
			->whereIn('event_key', Events::getAgreementEvents());
	}

	public function audits_agreements()
	{
		return $this->audits()
			->whereIn('event_key', Events::getAgreementEvents());
	}

	public function getDepartmentTitle()
	{
		return $this->department ? $this->department->title : null;
	}

	public function getApplicationNumber()
	{
		return $this->application ? $this->application->getFormattedId() : null;
	}

	/**
	 * Create new application agreement
	 * @param $applicationId
	 * @param $departmentId
	 * @return ApplicationAgreement
	 */
	public static function create($applicationId, $departmentId)
	{
		$existingItem = self
			::where('application_id', $applicationId)
			->where('department_id', $departmentId)
			->first();
		
		if(!$existingItem){
			$existingItem = new ApplicationAgreement();
			$existingItem->application_id = $applicationId;
			$existingItem->department_id = $departmentId;
			$existingItem->status = self::STATUS_NEW;
			$existingItem->save();
		}
		
		return $existingItem;
	}
	
	/**
	 * @param int $applicationId
	 */
	public static function resetForApp($applicationId)
	{
		ApplicationAgreement::where('application_id', $applicationId)->delete();
	}
	
	/**
	 * @param User $user
	 * @param string $comment
	 * @return bool
	 */
	public function decline($user, $comment)
	{
		$this->status = ApplicationAgreement::STATUS_DECLINED;
		$this->user_id = $user->id;
		$this->accept_date = date('Y-m-d H:i:s');
		$this->comment = $comment;
		$success = $this->save();
		
		self::where('application_id', $this->application_id)
			->update([
				'status' => self::STATUS_DECLINED
			]);
		
		return $success;
	}
	
	/**
	 * Accept application agreement
	 * @param User $user
	 * @return bool
	 */
	public function accept($user)
	{
		$this->status = ApplicationAgreement::STATUS_ACCEPTED;
		$this->user_id = $user->id;
		$this->accept_date = date('Y-m-d H:i:s');
		return $this->save();
	}
	
	/**
	 * @return null|string
	 */
	public function acceptDateFormatted()
	{
		$dateObj = DateTime::createFromFormat('Y-m-d', $this->accept_date);
		return $dateObj  ? $dateObj ->format($this->myDateFormat) : null;
	}


	public function getObjectInfo()
	{
		return $this->getDepartmentTitle() . ' / ' . $this->getApplicationNumber();
	}

	/**
	 * Move agreement to work, bind to user
	 * @param User $user
	 * @return bool
	 */
	public function toWork($user)
	{
		$success = false;

		if (
			$this->status !== static::STATUS_NEW &&
			$this->user_id &&
			$user->id !== $this->user_id
		) {
			$success = false;
		} else {
			$this->status = ApplicationAgreement::STATUS_REVIEW;
			$this->user_id = $user->id;
			$success = $this->save();
		}

		if ($success) {
			if ($user->role_id === Role::ROLE_DEPARTMENT_AGENT) {
				$this->newAuditEvent(Events::DEPAGENT_APP_TOWORK);
			}
		}

		return $success;
	}
}
