<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 07 Jul 2018 12:58:11 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class VehicleAxle
 * 
 * @property int $id
 * @property int $vehicle_id
 * @property int $num
 * @property int $wheel_count
 * @property int $wheels
 * @property int $type_id
 * @property int $is_lifting
 * @property float $distance
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property Vehicle $vehicle
 *
 * @package App\Models
 */
class VehicleAxle extends Eloquent
{
	protected $casts = [
		'vehicle_id' => 'int',
		'num' => 'int',
		'wheel_count' => 'int',
		'wheels' => 'int',
		'type_id' => 'int',
		'is_lifting' => 'int',
		'distance' => 'float'
	];

	protected $fillable = [
		'vehicle_id',
		'num',
		'wheel_count',
		'wheels',
		'type_id',
		'is_lifting',
		'distance'
	];

	public function vehicle()
	{
		return $this->belongsTo(Vehicle::class);
	}
	
	/**
	 * @param Vehicle $vehicle
	 * @param array $data
	 */
	public  static function createByVehicle($vehicle, $data)
	{
		//remove old axles
		VehicleAxle::where('vehicle_id', $vehicle->id)->delete();
		
		//create axles info
		if($vehicle){
			for ($i = 0; $i < $vehicle->axle_count; $i++) {
				$distanceStr = $data['axle_distances'][$i] ?? 0;
				$distance = floatval($distanceStr);
				$axleData = [
					'vehicle_id' => $vehicle->id,
					'num' => $i + 1,
					'wheel_count' => $data['axle_length'][$i] ?? 0,
					'wheels' => $data['axle_wheels'][$i] ?? 0,
					'type_id' => $data['axle_type'][$i] ?? 0,
					'is_lifting' => $data['axle_main'][$i] ?? 0,
					'distance' => $distance,
				];
				$vehicleAxle = self::create($axleData);
			}
		}
	}
}
