<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 06 Jun 2018 09:56:25 +0000.
 */

namespace App\Models;

use App\Mail\UserCreate;
use App\Models\Interfaces\ObjectInfoInterface;
use DateTime;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Mail;
use App\Models\Audit\AuditableTrait;
use App\Models\Audit\Enum\Events;
use App\Observers\UserObserver;

/**
 * Class User
 *
 * @property int $id
 * @property string $name
 * @property string $phone
 * @property int $role_id
 * @property string $email
 * @property string $inn
 * @property string $password
 * @property string $remember_token
 * @property string $deleted_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $api_token
 * @property bool $email_confirmed
 * @property int $confirmation_code
 * @property int $confirmation_status
 * @property int $locked
 * @property int $owner_id
 * @property int $department_id
 * @property int $control_post_id
 * @property int $smev_enabled
 * @property string $position
 *
 * @property Role $role
 * @property Firm $firm
 * @property Individual $individual
 * @property RegApp $regApp
 * @property User[] $users
 * @property User $owner
 * @property Department $department
 *
 * @package App\Models
 */
class User extends Authenticatable implements ObjectInfoInterface
{
    use Notifiable;
    use SoftDeletes;
	use AuditableTrait;

    const CONFIRM_STATUS_NEW = 0,
        CONFIRM_STATUS_DECLINE = 1,
        CONFIRM_STATUS_REVIEW = 2,
        CONFIRM_STATUS_ACCEPT = 3;

    const LOCKED_NO = 0,
        LOCKED_YES = 1;

    protected $myDateFormat = 'Y-m-d H:i:s';

    protected static $adminUserSearchRoles = [
        Role::ROLE_ADMIN,
        Role::ROLE_OFFICER,
        Role::ROLE_DEPARTMENT_AGENT,
        Role::ROLE_WEIGHT_CONTROL
    ];
    
    protected $casts = [
        'role_id' => 'int',
		'department_id' => 'int',
		'control_post_id' => 'int',
        'email_confirmed' => 'int',
		'locked' => 'int',
		'smev_enabled' => 'int',
        'owner_id' => 'int'
    ];

    protected $hidden = [
        'password',
        'remember_token',
        'api_token'
    ];

    protected $fillable = [
        'name',
        'phone',
        'role_id',
        'email',
		'inn',
        'password',
        'owner_id',
        'department_id',
		'email_confirmed',
		'confirmation_code',
		'control_post_id',
		'smev_enabled',
		'position'
    ];
	
		public static function boot()
    {
        self::observe(new UserObserver); 
        parent::boot();
    }

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
    public function role()
    {
        return $this->belongsTo(Role::class);
    }
	
	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
    public function owner()
    {
        return $this->belongsTo(User::class, 'owner_id');
    }
	
	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
    public function department()
    {
        return $this->belongsTo(Department::class, 'department_id');
    }
	
	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function control_post()
	{
		return $this->belongsTo(ControlPost::class, 'control_post_id');
	}
	
	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasOne
	 */
    public function firm()
    {
        return $this->hasOne(Firm::class);
    }
	
	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasOne
	 */
    public function individual()
    {
        return $this->hasOne(Individual::class);
    }
	
	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasOne
	 */
    public function regApp()
    {
        return $this->hasOne(RegApp::class);
    }

    /**
     * The roles that belong to the user.
     */
    public function users()
    {
        return $this->belongsToMany(
            Vehicle::class,
            'user_to_vehicle',
            'user_id',
            'vehicle_id'
        );
    }

	public function files()
	{
		return $this->hasMany(\App\Models\File::class)
			->where('type_id', File::TYPE_FIRM_INN);
	}

    /**
     * Generate new api token
	 * @param bool $force
     * @return string
     */
    public function generateToken($force = false)
    {
        if(!$this->api_token || $force){
			$this->api_token = str_random(60);
			$this->save();
		}

        return $this->api_token;
    }

    /**
     * @return boolean
     */
    public function isLocked()
    {
        return $this->locked;
    }

    /**
     * @return bool
     */
    public function isConfirmed()
    {
        return $this->confirmation_status === self::CONFIRM_STATUS_ACCEPT;
    }

    /**
     * @return null|string
     */
    public function updatedAtFormatted()
    {
        return $this->updated_at ? $this->updated_at->format($this->myDateFormat) : null;
    }
	
	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
    public function applications()
    {
        return $this->hasMany(\App\Models\Application::class);
    }

    public function vehicles()
    {
        return $this->hasMany(\App\Models\Vehicle::class);
    }

	/**
	 * @return string
	 */
	public function getName(){
		return $this->name ?? '';
	}

	/**
	 * @return string
	 */
	public function getNameWithOwner() {
		if($this->role_id === Role::ROLE_FIRM_USER && $this->owner) {
			return $this->owner->getName();
		}
		return $this->getName();
	}

	/**
	 * @return string
	 */
    public function getInn(){
    	return $this->inn ?? '';
	}
	
	/**
	 * @return string
	 */
	public function getAddress(){
    	return $this->role_id === Role::ROLE_FIRM ?
			$this->firm->address :
			'';
	}

	public function getPosition() {
	    return $this->position;
    }
	
	public function getAvailableVehiclesIds()
	{
		return UserToVehicle::where('user_id', $this->id)->pluck('vehicle_id')->toArray();
	}
	
	public static function statusQuery($status, $role)
	{
		return User::where([
			['confirmation_status', '=', $status],
			['role_id', '=', $role],
		]);
	}
    
    /**
     * @param $query
     * @param array $data
     * @return mixed
     */
    public static function ownerDataFilter($query, $data)
    {
        if ($name = array_get($data, 'name')) {
            $query->where('name', 'like', '%' . $name . '%');
        }
        
        if ($email = array_get($data, 'email')) {
            $query->where('email', 'like', '%' . $email . '%');
        }
        
        return $query;
    }
    
    /**
     * Create firm user by owner
     * @param User $owner
     * @param array $data
     * @return User|null
     */
    public static function createFirmUser($owner, $data)
    {
        $password = str_random(8);
        $data['password'] = bcrypt($password);
        $data['role_id'] = Role::ROLE_FIRM_USER;
        $data['owner_id'] = $owner->id;
    
        if ($user = User::create($data)) {
            //send email
            Mail::to($user->email)->send(new UserCreate($user, $password));
        }
    
        return $user;
    }
    
	/**
	 * @param $query
	 * @param array $data
	 * @param int $role
	 * @return mixed
	 */
	public static function adminDataFilter($query, $data, $role)
	{
		if ($name = array_get($data, 'name')) {
			$query->where('name', 'like', '%' . $name . '%');
		}
		
		if ($email = array_get($data, 'email')) {
			$query->where('email', 'like', '%' . $email . '%');
		}
		
		if ($phone = array_get($data, 'phone')) {
			$query->where('phone', 'like', '%' . $phone . '%');
		}

		if ($inn = array_get($data, 'inn')) {
			$query->where('inn', 'like', '%' . $inn . '%');
		}
		
		if ($dateStr = array_get($data, 'date')) {
			$dateStart = (new DateTime($dateStr))->format('Y-m-d');
			$dateFinish =(new DateTime($dateStr))->modify('+1 day')->format('Y-m-d');
			$query->where('updated_at', '>=', $dateStart);
			$query->where('updated_at', '<', $dateFinish);
		}
		
		if($role === Role::ROLE_FIRM){
			if($contact_name = array_get($data, 'contact_name')){
				$query->leftjoin('firms', 'users.id', '=', 'firms.user_id');
				$query->where('firms.contact_fio', 'like', '%' . $contact_name . '%');
			}
		}
		
		return $query;
	}
	
	/**
	 * @param array $data
	 * @return User
	 */
	public static function createByRegData($data){
		$role_id = array_get($data, 'role_id');
		$user = self::create([
			'name' => array_get($data, 'name'),
			'phone' => array_get($data, 'phone'),
			'email' => array_get($data, 'email'),
			'password' => bcrypt(array_get($data, 'password')),
			'role_id' => array_get($data, 'role_id'),
			'confirmation_status' => User::CONFIRM_STATUS_NEW,
			'inn' => $role_id == Role::ROLE_INDIVIDUAL ? array_get($data, 'inn') : array_get($data, 'inn_org'),
			'confirmation_code' => str_random(60)
		]);
		
		if($user->role_id === Role::ROLE_INDIVIDUAL){
			Individual::create([
				'user_id' => $user->id,
				'inn' => array_get($data, 'inn')
			]);
		}
		
		if($user->role_id === Role::ROLE_FIRM){
			//create firm info
			$firm = Firm::create([
				'user_id' => $user->id,
				'inn_org' => array_get($data, 'inn_org'),
				'legal_form_id' => array_get($data, 'legal_form_id'),
				'address' => array_get($data, 'address'),
				
				'executive_fio' => array_get($data, 'executive_fio'),
				'executive_position' => array_get($data, 'executive_position'),
				
				'reason_key' => array_get($data, 'reason'),
				'contact_phone' => array_get($data, 'phone'),
				'phone_static' => array_get($data, 'phone_static'),
				'contact_fio' => array_get($data, 'contact_fio'),
			]);
			
			//create firm payment details
			if($firm){
				$firmPaymentDetails = FirmPaymentDetail::create([
					'firm_id' => $firm->id,
					'bank_name' => array_get($data, 'bank_name'),
					'bank_inn' => array_get($data, 'bank_inn'),
					'correspondent_account' => array_get($data, 'correspondent_account'),
					'bank_bik' => array_get($data, 'bank_bik'),
					'account' => array_get($data, 'account'),
				]);
			}
		}
		
		return $user;
	}
	
	/**
	 * Query for users filtering
	 * @param array $data
	 * @return mixed
	 */
	public static function adminUserFilterQuery($data)
	{
		$role_id = array_get($data, 'role_id', Role::ROLE_ADMIN);
		$role_id = self::mapAdminUserSearchRoleId($role_id);
		$department_id = array_get($data, 'department_id');
		
		$query = self::select([
			'id', 'name', 'role_id', 'email', 'smev_enabled', 'position'
		]);
		
		if($role_id){
		    if(is_array($role_id)) {
		        $query->whereIn('role_id', $role_id);
            }else {
                $query->where('role_id', $role_id);
            }
		}
		
		if($department_id){
			$query->where('department_id', $department_id);
		}
        
        if ($name = array_get($data, 'name')) {
            $query->where('name', 'like', '%' . $name . '%');
        }
        
        if ($email = array_get($data, 'email')) {
            $query->where('email', 'like', '%' . $email . '%');
        }
		
		return $query;
	}
    
    /**
     * Allow to search only this roles
     * @param int $role_id
     * @return int
     */
	protected static function mapAdminUserSearchRoleId($role_id) {
	    if(array_search($role_id, self::$adminUserSearchRoles) === false) {
	        $role_id = self::$adminUserSearchRoles;
        }
        
        return $role_id;
    }
	
	/**
	 * @param int $userId
	 * @return mixed
	 */
	public static function firmUserQuery($userId)
	{
		$vehicle_ids = Vehicle::where('user_id', $userId)->pluck('id')->toArray();
		$usersIds = UserToVehicle::whereIn('vehicle_id', $vehicle_ids)->pluck('user_id')->toArray();
		
		return User::whereIn('id', $usersIds);
	}


	/**
	 * Update user info in cabinet
	 * @param $data
	 */
	public function cabinetUpdate($data)
	{
		if($name = array_get($data, 'name')){
			$this->name = $name;
		}
		if($phone = array_get($data, 'phone')){
			$this->phone = $phone;
		}
		if($email = array_get($data, 'email')){
			$this->email = $email;
		}
		if($inn = array_get($data, 'inn')){
			$this->inn = $inn;
		}
		//change status
		if ($this->confirmation_status !== User::CONFIRM_STATUS_NEW) {
			$this->confirmation_status = User::CONFIRM_STATUS_REVIEW;
		}
		$this->save();
		
		if ($this->role_id === Role::ROLE_INDIVIDUAL) {
			if($this->individual){
				$this->individual->updateCabinet($data);
			}else{
				Individual::createByData($this->id, $data);
				$this->load('individual');
			}
		}
		
		if ($this->role_id === Role::ROLE_FIRM) {
			if($this->firm){
				$this->firm->updateCabinet($data);
			}else{
				Firm::createByData($this->id, $data);
				$this->load('firm');
			}

		}
	}
	
	/**
	 * Accept user
	 * @param User $admin
	 * @return bool
	 */
	public function accept($admin){
		$this->confirmation_status = User::CONFIRM_STATUS_ACCEPT;
		
		if (RegApp::accept($this->id, $admin) && $this->save()) {
			if (Role::isAdminOrGbu()) {
				$this->newAuditEvent(Events::ADMINGBU_USERREG_ACCEPT);
			}
			return true;
		}
		return false;
	}
	
	/**
	 * @param User $admin
	 * @param string $note
	 * @return bool
	 */
	public function decline($admin, $note){
		$this->confirmation_status = User::CONFIRM_STATUS_DECLINE;
		
		if (RegApp::decline($this->id, $admin, $note) && $this->save()) {
			if (Role::isAdminOrGbu()) {
				$this->newAuditEvent(Events::ADMINGBU_USERREG_DECLINE);
			}
			return true;
		}
		return false;
	}
	
	/**
	 * Lock user
	 * @return bool
	 */
	public function lock(){
		$this->locked = User::LOCKED_YES;
		$this->generateToken(true);
		if ($this->save()) {
			if (Role::isAdminOrGbu()) {
				$this->newAuditEvent(Events::ADMINGBU_USER_LOCK);
			}
			return true;
		}
		return false;
	}
	
	/**
	 * Unlock user
	 * @return bool
	 */
	public function unlock(){
		$this->locked = User::LOCKED_NO;
		$this->generateToken(true);
		if ($this->save()) {
			if (Role::isAdminOrGbu()) {
				$this->newAuditEvent(Events::ADMINGBU_USER_UNLOCK);
			}
			return true;
		}
		return false;
	}

	public function delete()
	{
		if (parent::delete() && Role::isAdmin()) {
			$this->newAuditEvent(Events::ADMIN_USER_SOFT_DELETE);
			return true;
		}
		return false;
	}

    public function update(array $attributes = [], array $options = [])
	{
		if (parent::update($attributes, $options) && Role::isAdmin()) {
			$this->newAuditEvent(Events::ADMIN_USER_EDIT);
			return true;
		}
		return false;
	}

	public function getObjectInfo()
	{
		return Role::getNameById($this->role_id) . ' / ' .
			$this->getName() . ' / ' .
			$this->email;
	}

	/**
	 * @return int
	 */
	public function isSmevEnabled()
	{
		return $this->smev_enabled;
	}
}
