<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 06 Jun 2018 09:46:43 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Individual
 * 
 * @property int $id
 * @property int $user_id
 * @property string $inn
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property User $user
 *
 * @package App\Models
 */
class Individual extends Eloquent
{
	protected $casts = [
		'user_id' => 'int'
	];

	protected $fillable = [
		'user_id',
		'inn'
	];

	public function user()
	{
		return $this->belongsTo(User::class);
	}

    /**
     * @return null|string
     */
    public function updatedAtFormatted(){
        return $this->updated_at ? $this->updated_at->format($this->dateFormat) : null;
    }
	
	/**
	 * Update user's info in cabinet
	 * @param array $data
	 */
    public function updateCabinet($data)
	{
		if($inn = array_get($data, 'inn')){
			$this->inn = $inn;
		}
		$this->save();
		
		$filesData = array_get($data, 'inn_files');
		$this->updateFiles($filesData, File::TYPE_FIRM_INN);
	}

	public static function createByData($userId, $data)
	{
		$cabinet = new static([
			'user_id' => $userId
		]);
		if($inn = array_get($data, 'inn')){
			$cabinet->inn = $inn;
		}
		return $cabinet->save() ? $cabinet : null;
	}
	
	/**
	 * Update user's inn files
	 * @param array $filesData
	 * @param int $fileType
	 */
	public function updateFiles($filesData, $fileType){
    	//@todo don't delete files, which wasn't changed
		
		//new file ids
		$ids = [];
		if (!empty($filesData)) {
			foreach ($filesData as $fileData) {
				$ids[] = $fileData['id'];
			}
		}
		
		//remove all files
		File::where('user_id', $this->id)
			->where('type_id', $fileType)
			->whereNotIn('id', $ids)
			->delete();
		
		if (!empty($ids)) {
			foreach ($ids as $id) {
				if (intval($id)) {
					$file = File::find($id);
					if ($file) {
						$file->user_id = $this->id;
						$file->save();
					}
				}
			}
		}
	}
}
