<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 11 Jul 2018 21:21:07 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class VehicleModel
 * 
 * @property int $id
 * @property string $title
 * @property int $accepted
 * @property int $brand_id
 * @property int $hidden
 * @property int $is_trailer
 * @property int $vehicle_type_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property int $length
 * @property int $width
 * @property int $height
 * @property int $axle_count
 * @property string $axles_wheel_count
 * @property string $axles_distance
 * 
 * @property \App\Models\VehicleBrand $vehicle_brand
 * @property \Illuminate\Database\Eloquent\Collection $vehicles
 * @property VehicleAxle[] $vehicle_axles
 *
 * @package App\Models
 */
class VehicleModel extends Eloquent
{
	const STATUS_ACCEPTED_NO = 0,
		STATUS_ACCEPTED_YES = 1,
		STATUS_ACCEPTED_HIDE = 2;

	use \Illuminate\Database\Eloquent\SoftDeletes;

    protected $casts = [
		'brand_id' => 'int',
		'hidden' => 'int',
		'is_trailer' => 'int',
		'vehicle_type_id' => 'int',
		'axle_count' => 'int'
	];

	protected $fillable = [
		'title',
		'brand_id',
		'hidden',
		'is_trailer',
		'vehicle_type_id',
		'length',
		'width',
		'height',
		'axle_count',
		'axles_wheel_count',
		'axles_distance',
		'accepted'
	];

	protected static $adminSortColumnMap = [
	    'brand' => 'vehicle_brands.title',
	    'brand_title' => 'vehicle_brands.title',
     
	    'model' => 'vehicle_models.title',
	    'model_title' => 'vehicle_models.title',
        
        'is_trailer' => 'vehicle_models.is_trailer',
        
        'id' => 'vehicle_models.id'
    ];
	
	public function vehicle_brand()
	{
		return $this->belongsTo(VehicleBrand::class, 'brand_id');
	}

	public function vehicles()
	{
		return $this->hasMany(Vehicle::class, 'model_id');
	}

	public function vehicle_axles()
	{
		return $this->hasMany(VehicleAxle::class, 'vehicle_id');
	}

	/**
	 * Create model by user
	 * @param array $data
	 * @param User $user
	 * @param int $brandId
	 *
	 * @return static|null
	 */
	public static function createByUser($data, $user, $brandId)
	{
		$wheelCounts = [];
		$wheelDistances = [];
		
		$axle_count = intval($data['axle_count']);
		$dataWheelCounts = array_get($data, 'axle_length', []);
		$dataAxleDistances = array_get($data, 'axle_distances', []);
		
		for($i = 0; $i < $axle_count; $i++){
			$wheelCounts[] = $dataWheelCounts[$i] ?? 0;
			if($i < $axle_count -1){
				$wheelDistances[] = $dataAxleDistances[$i] ?? 0;
			}
		}
		
		$vehicleModel = new VehicleModel([
			'accepted' => 0,
			'title' => array_get($data, 'model_title'),
			'brand_id' => $brandId,
			'hidden' => 0,
			'is_trailer' => array_get($data, 'is_trailer'),
			'vehicle_type_id' => array_get($data, 'vehicle_type_id'),
			'width' => array_get($data, 'width'),
			'height' => array_get($data, 'height'),
			'length' => array_get($data, 'length'),
			'axle_count' => $axle_count,
			'axles_wheel_count' => implode(',', $wheelCounts),
			'axles_distance' => implode(',', $wheelDistances),
		]);
		
		return $vehicleModel->save() ? $vehicleModel : null;
	}
    
    /**
     * @param $query
     * @param array $data
     * @return mixed
     */
	public static function adminDataFilter($query, $data)
    {
        $sortColumn = array_get($data, 'sort_column', 'vehicle_models.id');
        $sortColumn = self::mapSortColumnForAdminVehicleModels($sortColumn);
        
        $sortType = array_get($data, 'sort_type', 'desc');
        
        $is_trailer = array_get($data, 'is_trailer');
        if ($is_trailer !== null) {
            $query->where('vehicle_models.is_trailer', $is_trailer);
        }
    
    
        $brand_title = array_get($data, 'brand_title');
        //check if we need join vehicle_brands
        if ($brand_title || $sortColumn === 'vehicle_brands.title') {
            $query->leftjoin('vehicle_brands', 'vehicle_brands.id', '=', 'vehicle_models.brand_id');
            if($brand_title) {
                $query->where('vehicle_brands.title', 'like', '%' . $brand_title . '%');
            }
        }
    
        if ($model_title = array_get($data, 'model_title')) {
            $query->where('vehicle_models.title', 'like', '%' . $model_title . '%');
        }
    
        $query->orderBy($sortColumn, $sortType);
        
        return $query;
    }
    
    /**
     * Map sort column
     * @param string $sortColumn
     * @return mixed
     */
    protected static function mapSortColumnForAdminVehicleModels($sortColumn) {
	    return isset(self::$adminSortColumnMap[$sortColumn]) ? self::$adminSortColumnMap[$sortColumn] : 'vehicle_models.id';
    }
}
