<?php

namespace App\Models;

use App\Helpers\GradoserviceDistanceHelper;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

/**
 * Class RouteList
 * @package App\Models
 */
class RouteList extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'lat_lon',
        'points',
        'legs'
    ];

    protected static $dayUpdate = 30;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function RouteListsPoints(){
        return $this->hasMany(RouteListsPoint::class);
    }

    /**
     * @param string $name
     * @param $routeInfo
     * @param $points
     * @return RouteList|void
     */
    public static function checkPointsAndSave($name = '', $routeInfo, $points){

        RouteListsPoint::search($points);
        if(!RouteListsPoint::isCheck($points)) return;

        $t = new self();
        $t->name = $name;
        $t->legs = \GuzzleHttp\json_encode(array_get($routeInfo, 'legs', []));
        $t->distance = array_get($routeInfo, 'distance', 0);
        $t->distance_info = \GuzzleHttp\json_encode(array_get($routeInfo, 'distance_info', []));
        $t->save();

        foreach ($points as $coord)
            $t->RouteListsPoints()->save(new RouteListsPoint($coord));

        return $t;
    }

    /**
     * @param $info
     * @return array
     */
    public static function sumInfo($info){
        return [
          ApplicationRoute::ROAD_TYPE_REGIONAL => array_sum(array_column($info,ApplicationRoute::ROAD_TYPE_REGIONAL)),
          ApplicationRoute::ROAD_TYPE_FEDERAL => array_sum(array_column($info,ApplicationRoute::ROAD_TYPE_FEDERAL)),
          ApplicationRoute::ROAD_TYPE_LOCAL => array_sum(array_column($info,ApplicationRoute::ROAD_TYPE_LOCAL))
        ];
    }

    /**
     * @param $id_list
     */
    public static function getListUpdate($id_list){
        $routeList = self::find($id_list);

        if(!$routeList) return null;

        if($routeList->updated_at->diffInDays() > self::$dayUpdate){

            $coords = [];
            foreach ($routeList->RouteListsPoints as $list){
                $coords[] = ['lon' => $list->lon, 'lat' => $list->lat];
            }

            $distanceHelper = new GradoserviceDistanceHelper();
            $routeInfo = $distanceHelper->getRouteInfo($coords);

            $routeList->updated_at = Carbon::now();
            $routeList->legs = \GuzzleHttp\json_encode(array_get($routeInfo, 'legs', []));
            $routeList->distance = array_get($routeInfo, 'distance', 0);
            $routeList->distance_info =  \GuzzleHttp\json_encode(array_get($routeInfo, 'distance_info', []));
            $routeList->save();

        }
        $description = [];
        $RouteListsPoints = $routeList->RouteListsPoints()->orderBy('id')->get();
        foreach($RouteListsPoints as $RouteListsPoint) array_push($description, $RouteListsPoint->name);

        return ['legs' => \GuzzleHttp\json_decode($routeList->legs, true), 'distance' => $routeList->distance, 'distance_info' => \GuzzleHttp\json_decode($routeList->distance_info, true),'description' => $description];

    }

}
