<?php
/**
 * Created by PhpStorm.
 * User: gserg13
 * Date: 27.07.2019
 * Time: 18:31
 */

namespace App\Models\Interfaces;


interface ObjectInfoInterface
{
	public function getObjectInfo();
}