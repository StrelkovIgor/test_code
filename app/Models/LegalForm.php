<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 06 Jun 2018 09:53:04 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class LegalForm
 * 
 * @property int $id
 * @property string $title
 * @property string $short_title
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $firms
 *
 * @package App\Models
 */
class LegalForm extends Eloquent
{
	protected $fillable = [
		'title', 'short_title'
	];

	public function firms()
	{
		return $this->hasMany(Firm::class);
	}
}
