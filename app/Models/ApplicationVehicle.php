<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 17 Aug 2019 22:26:27 +0300.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class ApplicationVehicle
 * 
 * @property int $id
 * @property int $application_id
 * @property int $is_trailer
 * @property string $brand_title
 * @property string $model_title
 * @property int $vehicle_type_id
 * @property int $is_owner
 * @property string $owner_name
 * @property string $real_number
 * @property string $pts_number
 * @property string $sts_number
 * @property float $pts_weight
 * @property float $pts_max_weight
 * @property float $height
 * @property float $length
 * @property float $width
 * @property int $axle_count
 * @property string $axles
 * @property string $files
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\Application $application
 *
 * @package App\Models
 */
class ApplicationVehicle extends Eloquent
{
	protected $casts = [
		'application_id' => 'int',
		'is_trailer' => 'int',
		'vehicle_type_id' => 'int',
		'is_owner' => 'int',
		'pts_weight' => 'float',
		'pts_max_weight' => 'float',
		'height' => 'float',
		'length' => 'float',
		'width' => 'float',
		'axle_count' => 'int',
		'axles' => 'array',
		'files' => 'array'
	];

	protected $fillable = [
		'application_id',
		'is_trailer',
		'brand_title',
		'model_title',
		'vehicle_type_id',
		'is_owner',
		'owner_name',
		'real_number',
		'pts_number',
		'sts_number',
		'pts_weight',
		'pts_max_weight',
		'height',
		'length',
		'width',
		'axle_count',
		'axles',
		'files'
	];

	public function application()
	{
		return $this->belongsTo(\App\Models\Application::class);
	}

	public function privilege()
	{
		$date = date('Y-m-d');
		return $this->belongsTo(PrivilegeVehicle::class, 'real_number', 'real_number')
			->leftJoin('privilege_statuses', 'privilege_vehicles.privilege_status_id', '=', 'privilege_statuses.id')
			->where('privilege_statuses.start_date', '<=', $date)
			->where('privilege_statuses.finish_date', '>=', $date);
	}

	/**
	 * Create Application Vehicle with frozen data
	 * @param Application $application
	 * @param Vehicle $vehicle
	 */
	public static function createByVehicle($application, $vehicle) {
		$filesData = [];
		if(!empty($vehicle->files)) {
			foreach($vehicle->files as $file) {
				$filesData[] = [
					'id' => $file->id,
					'name' => $file->name,
					'source' => $file->source,
					'url' => $file->url,
				];
			}
		}
		$data = [
			'application_id' => $application->id,
			'is_trailer' => $vehicle->is_trailer,
			'brand_title' => $vehicle->getBrandTitle(),
			'model_title' => $vehicle->getModelTitle(),
			'vehicle_type_id' => $vehicle->vehicle_type_id,
			'is_owner' => intval($vehicle->is_owner),
			'owner_name' => $vehicle->owner_name,
			'real_number' => $vehicle->real_number,
			'pts_number' => $vehicle->pts_number,
			'sts_number' => $vehicle->sts_number,
			'pts_weight' => $vehicle->pts_weight,
			'pts_max_weight' => $vehicle->pts_max_weight,
			'height' => $vehicle->height,
			'length' => $vehicle->length,
			'width' => $vehicle->width,
			'axle_count' => $vehicle->axle_count,
			'axles' => $vehicle->getAxlesInfo(),
			'files' => !empty($filesData) ? $filesData : null
		];

		static::create($data);
	}

	/**
	 * @return string
	 */
	public function getFullInfo()
	{
		return $this->brand_title . ' ' . $this->model_title . ' ' . $this->real_number;
	}
}
