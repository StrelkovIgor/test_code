<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class ApplicationSmev
 * 
 * @property int $id
 * @property int $application_id
 * @property string $approval_id
 * @property string $authority_name
 * @property string $output_number
 * @property \Carbon\Carbon $output_date
 * @property string $special_conditions
 * @property string $ecp
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\Application $application
 *
 * @package App\Models
 */
class ApplicationSmev extends Eloquent
{

    const DAY_FOR_DELAY = 5;

	protected $casts = [
		'application_id' => 'int'
	];

	protected $dates = [
		'output_date'
	];

	protected $fillable = [
		'application_id',
		'approval_id',
		'authority_name',
		'output_number',
		'output_date',
        'special_conditions',
        'ecp'
	];
    
    /**
     * @param Application $application
     * @param array $data
     * @return ApplicationRoute
     */
    public static function updateByData($application, $data)
    {
        //get route object or create
        /** @var ApplicationSmev $smev */
        $smev = self::where('application_id', $application->id)->first();
        if (!$smev) {
            $smev = new self(['application_id' => $application->id]);
        }
        
        $applicationData = isset($data['smev']) ? $data['smev'] : [];
        $applicationData['application_id'] = $application['id'];
        
        //update request
        if ($smev->id) {
            $smev->update($applicationData);
        } else {
            $smev = ApplicationSmev::create($applicationData);
        }
        
        return $smev;
    }
	
	public function application()
	{
		return $this->belongsTo(\App\Models\Application::class);
	}

	public function formattedDate($format = 'Y-m-d') {
        return $this->output_date ? $this->output_date->format($format)  :null;
    }

    public function getAnswerDate($format = 'Y-m-d H:i:s') {
        $date = $this->created_at;
        if ($date) {
            $date->addDays(self::DAY_FOR_DELAY);
            return $date->format($format);
        }

        return null;
    }
}
