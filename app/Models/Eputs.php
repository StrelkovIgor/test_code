<?php


namespace App\Models;


use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Cache;

class Eputs
{
    const AUTH_KEY = 'isAuth',
            AUTH_TOKEN = 'auth_token';

    public static $ins = null;

    private $token = null;
    private $data = [];


    public function __construct()
    {
        $data = request()->all();
        $this->token = array_get($data, self::AUTH_TOKEN, null);

        if($this->hasToken()){
            $this->get();
            $this->set();
        }

        return $this;
    }

    public static function i(){
        if(!self::$ins){
            self::$ins = new static();
        }
        return self::$ins;
    }

    public function isToken(){
        return (bool) $this->token;
    }
    public function hasToken(){
        if(!$this->isToken()) return false;
        return Cache::has($this->token);
    }

    private function set(){
        if(!$this->isToken()) return false;
        $s = config('settings.auth.eputs');
        Cache::put($this->token, json_encode($this->data), array_get($s, 'time', 5));
    }
    private function get(){
        if(!$this->isToken()) return false;
        $this->data = json_decode(Cache::get($this->token), true);
    }

    public function loads(){
        if($this->hasToken()) $this->get();
    }

    protected function setToken(){
        $this->token = hash("sha256",time());
        return $this;
    }

    public function setData($key, $value = null){
        if(is_array($key))
            foreach ($key as $k => $v) $this->data[$k] = $v;
        else
            $this->data[$key] = $value;

        return $this;
    }

    public function getData($key = null){
        if($key)
            return $this->data[$key];
        else
            return $this->data;
    }

    public function login($login, $password){
        $users = config('settings.auth.eputs');
        if ($login === $users['login'] && $password === $users['password']) {
            $this->setData(self::AUTH_KEY, true)->setToken()->set();
            return $this->token;
        } else {
            return false;
        }
    }
}