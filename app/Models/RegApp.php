<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 11 Jun 2018 10:25:56 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class RegApp
 *
 * @property int $id
 * @property int $user_id
 * @property int $admin_id
 * @property string $note
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @property User $user
 * @property User $admin
 *
 * @package App\Models
 */
class RegApp extends Eloquent
{
	protected $casts = [
		'user_id' => 'int',
		'admin_id' => 'int'
	];
	
	protected $fillable = [
		'user_id',
		'admin_id',
		'note'
	];
	
	/**
	 * @param int $userId
	 * @param User $admin
	 * @return bool
	 */
	public static function accept($userId, $admin)
	{
		$regApp = self::findByUserOrCreate($userId);
		$regApp->admin_id = $admin->id;
		return $regApp->save();
	}
	
	/**
	 * @param int $userId
	 * @return RegApp
	 */
	public static function findByUserOrCreate($userId)
	{
		$regApp = RegApp::where(['user_id' => $userId])->first();
		if (!$regApp) {
			$regApp = new RegApp(['user_id' => $userId]);
		}
		
		return $regApp;
	}
	
	/**
	 * @param int $userId
	 * @param User $admin
	 * @param string $note
	 * @return bool
	 */
	public static function decline($userId, $admin, $note)
	{
		$regApp = self::findByUserOrCreate($userId);
		
		$regApp->note = $note;
		$regApp->admin_id = $admin->id;
		return $regApp->save();
	}
	
	public function user()
	{
		return $this->belongsTo(User::class);
	}
	
	public function admin()
	{
		return $this->belongsTo(User::class, 'FK_reg_apps_user_id');
	}
}
