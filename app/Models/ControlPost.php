<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 05 Nov 2018 10:09:41 +0000.
 */

namespace App\Models;

use App\Models\Interfaces\ObjectInfoInterface;
use Illuminate\Database\Eloquent\Model as Eloquent;
use App\Models\Audit\AuditableTrait;
use App\Observers\ControlPostObserver;

/**
 * Class ControlPost
 * 
 * @property int $id
 * @property string $title
 * @property string $deleted_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $users
 *
 * @package App\Models
 */
class ControlPost extends Eloquent implements ObjectInfoInterface
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	use AuditableTrait;

	protected $fillable = [
		'title'
	];
		
	public static function boot()
	{
			self::observe(new ControlPostObserver); 
			parent::boot();
	}

	public function users()
	{
		return $this->hasMany(\App\Models\User::class);
	}

	public function getObjectInfo()
	{
		return $this->title;
	}
}
