<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 19 Aug 2018 12:18:50 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class ApplicationTrailer
 * 
 * @property int $id
 * @property int $application_id
 * @property int $vehicle_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\Application $application
 * @property \App\Models\Vehicle $vehicle
 *
 * @package App\Models
 */
class ApplicationTrailer extends Eloquent
{
	protected $casts = [
		'application_id' => 'int',
		'vehicle_id' => 'int'
	];

	protected $fillable = [
		'application_id',
		'vehicle_id'
	];

	public function application()
	{
		return $this->belongsTo(\App\Models\Application::class);
	}

	public function vehicle()
	{
		return $this->belongsTo(\App\Models\Vehicle::class, 'application_id');
	}
	
	/**
	 * Create trailers for app by trailers data
	 * @param Application $application
	 * @param array $data
	 */
	public static function createApplicationBinds($application, $data)
	{
		if ($application && !empty($data)) {
			foreach ($data as $trailer) {
				if ($trailerId = array_get($trailer, 'id')) {
					$bindData = [
						'application_id' => $application->id,
						'vehicle_id' => $trailerId,
					];
					
					ApplicationTrailer::create($bindData);
				}
			}
		}
	}

	/**
	 * Create trailers for app by trailers data
	 * @param Application $application
	 * @param Vehicle[] $trailers
	 */
	public static function createFastApplicationBinds($application, $trailers)
	{
		if ($application && !empty($trailers)) {
			foreach ($trailers as $trailer) {
				$bindData = [
					'application_id' => $application->id,
					'vehicle_id' => $trailer->id,
				];

				ApplicationTrailer::create($bindData);
			}
		}
	}
}
