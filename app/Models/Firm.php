<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 06 Jun 2018 09:51:56 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Firm
 * 
 * @property int $id
 * @property int $user_id
 * @property string $inn_org
 * @property int $legal_form_id
 * @property string $address
 * @property string $executive_fio
 * @property string $executive_position
 * @property int $reason_key
 * @property string $contact_phone
 * @property string $phone_static
 * @property string $contact_fio
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property LegalForm $legal_form
 * @property User $user
 * @property FirmPaymentDetail $firm_payment_details
 * @property File[] files
 *
 * @package App\Models
 */
class Firm extends Eloquent
{
	protected $casts = [
		'user_id' => 'int',
		'legal_form_id' => 'int',
		'reason_key' => 'int',
	];

	protected $fillable = [
		'user_id',
        'inn_org',
		'legal_form_id',
		'address',
		'executive_fio',
		'executive_position',
		'reason_key',
		'contact_phone',
		'contact_fio',
		'phone_static'
	];

	public function legal_form()
	{
		return $this->belongsTo(LegalForm::class);
	}

	public function user()
	{
		return $this->belongsTo(User::class);
	}

	public function firm_payment_details()
	{
		return $this->hasOne(FirmPaymentDetail::class);
	}
	
	/**
	 * Update user's info in cabinet
	 * @param array $data
	 */
	public function updateCabinet($data)
	{
		$this->inn_org = array_get($data, 'inn_org');
		$this->legal_form_id = array_get($data, 'legal_form_id');
		$this->address = array_get($data, 'address');
		
		$this->executive_fio = array_get($data, 'executive_fio');
		$this->executive_position = array_get($data, 'executive_position');
		
		$this->reason_key = array_get($data, 'reason_key');
		$this->contact_phone = array_get($data, 'phone');
		$this->phone_static = array_get($data, 'phone_static');
		$this->contact_fio = array_get($data, 'contact_fio');
		$this->save();

		$this->updateFiles(array_get($data, 'innFiles'), File::TYPE_FIRM_INN);

		if($this->firm_payment_details){
			$this->firm_payment_details->updateCabinet($data);
		}else{
			FirmPaymentDetail::createByData($this->id, $data);
			$this->load('firm_payment_details');
		}
	}

	public static function createByData($userId, $data)
	{
		$firm = new static([
			'user_id' => $userId
		]);

		$firm->inn_org = array_get($data, 'inn_org');
		$firm->legal_form_id = array_get($data, 'legal_form_id');
		$firm->address = array_get($data, 'address');

		$firm->executive_fio = array_get($data, 'executive_fio');
		$firm->executive_position = array_get($data, 'executive_position');

		$firm->reason_key = array_get($data, 'reason_key');
		$firm->contact_phone = array_get($data, 'phone');
		$firm->phone_static = array_get($data, 'phone_static');
		$firm->contact_fio = array_get($data, 'contact_fio');
		$firm->save();

		$firm->updateFiles(array_get($data, 'innFiles'), File::TYPE_FIRM_INN);

		FirmPaymentDetail::createByData($firm->id, $data);
	}
	
	/**
	 * Update user's inn files
	 * @param string $filesData
	 * @param int $fileType
	 */
	public function updateFiles($filesData, $fileType){
		//@todo change to individual inn
		//update inn files
		$ids = explode(',', $filesData);
		
		File::where('user_id', $this->id)
			->where('type_id', File::TYPE_FIRM_INN)
			->whereNotIn('id', $ids)
			->delete();
		
		foreach ($ids as $id) {
			if (intval($id)) {
				$file = File::find($id);
				if ($file) {
					$file->user_id = $this->id;
					$file->save();
				}
			}
		}
	}
}
