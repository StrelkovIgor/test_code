<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 29 Sep 2018 15:38:39 +0000.
 */

namespace App\Models;

use DateTime;
use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class ApplicationDate
 * 
 * @property int $id
 * @property int $application_id
 * @property int $runs_count
 * @property int $runs_used
 * @property bool $is_penalty
 * @property string $penalty_number
 * @property string $penalty_place
 * @property int $penalty_post_id
 * @property string $issue_place_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\Application $application
 * @property \App\Models\IssuePlace $issue_place
 * @property \App\Models\ControlPost $penalty_post
 *
 * @package App\Models
 */
class ApplicationDate extends Eloquent
{
	protected $casts = [
		'application_id' => 'int',
		'runs_count' => 'int',
		'runs_used' => 'int',
		'is_penalty' => 'bool',
		'issue_place_id' => 'int'
	];

	protected $fillable = [
		'application_id',
		'runs_count',
		'runs_used',
		'is_penalty',
		'penalty_number',
		'penalty_place',
		'penalty_post_id',
		'issue_place_id'
	];

	public function application()
	{
		return $this->belongsTo(\App\Models\Application::class);
	}

	public function issue_place()
	{
		return $this->belongsTo(\App\Models\IssuePlace::class);
	}

	public function penalty_post()
	{
		return $this->belongsTo(\App\Models\ControlPost::class);
	}

	public static function updateByData($application, $data)
	{
		//get dates object or create
		/** @var ApplicationDate $dates */
		$dates = ApplicationDate::where('application_id', $application->id)->first();
		if (!$dates) {
			$dates = new ApplicationDate(['application_id' => $application->id]);
		}
		
		$applicationData = $data;
		$applicationData['application_id'] = $application['id'];
		$applicationData['runs_count'] = array_get($data, 'runs_count', 0);
		$applicationData['is_penalty'] = array_get($data, 'is_penalty', 0);
		if ($applicationData['is_penalty']) {
			$applicationData['penalty_number'] = $data['penalty_number'];
			$applicationData['penalty_place'] = $data['penalty_place'];
			$applicationData['penalty_post_id'] = $data['penalty_post_id'];
		}
		
		
		//update request
		if ($dates->id) {
			$dates->update($applicationData);
		} else {
			$dates = ApplicationDate::create($applicationData);
		}
		
		return $dates;
	}
	
	/**
	 * @param ControlMark $controlMark
	 * @return bool was run used or not
	 */
	public function addMark($controlMark){
		if($controlMark->reverse){
			$activeRun = Run::getActive($this->application_id);
			if($activeRun){
				$activeRun->markComplete();
			}
			return false;
		}
		
		$runCreated = false;
		
		//try to get existing run
		$activeRun = Run::getActive($this->application_id);
		if(!$activeRun){
			$activeRun = Run::createNewActive($this->application_id);
			$runCreated = true;
		}
		
		//if active run wasn't created right now
		if(!$runCreated){
			$postMark = ControlMark::getPostMarkByRun($activeRun->id, $controlMark->control_post_id);
			if($postMark){
				$activeRun->markComplete();
				$activeRun = Run::createNewActive($this->application_id);
				$runCreated = true;
			}else{
				$controlMark->run_id = $activeRun->id;
				$controlMark->save();
			}
		}
		
		//count run
		if($runCreated){
			$controlMark->active = 1;
			$controlMark->run_id = $activeRun->id;
			$controlMark->save();
			
			if($this->runs_used < $this->runs_count){
				$this->runs_used++;
			}
			$success = $this->save();
			
			return $success;
		}

		return false;
	}
}
