<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 17 Mar 2019 12:56:42 +0300.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class AxleLoadLimit
 * 
 * @property int $id
 * @property string $title
 * @property bool $is_spring
 * @property int $group_axle_count
 * @property int $wheel_count
 * @property int $wheels
 * @property int $distance_index
 * @property int $axle_type
 * @property float $value0
 * @property float $value1
 * @property float $value2
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class AxleLoad extends Eloquent
{
	protected $table = 'axle_load_limits';

	const DISTANCE_INDEX_LESS_1 = 0,
		DISTANCE_INDEX_1_13 = 1,
		DISTANCE_INDEX_13_18 = 2,
		DISTANCE_INDEX_18_25 = 3,
		DISTANCE_INDEX_MORE_25 = 4;

	const AXLE_TYPE_SPRING = 0,
		AXLE_TYPE_NON_SPRING = 1;//Pneumatic

	const WHEELS_LESS_4 = 1,
		WHEELS_MORE_4 = 5;

	protected $casts = [
		'is_spring' => 'bool',
		'group_axle_count' => 'int',
		'wheel_count' => 'int',
		'distance_index' => 'int',
		'axle_type' => 'int',
		'wheels' => 'int',
		'value0' => 'float',
		'value1' => 'float',
		'value2' => 'float'
	];

	protected $fillable = [
		'title',
		'is_spring',
		'group_axle_count',
		'wheel_count',
		'distance_index',
		'axle_type',
		'wheels',
		'value0',
		'value1',
		'value2'
	];

	/**
	 * @param AxleLoad[] $axleLoads
	 * @param int $axlesCount
	 * @param int $distanceId
	 * @param int $wheelCount
	 * @param int $wheels
	 * @param int $axleType
	 * @param int $isSpring
	 * @param array $info
	 * @return AxleLoad|null
	 */
	public static function findByOptions($axleLoads, $axlesCount, $distanceId, $wheelCount, $wheels, $axleType, $isSpring, &$info){

		self::normalizeParams($axlesCount, $distanceId, $wheelCount, $wheels, $axleType);
		$result = self::findByNormalizedParams($axleLoads, $axlesCount, $distanceId, $wheelCount, $wheels, $axleType, $isSpring);

		$info = [
			'group_axle_count' => $axlesCount,
			'distance_index' => $distanceId,
			'wheel_count' => $wheelCount,
			'wheels' => $wheels,
			'axle_type' => $axleType,
			'is_spring' => $isSpring,
			'axleTitle' => $result ? $result->title : 'No found'
		];
		return $result;
	}

	private static function normalizeParams(&$axleCount, &$distanceId, &$wheelCount, &$wheels, &$axleType){
		//distance fix for single axles
		if($axleCount === 1){
			$distanceId = AxleLoad::DISTANCE_INDEX_MORE_25;
			if($wheelCount > 2) {
				$wheelCount = 2;
			}
			$wheels = 1;
		}
		if($axleCount > 4){
			$axleCount = 4;
		}
		//axle type fix
		if(
			$axleCount === 3 &&
			$wheelCount === 1 &&
			$distanceId === AxleLoad::DISTANCE_INDEX_13_18
		){
			//do nothing
		}else{
			//set default value
			$axleType = AxleLoad::AXLE_TYPE_SPRING;
		}

		//wheels count fix
		if($wheels <=4){
			$wheels = AxleLoad::WHEELS_LESS_4;
		}else{
			$wheels = AxleLoad::WHEELS_MORE_4;
		}

		//wheels fix
		if($axleCount > 1 && $wheels === AxleLoad::WHEELS_MORE_4){
			$axleCount = 2;
			$wheelCount = 1;
			$axleType = AxleLoad::AXLE_TYPE_SPRING;
		}
	}

	/**
	 * @param AxleLoad[] $axleLoads
	 * @param int $axlesCount
	 * @param int $distanceId
	 * @param int $wheelCount
	 * @param int $wheels
	 * @param int $axleType
	 * @param int $isSpring
	 * @return AxleLoad|null
	 */
	private static function findByNormalizedParams($axleLoads, $axlesCount, $distanceId, $wheelCount, $wheels, $axleType, $isSpring){
		$data = [
			'group_axle_count' => $axlesCount,
			'distance_index' => $distanceId,
			'wheel_count' => $wheelCount,
			'wheels' => $wheels,
			'axle_type' => $axleType,
			'is_spring' => $isSpring
		];


		foreach($axleLoads as $axleLoad){
			if(
				$axleLoad->group_axle_count === $axlesCount &&
				$axleLoad->distance_index === $distanceId &&
				$axleLoad->wheel_count === $wheelCount &&
				$axleLoad->wheels === $wheels &&
				$axleLoad->axle_type === $axleType &&
				intval($axleLoad->is_spring) === $isSpring
			){
				return $axleLoad;
			}
		}
		//print_r($data);die;
		return null;
	}

	/**
	 * @param float $value
	 * @return int
	 */
	public static function getDistanceIndexByValue($value){
		if($value <= 1){
			return self::DISTANCE_INDEX_LESS_1;
		}
		if($value <= 1.3){
			return self::DISTANCE_INDEX_1_13;
		}
		if($value <= 1.8){
			return self::DISTANCE_INDEX_13_18;
		}
		if($value <= 2.5){
			return self::DISTANCE_INDEX_18_25;
		}

		return self::DISTANCE_INDEX_18_25;
	}

    /**
     * @param int $roadType
     * @return float|null
     */
	public function getValue($roadType) {
	    $fieldName = 'value' . $roadType;
	    return isset($this->{$fieldName}) ? $this->{$fieldName} : null;
    }
}
