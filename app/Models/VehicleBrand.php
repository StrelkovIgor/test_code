<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 07 Jul 2018 12:54:16 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Support\Facades\DB;

/**
 * Class VehicleBrand
 * 
 * @property int $id
 * @property string $title
 * @property int $accepted
 * @property string $deleted_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $vehicle_models
 * @property \Illuminate\Database\Eloquent\Collection $vehicles
 *
 * @package App\Models
 */
class VehicleBrand extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	const STATUS_ACCEPTED_NO = 0,
		STATUS_ACCEPTED_YES = 1,
		STATUS_ACCEPTED_HIDE = 2;

	protected $fillable = [
		'title',
		'accepted'
	];

	public function vehicle_models()
	{
		return $this->hasMany(VehicleModel::class, 'brand_id');
	}

	public function vehicles()
	{
		return $this->hasMany(Vehicle::class, 'brand_id');
	}
	
	/**
	 * @param $brandsTitles
	 * @return int new created brands count
	 */
	public static function importList($brandsTitles)
	{
		$date = date('Y-m-d H:i:s');
		foreach($brandsTitles as $brandsTitle){
			$existingBrand = self::where('title', $brandsTitle)->first();
			if(!$existingBrand){
				$data[] = [
					'title' => $brandsTitle,
					'accepted' => 1,
					'created_at' => $date,
					'updated_at' => $date,
				];
			}
		}
		
		if(!empty($data)){
			return DB::table('vehicle_brands')->insert($data);
		}
		
		return 0;
	}
	
	/**
	 * @param array $data
	 * @param User $user
	 * @return VehicleBrand|null
	 */
	public static function createByUser($data, $user)
	{
		$brand = new self([
			'accepted' => 0,
			'title' => array_get($data, 'brand_title')
		]);
		return $brand->save() ? $brand : null;
	}
    
    /**
     * @param $query
     * @param array $data
     *
     * @return
     */
	public static function adminDataFilter($query, $data)
    {
        if($title = array_get($data, 'title')) {
            $query->where('vehicle_brands.title', $title);
        }
        
        return $query;
    }
    
    public static function adminDataSort($query, $data) {
        $sortColumn = array_get($data, 'sort_column', 'id');
        $sortType = array_get($data, 'sort_type', 'desc');
    
        $query->orderBy($sortColumn, $sortType);
        
        return $query;
    }
}
