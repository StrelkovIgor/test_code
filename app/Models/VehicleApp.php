<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 07 Jul 2018 12:59:25 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class VehicleApp
 *
 * @property int $id
 * @property int $vehicle_id
 * @property int $admin_id
 * @property string $note
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @property User $user
 * @property Vehicle $vehicle
 *
 * @package App\Models
 */
class VehicleApp extends Eloquent
{
	protected $casts = [
		'vehicle_id' => 'int',
		'admin_id' => 'int'
	];
	
	protected $fillable = [
		'vehicle_id',
		'admin_id',
		'note'
	];
	
	/**
	 * @param int $vehicleId
	 * @param User $admin
	 * @return bool
	 */
	public static function accept($vehicleId, $admin)
	{
		$vehicleApp = self::findByVehicleOrCreate($vehicleId);
		$vehicleApp->admin_id = $admin->id;
		return $vehicleApp->save();
	}
	
	/**
	 * @param int $vehicleId
	 * @param User $admin
	 * @param string $note
	 * @return bool
	 */
	public static function decline($vehicleId, $admin, $note)
	{
		$vehicleApp = self::findByVehicleOrCreate($vehicleId);
		$vehicleApp->admin_id = $admin->id;
		$vehicleApp->note = $note;
		return $vehicleApp->save();
	}
	
	/**
	 * @param int $vehicleId
	 * @return VehicleApp
	 */
	public static function findByVehicleOrCreate($vehicleId)
	{
		$vehicleApp = self::where(['vehicle_id' => $vehicleId])->first();
		if (!$vehicleApp) {
			$vehicleApp = new VehicleApp(['vehicle_id' => $vehicleId]);
		}
		
		return $vehicleApp;
	}
	
	public function user()
	{
		return $this->belongsTo(User::class, 'admin_id');
	}
	
	public function vehicle()
	{
		return $this->belongsTo(Vehicle::class);
	}
}
