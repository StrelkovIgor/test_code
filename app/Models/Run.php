<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 18 Dec 2018 15:28:31 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Run
 * 
 * @property int $id
 * @property int $application_id
 * @property int $complete
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\Application $application
 * @property \Illuminate\Database\Eloquent\Collection $control_marks
 *
 * @package App\Models
 */
class Run extends Eloquent
{
	protected $casts = [
		'application_id' => 'int',
		'complete' => 'int'
	];

	protected $fillable = [
		'application_id',
		'complete'
	];

	public function application()
	{
		return $this->belongsTo(\App\Models\Application::class);
	}

	public function control_marks()
	{
		return $this->hasMany(\App\Models\ControlMark::class);
	}
	
	/**
	 * Mark run as complete
	 */
	public function markComplete()
	{
		$this->complete = true;
		$this->save();
	}
	
	/**
	 * Find active run by application or create new one
	 * @param int $applicationId
	 * @return static
	 */
	public static function getActive($applicationId)
	{
		$activeRun = static
			::where('application_id', $applicationId)
			->where('complete', 0)
			->first();
		
		return $activeRun;
	}
	
	/**
	 * Create new active run
	 * @param int $applicationId
	 * @return static|null
	 */
	public static function createNewActive($applicationId)
	{
		$run = static::create([
			'application_id' => $applicationId,
			'complete' => 0
		]);
		
		return $run;
	}
}
