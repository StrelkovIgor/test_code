<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 13 Aug 2018 21:11:11 +0000.
 */

namespace App\Models;

use App\Models\Interfaces\ObjectInfoInterface;
use DateTime;
use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Support\Facades\DB;
use App\Models\Audit\AuditableTrait;
use App\Observers\PrivilegeStatusObserver;

/**
 * Class PrivilegeStatus
 *
 * @property int $id
 * @property string $title
 * @property string $route_info
 * @property int $user_id
 * @property \Carbon\Carbon $start_date
 * @property \Carbon\Carbon $finish_date
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 *
 * @property \Illuminate\Database\Eloquent\Collection $privilege_vehicles
 * @property \App\Models\User $user
 * @property \App\Models\APVGK $apvgk
 *
 * @package App\Models
 */
class PrivilegeStatus extends Eloquent implements ObjectInfoInterface
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	use AuditableTrait;

	protected $casts = [
		'user_id' => 'int'
	];
	
	protected $dates = [
		'start_date',
		'finish_date'
	];
	
	protected $fillable = [
		'title',
		'route_info',
		'user_id',
		'start_date',
		'finish_date'
	];
		
	public static function boot()
	{
			self::observe(new PrivilegeStatusObserver); 
			parent::boot();
	}

	/**
	 * Find privilege status by number
	 * @param string $number
	 * @return PrivilegeStatus|null
	 */
	public static function findByNumber($number)
	{
		$currentDate = date('Y-m-d');
		$status = PrivilegeStatus
			::whereExists(function ($query) use ($number, $currentDate) {
				$query->select(DB::raw(1))
					->from('privilege_vehicles')
					->where('privilege_vehicles.number', $number)
					->where('privilege_statuses.start_date', '<=', $currentDate)
					->where('privilege_statuses.finish_date', '>=', $currentDate)
					->whereRaw('privilege_vehicles.privilege_status_id = privilege_statuses.id');
			})
			->first();
		
		return $status;
	}
	
	public static function findByVehicleId($id)
	{
		/** @var Vehicle $vehicle */
		$vehicle = Vehicle::find($id);
		
		if ($vehicle && $vehicle->privilege) {
			return $vehicle->privilege->privilege_status_id;
		}
		return null;
	}
	
	public function privilege_vehicles()
	{
		return $this->hasMany(\App\Models\PrivilegeVehicle::class);
	}
	
	public function user()
	{
		return $this->belongsTo(\App\Models\User::class);
	}

    public function apvgk()
    {
        return $this->belongsToMany(\App\Models\apvgk::class, "privilege_status_to_apvgk");
    }

	public function files()
	{
		return $this->hasMany(\App\Models\File::class);
	}

	/**
	 * Check is status active
	 * @return bool
	 */
	public function isActive()
	{
		$nowDate = DateTime::createFromFormat('Y-m-d', date('Y-m-d'));
		$startDate = DateTime::createFromFormat('Y-m-d', $this->start_date->format('Y-m-d'));
		$finishDate = DateTime::createFromFormat('Y-m-d', $this->finish_date->format('Y-m-d'));
		
		return $nowDate >= $startDate && $nowDate <= $finishDate;
	}
	
	/**
	 * Update privilege status files
	 * @param array $filesData
	 * @param string $fileType
	 * @param bool $needClear
	 */
	public function updateFiles($filesData, $fileType, $needClear = true)
	{
		$ids = [];
		if (!empty($filesData)) {
			foreach ($filesData as $fileData) {
				$ids[] = $fileData['id'];
			}
		}
		
		if($needClear){
			File::where('privilege_status_id', $this->id)
				->where('type_id', $fileType)
				->whereNotIn('id', $ids)
				->delete();
		}
		
		if (!empty($ids)) {
			foreach ($ids as $id) {
				if (intval($id)) {
					$file = File::find($id);
					if ($file) {
						$file->privilege_status_id = $this->id;
						$file->save();
					}
				}
			}
		}
	}

	public function getObjectInfo()
	{
		return $this->title;
	}

	public function createApvgk($data){
        PrivilegeStatusToApvgk::createPole($this,$data);
    }
}
