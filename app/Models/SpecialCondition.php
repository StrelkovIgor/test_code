<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 11 Aug 2018 22:26:56 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Support\Facades\DB;

/**
 * Class SpecialCondition
 * 
 * @property int $id
 * @property string $title
 * @property bool $value
 * @property string $start_date
 * @property string $finish_date
 * @property bool $has_comment
 * @property string $comment
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class SpecialCondition extends Eloquent
{
	const SPRING_ID = 1;

	protected $casts = [
		'value' => 'bool',
		'has_comment' => 'bool'
	];

	protected $fillable = [
		'title',
		'value',
		'start_date',
		'finish_date',
		'has_comment',
		'comment'
	];


	/**
	 * @return mixed
	 */
	public static function createDefaultSpring()
	{
		DB::table('special_conditions')->insert([
			'id' => self::SPRING_ID,
			'title' => 'Весеннее ограничение',
			'value' => 0
		]);

		$spring = self::find(self::SPRING_ID);

		return $spring;
	}
}
