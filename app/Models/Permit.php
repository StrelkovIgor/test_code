<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 27 Oct 2018 14:01:58 +0000.
 */

namespace App\Models;

use App\Helpers\DataConverter;
use App\Helpers\PriceCalculator;
use DateTime;
use Illuminate\Database\Eloquent\Model as Eloquent;
use App\Http\Resources\ApplicationFull as ApplicationFullResource;

/**
 * Class Permit
 * 
 * @property int $id
 * @property int $application_id
 * @property string $price_info
 * @property string $application_info
 * @property string $invoice_link
 * @property \Carbon\Carbon $activation_date
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\Application $application
 *
 * @package App\Models
 */
class Permit extends Eloquent
{
	protected $dbDateTimeFormat = 'Y-m-d H:i:s';
	protected $myDateFormat = 'd.m.Y';

	protected $casts = [
		'application_id' => 'int'
	];

	protected $dates = [
		'activation_date'
	];

	protected $fillable = [
		'application_id',
		'price_info',
		'application_info',
		'invoice_link',
		'activation_date'
	];

	public function application()
	{
		return $this->belongsTo(Application::class);
	}
	
	/**
	 * Create permit for app
	 * @param Application $application
	 * @param bool $recalculatePrice
	 * @return array
	 */
	public static function createForApp($application, $recalculatePrice = true)
	{
		$permit = Permit::where('application_id', $application->id)->first();
		if (!$permit) {
			$permit = new Permit(['application_id' => $application->id]);
		}

		$price = $application->price;
		$springPrice = $application->spring_price;
		if($recalculatePrice) {
			$priceCalculator = new PriceCalculator($application);
			$price = $priceCalculator->calculatePrice();
			$permit->price_info = json_encode($priceCalculator->getLastCalculationInfo());

			$springPrice = $priceCalculator->calculatePrice(1);
		}


		//store current application info
		//$permit->application_info = \GuzzleHttp\json_encode(DataConverter::getApplicationData($application));
		$permit->save();
		
		return [
			'price' => $price,
			'spring_price' => $springPrice
		];
	}

	/**
	 * @param $date
	 * @return null|string
	 */
	public  function dbDateToMyDate($date){
		if(!$date){
			return null;
		}
		$date = DateTime::createFromFormat($this->dbDateTimeFormat, $date);
		if(!$date){
			return null;
		}
		return $date->format($this->myDateFormat);
	}

	/**
	 * @return null|string
	 */
	public function activateDateFormatted()
	{
		return $this->dbDateToMyDate($this->activation_date);
	}
}
