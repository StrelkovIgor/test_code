<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 23 Jul 2019 23:18:47 +0300.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Process
 * 
 * @property int $id
 * @property int $type
 * @property array $params
 * @property string $hash
 * @property int $status
 * @property int $progress
 * @property string $result
 * @property int $user_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\User $user
 *
 * @package App\Models
 */
class Process extends Eloquent
{
	const STATUS_NEW = 0,
		STATUS_PROCESSING = 1,
		STATUS_SUCCEED = 2,
		STATUS_FAILED = 3;

	const TYPE_ACTIVE_REPORT = 0,
		TYPE_LOGS = 1,
        TYPE_ACTIVE_REPORT_SMEV = 2;

	protected $casts = [
		'type' => 'int',
		'params' => 'array',
		'result' => 'array',
		'status' => 'int',
		'progress' => 'int',
		'user_id' => 'int'
	];

	protected $fillable = [
		'type',
		'params',
		'hash',
		'status',
		'progress',
		'result',
		'user_id'
	];

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class);
	}

	/**
	 * @param $filterData
	 * @return string
	 */
	public static function reportsHash($filterData)
	{
		return md5(json_encode($filterData));
	}

	/**
	 * @param int $userId
	 * @param array $filterData
	 * @param int $type
	 *
	 * @return Process
	 */
	public static function createForType($userId, $filterData, $type = self::TYPE_ACTIVE_REPORT)
	{
		$data = [
			'type' => $type,
			'params' => !empty($filterData) ? $filterData : null,
			'hash' => static::reportsHash($filterData),
			'status' => static::STATUS_NEW,
			'user_id' => $userId
		];

		return static::create($data);
	}
}
