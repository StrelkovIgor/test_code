<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 22 Jul 2018 12:45:51 +0000.
 */

namespace App\Models;

use App\Helpers\TokenGenerator;
use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class File
 *
 * @property int $id
 * @property int $type_id
 * @property int $user_id
 * @property int $vehicle_id
 * @property int $privilege_status_id
 * @property int $application_id
 * @property string $name
 * @property string $source
 * @property string $url
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @property \App\Models\User $user
 * @property \App\Models\Vehicle $vehicle
 * @property \App\Models\PrivilegeStatus $privilege_status
 * @property \App\Models\Application $application
 *
 * @package App\Models
 */
class File extends Eloquent
{
	const TYPE_FIRM_INN = 0,
		TYPE_LICENSE = 1,
		TYPE_PTS = 2,
		TYPE_LEASE = 3,
		TYPE_PRIVILEGE_STATUS = 4,
		TYPE_APPLICATION_LOAD = 5,
		TYPE_APPLICATION_PAY = 6,
		TYPE_APPLICATION_PENALTY = 7,
        TYPE_SMEV_ATTACHMENT = 8,
        TYPE_APPLICATION_IN_WORK = 9;

	/**
	 * @var array
	 */
	private static $keys = [
		self::TYPE_FIRM_INN => 'inn',
		self::TYPE_LICENSE => 'license',
		self::TYPE_PTS => 'pts',
		self::TYPE_PRIVILEGE_STATUS => 'privilege_status',
		self::TYPE_APPLICATION_LOAD => 'load',
		self::TYPE_APPLICATION_PAY => 'pay',
		self::TYPE_APPLICATION_PENALTY => 'penalty',
	];

	/**
	 * @param int $type
	 * @return mixed|null
	 */
	public static function getNameKey($type){
		return self::$keys[$type] ?? null;
	}

	/**
	 * @param int $type
	 * @param string $default
	 * @return null|string
	 */
	public static function getResourceKey($type, $default = 'default'){
		$key = self::getNameKey($type);
		if(!$key) {
			return $default;
		}
		return $key . '_files';
	}
	
	protected $casts = [
		'type_id' => 'int',
		'user_id' => 'int',
		'vehicle_id' => 'int',
		'privilege_status_id' => 'int',
		'application_id' => 'int',
	];
	
	protected $fillable = [
		'type_id',
		'user_id',
		'vehicle_id',
		'privilege_status_id',
		'application_id',
		'name',
		'source',
		'url'
	];
	
	public function user()
	{
		return $this->belongsTo(\App\Models\User::class);
	}
	
	public function vehicle()
	{
		return $this->belongsTo(\App\Models\Vehicle::class, 'user_id');
	}
	
	public function privilege_status()
	{
		return $this->belongsTo(\App\Models\PrivilegeStatus::class, 'privilege_status_id');
	}
	
	public function application()
	{
		return $this->belongsTo(\App\Models\Application::class, 'application_id');
	}

	/**
	 * Remove all files of this type binded to the app
	 * @param int $type
	 * @param int $application_id
	 * @return bool
	 */
	public static function removeWithFiles($type, $application_id)
	{
		$success = false;

		if(!empty($application_id)) {
			/** @var static[] $files */
			$files = static
				::where('application_id', $application_id)
				->where('type_id', $type)
				->get();

			if(!empty($files)) {
				foreach($files as $file) {
					$file->unlink();
				}
			}

			$success = static
				::where('application_id', $application_id)
				->where('type_id', $type)
				->delete();
		}

		return $success;
	}

	/**
	 * Unlink file from storage
	 * @return bool
	 */
	public function unlink()
	{
		\Storage::disk('public')->delete($this->source);
	}

	public function getUrl()
	{
		return $this->getUrlWithToken();
//		return $this->url;
	}

    public function getUrlNew()
    {
        return url($this->url);
    }

	/**
	 * @return \Illuminate\Contracts\Routing\UrlGenerator|string
	 */
	public function getUrlWithToken()
	{
		return url('/api/files/download/' . $this->id . '/' . $this->name . '?token=' . $this->generateToken());
	}

	/**
	 * @return bool
	 */
	private function generateToken()
	{
		return TokenGenerator::generateForFile($this);
	}

	/**
	 * @param string $token
	 * @return bool
	 */
	public function checkHash($token)
	{
		return $token === $this->generateToken();
	}

	/**
	 * @return string
	 */
	public function getPath()
	{
		return storage_path('app/public/' . $this->source);
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * @return string
	 */
	public function getContentType()
	{
		$result = null;
		$path = $this->getPath();
		try {
			$result = $path ? mime_content_type($path) : null;
		} catch (\Exception $e) {

		}
		return $result;
	}

	/**
	 * @return string
	 */
	public function isImage()
	{
		$contentType = $this->getContentType();
		if(!$contentType) {
			return false;
		}
		$parts = explode('/', $contentType);
		return $parts[0] === 'image';
	}

	/**
	 * @return bool
	 */
	public function fileExists()
	{
		$path = $this->getPath();
		return file_exists($path);
	}
}
