<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class VarsStorage
 * 
 * @property int $id
 * @property string $key
 * @property string $value
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class VarsStorage extends Eloquent
{
    const GRADOSERVICE_TOKEN = 'gradoservice_token';

	protected $table = 'vars_storage';

	protected $fillable = [
		'key',
		'value'
	];

    /**
     * Get value by key, or default if not found
     * @param $key
     * @param null $default
     * @return string|null
     */
	public static function findByKey($key, $default = null) {
	    /** @var VarsStorage $var */
	    $var = static::where('key', $key)->first();

	    if($var) {
	        return $var->value;
        }

	    return $default;
    }

    /**
     * Set value and save it in storage
     * @param string $key
     * @param string $value
     * @return bool
     */
    public static function setValue($key, $value) {
        /** @var VarsStorage $var */
        $var = static::where('key', $key)->first();
        if(!$var) {
            $var = new static();
            $var->key = $key;
            $var->value = $value;
        }

        $result = false;
        if($var) {
            $var->value = $value;
            $success = $var->save();
        }

        return $result;
    }
}
