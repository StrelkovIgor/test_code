<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 13 Aug 2018 21:06:32 +0000.
 */

namespace App\Models;

use App\Helpers\VehicleNumberHelper;
use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Support\Facades\DB;

/**
 * Class PrivilegeVehicle
 *
 * @property int $id
 * @property int $is_trailer
 * @property int $privilege_status_id
 * @property string $number
 *
 * @property string $region
 * @property string $is_non_standard_number
 * @property string $non_standard_number
 * @property string $real_number
 *
 * @property string $brand_title
 * @property string $model_title
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @property \App\Models\PrivilegeStatus $privilege_status
 *
 * @package App\Models
 */
class PrivilegeVehicle extends Eloquent
{
    protected $casts = [
        'privilege_status_id' => 'int',
        'is_trailer' => 'int',
        'is_non_standard_number' => 'int',
    ];

    protected $fillable = [
        'is_trailer',
        'privilege_status_id',
        'number',
        'region',
        'is_non_standard_number',
        'non_standard_number',
        'brand_title',
        'model_title',
		'real_number'
    ];

    public static function findByNumbers($numbers)
    {
        if(empty($numbers) || is_array($numbers)){
            return [];
        }
        $privilegeVehicles = self
            ::whereIn('number', $numbers)
            ->with('privilege_status')
            ->get();

        return $privilegeVehicles;
    }

    public function privilege_status()
    {
        //@todo by database
    	$currentDate = date('Y-m-d');
        return $this->belongsTo(\App\Models\PrivilegeStatus::class)
            ->where('privilege_statuses.start_date', '<=', $currentDate)
            ->where('privilege_statuses.finish_date', '>=', $currentDate);
    }

	/**
	 * @return string
	 */
    public function realNumber()
    {
        return $this->real_number;
    }
	
	/**
	 * @param array $numbers
	 * @return mixed
	 */
    public static function numberQuery($numbers)
	{
		$todayDate = date('Y-m-d');
		
		return self::whereIn('number', $numbers)
			->leftjoin('privilege_statuses', 'privilege_statuses.id', '=', 'privilege_vehicles.privilege_status_id')
			->where('privilege_statuses.start_date', '<=', $todayDate)
			->where('privilege_statuses.finish_date', '>=', $todayDate);
	}
	
	
	/**
	 * @param $vehiclesData
	 * @param $privilege_status_id
	 * @return int new created privilege vehicles
	 */
	public static function importList($vehiclesData, $privilege_status_id)
	{
		$date = date('Y-m-d H:i:s');
		$insertData = [];
		foreach($vehiclesData as $vehicle){
			if(!empty($vehicle['number']) && !empty($vehicle['region'])){
				$existingVehicle = self::where('privilege_status_id', $privilege_status_id)
					->where('number', $vehicle['number'])
					->where('region', $vehicle['region'])
					->first();
				if(!$existingVehicle){
					$insertData[] = [
						'privilege_status_id' => $privilege_status_id,
						'number' => $vehicle['number'],
						'region' => $vehicle['region'],
						'real_number' => $vehicle['real_number'],
						'brand_title' => $vehicle['brand_title'],
						'model_title' => $vehicle['model_title'],
						'is_non_standard_number' => 0,
						'created_at' => $date,
						'updated_at' => $date,
					];
				}
			}
		}
		
		if(!empty($insertData)){
			return DB::table('privilege_vehicles')->insert($insertData);
		}
		
		return 0;
	}

	public function save(array $options = [])
	{
		$this->number = VehicleNumberHelper::formatNumber($this->number);
		$this->real_number = $this->number . ' ' . $this->region;
		return parent::save();
	}
}
