<?php

namespace App\Models;

use App\Helpers\ExcelHelper;
use App\Helpers\PriceCalculator;
use App\Helpers\VehicleNumberHelper;
use App\Models\Audit\Audit;
use App\Models\Audit\AuditableTrait;
use App\Models\Audit\Enum\Events;
use App\Models\Interfaces\ObjectInfoInterface;
use DateTime;
use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

/**
 * Class Application
 *
 * @property int $id
 * @property int $user_id
 * @property string $username
 * @property int $vehicle_id
 * @property int $trailer_id
 * @property int $admin_id
 * @property string $comment
 * @property int $status
 * @property \Carbon\Carbon $start_date
 * @property \Carbon\Carbon $finish_date
 * @property float $price
 * @property float $spring_price
 * @property array $price_info
 * @property string $real_number
 * @property float $federal_price
 * @property float $regional_price
 * @property string $start_address
 * @property string $finish_address
 * @property integer $special_condition_id
 * @property integer $privilege_status_id
 * @property integer $department_id
 * @property integer $is_template
 * @property integer $template_id
 * @property integer $is_draft
 * @property string $accept_date
 * @property string $activate_date
 * @property integer $employee_id
 * @property string $form_id
 * @property string $form_year
 * @property integer $is_spring
 * @property integer $is_fast
 * @property integer $is_smev
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property integer $is_locked
 * @property integer $location_type
 * @property string $special_conditions
 *
 * @property \App\Models\User $user
 * @property \App\Models\User $employee
 * @property \App\Models\User $admin
 * @property \App\Models\Vehicle $vehicle
 * @property \App\Models\Vehicle $trailer
 * @property \App\Models\ApplicationVehicle $frozen_vehicle
 * @property \App\Models\ApplicationVehicle[] $frozen_trailers
 * @property \App\Models\ApplicationLoad $application_load
 * @property \App\Models\ApplicationRoute $application_route
 * @property \App\Models\PrivilegeStatus $privilege_status
 * @property \App\Models\FastApplication $fast_application
 * @property \App\Models\ApplicationSmev $smev_application
 * @property \App\Models\ApplicationDate $application_dates
 * @property \App\Models\ApplicationTrailer[] $application_trailers
 * @property \App\Models\Vehicle[] $trailers
 * @property \App\Models\ControlMark $control_marks
 * @property \App\Models\File[] $files
 * @property \App\Models\ApplicationAgreement[] $application_agreements
 * @property \App\Models\Permit $permit
 * @property \App\Models\Audit\Audit[] $audits
 * @property \App\Models\Audit\Audit[] $audits_active
 *
 * @package App\Models
 */
class Application extends Eloquent implements ObjectInfoInterface
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	use AuditableTrait;

	const DOCUMENTS_APP_CODE = '01',
		DOCUMENTS_APP_FAST_CODE = '02',
		DOCUMENTS_APP_SMEV_CODE = '03';

	const STATUS_NEW = 0,
		STATUS_REVIEW = 1,
		STATUS_DECLINE = 2,
		STATUS_ACCEPTED_WITH_CHANGES = 3,
		STATUS_ACCEPTED = 4,
		STATUS_ACTIVE = 5,
		STATUS_REPEAT = 6,
		STATUS_EXPIRED = 7,
		STATUS_REMOVED = 8,
        STATUS_AWAITING_ATTACHMENTS = 9;

	const USER_TAB_NEW = 0,
		USER_TAB_WAITING = 1,
		USER_TAB_DECLINED = 2,
		USER_TAB_ACCEPTED = 4,
		USER_TAB_ACTIVE = 5,
		USER_TAB_EXPIRED = 6;

    /**
     *
     */
    const ADMIN_TAB_NEW = 0,
		ADMIN_TAB_WORK = 1,
		ADMIN_TAB_DECLINED = 2,
		ADMIN_TAB_REPEAT = 3,
		ADMIN_TAB_ACCEPTED = 4,
		ADMIN_TAB_ACTIVE = 5,
		ADMIN_TAB_EXPIRED = 7,
		ADMIN_TAB_REMOVED = 8;

	const SMEV_TAB_NEW = 0,
        SMEV_TAB_WORK = 1,
        SMEV_TAB_HANDLED = 2;

	const TEMPLATE_NO = 0,
		TEMPLATE_YES = 1;

	const IS_DRAFT_YES = 1,
		IS_DRAFT_NO = 0;

	const LOCKED_YES = 1,
		LOCKED_NO = 0;

	const LOCATION_LOCAL = 0,
		LOCATION_REGIONAL = 1,
		LOCATION_INTERNATIONAL = 2;


	const PRICE_COEFF = 1.92;
	const CHECK_NUM = 30;

	const TYPE_NORMAL = 'normal',
        TYPE_FAST = 'fast',
        TYPE_SMEV = 'smev';


	const SMEV_STATUS_IN_PROGRESS = 'InProgress',
        SMEV_STATUS_WAITING_ATTACHMENTS = 'WaitingAttachments',
        SMEV_STATUS_APPROVED = 'Approved',
        SMEV_STATUS_DECLINED = 'Declined';

	/**
	 * All available application statuses
	 * @var array
	 */
	private static $allStatuses = [
		self::STATUS_NEW,
		self::STATUS_REVIEW,
		self::STATUS_DECLINE,
		self::STATUS_ACCEPTED_WITH_CHANGES,
		self::STATUS_ACCEPTED,
		self::STATUS_ACTIVE,
		self::STATUS_REPEAT,
		self::STATUS_EXPIRED,
		self::STATUS_REMOVED
	];

	/** @var $clientAdminStatuses array map admin statuses from user's tab on frontend app to db statuses */
	private static $clientAdminStatuses = [
		self::ADMIN_TAB_NEW => self::STATUS_NEW,
		self::ADMIN_TAB_WORK => self::STATUS_REVIEW,
		self::ADMIN_TAB_DECLINED => self::STATUS_DECLINE,
		self::ADMIN_TAB_REPEAT => self::STATUS_REPEAT,
		self::ADMIN_TAB_ACCEPTED => self::STATUS_ACCEPTED,
		self::ADMIN_TAB_ACTIVE => self::STATUS_ACTIVE,
		self::ADMIN_TAB_EXPIRED => self::STATUS_ACTIVE,
		self::ADMIN_TAB_REMOVED => null,
	];
	private static $clientSmevStatuses = [
        self::SMEV_TAB_NEW => self::STATUS_NEW,
        self::SMEV_TAB_WORK => self::STATUS_REVIEW,
        self::SMEV_TAB_HANDLED => [
            self::STATUS_ACCEPTED_WITH_CHANGES,
            self::STATUS_ACCEPTED,
            self::STATUS_ACTIVE,
            self::STATUS_DECLINE,
            self::STATUS_EXPIRED,
            self::STATUS_AWAITING_ATTACHMENTS,
        ]
    ];

	public $dbDateFormat = 'Y-m-d';
	public $myDateFormat = 'd.m.Y';
	public $dateTimeFormat = 'Y-m-d H:i';
	public $fullDateTimeFormat = 'Y-m-d H:i:s';

	private static $csvExportHeaders = [
		'Порядковый номер',
		'Территориальное управление ГБУ БДД',
		'Номер заявки на разрешение',
		'Статус',
		'Наименование организации',
		'Гос. номер ТС',
		'Сумма руб',
		'Дата и Время поступления',
		'Дата и Время расмотрения (одобрения, отклонения)'
	];

	protected $casts = [
		'user_id' => 'int',
		'vehicle_id' => 'int',
		'trailer_id' => 'int',
		'admin_id' => 'int',
		'status' => 'int',
		'price' => 'float',
        'price_info' => 'array',
		'spring_price' => 'float',
		'federal_price' => 'float',
		'regional_price' => 'float',
		'special_condition_id' => 'int',
		'privilege_status_id' => 'int',
		'department_id' => 'int',
		'is_template' => 'int',
		'template_id' => 'int',
		'is_draft' => 'int',
		'employee_id' => 'int',
		'is_locked' => 'int',
		'is_spring' => 'int',
		'is_fast' => 'int',
		'is_smev' => 'int',
	];
	protected $fillable = [
		'user_id',
		'username',
		'vehicle_id',
		'trailer_id',
		'admin_id',
		'comment',
		'status',
		'start_date',
		'finish_date',
		'price',
		'spring_price',
        'price_info',
		'real_number',
		'federal_price',
		'regional_price',
		'start_address',
		'finish_address',

		'special_condition_id',
		'privilege_status_id',
		'department_id',
		'is_template',
		'template_id',
		'is_draft',
		'accept_date',
		'activate_date',
		'employee_id',
		'form_id',
		'form_year',
		'is_locked',
		'is_spring',
		'is_fast',
		'is_smev',
        'special_conditions'
	];
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

	protected $appends = ['formatted_id'];

	/**
	 * Get query to get user's templates
	 * @param User $user
	 * @return mixed
	 */
	public static function userTemplatesQuery($user)
	{
		$query = self::isTemplateQuery();

		if ($user->role_id == Role::ROLE_FIRM_USER) {
			$query->where('employee_id', $user->id);
		} else {
			$query->where('user_id', $user->id);
		}

		return $query;
	}

	/**
	 * Get templates query
	 * @return mixed
	 */
	public static function isTemplateQuery()
	{
		return self::where('is_template', self::TEMPLATE_YES);
	}

	/**
	 * Get query by user and application status
	 * @param int $status
	 * @return mixed
	 */
	public static function userStatusQuery($status)
	{
		$query = Application::notTemplateQuery();

		if ($status === Application::USER_TAB_NEW) {
			$query->where('status', Application::STATUS_NEW)
				->where('is_draft', Application::IS_DRAFT_YES);
		} else {
			$query->where('is_draft', Application::IS_DRAFT_NO);
		}

		if ($status === Application::USER_TAB_WAITING) {
			$query->where(function ($q) {
				$q->where('status', Application::STATUS_NEW)
					->orWhere('status', Application::STATUS_REVIEW)
					->orWhere('status', Application::STATUS_REPEAT);
			});
		}

		if ($status === Application::USER_TAB_ACCEPTED) {
			$query->where(function ($q) {
				$q->where('status', Application::STATUS_ACCEPTED)
					->orWhere('status', Application::STATUS_ACCEPTED_WITH_CHANGES);
			});
		}

		if (
			$status !== Application::USER_TAB_NEW &&
			$status !== Application::USER_TAB_WAITING &&
			$status !== Application::USER_TAB_ACCEPTED
		) {
			$query->where('status', $status);
		}

		return $query;
	}

	/**
	 * Get not templates query
	 * @return mixed
	 */
	public static function notTemplateQuery()
	{
		return self::where('is_template', self::TEMPLATE_NO);
	}

	/**
	 * @param mixed $query
	 * @param User $user
	 * @return mixed
	 */
	public static function userRoleFilter($query, $user)
	{
		if ($user->role_id == Role::ROLE_FIRM_USER) {
			//get all available vehicles for firm_user
			$availableVehiclesList = $user->getAvailableVehiclesIds();

			if (!empty($availableVehiclesList)) {
				//for available trailers check
				$query->leftjoin('application_trailers', 'application_trailers.application_id', '=', 'applications.id');

				$query->where(function ($q) use ($user, $availableVehiclesList) {
					$availableInList = implode(',', $availableVehiclesList);
					$q->whereIn('applications.vehicle_id', $availableVehiclesList)
						//user has access to application's trailer
						->orWhereRaw('application_trailers.vehicle_id IN (' . $availableInList . ')');
				});
			} else {
				$query->where('employee_id', $user->id);
			}
		} else {
			$query->where('user_id', $user->id);
		}

		return $query;
	}

	/**
	 * @param mixed $query
	 * @param array $data
	 * @return mixed
	 */
	public static function userDataFilter($query, $data)
	{
		if ($start_date = array_get($data, 'start_date')) {
			$query->where('start_date', $start_date);
		}

		if ($finish_date = array_get($data, 'finish_date')) {
			$query->where('finish_date', $finish_date);
		}

		if($id = array_get($data, 'id')){
		    $query->where('applications.id', $id);
        }

        if($number = array_get($data, 'number')){
            $query->where('applications.real_number', 'like', '%' . $number . '%');
        }

		return $query;
	}

	public static function acivatedStatusQuery()
	{
		$query = Application::notTemplateQuery()
			->withTrashed()
			->where(function ($q) {
				$q->where('status', Application::STATUS_ACTIVE)
					->orWhere('status', Application::STATUS_EXPIRED);
			});

		return $query;
	}

	/**
	 * @param int $tab
	 * @param User $user
	 * @param array $data
	 * @return mixed
	 */
	public static function adminStatusQuery($tab, $user, $data)
	{
	    $isSmev = array_get($data, 'is_smev');
	    if ($isSmev) {
	        $status = array_get($data, 'status', []);
        } else {
            $status = self::mapAdminStatus($tab, $data);
        }


		if ($user->role_id === Role::ROLE_ADMIN && $tab == Application::ADMIN_TAB_REMOVED) {
			return Application::onlyTrashed()->where('status', '<>', self::STATUS_NEW);
		}

		$query = Application::notTemplateQuery()->where('is_draft', Application::IS_DRAFT_NO);

		$statusField = 'applications.status';
		if ($user->role_id === Role::ROLE_DEPARTMENT_AGENT) {
			$query->leftjoin('application_agreements', 'application_agreements.application_id', '=', 'applications.id');
			$query->whereNull('application_agreements.deleted_at');
			$statusField = 'application_agreements.status';
			if($status === Application::STATUS_NEW || $status ===  Application::STATUS_REVIEW) {
				$query
					->whereIn('applications.status', [Application::STATUS_REVIEW, Application::STATUS_REPEAT]);
			}
		}

		if ($status !== null) {
            if (is_array($status)) {
                $query->whereIn($statusField, $status);
            } else {
                if ($status === Application::STATUS_ACCEPTED) {
                    $query->where(function ($q) use ($statusField) {
                        $q->where($statusField, Application::STATUS_ACCEPTED)
                            ->orWhere($statusField, Application::STATUS_ACCEPTED_WITH_CHANGES);
                    });
                } else {
                    $query->where($statusField, $status);
                }

                if ($status === Application::STATUS_ACTIVE) {
                    if ($tab === Application::STATUS_EXPIRED) {
                        $query->where('finish_date', '<', date('Y-m-d'));
                    } else {
                        $query->where('finish_date', '>=', date('Y-m-d'));
                    }
                }
            }
        }

		return $query;
	}

	/**
	 * @param int $clientStatus
	 * @param array $data
	 * @return int|array
	 */
	protected static function mapAdminStatus($clientStatus, $data)
	{
	    $isSmev = array_get($data,  'is_smev');
	    if ($isSmev) {
	        return isset(self::$clientSmevStatuses[$clientStatus]) ? self::$clientSmevStatuses[$clientStatus] : $clientStatus;
        }
		return isset(self::$clientAdminStatuses[$clientStatus]) ? self::$clientAdminStatuses[$clientStatus] : $clientStatus;
	}

	/**
	 * Apply admin filter
	 * @param mixed $query
	 * @param array $data
	 * @param User $user
	 * @return mixed
	 */
	public static function adminDataFilter($query, $data, $user = null)
	{
		if ($formattedId = array_get($data, 'id')) {
			$id = Application::formattedIdToId($formattedId);
			$query->where('applications.id', $id);
		}

		if ($form_id = array_get($data, 'form_id')) {
			$query->where('applications.form_id',  $form_id);
		}

		$isFast = array_get($data, 'is_fast', 0);
		$isSmev = array_get($data, 'is_smev', 0);
		
		$sortColumn = array_get($data, 'sort_column');
		if($sortColumn === 'users.name') {
			$sortColumn = 'applications.username';
		}
		if($sortColumn === 'v.number') {
			$sortColumn = 'applications.real_number';
		}

		if($name = array_get($data, 'name')) {
            $query->leftJoin("users", "users.id", "=", "applications.employee_id")
                ->where(function($query) use($name){
                    $query->where('applications.username', 'like', '%' . $name . '%')
                        ->orWhere("users.name", "like", "%$name%");
                });

		}

		//vehicle number
		$number = array_get($data, 'number');
		if($number) {
			$query->where('applications.real_number', 'like', '%' . $number . '%');
		}


		//trailer_number
		if ($trailerNumber = array_get($data, 'trailer_number') || $sortColumn === 't.number') {
			$query->leftjoin('vehicles as t', 't.id', '=', 'applications.trailer_id');
			$query->where('t.real_number', 'like', '%' . $trailerNumber . '%');
		}

		//addresses
		if ($startAddress = array_get($data, 'start_address')) {
			$query
				->where('start_address', 'like', '%' . $startAddress . '%');
		}
		if ($finishAddress = array_get($data, 'finish_address')) {
			$query
				->where('finish_address', 'like', '%' . $finishAddress . '%');
		}

		if ($issue_place_id = array_get($data, 'issue_place_id')) {
			$query->leftjoin('application_dates', 'application_dates.application_id', '=', 'applications.id');
			$query->where('application_dates.issue_place_id', $issue_place_id);
		}

		if ($adminName = array_get($data, 'admin_name')) {
			$query->leftjoin('users as admins', 'admins.id', '=', 'applications.admin_id')
				->where('admins.name', 'like', '%' . $adminName . '%');
		}

		//is fast
		if($user && $user->role_id !== Role::ROLE_DEPARTMENT_AGENT) {
			$query->where('applications.is_fast', $isFast);
			$query->where('applications.is_smev', $isSmev);
		}

		//smev filter
        if ($min_date = array_get($data, 'min_date')) {
            $query->where('start_date', '<=', $min_date);
            $query->where('finish_date', '>=', $min_date);
        }
        if ($max_date = array_get($data, 'max_date')) {
            $query->where('start_date', '<=', $max_date);
            $query->where('finish_date', '>=', $max_date);
        }
        $approval_id = array_get($data, 'approval_id');
        $output_number = array_get($data, 'output_number');
        $authority_name = array_get($data, 'authority_name');
        $overdue = array_get($data, 'overdue', null);

        if ($approval_id || $output_number || $authority_name || $overdue) {
            $query->leftjoin('application_smevs', 'application_smevs.application_id', '=', 'applications.id');
            if ($approval_id) {
                $query->where('application_smevs.approval_id',$approval_id);
            }
            if ($output_number) {
                $query->where('application_smevs.output_number','like', "%$output_number%");
            }
            if ($authority_name) {
                $query->where('application_smevs.authority_name', 'like', '%' . $authority_name . '%');
            }
            if($overdue){
                try{
                    $now = Carbon::createFromFormat('d.m.Y', $overdue);
                    $now->addDay(ApplicationSmev::DAY_FOR_DELAY);
                    $query->where('application_smevs.created_at', '>=',$now->format('Y-m-d'));
                }catch (\Exception $e){}
            }
        }


		return $query;
	}

	/**
	 * Filter for active apps report
	 * @param $query
	 * @param $data
	 * @return mixed
	 */
	public static function activeReportsFilter($query, $data)
	{
		//accept / activate date filters
		if($accept_date_from = array_get($data, 'accept_date_from')) {
			$query->where('applications.accept_date', '>=', $accept_date_from . ' 00:00:00');
		}
		if ($accept_date_to = array_get($data, 'accept_date_to')) {
			$query->where('applications.accept_date', '<=', $accept_date_to . ' 23:59:59');
		}

		if($activate_date_from = array_get($data, 'activate_date_from')) {
			$query->where('applications.activate_date', '>=', $activate_date_from . ' 00:00:00');
		}
		if ($activate_date_to = array_get($data, 'activate_date_to')) {
			$query->where('applications.activate_date', '<=', $activate_date_to . ' 23:59:59');
		}
        
        if ($type = array_get($data, 'type')) {
            switch($type) {
                case self::TYPE_NORMAL:
                    $query->where('is_fast', 0);
                    $query->where('is_smev', 0);
                    break;
                case self::TYPE_FAST:
                    $query->where('is_fast', 1);
                    $query->where('is_smev', 0);
                    break;
                case self::TYPE_SMEV:
                    $query->where('is_fast', 0);
                    $query->where('is_smev', 1);
            }
        }
		
		//department filters
		$departments = array_get($data, 'departments');
		if(!empty($departments)) {
			if(!is_array($departments)) {
				$departments = [$departments];
			}
			$query
				->whereExists(function ($query) use ($departments) {
					$query->select(DB::raw(1))
						->from('application_agreements')
						->whereRaw('application_agreements.application_id = applications.id');
					if(count($departments) === 1) {
						$query->where('application_agreements.department_id', $departments);
					}else {
						$query->whereIn('application_agreements.department_id', $departments);
					}
				});
		}

		return $query;
	}

    /**
     * Filter for active apps report
     * @param $query
     * @param $data
     * @return mixed
     */
    public static function allReportsFilter($query, $data)
    {
        return $query;
    }

	/**
	 * @param mixed $query
	 * @param User $user
	 * @param int $status
	 * @param array $data
	 * @return mixed
	 */
	public static function adminRoleFilter($query, $user, $status, $data)
	{
		//for not admin show only binded not new applications, without showAll param
		$showAll = array_get($data, 'showAll');
		if (
			$user->role_id !== Role::ROLE_ADMIN &&
			$user->role_id !== Role::ROLE_DEPARTMENT_AGENT &&
//			$status != Application::STATUS_NEW &&
            ($status != Application::STATUS_NEW && $status != -1) &&
			!$showAll
		) {
			$query->where('applications.admin_id', $user->id);
		}

		//for department agent - show all new and applications user works on
		if (
			$user->role_id === Role::ROLE_DEPARTMENT_AGENT &&
			$status != Application::STATUS_NEW &&
			!$showAll
		) {
			$query->where('application_agreements.user_id', $user->id);
		}


		if ($user->role_id === Role::ROLE_DEPARTMENT_AGENT) {
			if($department_id = $user->department_id) {
			//if ($department_id = array_get($data, 'department_id')) {
				//$query->leftjoin('application_agreements', 'application_agreements.application_id', '=', 'applications.id');
				$query->where('application_agreements.department_id', $department_id);
			}
		}

		return $query;
	}

	/**
	 * Apply sorting
	 * @param mixed $query
	 * @param array $data
	 * @return mixed
	 */
	public static function adminSorting($query, $data)
	{
		$isFast = array_get($data, 'is_fast', 0);
		$sortCol = array_get($data, 'sort_column', 'applications.updated_at');
		if($isFast && $sortCol === 'users.name') {
			$sortCol = 'fast_applications.name';
		}
		if($sortCol === 'v.number') {
			$sortCol = 'applications.real_number';
		}

		$sortType = array_get($data, 'sort_type', 'asc');

		$query->orderBy($sortCol, $sortType);

		return $query;
	}

	/**
	 * Get query for control apps filter
	 * @return mixed
	 */
	public static function controlStatusQuery()
	{
		$query = Application::notTemplateQuery()
			->where(function ($q) {
				$q->where('status', Application::STATUS_ACTIVE)
					->orWhere('status', Application::STATUS_EXPIRED);
			});

		return $query;
	}

	/**
	 * Filter control apps
	 * @param mixed $query
	 * @param array $data
	 * @return mixed
	 */
	public static function controlDataFilter($query, $data)
	{
		$application_id = array_get($data, 'application_id');
        $form_id = array_get($data, 'form_id');
		$number = array_get($data, 'number');

		//emtpy result if no search params set @todo change
		if (!$application_id && !$number && !$form_id) {
			$query->where('status', -1);
			return $query;
		}

		$query->where('is_locked', 0);
		$date = date('Y-m-d');
		$query->where('finish_date', '>=', $date);

		//get real id from formatted id, get by app id
		if ($application_id) {
			$formattedId = Application::formattedIdToId($application_id);
			$query->where('applications.id', $formattedId);
		}
		
		if($form_id) {
		    $query->where('form_id', $form_id);
        }

		//get by vehicle number
		if ($number) {
			$query->where('applications.real_number', 'like', '%' . $number . '%');
		}

		return $query;
	}

	/**
	 * @return mixed
	 */
	public static function cafapStatusQuery()
	{
		$query = Application::notTemplateQuery()
			->where(function ($q) {
				$q->where('status', Application::STATUS_ACTIVE)
					->orWhere('status', Application::STATUS_EXPIRED);
			});

		return $query;
	}

	/**
	 * Filter control apps
	 * @param mixed $query
	 * @param array $data
	 * @return mixed
	 */
	public static function cafapDataFilter($query, $data)
	{
		$number = array_get($data, 'number');
		$date = array_get($data, 'date');

		$query->where('is_locked', 0);
		$query->where('finish_date', '>=', $date);
		$query->where('start_date', '<=', $date);

		//get by vehicle number
		if ($number) {
			self::numberFilterStrong('applications', $number, $query);
		}

		return $query;
	}

	/**
	 * Formatted app num to int id
	 * @param string $formatted
	 * @return int|null
	 */
	public static function formattedIdToId($formatted)
	{
		if (strlen($formatted) > 6) {
			$strId = substr($formatted, -6);
			return (int)$strId;
		}
		return $formatted;
	}

	/**
	 * @param $number
	 * @param $query
	 */
	private static function numberFilterStrong($table, $number, &$query)
	{
		$formattedNumber = VehicleNumberHelper::formatNumber($number);
		$query->where($table . '.real_number', $formattedNumber);
	}

	/**
	 * @param $number
	 * @param $query
	 */
	private static function numberFilterLike($table, $number, &$query)
	{
		$formattedNumber = VehicleNumberHelper::formatNumber($number);
		$query->where($table . '.real_number', 'like', '%' . $formattedNumber . '%');
	}

	/**
	 * @param User $user
	 * @param array $data
	 * @param User $admin
	 * @return self|null
	 */
	public static function createByVehicles($user, $data, $admin = null)
	{
		$vehicleId = $data['vehicle']['id'] ?? null;
		$usePrivilegeStatus = array_get($data, 'use_privilege_status');

		/** @var Vehicle $vehicle */
		$vehicle = Vehicle::find($vehicleId);
		$applicationData = [
			'vehicle_id' => $vehicleId,
			'real_number' => $vehicle->real_number
		];

		if(!empty($user)) {
			$applicationData['username'] = $user->getNameWithOwner();
			if ($user->role_id == Role::ROLE_FIRM_USER) {
				$applicationData['employee_id'] = $user->id;
				$applicationData['user_id'] = $user->owner_id;
			} else {
				$applicationData['user_id'] = $user->id;
			}
			$applicationData['is_fast'] = 0;
		}else{
            $applicationData['is_smev'] = array_get($data, 'is_smev', 0);
			$applicationData['is_fast'] = $applicationData['is_smev'] ? 0 : 1;
			$applicationData['is_draft'] = 0;
			$applicationData['admin_id'] = $admin ? $admin->id : null;

			$applicationData['status'] = $applicationData['is_smev'] ? self::STATUS_NEW : self::STATUS_REVIEW;
			
			if(isset($data['username'])) {
				$applicationData['username'] = $data['username'];
			}
		}
  


		//privilege status
		if ($vehicleId && $usePrivilegeStatus) {
			$privilegeStatusId = PrivilegeStatus::findByVehicleId($vehicleId);
			if ($privilegeStatusId) {
				$applicationData['privilege_status_id'] = $privilegeStatusId;
			}
		}

		//special condition
		if (array_get($data, 'use_special_condition')) {
			$specialCondition = SpecialCondition::find(1);
			if ($specialCondition) {
				$applicationData['special_condition_id'] = $specialCondition->id;
			}
		}

		$application = Application::create($applicationData);
		$trailersData = array_get($data, 'trailers');
		if (!empty($trailersData)) {
			ApplicationTrailer::createApplicationBinds($application, $trailersData);
		}

		return $application;
	}

	/**
	 * @param array $ids
	 * @param User $user
	 */
	public static function multipleToWork($ids, $user)
	{
//		Application::whereIn('id', $ids)
//			->update([
//				'status' => self::STATUS_REVIEW,
//				'admin_id' => $user->id
//			]);
		/** @var Application[] $applications */
		$applications = Application::whereIn('id', $ids)->get();
		if(!empty($applications)) {
			foreach ($applications as $application) {
				$application->toWork($user);
			}
		}
	}

	/**
	 * Check if there are application, where vehicle or trailer used
	 * @param $userId
	 * @param Vehicle $vehicle
	 * @return bool
	 */
	public static function checkVehicleUsed($userId, $vehicle)
	{
		if (!$vehicle->is_trailer) {
			$vehicleCount = Application
				::where('user_id', $userId)
				->where('vehicle_id', $vehicle->id)
				->count();
			if ($vehicleCount) {
				return true;
			}
		} else {
			$trailersCount = Application
				::leftjoin('application_trailers', 'application_trailers.application_id', '=', 'applications.id')
				->where('user_id', $userId)
				->where('application_trailers.vehicle_id', $vehicle->id)
				->count();
			if ($trailersCount) {
				return true;
			}
		}

		return false;
	}

	/**
	 * Update application by vehicles data
	 * @param array $data request data with vehicles
	 */
	public function updateByVehicles($data)
	{
		$vehicleId = $data['vehicle']['id'] ?? null;



		$usePrivilegeStatus = array_get($data, 'use_privilege_status');

		//check if we need clear load
		if ($this->vehiclesChanged($data)) {
			if ($this->application_load) {
				$this->application_load->delete();
			}

			//remove all trailers bound @todo change it
			ApplicationTrailer::where('application_id', $this->id)->delete();
			$trailersData = array_get($data, 'trailers');
			if (!empty($trailersData)) {
				ApplicationTrailer::createApplicationBinds($this, $trailersData);
			}
		}

		$realNumber = null;
		if($vehicleId) {
			/** @var Vehicle $vehicle */
			$vehicle = Vehicle::find($vehicleId);
			$realNumber = $vehicle->real_number;
		}
		$applicationData = [
			'vehicle_id' => $data['vehicle']['id'] ?? null,
			'real_number' => $realNumber
		];


		//privilege status
		$applicationData['privilege_status_id'] = null;
		if ($vehicleId && $usePrivilegeStatus) {
			$privilegeStatusId = PrivilegeStatus::findByVehicleId($vehicleId);
			if ($privilegeStatusId) {
				$applicationData['privilege_status_id'] = $privilegeStatusId;
			}
		}

		//special condition
		$applicationData['special_condition_id'] = null;
		if (array_get($data, 'use_special_condition')) {
			$specialCondition = SpecialCondition::find(1);
			if ($specialCondition) {
				$applicationData['special_condition_id'] = $specialCondition->id;
			}
		}

		$this->update($applicationData);
	}

	/**
	 * Check if application vehicles list was changed
	 * @param array $data
	 * @return boolean
	 */
	private function vehiclesChanged($data)
	{
		$newVehicleId = $data['vehicle']['id'] ?? null;

		if ($this->vehicle_id != $newVehicleId) {
			return true;
		}

		if (count($this->application_trailers) != count($data['trailers'])) {
			return true;
		}
		for ($i = 0; $i < count($this->application_trailers); $i++) {
			$trailerId = $data['trailers'][$i]['id'] ?? null;
			if ($this->application_trailers[$i]['vehicle_id'] != $trailerId) {
				return true;
			}
		}

		return false;
	}

	public function updateRoute($data)
	{
		if (isset($data['markers'][0])) {
			$this->start_address = $data['markers'][0]['text'];
		}
		if (isset($data['markers'][count($data['markers']) - 1]['text'])) {
			$this->finish_address = $data['markers'][count($data['markers']) - 1]['text'];
		}
		$this->location_type = array_get($data, 'location_type', Application::LOCATION_LOCAL);
		//@todo save route length
		return $this->save();
	}

	public function updateDates($data)
	{
		if (isset($data['start_date'])) {
			$this->start_date = $data['start_date'];
		}
		if (isset($data['finish_date'])) {
			$this->finish_date = $data['finish_date'];
		}

		//?
		if ($this->status === Application::STATUS_DECLINE) {
			$this->status = Application::STATUS_REPEAT;
		}
	}

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class);
	}

	public function employee()
	{
		return $this->belongsTo(\App\Models\User::class, 'employee_id');
	}

	public function admin()
	{
		return $this->belongsTo(\App\Models\User::class, 'admin_id');
	}

	public function vehicle()
	{
		return $this->belongsTo(\App\Models\Vehicle::class);
	}

	public function trailer()
	{
		return $this->belongsTo(\App\Models\Vehicle::class, 'trailer_id');
	}

	public function application_trailers()
	{
		return $this->hasMany(\App\Models\ApplicationTrailer::class);
	}

	public function frozen_vehicle()
	{
		return $this->hasOne(\App\Models\ApplicationVehicle::class)
			->where('application_vehicles.is_trailer', 0);
	}
	public function frozen_trailers()
	{
		return $this->hasMany(\App\Models\ApplicationVehicle::class)
			->where('application_vehicles.is_trailer', 1);
	}

	public function files()
	{
		return $this->hasMany(\App\Models\File::class);
	}

	public function special_condition()
	{
		return $this->belongsTo(\App\Models\SpecialCondition::class, 'special_condition_id');
	}

	public function privilege_status()
	{
		$date = date('Y-m-d');
		return $this->belongsTo(\App\Models\PrivilegeStatus::class, 'privilege_status_id')
			->where('privilege_statuses.start_date', '<=', $date)
			->where('privilege_statuses.finish_date', '>=', $date);
	}

	public function trailers()
	{
		return $this->belongsToMany(\App\Models\Vehicle::class, 'application_trailers', 'application_id', 'vehicle_id');
	}

	public function application_dates()
	{
		return $this->hasOne(\App\Models\ApplicationDate::class);
	}

	public function application_load()
	{
		return $this->hasOne(\App\Models\ApplicationLoad::class);
	}

	public function application_route()
	{
		return $this->hasOne(\App\Models\ApplicationRoute::class);
	}

	public function fast_application()
	{
		return $this->hasOne(\App\Models\FastApplication::class);
	}

    public function smev_application()
    {
        return $this->hasOne(\App\Models\ApplicationSmev::class);
    }

	public function audits()
	{
		return $this
			->hasMany(\App\Models\Audit\Audit::class, 'auditable_id')
			->where('auditable_type_key', Audit::KEY_APPLICATION);
	}

	public function audits_active()
	{
		return $this->audits()
			->whereIn('event_key', [
				Events::APPLICANT_APP_TOREVIEW_KEY,
				Events::ADMINGBU_APP_TODEPARTMENT_KEY,
				Events::ADMINGBU_APP_ACCEPT_KEY,
				Events::ADMINGBU_APP_ACTIVATE_KEY,

				Events::DEPAGENT_APP_ACCEPT_KEY
			]);
	}

	//templates

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasOne
	 */
	public function permit()
	{
		return $this->hasOne(\App\Models\Permit::class);
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function control_marks()
	{
		return $this->hasMany(\App\Models\ControlMark::class);
	}

	/**
	 * @return null|string
	 */
	public function startDateFormatted()
	{
		return $this->dbDateToMyDate($this->start_date);
	}

	/**
	 * @return null|string
	 */
	public function finishDateFormatted()
	{
		return $this->dbDateToMyDate($this->finish_date);
	}

	/**
	 * @param null $format
	 *
	 * @return string
	 */
	public function getActivateDate($format = null)
	{
		if(!$format) {
			$format = $this->myDateFormat;
		}
		if($this->activate_date) {
			return $this->changeDateFormat($this->activate_date, $this->fullDateTimeFormat, $format);
		}

		return $this->updated_at->format($format);
	}

	public function dbDateToMyDate($date, $format = null)
	{
		if (!$date) {
			return null;
		}
		$date = DateTime::createFromFormat($this->dbDateFormat, $date);
		return $date->format($this->myDateFormat);
	}

	/**
	 * @param $date
	 * @param $sourceFormat
	 * @param $targetFormat
	 * @return null|string
	 */
	public function changeDateFormat($date, $sourceFormat, $targetFormat) {
		$date = DateTime::createFromFormat($sourceFormat, $date);
		if(!$date) {
			return null;
		}
		return $date->format($targetFormat);
	}

	/**
	 * @return null|string
	 */
	public function updatedAtFormatted($format = null)
	{
		if(!$format) {
			$format = $this->dateTimeFormat;
		}
		return $this->updated_at ? $this->updated_at->format($format) : null;
	}

	public function application_agreements()
	{
		return $this->hasMany(\App\Models\ApplicationAgreement::class);
	}

	/**
	 * Recalculate application status by agreements statuses
	 */
	public function recalculateStatusByAgreements()
	{
		$oldStatus = $this->status;

		/** @var ApplicationAgreement[] $agreements */
		$agreements = $this->application_agreements;

		if (!empty($agreements)) {
			$allAccepted = true;
			$anyDecline = false;
			foreach ($agreements as $agreement) {
				if ($agreement->status !== ApplicationAgreement::STATUS_ACCEPTED) {
					$allAccepted = false;
				}
				if ($agreement->status === ApplicationAgreement::STATUS_DECLINED) {
					$anyDecline = true;
				}
			}

			if ($allAccepted) {
				$this->status = Application::STATUS_ACCEPTED;
			}
			if ($anyDecline) {
				$this->status = Application::STATUS_DECLINE;
			}
		}

		return $this->status !== $oldStatus;
	}

	/**
	 * Save template
	 * @return Application
	 */
	public function saveToTemplate()
	{
		/** @var Application $template */
		$template = Application::isTemplateQuery()->where('template_id', $this->id)->first();
		if (!$template) {
			$template = $this->replicate(['id', 'is_template', 'template_id']);
			$template->is_template = Application::TEMPLATE_YES;
			$template->template_id = $this->id;
			$template->save();
			//save relations
			$this->copyTemplateRelations($template->id);
		} else {
//            $newTemplate = $application->replicate(['id', 'is_template', 'template_id']);
//            $newTemplate->id = $template->id;
//            $newTemplate->save();
		}

		return $template;
	}

	private function copyTemplateRelations($copyId)
	{
		//copy loads
		Application::copyApplicationBindModel($this->application_load, $copyId);
		Application::copyApplicationBindModel($this->application_route, $copyId);
		Application::copyApplicationBindModel($this->application_dates, $copyId);

		//copy trailers boundss
		$currentTrailers = $this->application_trailers;
		foreach ($currentTrailers as $currentTrailer) {
			Application::copyApplicationBindModel($currentTrailer, $copyId);
		}

		//files
        $files = $this->files;
        foreach ($files as $file) {
            Application::copyApplicationBindModel($file, $copyId);
        }
	}

	/**
	 * @param Model $bindModel
	 * @param int $copyId
	 * @return Model
	 */
	private static function copyApplicationBindModel($bindModel, $copyId)
	{
		if (!$bindModel) {
			return null;
		}
		$bindCopy = $bindModel->replicate(['id', 'application_id']);
		$bindCopy->application_id = $copyId;
		$bindCopy->save();

		return $bindCopy;
	}

	/**
	 * @return Application
	 */
	public function templateToNewApplication()
	{
		/** @var Application $application */
		$application = $this->replicate(['id', 'is_template', 'template_id']);
		$application->is_template = Application::TEMPLATE_NO;
		$application->template_id = null;
//		$application->form_id = null;
		$application->status = Application::STATUS_NEW;
		$application->save();

		//save relations
		$this->copyTemplateRelations($application->id);

		return $application;
	}

	/**
	 * Withdraw application from admin review to user rework
	 * @return bool
	 */
	public function withdraw()
	{
		$this->status = Application::STATUS_NEW;
		$this->is_draft = Application::IS_DRAFT_YES;
		if (!$this->save()) {
			return false;
		}
		if (Role::isApplicant()) {
			$this->newAuditEvent(Events::APPLICANT_APP_WITHDRAW);
		}
		return true;
	}

	/**
	 * Send app to admin review
	 * @return bool
	 */
	public function toReview()
	{
		$this->status = Application::STATUS_NEW;
		$this->is_draft = Application::IS_DRAFT_NO;

		//recalculate price
        $priceCalculator = new PriceCalculator($this);
        $this->price = $priceCalculator->calculatePrice();
        $priceInfo = $priceCalculator->getPriceInfo();
        $this->spring_price = $priceCalculator->calculatePrice(1);
        $springPriceInfo = $priceCalculator->getPriceInfo();
        $priceInfo = array_merge($priceInfo, $springPriceInfo);
        $this->price_info = $priceInfo;

		if (!$this->save()) {
			return false;
		}
		if (Role::isApplicant()) {
			$this->newAuditEvent(Events::APPLICANT_APP_TOREVIEW);
		}
		return true;
	}

	public function delete()
	{
		$res = parent::delete();
		if (Role::isApplicant()) {
			$this->newAuditEvent(Events::APPLICANT_APP_SOFT_DELETE);
		}
		return $res;
	}

	public function restore()
	{
	    $this->deleted_at = null;
        $res = $this->save();
//		$res = parent::restore();
		if (Role::isAdmin()) {
			$this->newAuditEvent(Events::ADMIN_APP_RESTORE);
		}
		return $res;
	}

	/**
	 * Get to work by admin or department agent
	 * @param User $user
	 * @return boolean
	 */
	public function toWork($user)
	{
		$success = false;

		if ($user->role_id === Role::ROLE_DEPARTMENT_AGENT) {
			/** @var ApplicationAgreement $agreement */
			$agreement = ApplicationAgreement
				::where('application_id', $this->id)
				->where('department_id', $user->department_id)
				->first();
			if ($agreement) {
				$success = $agreement->toWork($user);
			}
		} else {
			if(
				$this->status !== Application::STATUS_NEW &&
				$this->status !== Application::STATUS_REPEAT &&
				$this->admin_id &&
				$user->id !== $this->admin_id
			) {
				$success = false;
			} else {
				$this->status = Application::STATUS_REVIEW;
				$this->admin_id = $user->id;
				$success = $this->save();
			}
		}

		if ($success) {
			if (Role::isAdminOrGbu()) {
				$this->newAuditEvent(Events::ADMINGBU_APP_TOWORK);
			}
		}

		return $success;
	}

	/**
	 * Accept application
	 * @param User $user
	 * @param array $data
	 * @param int $status
	 * @return bool
	 */
	public function accept($user, $data, $status = Application::STATUS_ACCEPTED)
	{
		//@todo refactoring
		$this->status = $status;
		$this->admin_id = $user->id;
		$this->accept_date = date('Y-m-d H:i:s');
		if (array_get($data, 'comment')) {
			$this->comment = array_get($data, 'comment');
		}
		$is_spring = array_get($data, 'is_spring', false);
		if ($is_spring !== false) {
			$this->is_spring = $is_spring;
		}
		//start and finish address
		$points = array_get($data, 'waypoints');
		if (!empty($points)) {
			$this->start_address = $points[0]['text'];
			$this->finish_address = $points[count($points) - 1]['text'];
		}

		//store special condition, ecp
		if ($this->isSmev() && $this->smev_application) {
		    $this->smev_application->special_conditions = array_get($data, 'special_conditions');
		    $this->smev_application->ecp = array_get($data, 'ecp');
		    $this->smev_application->save();
        }

		$success = $this->save();

		if ($success) {
			//fix application info
			$this->freezeData();

			if (Role::isAdminOrGbu()) {
				$changeStatus = intval(array_get($data, 'changeStatus', 1));
				if ($changeStatus) {
					$this->newAuditEvent(Events::ADMINGBU_APP_ACCEPT);
				} else {
					$this->newAuditEvent(Events::ADMINGBU_CHANGE_PATH);
				}
			}
		}

		//generate invoice
		if ($success) {
			$permit = Permit::where('application_id', $this->id)->first();
			if (!$permit) {
				$permit = new Permit(['application_id' => $this->id]);
			}

			$permit->activation_date = date('Y-m-d H:i:s');
			$excelHelper = new ExcelHelper();
			$permit->invoice_link = $excelHelper->generateInvoice($this);
			$permit->save();
		}

		return $success;
	}

	/**
	 * Activate application
	 * @param User $user
	 * @return bool
	 */
	public function activate($user)
	{
		$nowDate = Carbon::now();
		$time = $nowDate->format(' H:i:s');
		$startDate = Carbon::createFromFormat('Y-m-d H:i:s', $this->start_date.$time);
		if ($nowDate > $startDate) {
			// if accept(now) date bigger than start date, change start and finish
			$finishDate = Carbon::createFromFormat('Y-m-d H:i:s', $this->finish_date.$time);
			$num = $startDate->diffInDays($nowDate);
			$this->start_date = $startDate->addDays($num)->format('Y-m-d');
			$this->finish_date = $finishDate->addDays($num)->format('Y-m-d');
		}
		$this->status = Application::STATUS_ACTIVE;
		$this->admin_id = $user->id;

//		$this->accept_date = $nowDate->format('Y-m-d H:i:s');
		$this->activate_date = $nowDate->format('Y-m-d H:i:s');
		$success = $this->save();


		if ($success) {
			if (Role::isAdminOrGbu()) {
				$this->newAuditEvent(Events::ADMINGBU_APP_ACTIVATE);
			}
		}
		return $success;

	}

	/**
	 * Freeze data
	 */
	public function freezeData()
	{
		//create permit
		Permit::createForApp($this);

		//create frozen vehicles
		$this->freezeVehicles();

		//store real number for future search
		$this->real_number = $this->vehicle->real_number;
		$this->timestamps = false;

		$this->save();
	}

	/**
	 *
	 */
	private function freezeVehicles()
	{
		ApplicationVehicle::where('application_id', $this->id)->delete();

		ApplicationVehicle::createByVehicle($this, $this->vehicle);
		$this->load(['trailers.vehicle_brand', 'trailers.vehicle_model']);
		foreach($this->trailers as $trailer) {
			ApplicationVehicle::createByVehicle($this, $trailer);
		}
	}

	/**
	 * @return bool
	 */
	public function isFrozen()
	{
		return
			$this->status === self::STATUS_ACCEPTED_WITH_CHANGES ||
			$this->status === self::STATUS_ACCEPTED ||
			$this->status === self::STATUS_ACTIVE ||
			$this->status === self::STATUS_EXPIRED;
	}

	/**
	 * Decline application
	 * @param User $user
	 * @param array $data
	 */
	public function decline($user, $data)
	{
		$this->status = Application::STATUS_DECLINE;
		$this->admin_id = $user->id;
		$this->comment = array_get($data, 'note');

        //store special condition, ecp
        if ($this->isSmev() && $this->smev_application) {
            $this->smev_application->special_conditions = array_get($data, 'special_conditions');
            $this->smev_application->ecp = array_get($data, 'ecp');
            $this->smev_application->save();
        }

		$success = $this->save();

        if ($success) {
			if (Role::isAdminOrGbu()) {
				$this->newAuditEvent(Events::ADMINGBU_APP_DECLINE);
			}
		}
		return $success;
	}

    /**
     * @param User $user
     * @param array $data
     * @return bool
     */
	public function awaitingAttachments($user, $data) {
        $this->status = Application::STATUS_AWAITING_ATTACHMENTS;
        $this->admin_id = $user->id;
        $this->comment = array_get($data, 'note');

        //store special condition, ecp
        if ($this->isSmev() && $this->smev_application) {
            $this->smev_application->special_conditions = array_get($data, 'special_conditions');
            $this->smev_application->ecp = array_get($data, 'ecp');
            $this->smev_application->save();
        }

        $success = $this->save();

        return $success;
    }

	/**
	 * @return bool
	 */
	public function isExpired()
	{
		$nowDate = DateTime::createFromFormat('Y-m-d', date('Y-m-d'));
		$finishDate = DateTime::createFromFormat('Y-m-d', $this->finish_date);

		return $nowDate > $finishDate;
	}

	/**
	 * @return bool
	 */
	public function runsUsed()
	{
		if (!$this->application_dates) {
			return false;
		}

		return $this->application_dates->runs_count <= $this->application_dates->runs_used;
	}

	/**
	 * @param PrivilegeVehicle $privilegeVehicle
	 * @return bool
	 */
	public static function handlePrivilegeVehicleRemove($privilegeVehicle)
	{
		$success = false;

		$changeIds = [];
		/** @var Application[] $applications */
		$applications = self
			::select('applications.*')
			->distinct()
			->leftjoin('vehicles', 'applications.vehicle_id', '=', 'vehicles.id')
			->where('privilege_status_id', $privilegeVehicle->privilege_status_id)
			->where('vehicles.number', $privilegeVehicle->number)
			->where('vehicles.regions', $privilegeVehicle->region)
			->get();


		foreach ($applications as $application) {
			if (
				$application->status !== self::STATUS_ACTIVE
			) {
				$changeIds[] = $application->id;
			}
		}

		if (!empty($changeIds)) {
			$success = self::whereIn('id', $changeIds)->update(['status' => self::STATUS_DECLINE]);
		}

		return $success;
	}

	public function getFormattedIdAttribute()
	{
		return $this->getFormattedId();
	}

	/**
	 * @param $id
	 * @param null $created_at
	 * @param bool $isFast
	 * @param bool $isSmev
	 * @return string
	 */
	public static function idToFormattedId($id, $created_at = null, $isFast = false, $isSmev = false)
	{
		$year = null;
		if($created_at) {
			$parts = explode('-', $created_at);
			if(count($parts) > 1) {
				$year = intval($parts[0]) % 100;
			}
		}
		if(!$year) {
			$year = date('y');
		}

		$formattedId = str_pad($id, 6, 0, STR_PAD_LEFT);
		$appCode = self::getAppPrefix($isFast, $isSmev);
		return $year . '-' . $appCode . $formattedId;
	}

	/**
	 * @param bool $isFast
	 * @param bool $isSmev
	 * @return string
	 */
	public static function getAppPrefix($isFast, $isSmev) {
	    if($isFast) {
	        return self::DOCUMENTS_APP_FAST_CODE;
        }
        if($isSmev) {
	        return self::DOCUMENTS_APP_SMEV_CODE;
        }
		return self::DOCUMENTS_APP_CODE;
	}

	/**
	 * @return string
	 */
	public function getFormattedId()
	{
		return self::idToFormattedId($this->id, $this->getCreatedAtFormatted(), $this->isFast(), $this->isSMev());
	}

	/**
	 * @return null|string
	 */
	public function getUserName()
	{
		if($this->username) {
			return $this->username;
		}
		if($this->isFast()) {
			return $this->fast_application->name ?? null;
		}
		return $this->user->getName() ?? null;
	}

	/**
	 * @return null|string
	 */
	public function getUserAddress()
	{
		if($this->isFast())
			return $this->fast_application->address ?? null;

		if($this->isSmev())
		    return $this->username;

		if($this->user)
		    return $this->user->getAddress() ?? null;
		return '';
	}

	/**
	 * @return null|string
	 */
	public function getUserInn()
	{
		if($this->isFast()) {
			return $this->fast_application->inn ?? null;
		}
        if($this->isSmev()) {
            return $this->username;
        }
		return $this->user ? $this->user->getInn() : null;
	}

	/**
	 * @return null|string
	 */
	public function getIssuePlaceTitle()
	{
		$issuePlace = $this->application_dates ? $this->application_dates->issue_place : null;
		return $issuePlace ? $issuePlace->title : null;
	}

	/**
	 * @return bool
	 */
	public function isFast() {
		return (bool)$this->is_fast;
	}
    
    /**
     * @return bool
     */
	public function isSmev() {
	    return (bool)$this->is_smev;
    }


	//get vehicles data

	/**
	 * @return bool
	 */
	public function hasFrozenData()
	{
		if($this->isFast()) {
			return false;
		}
		return $this->frozen_vehicle ? true : false;
	}

	/**
	 * @return string
	 */
	public function getVehicleBrandTitle()
	{
		if($this->isFrozen() && $this->hasFrozenData()) {
			return $this->frozen_vehicle ? $this->frozen_vehicle->brand_title : '';
		}
		return $this->vehicle ? $this->vehicle->getBrandTitle() : '';
	}

	/**
	 * @return string
	 */
	public function getVehicleModelTitle()
	{
		if($this->isFrozen() && $this->hasFrozenData()) {
			return $this->frozen_vehicle ? $this->frozen_vehicle->model_title : '';
		}
		return $this->vehicle ? $this->vehicle->getModelTitle() : '';
	}

	/**
	 * @return string
	 */
	public function getVehicleRealNumber()
	{
		if($this->isFrozen()) {
			return $this->real_number ?? '';
		}
		return $this->vehicle ? $this->vehicle->real_number : '';
	}

	/**
	 * Get vehicle full info
	 */
	public function getVehicleFullInfo()
	{
		return $this->getVehicleBrandTitle() . ' ' . $this->getVehicleModelTitle() . ' ' . $this->getVehicleRealNumber();
	}

	/**
	 * Get trailers full info
	 * @param string $glue
	 * @return string
	 */
	public function getTrailersFullInfo($glue = ', ')
	{
		$result = '';

		if($this->isFrozen() && $this->hasFrozenData()) {
			foreach($this->frozen_trailers as $trailer) {
				$result .= $glue . $trailer->getFullInfo();
			}
		}else{
			foreach($this->trailers as $trailer) {
				$result .= $glue . $trailer->getFullInfo();
			}
		}

		return $result;
	}

	/**
	 * @return string
	 */
	public function getVehicePtsWeightFormatted()
	{
		if($this->isFrozen() && $this->hasFrozenData()) {
			return number_format($this->frozen_vehicle->pts_weight, 1);
		}
		return $this->vehicle ? $this->vehicle->getPtsWeightFormatted() : '';
	}

	/**
	 * @return float|int
	 */
	public function getTrailersTotalPtsWeight()
	{
		$result = 0;

		if($this->isFrozen() && $this->hasFrozenData()) {
			foreach($this->frozen_trailers as $trailer) {
				$result += $trailer->pts_weight;
			}
		}else{
			foreach($this->trailers as $trailer) {
				$result += $trailer->pts_weight;
			}
		}

		return $result;
	}

	/**
	 * @return float
	 */
	public function getVehiclesEmptyWeight()
	{
		$result = $this->vehicle ? $this->vehicle->pts_weight : 0;

		foreach ($this->trailers as $trailer) {
			$result += $trailer->pts_weight;
		}

		return $result;
	}

	public function getAdminName() {
	    return $this->admin ? $this->admin->getName() : '';
    }

    public function getAdminPosition() {
	    return $this->admin ? $this->admin->getPosition() : '';
    }

    public function getComment() {
        return $this->comment;
    }

    public function getRouteText() {
        $routeText = '';
        if($this->privilege_status){
            $routeText = $this->privilege_status->route_info;
        }else{
            if($this->application_route){
                $routeText = $this->application_route->getRouteText();
            }
        }

        return $routeText;
    }

	/**
	 * Lock app
	 * @return bool
	 */
	public function lock()
	{
		$this->is_locked = self::LOCKED_YES;
		$success = $this->save();
		if ($success && Role::isAdmin()) {
			$this->newAuditEvent(Events::ADMIN_APP_LOCK);
		}
		return $success;
	}

	/**
	 * Unlock app
	 * @return bool
	 */
	public function unlock()
	{
		$this->is_locked = self::LOCKED_NO;
		$success = $this->save();
		if ($success && Role::isAdmin()) {
			$this->newAuditEvent(Events::ADMIN_APP_UNLOCK);
		}
		return $success;
	}

	/**
	 * @return float
	 */
	public function getRealPrice()
	{
		return $this->is_spring ? $this->spring_price : $this->price;
	}

	/**
	 * Is appliation with spring restrict
	 * @param Application $application
	 * @param SpecialCondition $springCondition
	 *
	 * @return bool
	 */
	public static function isSpringApplication($application, $springCondition)
	{
		$applicationStartDate = DateTime::createFromFormat('Y-m-d', $application->start_date);
		$applicationFinishDate = DateTime::createFromFormat('Y-m-d', $application->finish_date);

		$springStartDate = DateTime::createFromFormat('Y-m-d', $springCondition->start_date);
		$springFinishDate = DateTime::createFromFormat('Y-m-d', $springCondition->finish_date);

		return $applicationStartDate >= $springStartDate && $applicationStartDate <= $springFinishDate;
	}

	public static function getCsvExportHeaders()
	{
		return self::$csvExportHeaders;
	}

	/**
	 * @return array
	 */
	public function toCsvExportData()
	{
		$issuePlace = isset($this->application_dates->issue_place) ? $this->application_dates->issue_place->title : '';
		$orgName = $this->getUserName();
		$number = isset($this->vehicle) ? $this->vehicle->realNumber() : '';
		$created_at = $this->created_at ? $this->created_at->format('Y-m-d H:i:s') : '';

		$accepted_at = $this->accept_date;
		//if($this->status === self::STATUS_DECLINE){
		if(!$accepted_at){
			$accepted_at = $this->updatedAtFormatted();
		}

		return [
			$issuePlace,                	//Территориальное управление
			$this->getFormattedId(),    	//Номер заявки
			$this->getStatusTitle(),    	//Статус
			$orgName,                		//Наименование организации
			$number,                		//Гос номер ТС
			$this->price,                   //Сумма, руб
			$created_at,                    //Дата и время поступления
			$accepted_at,                   //Дата и время одобрения / отклонения
		];
	}

	public function getStatusTitle()
	{
		switch ($this->status) {
			case self::STATUS_NEW:
				return 'Новое';
			case self::STATUS_REVIEW:
				return 'На рассмотрении';
			case self::STATUS_DECLINE:
				return 'Отклонено';
			case self::STATUS_ACCEPTED_WITH_CHANGES:
				return 'Одобрено с изменениями';
			case self::STATUS_ACCEPTED:
				return 'Одобрено';
			case self::STATUS_ACTIVE:
				if($this->isExpired()){
					return 'Просрочено';
				}
				return 'Активно';
			case self::STATUS_REPEAT:
				return 'На повторном рассмотрении';
			case self::STATUS_EXPIRED:
				return 'Просрочено';
			case self::STATUS_REMOVED:
				return 'Удалено';
		}
		return '';
	}

	/**
	 * @return string
	 *
	 */
	public function getLocationTypeName()
	{
		return self::getLocationTypeNameByKey($this->location_type);
	}

	/**
	 * @param int $locationType
	 * @return string
	 */
	public static function getLocationTypeNameByKey($locationType)
	{
		switch($locationType) {
			case self::LOCATION_LOCAL:
				return 'местная';
			case self::LOCATION_REGIONAL:
				return 'региональная';
			case self::LOCATION_INTERNATIONAL:
				return 'международная';
		}

		return '';
	}

	/**
	 * @return mixed
	 */
	public static function getStatistic($data)
	{
		$query = Application::notTemplateQuery();

		$query = $query
            ->distinct()
			->selectRaw('count(*) as count, status')
			->groupBy('status')
			->where('is_draft', 0)
			->where('status', '<>', self::STATUS_REMOVED);

		$start = array_get($data,'start_date', null);
		$finish = array_get($data,'finish_date', null);

		if($start && $finish)
            self::dateRange($query, date('Y-m-d', strtotime($start)),date('Y-m-d', strtotime($finish)));

//		var_dump($query->toSql());
//		var_dump($query->getBindings());
//		exit;

        $appCounts = $query->get();

		$result = [];
		foreach(self::$allStatuses as $status) {
			$result[$status] = 0;
		}

		$allCount = 0;
		foreach($appCounts as $appCount) {
			$allCount+=$appCount['count'];
			$result[$appCount['status']] = $appCount['count'];
		}

		$activeNotUsed = self::getActiveNotUsedCount();

		$statistic = [
			'new' => $result[self::STATUS_NEW],
			'work' => $result[self::STATUS_REVIEW],
			'decline' => $result[self::STATUS_DECLINE],
			'repeat' => $result[self::STATUS_REPEAT],
			'accept' => $result[self::STATUS_ACCEPTED] + $result[self::STATUS_ACCEPTED_WITH_CHANGES],

			'active' => $activeNotUsed,
			'used' => $result[self::STATUS_EXPIRED] + $result[self::STATUS_ACTIVE] - $activeNotUsed,

			'all' => $allCount
		];

		return $statistic;
	}

	public static function dateRange($query, $start_date, $finish_date){
//	    $query->where(function($query) use ($start_date, $finish_date){
//            $query->where(function ($q) use ($start_date, $finish_date){
//                $q->where('start_date', '>=', $start_date)
//                    ->where('finish_date','>=',$finish_date)
//                    ->where('start_date','<=', $finish_date);
//            })->orWhere(function($q) use ($start_date, $finish_date){
//                $q->where('start_date', '<=', $start_date)
//                    ->where('finish_date','<=',$finish_date)
//                    ->where('finish_date','>=', $start_date);
//            })->orWhere(function($q) use ($start_date, $finish_date){
//                $q->where('start_date', '>=', $start_date)
//                    ->where('finish_date','<=',$finish_date);
//            })->orWhere(function($q) use ($start_date, $finish_date){
//                $q->where('start_date', '<=', $start_date)
//                    ->where('finish_date','>=',$finish_date);
//            });
//        });

            $query->where(function ($q) use ($start_date, $finish_date){
                $q->where('start_date', '>=', $start_date)
                    ->where('finish_date','<=', $finish_date);
            });

    }


	public static function getActiveNotUsedCount()
	{
		$query = Application::notTemplateQuery();
		$date = date('Y-m-d');

		$appCounts = $query

			->where('is_draft', 0)
			->where('status', self::STATUS_ACTIVE)
			->where('finish_date', '>=', $date)
			->where('start_date', '<=', $date)

			->leftjoin('application_dates', 'application_dates.application_id', '=', 'applications.id')
			->whereRaw('application_dates.runs_count > application_dates.runs_used')

			->count();

		return $appCounts;
	}

	/**
	 * @return Carbon|string
	 */
	public function getCreatedAtFormatted()
	{
		$created_at =  $this->created_at;
		if(!$created_at) {
			return '';
		}
		if(is_string($created_at)){
			return $created_at;
		}
		return method_exists($created_at, 'format') ? $this->created_at->format('Y-m-d H:i:s') : '';
	}

	/**
	 * @param array $filesData
	 * @param int $fileType
	 */
	public function updateFiles($filesData, $fileType)
	{
		//@todo optimize
		$ids = [];
		if(!empty($filesData)){
			foreach($filesData as $fileData){
				$ids[] = $fileData['id'];
			}
		}

		File::where('application_id', $this->id)
			->where('type_id', $fileType)
			->whereNotIn('id', $ids)
			->delete();

		if(!empty($ids)){
			foreach($ids as $id){
				if(intval($id)){
					/** @var File $file */
					$file = File::find($id);
					if($file){
						$file->user_id = $this->user_id;
						$file->application_id = $this->id;
						$file->type_id = $fileType;
						$file->save();
					}
				}
			}
		}
	}

    /**
     * @param string $approvalId
     * @return static|null
     */
	public static function findByApprovalId($approvalId) {
	    return self::leftjoin('application_smevs', 'application_smevs.application_id', '=', 'applications.id')
            ->where('application_smevs.approval_id', $approvalId)
            ->first();
    }

    /**
     * Get smev status
     * @return string|null
     */
    public function getSmevStatus() {
	    $result = null;

	    //declined
	    if ($this->status === self::STATUS_DECLINE) {
	        return self::SMEV_STATUS_DECLINED;
        }

	    //approved
	    if (array_search($this->status, [
                self::STATUS_ACCEPTED_WITH_CHANGES,
                self::STATUS_ACCEPTED,
                self::STATUS_ACTIVE,
                self::STATUS_EXPIRED
            ]) !== false
        ) {
	        return self::SMEV_STATUS_APPROVED;
        }

	    //in progress
        if (array_search($this->status, [
                self::STATUS_REVIEW,
                //self::STATUS_REPEAT,
            ]) !== false
        ) {
//            if ($this->isAwaitingFiles()) {
//                return self::SMEV_STATUS_WAITING_ATTACHMENTS;
//            }
            return self::SMEV_STATUS_IN_PROGRESS;
        }

        //awaiting files
        if ($this->status === self::STATUS_AWAITING_ATTACHMENTS) {
            return self::SMEV_STATUS_WAITING_ATTACHMENTS;
        }

        return null;
    }

    /**
     * Check if application is awaiting files for smev
     * @return bool
     */
    public function isAwaitingFiles() {
        //@todo implement
        return false;
    }

	public function getObjectInfo()
	{
		return $this->getFormattedId();
	}
    public function cancelStatus(){
        return in_array($this->status, [
            self::STATUS_REVIEW,
            self::STATUS_DECLINE,
            self::STATUS_ACCEPTED_WITH_CHANGES,
            self::STATUS_ACCEPTED,
            self::STATUS_ACTIVE
        ]);
    }

    public function getNumberChecked($is_smev = 0){
        $number = $this->updated_at->format('y')."-";
        $number .= SELF::CHECK_NUM;
        $number .= str_pad($this->id, 6, '0', STR_PAD_LEFT);

        if($is_smev)
            return $this->is_smev ? $number : null ;
        else
            return $number;
    }

    public static function parseCheckNumber($number){
        preg_match('/^([0-9]{2})\-'.SELF::CHECK_NUM.'([0-9]+)$/',$number, $preg);
        return isset($preg[2]) ? ['yer' => $preg[1], 'number' => $preg[2], 'id' => (int) $preg[2]] : [] ;
    }

    public static function getApplicationToNuberChecked($number, $is_smev=0){
        $number = self::parseCheckNumber($number);
        $application = self::where('id',array_get($number, 'id', 0));
        if($is_smev){
            $application->where('is_smev', 1);
        }
        return $application->first();
    }

    /**
     * @param bool $binary binary|path
     * @return |null
     */
    public function getFileArchive($binary = true, $base64 = true){
        $files = $this->files;
        if (!$files) return null;
        $binaryData = null;


        $zip = new \ZipArchive();
        $name = storage_path('app/public/smev/' . $this->id) . '/' . $this->getNumberChecked().'.zip';
        if($zip->open($name,\ZipArchive::CREATE) === true){
            foreach ($files as $file){
                $zip->addFile($file->getPath(), $file->getName());
            }

            $zip->close();
        }
        if($binary){
            if (file_exists($name)){
                $file = file_get_contents($name);
                if($base64)
                    return base64_encode($file);
                else
                    return $file;
            }
        }else
            return $name;
    }

    public static function EputsList($data){
        $query = Application::notTemplateQuery();
        $start = date("Y-m-d H:i:s", strtotime(array_get($data, 'start_date')));
        $end = date("Y-m-d H:i:s", strtotime(array_get($data, 'end_date')));
        return $query->where('accept_date', '>=', $start)
            ->where('accept_date', '<=', $end)
            ->where('is_smev','0')
            ->whereIn('status',[Application::STATUS_ACTIVE,Application::STATUS_EXPIRED])
            ->withTrashed();

    }
}