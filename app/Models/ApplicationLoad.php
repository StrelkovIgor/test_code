<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 23 Sep 2018 16:38:12 +0000.
 */

namespace App\Models;

use App\Helpers\AxleGroup;
use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class ApplicationLoad
 * 
 * @property int $id
 * @property int $application_id
 * @property int $type_id
 * @property string $name
 * @property float $length
 * @property float $width
 * @property float $height
 * @property float $weight
 * @property float $load_weight
 * @property int $axles_count
 * @property string $axles_info
 * @property float $radius
 * @property string $escort
 * @property int $escort_count
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\Application $application
 *
 * @package App\Models
 */
class ApplicationLoad extends Eloquent
{
	protected $casts = [
		'application_id' => 'int',
		'type_id' => 'int',
		'length' => 'float',
		'width' => 'float',
		'height' => 'float',
		'weight' => 'float',
		'load_weight' => 'float',
		'load_dimension' => 'string',
		'axles_count' => 'int',
		'radius' => 'float',
		'escort_count' => 'int',
	];

	protected $fillable = [
		'application_id',
		'type_id',
		'name',
		'length',
		'width',
		'height',
		'weight',
		'load_weight',
		'load_dimension',
		'axles_count',
		'axles_info',
        'radius',
        'escort',
		'escort_count'
	];
	
	protected static $escortInfo = [
		'Без сопровождения',
		'Один автомобиль прикрытия спереди',
		'Один автомобиль прикрытия спереди и один сзади',
		'Два автомобиля прикрытия спереди и один сзади',
	];
	
	protected static $loadTypes = [
		'Инертные материалы',
		'Пищевые продукты',
		'Металлоконструкция',
		'Сельхозпродукция',
		'Горюче-смазочные материалы',
		'Железо-бетонные изделия',
		'Пиломатериал',
		'Опасный груз',
		'Товары бытового назначения',
		'Промышленные товары',
		'Вагончик',
		'Бытовка',
		'Специализированная техника',
		'Другое',
		'Животные',
		'Лекарственные препараты',
		'Семенной фонд',
		'Удобрения',
		'Почта и почтовые грузы',
	];

	public function application()
	{
		return $this->belongsTo(\App\Models\Application::class);
	}
	
	/**
	 * @param Application $application
	 * @param array $data
	 * @return ApplicationLoad
	 */
	public static function updateByData($application, $data)
	{
		//get load object or create
		/** @var ApplicationLoad $load */
		$load = ApplicationLoad::where('application_id', $application->id)->first();
		if (!$load) {
			$load = new ApplicationLoad(['application_id' => $application->id]);
		}

		$applicationData = $data;
		$applicationData['application_id'] = $application['id'];

		//$applicationData['axles_info'] = json_encode($data['axles_info']);
		//recalculate permissible loads
		$axles_info = AxleGroup::recalculatePermissibleLoads($data['axles_info']);
		$applicationData['axles_info'] = json_encode($axles_info);

		$applicationData['escort'] = json_encode($data['escort']);

		//calculate load weight
		$loadWeight = floatval(array_get($data, 'weight', 0));
		$vehiclesWeight = $application->getVehiclesEmptyWeight();
		$loadWeight -= $vehiclesWeight;
		$applicationData['load_weight'] = max(0, $loadWeight);

		//fix for fast apps
		if($application->isFast()){
			$applicationData['name'] = array_get($data, 'load_name');
		}

		//update request
		if ($load->id) {
			$load->update($applicationData);
		} else {
			$load = ApplicationLoad::create($applicationData);
		}
		
		//bind files @todo change it
		$ids = [];
		
		$load_ids = [];
		$filesData = array_get($data, 'load_files', []);
		if (!empty($filesData)) {
			foreach ($filesData as $fileData) {
				if(isset($fileData['id'])) {
					$load_ids[] = $fileData['id'];
					$ids[] = $fileData['id'];
				}
			}
		}
		File::where('application_id', $application->id)
			->where('type_id', File::TYPE_APPLICATION_LOAD)
			->whereNotIn('id', $ids)
			->delete();
		
		if (!empty($load_ids)) {
			foreach ($load_ids as $id) {
				if (intval($id)) {
					$file = File::find($id);
					if ($file) {
						$file->user_id = $application->user->id;
						$file->application_id = $application->id;
						$file->type_id = File::TYPE_APPLICATION_LOAD;
						$file->save();
					}
				}
			}
		}
		
		return $load;
	}
	
	public function getSizes($separator = ' x '){
		$data = [
			$this->length . 'м',
			$this->width . 'м',
			$this->height . 'м',
		];
		
		return implode($separator, $data);
	}
	
	public function getWeight(){
		return $this->weight . 'т';
	}

	public function getLoadWeight(){
		return $this->load_weight . 'т';
	}
	
	public function getAxleDistances(){
		$distances = [];
		$axlesInfo = \GuzzleHttp\json_decode($this->axles_info, true);
		if(!empty($axlesInfo)){
			for($i =0; $i < count($axlesInfo) -1; $i++){
				$distances[] = isset($axlesInfo[$i]['distance']) ? $axlesInfo[$i]['distance'] : 0;
			}
		}
		
		return implode(' x ', $distances);
	}
	
	public function getAxleLoads(){
		$distances = [];
		$axlesInfo = \GuzzleHttp\json_decode($this->axles_info, true);
		if(!empty($axlesInfo)){
			for($i =0; $i < count($axlesInfo); $i++){
				$distances[] = isset($axlesInfo[$i]['axle_load']) ? $axlesInfo[$i]['axle_load'] : 0;
			}
		}
		
		return implode(' x ', $distances);
	}

    public function getAxleWheelCount(){
        $distances = [];
        $axlesInfo = \GuzzleHttp\json_decode($this->axles_info, true);
        if(!empty($axlesInfo)){
            for($i =0; $i < count($axlesInfo); $i++){
                $distances[] = isset($axlesInfo[$i]['wheel_count']) ? $axlesInfo[$i]['wheel_count'] : 0;
            }
        }

        return implode(' x ', $distances);
    }

    public function getAxleWheels(){
        $distances = [];
        $axlesInfo = \GuzzleHttp\json_decode($this->axles_info, true);
        if(!empty($axlesInfo)){
            for($i =0; $i < count($axlesInfo); $i++){
                $distances[] = isset($axlesInfo[$i]['wheels']) ? $axlesInfo[$i]['wheels'] : 0;
            }
        }

        return implode(' x ', $distances);
    }

    public function getAxleIsLifting(){
        $distances = [];
        $axlesInfo = \GuzzleHttp\json_decode($this->axles_info, true);
        if(!empty($axlesInfo)){
            for($i =0; $i < count($axlesInfo); $i++){
                $distances[] = (isset($axlesInfo[$i]['is_lifting']) && $axlesInfo[$i]['is_lifting']) ? 'Пневматическая' : 'Рессорная';
            }
        }

        return implode(' x ', $distances);
    }
	
	/**
	 * @return mixed
	 */
	public function getEscortTitle(){
		return array_get(self::$escortInfo, $this->escort_count, '');
	}
	
	/**
	 * @return mixed
	 */
	public function getLoadTypeInfo(){
		return array_get(self::$loadTypes, $this->type_id - 1, '');
	}

	/**
	 * @return mixed|null
	 */
	public function getDecodedEscort() {
		$result = null;
		if($this->escort) {
			$result = \GuzzleHttp\json_decode($this->escort);
		}

		return $result;
	}
}
