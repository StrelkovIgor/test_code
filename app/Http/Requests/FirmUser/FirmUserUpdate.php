<?php

namespace App\Http\Requests\FirmUser;

use App\Models\Role;
use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;

class FirmUserUpdate extends FormRequest
{
    public function authorize()
    {
        /** @var User $user */
        $user = $this->user();
        return $user->role_id == Role::ROLE_FIRM;
    }
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required|string',
            'email' => 'required|string|email|max:255|unique:users,email,'. $this->user->id
        ];
        
        return $rules;
    }
    
    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => 'Поле не может быть пустым!',
            'email.required' => 'Поле не может быть пустым!',
            'email.unique' => 'Email уже занят другим пользователем!'
        ];
    }
}
