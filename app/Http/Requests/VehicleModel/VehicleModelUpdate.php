<?php

namespace App\Http\Requests\VehicleModel;

use App\Models\Role;
use App\Models\User;
use App\Models\Vehicle;
use Illuminate\Foundation\Http\FormRequest;

class VehicleModelUpdate extends FormRequest
{
    public function authorize()
    {
        /** @var User $user */
        $user = $this->user();
		return $user->role_id == Role::ROLE_ADMIN || $user->role_id == Role::ROLE_OFFICER;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $vehicleTypes = Vehicle::getApiTypeList();
        $typeIds = [];
        foreach($vehicleTypes as $type){
            $typeIds[] = $type['id'];
        }
        $typeStr = implode(',', $typeIds);

        return [
            'is_trailer' => 'required',
            'title' => 'required|string|max:255',
            'brand_id' => 'required|integer|exists:vehicle_brands,id',
            'vehicle_type_id' => 'required|integer|in:' . $typeStr,

            'size' => 'required',
            'axle_count' => 'required',
            'axles_distance' => 'required',
            'axles_wheel_count' => 'required',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'title.required'  => 'Название не может быть пустым!',

            'brand_id.required'  => 'Поле Марка не может быть пустым!',
            'brand_id.exists'  => 'Выбрана недопустимая марка тс!',

            'vehicle_type_id.required'  => 'Тип тс не может быть пустым!',
            'vehicle_type_id.in'  => 'Выбран недопустимый тип тс!',

            'size'  => 'Поле не может быть пустым!',
            'axle_count'  => 'Поле не может быть пустым!',
            'axles_distance'  => 'Поле не может быть пустым!',
            'axles_wheel_count'  => 'Поле не может быть пустым!',
        ];
    }
}
