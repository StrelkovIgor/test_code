<?php

namespace App\Http\Requests\Vehicle;

use App\Models\Role;
use App\Models\User;
use App\Models\Vehicle;
use Illuminate\Foundation\Http\FormRequest;

class VehicleCreate extends FormRequest
{
    public function authorize()
    {
        /** @var User $user */
        $user = $this->user();
        return $user->role_id == Role::ROLE_INDIVIDUAL || $user->role_id == Role::ROLE_FIRM;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $vehicleTypes = Vehicle::getApiTypeList();
        $typeIds = [];
        foreach ($vehicleTypes as $type) {
            $typeIds[] = $type['id'];
        }
        $typeStr = implode(',', $typeIds);

        $rules = [
            'is_trailer' => 'required|integer',
            'vehicle_type_id' => 'required|integer|in:' . $typeStr,


            'pts_number' => 'required_without:sts_number',
            'sts_number' => 'required_without:pts_number',
            'pts_weight' => 'required|numeric',
            'pts_max_weight' => 'required|numeric|max:150',

            'height' => 'required|numeric',
            'length' => 'required|numeric',
            'width' => 'required|numeric',
            'axle_count' => 'required|integer',
        ];

        //brand, model
        if(!$this->get('is_new_brand')){
            $rules['brand_id'] = 'required|integer|exists:vehicle_brands,id';
        }else{
			$rules['brand_title'] = 'required|string';
		}
        if(!$this->get('is_new_brand') && !$this->get('is_new_model')){
            $rules['model_id'] = 'required|integer|exists:vehicle_models,id';
        }else {
			$rules['model_title'] = 'required|string';
		}


        if (!$this->get('is_owner', 0)) {
            $rules['owner_name'] = 'required|string';
        }
        if(!$this->get('is_non_standard_number')){
            $rules['number'] = 'required|string';
            $rules['regions'] = 'required|string';
        }else{
            $rules['non_standard_number'] = 'required|string';
        }
        return $rules;
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'is_trailer.required' => 'Поле не может быть пустым!',
			'vehicle_type_id.required' => 'Поле не может быть пустым!',
			'vehicle_type_id.in' => 'Некорректное значение!',

			'pts_number.required' => 'Поле не может быть пустым!',
			'sts_number.required' => 'Поле не может быть пустым!',
			'pts_weight.required' => 'Поле не может быть пустым!',
			'pts_max_weight.required' => 'Поле не может быть пустым!',

			'length.required' => 'Поле не может быть пустым!',
			'height.required' => 'Поле не может быть пустым!',
			'width.required' => 'Поле не может быть пустым!',
			'axle_count.required' => 'Поле не может быть пустым!',

            'brand_id.required' => 'Поле не может быть пустым!',
            'brand_id.exists' => 'Выбрана недопустимая марка!',

            'model_id.exists' => 'Выбрана недопустимая марка!',


            'owner_name' => 'Выбрана недопустимая марка!',
            'number' => 'Выбрана недопустимая марка!',
            'regions' => 'Выбрана недопустимая марка!',
            'non_standard_number' => 'Выбрана недопустимая марка!',
        ];
    }
}
