<?php

namespace App\Http\Requests\Eputs;

use App\Models\Role;
use Illuminate\Foundation\Http\FormRequest;

class ApplicationList extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
    	return [
    	    'start_date' => 'required|date',
    	    'end_date' => 'required|date',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'start_date.required'  => 'Начальная дата не может быть пустой!',
            'end_date.required'  => 'Конечная дата не может быть пустой!',
        ];
    }
}
