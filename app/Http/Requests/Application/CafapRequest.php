<?php

namespace App\Http\Requests\Application;

use Illuminate\Foundation\Http\FormRequest;

class CafapRequest extends FormRequest
{

    /**
     * env CAFAP_SECRET_KEY
     */
    const CAFAP_SECRET_KEY = "a37zQ8wS2Ufrd80E" ;

	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'ID_BETAMOUNT' => 'required|string',
			'ID_VIOLATION' => 'required|string',
			'GOSNUM' => 'required|string',
			'SENSORNAME' => 'required|string',
			'EVENT_DATETIME' => 'required|string',
			'SIGNATURE' => 'required|string'
		];
	}

	/**
	 * Get the error messages for the defined validation rules.
	 *
	 * @return array
	 */
	public function messages()
	{
		return [
			//'ID_BETAMOUNT.required' => ''
		];
	}

	/**
	 * @return array
	 */
	public function parseData()
	{
		$data = $this->all();

		$EVENT_DATETIME = array_get($data, 'EVENT_DATETIME');
		$date = null;
		if($EVENT_DATETIME) {
			$date = $this->convertEventDate($EVENT_DATETIME);
		}

		$number = array_get($data, 'GOSNUM');

		$result = [
			'number' => $number,
			'date' => $date
		];

		return $result;
	}

	/**
	 * @param string $cafapDate
	 * @return false|string
	 */
	private function convertEventDate($cafapDate)
	{
		$dateTime = \DateTime::createFromFormat('Y.m.d H:i:s', $cafapDate);
		if(!$dateTime) {
			$dateTime = \DateTime::createFromFormat('Y-m-d H:i:s', $cafapDate);
		}
		if($dateTime){
			return $dateTime->format('Y-m-d H:i:s');
		}

		return date('Y-m-d H:i:s');
	}

	/**
	 * @return bool
	 */
	public function checkSignature()
	{
		$data = $this->all();
		$signature = array_get($data, 'SIGNATURE');
		$calculatedSign = $this->generateSignature();
		return $signature === $calculatedSign;
	}

	/**
	 * @return string
	 */
	private function generateSignature()
	{
		$requestData = $this->all();

		$ID_BETAMOUNT = array_get($requestData, 'ID_BETAMOUNT');
		$ID_VIOLATION = array_get($requestData, 'ID_VIOLATION');
		$GOSNUM = array_get($requestData, 'GOSNUM');
		$SENSORNAME = array_get($requestData, 'SENSORNAME');
		$EVENT_DATETIME = array_get($requestData, 'EVENT_DATETIME');
		
		$data = [$ID_BETAMOUNT, $ID_VIOLATION, $GOSNUM, $SENSORNAME, $EVENT_DATETIME, env("CAFAP_SECRET_KEY",self::CAFAP_SECRET_KEY)];
		$strToSign = implode(';', $data);

		return md5($strToSign);
	}
}
