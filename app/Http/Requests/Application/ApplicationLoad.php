<?php

namespace App\Http\Requests\Application;

use App\Models\Role;
use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;

class ApplicationLoad extends FormRequest
{
    public function authorize()
    {
		/** @var User $user */
		$user = $this->user();
		return $user->role_id == Role::ROLE_INDIVIDUAL ||
			$user->role_id == Role::ROLE_FIRM ||
			$user->role_id == Role::ROLE_FIRM_USER;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //load
            //'name'=> 'required|string',
            'type_id'=> 'required|integer',


            //size
            'length' => 'required|numeric',
            'width' => 'required|numeric',
            'height' => 'required|numeric',

            //axles
            'axles_count' => 'required|integer',


            'weight' => 'required|numeric',
            //'load_weight' => 'required|numeric',
            //'radius' => 'required|numeric'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [

        ];
    }
}
