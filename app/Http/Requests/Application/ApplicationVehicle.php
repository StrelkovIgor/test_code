<?php

namespace App\Http\Requests\Application;

use App\Models\Role;
use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;

class ApplicationVehicle extends FormRequest
{
    public function authorize()
    {
        /** @var User $user */
        $user = $this->user();
        return $user->role_id == Role::ROLE_INDIVIDUAL ||
			$user->role_id == Role::ROLE_FIRM ||
			$user->role_id == Role::ROLE_FIRM_USER;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [

        ];
    }
}
