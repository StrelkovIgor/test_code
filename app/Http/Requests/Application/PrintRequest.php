<?php

namespace App\Http\Requests\Application;

use App\Models\Role;
use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class PrintRequest extends FormRequest
{
	public function authorize()
	{
		/** @var User $user */
		$user = $this->user();
		return
			$user->role_id === Role::ROLE_ADMIN ||
			$user->role_id === Role::ROLE_OFFICER;
	}
	
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
	    $application = $this->route('application');
	    $application_id = $application->id;
		$form_id = $this->get('form_id');
		$year = date('Y');

		return [
            'form_id' => [
                'required',
                Rule::unique('applications')->where(function ($query) use($application_id,$form_id, $year) {
                    return $query->where('form_id', $form_id)
                        ->where('form_year', $year)
                        ->where('id', '<>', $application_id);
                }),
            ],
		];
	}
	
	/**
	 * Get the error messages for the defined validation rules.
	 *
	 * @return array
	 */
	public function messages()
	{
		return [
			'form_id.unique'  => 'Номер бланка уже был использован ранее!',
		];
	}
}
