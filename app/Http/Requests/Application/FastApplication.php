<?php
/**
 * Created by PhpStorm.
 * User: gserg13
 * Date: 03.06.2019
 * Time: 18:02
 */

namespace App\Http\Requests\Application;

use App\Models\Role;
use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;
use App\Models\Application;

class FastApplication extends FormRequest
{
	public function authorize()
	{
		/** @var User $user */
		$user = $this->user();
		return
			$user->role_id === Role::ROLE_ADMIN ||
			$user->role_id === Role::ROLE_OFFICER;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		$role_id = $this->get('role_id');
		$innSymbols = $role_id == Role::ROLE_INDIVIDUAL ? 12 : 10;

		$rules = [
			//Инфа о юзере
			'role_id' => 'required|integer|in:' . Role::ROLE_INDIVIDUAL . ',' . Role::ROLE_FIRM,
			'name' => 'required|string|max:255',
			'address' => 'string|max:255|nullable',
			'inn' => 'string|nullable|min:'. $innSymbols . '|max:' . $innSymbols,

			//тс
			'vehicle.brand_id' => 'integer|exists:vehicle_brands,id',
			'vehicle.model_id' => 'integer|exists:vehicle_models,id',
			'vehicle.number' => 'required|string',
			'vehicle.regions' => 'required|string|min:2|max:3',
			'vehicle.axle_count' => 'required|integer',
			'vehicle.pts_weight' => 'required|numeric',
			'vehicle.pts_max_weight' => 'required|numeric',

 			//прицепы
			'trailers.*.brand_id' => 'integer|exists:vehicle_brands,id',
			'trailers.*.model_id' => 'integer|exists:vehicle_models,id',
			'trailers.*.number' => 'required|string',
			'trailers.*.regions' => 'required|string|min:2|max:3',
			'trailers.*.axle_count' => 'required|integer',
			'trailers.*.pts_weight' => 'required|numeric',
			'trailers.*.pts_max_weight' => 'required|numeric',

			//Даты, число поездок
			'route_type_id'=> 'required|integer',
			'start_date'=> 'required|date',
			'finish_date' => 'required|date|after:start_date',
			'runs_count'=> 'required|integer',

			//Характер груза
			'load_type_id'=> 'required|integer',

			//size
			'length' => 'required|numeric',
			'width' => 'required|numeric',
			'height' => 'required|numeric',

			//axles
			'axles_count' => 'required|integer',
			'weight' => 'required|numeric',

			'axles_info.*.distance' => 'required|numeric',
			'axles_info.*.type_id' => 'required|integer|in:0,1',
			'axles_info.*.wheel_count' => 'required|integer',//скатность, так сложилось
			'axles_info.*.wheels' => 'required|integer',//число колес
//			'axles_info.*.permissible_load' => 'required|numeric',
			'axles_info.*.permissible_load' => 'numeric',
			'axles_info.*.axle_load' => 'required|numeric',
			//
			'escort_count' => 'required|integer',
			'is_penalty' => 'required|integer',

			'issue_place_id' => 'required|integer|exists:issue_places,id',
			'location_type' => 'required|integer|in:' .
				Application::LOCATION_LOCAL. ',' . Application::LOCATION_REGIONAL . ',' . Application::LOCATION_INTERNATIONAL
		];

		$escort_count = $this->get('escort_count');

		$is_penalty = $this->get('is_penalty');
		if($is_penalty) {
			$rules['penalty_number'] = 'required|string|max:10';
			$rules['penalty_place'] = 'required|string|max:150';
			$rules['penalty_post_id'] = 'required|integer|exists:control_posts,id';
		}

		return $rules;
	}

	/**
	 * Get the error messages for the defined validation rules.
	 *
	 * @return array
	 */
	public function messages()
	{
		return [
			'role_id.unique'  => 'Номер бланка уже был использован ранее!',
		];
	}
}
