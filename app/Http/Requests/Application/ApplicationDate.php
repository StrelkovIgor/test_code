<?php

namespace App\Http\Requests\Application;

use App\Models\Role;
use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;

class ApplicationDate extends FormRequest
{
    public function authorize()
    {
		/** @var User $user */
		$user = $this->user();
		return $user->role_id == Role::ROLE_INDIVIDUAL ||
			$user->role_id == Role::ROLE_FIRM ||
			$user->role_id == Role::ROLE_FIRM_USER;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'start_date'=> 'required|string',
            'finish_date'=> 'required|string',
            'runs_count'=> 'required|integer',
			'issue_place_id' => 'integer',
        ];

        if($this->get('is_penalty')){
			$rules['penalty_number'] = 'required|string|max:10';
			$rules['penalty_place'] = 'required|string|max:150';
			$rules['penalty_post_id'] = 'required|integer|exists:control_posts,id';
        }

        return $rules;
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [

        ];
    }
}
