<?php

namespace App\Http\Requests\Department;

use App\Models\Role;
use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;

class DepartmentUpdate extends FormRequest
{
    public function authorize()
    {
        /** @var User $user */
        $user = $this->user();
		return $user->role_id == Role::ROLE_ADMIN || $user->role_id == Role::ROLE_OFFICER;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|string|max:255',
            'short_title' => 'required|string|max:255',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'title.required'  => 'Название не может быть пустым!',
            'short_title.required'  => 'Краткое название не может быть пустым!',
        ];
    }
}
