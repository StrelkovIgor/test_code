<?php

namespace App\Http\Requests\RequestLog;

use App\Models\Role;
use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;

class RequestLogSearch extends FormRequest
{
	public function authorize()
	{
		/** @var User $user */
		$user = $this->user();
		if(!$user) {
			return false;
		}
		return $user->role_id == Role::ROLE_ADMIN;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [

		];
	}

	/**
	 * Get the error messages for the defined validation rules.
	 *
	 * @return array
	 */
	public function messages()
	{
		return [

		];
	}
}
