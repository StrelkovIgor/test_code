<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class ChangePassword extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'password_old' => 'required|string|min:8',
            'password' => 'required|string|min:8|confirmed',
            'password_confirmation' => 'required|string|min:8',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'password_old.required'  => 'Пароль не может быть пустым!',
            'password.required'  => 'Пароль не может быть пустым!',
            'password_confirmation.required'  => 'Пароль не может быть пустым!',

            'password_old.min'  => 'Пароль должен состоять минимум из 8 символов!',
            'password.min'  => 'Пароль должен состоять минимум из 8 символов!',
            'password_confirmation.min'  => 'Пароль должен состоять минимум из 8 символов!',
        ];
    }
}
