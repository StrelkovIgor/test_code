<?php

namespace App\Http\Requests\User;

use App\Models\Firm;
use App\Models\Role;
use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class Cabinet extends FormRequest
{
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}
	
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		/** @var User $user */
		$user = $this->user();

		$rules = [
			'name' => 'required|string|max:255',
			'email' => 'required|string|email|max:255|unique:users,email,' . $user->id,
		];
		
		if ($user->role_id === Role::ROLE_INDIVIDUAL) {
			$individual = [
				'phone' => 'required|string|max:255|unique:users,phone,' . $user->id,
				'inn' => 'required|string|min:12|max:12|unique:users,inn,' . $user->id
			];
			
			$rules = array_merge($rules, $individual);
		}
		
		if ($user->role_id === Role::ROLE_FIRM) {
			
			$firmObj = Firm::where('user_id', $user->id)->first();
			$firmId = $firmObj ? $firmObj->id : 0;
			$firm = [
				'phone' => 'required|string|max:255|unique:users,phone,' . $user->id,
				'inn_org' => 'required|string|min:10|max:10|unique:firms,inn_org,' . $firmId,
				//'innFiles' => 'required'
			];
			
			$rules = array_merge($rules, $firm);
		}
		
		return $rules;
	}
	
	/**
	 * Get the error messages for the defined validation rules.
	 *
	 * @return array
	 */
	public function messages()
	{
		return [
			'name.required' => 'Имя не может быть пустым!',
			
			'phone.required' => 'Телефон не может быть пустым!',
			'phone.unique' => 'Телефон уже занят другим пользователем!',
			
			'email.required' => 'Email не может быть пустым!',
			'email.unique' => 'Email уже был использован другим пользователем!',
			
			'inn_org.unique' => 'Инн уже был использован другим пользователем!',
			'inn.unique' => 'Инн уже был использован другим пользователем!',
		];
	}
}
