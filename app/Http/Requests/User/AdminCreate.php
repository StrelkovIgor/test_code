<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class AdminCreate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'role_id' => 'required',
            'email' => 'required|string|email|max:255|unique:users,email,' . $this->id,
            'name' => 'required|string|max:255',
        ];

		if($this->get('smev_enabled')){
			$rules['position'] = 'required|string';
		}

        return $rules;
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => 'Имя не может быть пустым!',
            'email.required'  => 'Email не может быть пустым!',
            'email.unique'  => 'Email уже был использован другим пользователем!',
            'role_id.required'  => 'Тип пользователя не выбран!',
			'position.required' => 'Необходимо указать должность!'
        ];
    }
}
