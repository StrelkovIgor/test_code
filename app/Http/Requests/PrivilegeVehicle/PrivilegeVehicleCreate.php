<?php

namespace App\Http\Requests\PrivilegeVehicle;

use App\Models\Role;
use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class PrivilegeVehicleCreate extends FormRequest
{
    public function authorize()
    {
        /** @var User $user */
        $user = $this->user();
        return $user->role_id == Role::ROLE_ADMIN;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $number = Request::get('number', null);

        $rules = [
            'privilege_status_id' => 'required:integer|unique:privilege_vehicles,privilege_status_id,NULL,id,number,' . $number,

            //'brand_title' => 'required|string',
            //'model_title' => 'required|string',
        ];

        if(!$this->get('is_non_standard_number')){
            $rules['number'] = 'required|string';
            $rules['region'] = 'required|string';
        }else{
            $rules['non_standard_number'] = 'required|string';
        }
        return $rules;
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'number.required'  => 'Номер не может быть пустым!',
            'number.unique'  => 'Тс с таким номером уже существует!',
            'brand_title.required'  => 'Марка не может быть пустой!',
            'model_title.required'  => 'Модель не может быть пустой!',
            'privilege_status_id.required'  => 'Статус не может быть пустым!',
            'privilege_status_id.unique'  => 'Тс с таким номером уже добавлено в список льготных тс!',
        ];
    }
}
