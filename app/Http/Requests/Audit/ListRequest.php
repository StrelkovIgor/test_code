<?php

namespace App\Http\Requests\Audit;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Role;
use App\Models\Audit\Enum\Events;

class ListRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Role::isAdmin();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $fields = [
            'date_from' => 'date_format:Y-m-d',
            'date_to' => 'date_format:Y-m-d',
            'fio' => 'string|min:3|max:255',
            'type' => 'string',
            'real_number' => 'string',
            'app_id' => 'string',
            'per_page' => 'max:100',
            'event' => [
                function ($attribute, $value, $fail) {
        			foreach($value as $val) {
						if (!Events::isValidValue($val)) {
							$fail($attribute.' is invalid.');
						}
					}
                },
            ],
        ];
        if ($this->get('date_from') && $this->get('date_to')) {
            $fields['date_from'] = 'date_format:Y-m-d|before_or_equal:date_to';
            $fields['date_to'] = 'date_format:Y-m-d|after_or_equal:date_from';
        }
        return $fields;
    }
}
