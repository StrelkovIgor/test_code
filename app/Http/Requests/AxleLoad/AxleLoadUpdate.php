<?php

namespace App\Http\Requests\AxleLoad;

use App\Models\Role;
use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;

class AxleLoadUpdate extends FormRequest
{
    public function authorize()
    {
        /** @var User $user */
        $user = $this->user();
        return $user->role_id == Role::ROLE_ADMIN;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|string|max:255',
            'value0' => 'required|numeric',
            'value1' => 'required|numeric',
            'value2' => 'required|numeric',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'title.required'  => 'Название не может быть пустым!',
            'value.required'  => 'Значение не может быть пустым!',
        ];
    }
}
