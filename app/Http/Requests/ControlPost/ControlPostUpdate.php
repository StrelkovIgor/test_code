<?php

namespace App\Http\Requests\ControlPost;

use App\Models\Role;
use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;

class ControlPostUpdate extends FormRequest
{
    public function authorize()
    {
        /** @var User $user */
        $user = $this->user();
        return $user->role_id == Role::ROLE_ADMIN;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|string|max:191',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'title.required'  => 'Название не может быть пустым!',
        ];
    }
}
