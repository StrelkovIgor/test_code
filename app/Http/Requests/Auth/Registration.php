<?php

namespace App\Http\Requests\Auth;

use App\Models\Role;
use Illuminate\Foundation\Http\FormRequest;

class Registration extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
    	$rules = [
            'name' => 'required|string|max:255',
            'phone' => 'required|string|max:255|unique:users',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:8|confirmed',
            'role_id' => 'required|integer',
        ];

        $role_id = $this->get('role_id');
        if($role_id == Role::ROLE_INDIVIDUAL){
            $individual = [
				'inn' => 'required|string|min:12|max:12|unique:users,inn'
            ];

            $rules = array_merge($rules, $individual);
        }

        if($role_id == Role::ROLE_FIRM){
            $firm = [
                'inn_org' => 'required|string|min:10|max:10|unique:users,inn',
				'bank_name' => 'required|string',
				'bank_inn' => 'required|string',
				'correspondent_account' => 'required|string',
				'bank_bik' => 'required|string',
				'account' => 'required|string',

				//firm
				'legal_form_id' => 'required|integer',
				'address' => 'required|string',
				'executive_fio' => 'required|string',
				'executive_position' => 'required|string',
				'reason' => 'required|integer',
				'contact_fio' => 'required|string',
            ];

            $rules = array_merge($rules, $firm);
        }

        return $rules;
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => 'Имя не может быть пустым!',

            'phone.required'  => 'Телефон не может быть пустым!',
            'phone.unique'  => 'Телефон уже занят другим пользователем!',

            'email.required'  => 'Email не может быть пустым!',
            'email.unique'  => 'Email уже был использован другим пользователем!',

            'password.required'  => 'Пароль не может быть пустым!',
            'password.min'  => 'Пароль должен состоять минимум из 8 символов!',

            'role_id.required' => 'Тип пользователя не может быть пустым!',
			
			'inn.unique' => 'Инн уже был использован другим пользователем',
			'inn_org.unique' => 'Инн уже был использован другим пользователем',
	
			'bank_name.required' => 'Не может быть пустым!',
			'bank_inn.required' => 'Не может быть пустым!',
			'correspondent_account.required' => 'Не может быть пустым!',
			'bank_bik.required' => 'Не может быть пустым!',
			'account.required' => 'Не может быть пустым!',
        ];
    }
}
