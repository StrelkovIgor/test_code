<?php

namespace App\Http\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;

class RestoreCodeConfirmation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'phone' => 'required|string|max:255',
            'code' => 'required|string|max:4|min:4',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'phone.required'  => 'Телефон не может быть пустым!',
            'code.required'  => 'Код подтверждения не может быть пустым!',
            'code.min'  => 'Код подтверждения должен состоять ровно из 4 символов!',
            'code.max'  => 'Код подтверждения должен состоять ровно из 4 символов!',
        ];
    }
}
