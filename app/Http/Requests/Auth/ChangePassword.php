<?php

namespace App\Http\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;

class ChangePassword extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'token' => 'required',
            //'email' => 'required',
            'password' => 'required|confirmed|min:8',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            //'email.required'  => 'Email не может быть пустым!',
            'token.required'  => 'Код не подтвержден!',
            'password.required'  => 'Пароль не может быть пустым!',
            'password.confirmed'  => 'Пароли не совпадают!',
            'password.min'  => 'Пароль должен состоять минимум из 8 символов!',
        ];
    }
}
