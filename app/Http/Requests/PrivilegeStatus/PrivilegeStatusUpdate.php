<?php

namespace App\Http\Requests\PrivilegeStatus;

use App\Models\Role;
use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;

class PrivilegeStatusUpdate extends FormRequest
{
    public function authorize()
    {
        /** @var User $user */
        $user = $this->user();
        return $user->role_id == Role::ROLE_ADMIN;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|string|max:255',
			'route_info' => 'required|string',
            //'user_id' => 'required|integer|exists:users,id',
            'start_date' => 'required|string',
            'finish_date' => 'required|string',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'title.required'  => 'Название не может быть пустым!',
			'route_info.required'  => 'Описание маршрута не может быть пустым!',
            'start_date.required'  => 'Начальная дата не может быть пустой!',
            'finish_date.required'  => 'Конечная дата не может быть пустой!',
        ];
    }
}
