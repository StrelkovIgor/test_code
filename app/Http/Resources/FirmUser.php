<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class FirmUser extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var \App\Models\User $this */
        return [
            'id' => $this->id,
            'name' => $this->name,
            'email' => $this->email,
            'can_edit' => $this->pivot ? $this->pivot->can_edit : 0
        ];
    }
}
