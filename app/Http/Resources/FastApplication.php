<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;


class FastApplication extends Resource
{
	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @return array
	 */
	public function toArray($request)
	{
		/** @var \App\Models\FastApplication $this */
		$fields = [
			'id' => $this->id,
			'name' => $this->name,
			'address' => $this->address,
			'inn' => $this->inn,
			'role_id' => $this->role_id,
			'created_at' =>  $this->created_at ? $this->created_at->format('Y-m-d H:i:s') : '',
			'updated_at' => $this->updated_at ? $this->updated_at->format('Y-m-d H:i:s') : '',
		];

		$fields['admin'] = new UserDepartment($this->whenLoaded('user'));

		return $fields;
	}
}
