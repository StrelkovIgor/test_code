<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class VehicleAxle extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var \App\Models\VehicleAxle $this */
        return [
            'id' => $this->id,
            'num' => $this->num,
            'wheel_count' => $this->wheel_count,
            'wheels' => $this->wheels,
            'type_id' => $this->type_id,
            'is_lifting' => $this->is_lifting,
            'distance' => $this->distance,
        ];
    }
}
