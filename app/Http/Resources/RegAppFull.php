<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\Individual as IndividualResource;
use App\Http\Resources\Firm as FirmResource;
use App\Models\User;

class RegAppFull extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var User $this */
        $mainField = [
            'id' => $this->id,
            'name' => $this->name,
            'phone' => $this->phone,
            'email' => $this->email,
			'inn' => $this->inn,
            'role_id' => $this->role_id,
            'note' => $this->regApp ? $this->regApp->note : null,
            'confirmation_status' => $this->confirmation_status,
            'email_confirmed' => $this->email_confirmed,
            'locked' => $this->locked,
            'updated_at' => $this->updatedAtFormatted(),

			'files' => File::sortedCollection($this->whenLoaded('files'))
        ];

        if($this->role_id == \App\Models\Role::ROLE_INDIVIDUAL){
            $mainField['individual'] = new IndividualResource($this->whenLoaded('individual'));
        }
        if($this->role_id == \App\Models\Role::ROLE_FIRM){
            $mainField['firm'] = new FirmResource($this->whenLoaded('firm'));
        }
        return $mainField;
    }
}
