<?php

namespace App\Http\Resources;

use App\Http\Resources\ApplicationDate as ApplicationDateResource;
use Illuminate\Http\Resources\Json\Resource;

class ApplicationRoute extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var \App\Models\ApplicationRoute $this */
        $fields = [
            'id' => $this->id,
            'application_id' => $this->application_id,

            'type_id' => $this->type_id,
            'points' => $this->points,
            'text_route' => $this->text_route,
			'distance' => $this->distance,
			'distance_info' => $this->distance_info,
			'federal_distance' => $this->federal_distance,
			'regional_distance' => $this->regional_distance,
            'tracks' => $this->getTracks(),
            'description' => $this->description
        ];

        $fields['dates'] = new ApplicationDateResource($this->application->application_dates);


        return  $fields;
    }
}
