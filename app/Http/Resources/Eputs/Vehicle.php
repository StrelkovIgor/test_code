<?php

namespace App\Http\Resources\Eputs;

use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\File as FileResource;
use App\Http\Resources\PrivilegeVehicle as PrivilegeVehicleResource;

class Vehicle extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var \App\Models\Vehicle $this */
        $fields =  [
            'vechicle_id' => $this->id,
            'vechicle_brand' => $this->getBrandTitle(),
            'vechicle_model' => $this->getModelTitle(),
            'real_number' => $this->real_number,
            'pts_weight' => $this->pts_weight
        ];

        return $fields;
    }
}
