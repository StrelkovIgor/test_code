<?php

namespace App\Http\Resources\Eputs;

use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\PrivilegeVehicle as PrivilegeVehicleResource;

class FrozenVehicle extends Resource
{
	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return array
	 */
	public function toArray($request)
	{
		/** @var \App\Models\ApplicationVehicle $this */
		$fields =  [
            'vechicle_id' => $this->id,
			'vechicle_brand' => $this->brand_title,
			'vechicle_model' => $this->model_title,
			'real_number' => $this->real_number,
            'pts_weight' => $this->pts_weight
		];

		return $fields;
	}
}
