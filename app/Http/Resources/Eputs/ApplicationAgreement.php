<?php

namespace App\Http\Resources\Eputs;

use App\Models\Audit\Enum\Events;
use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\Department as DepartmentResource;
use App\Http\Resources\UserDepartment as UserDepartmentResource;
use App\Http\Resources\Audit\Audit as AuditResource;

class ApplicationAgreement extends Resource
{
	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return array
	 */
	public function toArray($request)
	{
		/** @var \App\Models\ApplicationAgreement $this */
		$fields = [
			'id' => $this->id,
			'application_id' => $this->application_id,
			'department_id' => $this->department_id,
			'status' => $this->status,
			'comment' => $this->comment,
			'created_at' => $this->created_at->format('Y-m-d H:i:s'),
			'updated_at' => $this->updated_at->format('Y-m-d H:i:s'),
			
			'department' => new DepartmentResource($this->whenLoaded('department')),
			'user' => new UserDepartmentResource($this->whenLoaded('user'))
		];

		$fields['audits'] = AuditResource::collection($this->whenLoaded('audits'));

		return $fields;
	}

	/**
	 * @return array
	 */
	private function getEvents()
	{
		$result = [];

		/** @var \App\Models\ApplicationAgreement $this */
		foreach($this->audits_agreements as $audit) {
			if(array_search($audit->event_key, Events::getAgreementEvents()) !== false) {
				$eventCode = Events::getEventCodeByKey($audit->event_key);
				$result[$eventCode] = [
					'created_at' => $audit->created_at ? $audit->created_at->format('Y-m-d H:i:s') : '',
					'user_id' => $audit->user ? $audit->user->id : null,
					'user_name' => $audit->user ? $audit->user->name : null,
					'role_id' => $audit->user ? $audit->user->role_id : null
				];
			}
		}

		return $result;
	}
}
