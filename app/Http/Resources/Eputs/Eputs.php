<?php

namespace App\Http\Resources\Eputs;

use Illuminate\Http\Resources\Json\Resource;

class Eputs extends Resource
{


	/**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var \App\Helpers\Eputs $this */
        return [
            "success" => $this->success,
            "errors" => $this->errors,
            "data" => $this->data
        ];
    }

    public function withResponse($request, $response){
        /** @var \App\Helpers\Eputs $this */
        $response->setStatusCode($this->code)
            ->header('Content-Type','application/json; charset=UTF-8');
    }


}
