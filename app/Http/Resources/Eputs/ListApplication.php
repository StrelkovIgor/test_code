<?php

namespace App\Http\Resources\Eputs;

use App\Http\Resources\Eputs\ApplicationAgreement as ApplicationAgreementResource;
use App\Http\Resources\ApplicationDate as ApplicationDateResource;
use App\Http\Resources\ApplicationLoad as ApplicationLoadResource;
use App\Http\Resources\ApplicationRoute as ApplicationRouteResource;
use App\Http\Resources\ApplicationSmev as ApplicationSmevResource;
use App\Http\Resources\Audit\Audit as AuditResource;
use App\Http\Resources\ControlMark as ControlMarkResource;
use App\Http\Resources\FastApplication as FastApplicationResource;
use App\Http\Resources\File as FileResource;
use App\Http\Resources\FileNew;
//use App\Http\Resources\FrozenVehicle as FrozenVehicleResource;
use App\Http\Resources\Permit as PermitResource;
use App\Http\Resources\PrivilegeStatus as PrivilegeStatusResource;
use App\Http\Resources\RegAppFull;
use App\Http\Resources\User;
use App\Http\Resources\UserEmployee;
use App\Models\Audit\Audit;
use App\Models\Audit\Enum\Events;
use Illuminate\Http\Resources\Json\Resource;

class ListApplication extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var \App\Models\Application $this */
        $fields = [
            'id' => $this->id,
            'user_id' => $this->user_id,
            'username' => $this->username,
            'price' => $this->price,
            'start_date' => $this->startDateFormatted(),
            'finish_date' => $this->finishDateFormatted(),
            'status' => $this->status,
            'start_address' => $this->start_address,
            'finish_address' => $this->finish_address,
            'privilege_status_id' => $this->privilege_status_id,
            'accept_date' => $this->accept_date,
            'activate_date' => $this->activate_date,
            'form_id' => $this->form_id,
            'form_year' => $this->form_year,
            'created_at' =>  $this->getCreatedAtFormatted(),
            'updated_at' => $this->getUpdatedAtFormatted(),
            'special_conditions' => $this->special_conditions,
            'location_type' => $this->location_type
        ];
        $fields['vehicle'] = $this->getVehicleResource();
        $fields['trailers'] = $this->getTrailersResources();
        $fields['issue_place_id'] = $this->getDepartmentResources();

        if($this->application_dates){
            $fields['runs_count'] = $this->application_dates->runs_count;
            $fields['runs_used'] = $this->application_dates->runs_used;
        }
        if($this->application_load){
            $poles = ['type_id','name','length','width','height','weight','load_weight','load_dimension','axles_count','axles_info','escort','escort_count'];
            foreach ($poles as $pole)
                $fields[$pole] = @$this->application_load->{$pole} ?? null;
        }

        if($this->application_route){
            $fields['points'] = json_decode($this->application_route->points);
            $fields['text_route'] = $this->application_route->text_route;
            $fields['distance'] = $this->application_route->distance;
            $fields['distance_info'] = $this->distanceResponse($this->application_route->distance_info);
        }

        $events = $this->getEvents();

/*//        $fields['agreements'] = $this->getAgreements();*/

        $acceptEvent = $events[Events::ADMINGBU_APP_ACCEPT] ?? null;
        $fields['accept_username'] = $acceptEvent ? array_get($acceptEvent,'user_name','') : '';

        $fields['application_agreements'] = ApplicationAgreementResource::collection($this->whenLoaded('application_agreements'));

        return $fields;
    }

    private function distanceResponse($data){
        $result =[];
        $dist = ['regional_distance', 'federal_distance'];
        foreach($dist as $k => $v) $result[$v] = @$data[$k] ?? 0;
        return $result;
    }

    public function getCreatedAtFormatted()
    {
        /** @var \App\Models\Application $this */
        $result = null;
        if(is_string($this->created_at)) {
            return $this->created_at;
        }
        return $this->created_at ? $this->created_at->format('Y-m-d H:i:s') : '';
    }

    public function getUpdatedAtFormatted()
    {
        /** @var \App\Models\Application $this */
        $result = null;
        if(is_string($this->updated_at)) {
            return $this->updated_at;
        }
        return $this->updated_at ? $this->updated_at->format('Y-m-d H:i:s') : '';
    }

    public function getVehicleResource()
    {
        /** @var \App\Models\Application $this */
        if($this->isFrozen() && $this->hasFrozenData()) {
            return new FrozenVehicle($this->frozen_vehicle);
        }
        return new Vehicle($this->whenLoaded('vehicle'));
    }

    public function getTrailersResources()
    {
        /** @var \App\Models\Application $this */
        if($this->isFrozen() && $this->hasFrozenData()) {
            return FrozenVehicle::collection($this->whenLoaded('frozen_trailers'));
        }
        return Vehicle::collection(($this->whenLoaded('trailers')));
    }
    public function getDepartmentResources()
    {
        $data = new ApplicationDateResource($this->whenLoaded('application_dates'));
        return $data['issue_place_id'];
    }

    private function getEvents()
    {
        $result = [
            Events::DEPAGENT_APP_ACCEPT => []
        ];

        /** @var \App\Models\Application $this */
        foreach($this->audits as $audit) {
            if(array_search($audit->event_key, Events::getApplicationEvents()) !== false) {
                $eventCode = Events::getEventCodeByKey($audit->event_key);
                $result[$eventCode] = [
                    'created_at' => $audit->created_at ? $audit->created_at : ''
                ];
                if($audit->user){
                    $data = [
                        'user_id' => $audit->user->id,
                        'user_name' => $audit->user->name,
                        'role_id' => $audit->user->role_id
                    ];
                    $result[$eventCode] = array_merge($result[$eventCode], $data);
                }
            }

            if($audit->event_key === Events::DEPAGENT_APP_ACCEPT_KEY) {
                $result[Events::DEPAGENT_APP_ACCEPT][] = [
                    [
                        'created_at' => $audit->created_at ? $audit->created_at : '',
                        'user_id' => $audit->user->id,
                        'user_name' => $audit->user->name,
                        'department_id' => $audit->audi
                    ]
                ];
            }
        }

        return $result;
    }

    private function getAgreements()
    {
        /** @var \App\Models\Application $this */
        $result = [];
        $applicationAgreement = $this->application_agreements()->orderBy("id", "desc")->first();
        if($applicationAgreement) {
            $agrData = [];
            $agrData['status'] = $applicationAgreement->status;
            $agrData['department_id'] = $applicationAgreement->department_id;
            $result[] = $agrData;
        }

        return $result;
    }
}
