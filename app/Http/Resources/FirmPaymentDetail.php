<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class FirmPaymentDetail extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var \App\Models\FirmPaymentDetail $this */
        return [
            'id' => $this->id,
            'bank_name' => $this->bank_name,
            'bank_inn' => $this->bank_inn,
            'correspondent_account' => $this->correspondent_account,
            'bank_bik' => $this->bank_bik,
            'account' => $this->account,
        ];
    }
}
