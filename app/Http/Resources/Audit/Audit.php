<?php

namespace App\Http\Resources\Audit;

use App\Models\Audit\Enum\Events;
use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\UserDepartment as UserDepartmentResources;

class Audit extends Resource
{
	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return array
	 */
	public function toArray($request)
	{
		/** @var \App\Models\Audit\Audit $this */

		$eventCode = Events::getEventCodeByKey($this->event_key);
		$fields = [
			'id' => $this->id,

			'user_name' => $this->user ? $this->user->getName() : null,
			'user_role' => $this->user ? $this->user->role_id : null,
			'role_id' => $this->user ? $this->user->role_id : null,

			'event_key' => $this->event_key,
			'event_code' => $eventCode,
			'event_title' => Events::getEventTitle($eventCode),

			'audit_type' => \App\Models\Audit\Audit::getObjectTitleByKey($this->auditable_type_key),
			'audit_type_key' => $this->auditable_type_key,
			'audit_object' => $this->getAuditObjectResource(),

			'created_at' => $this->created_at->format('Y-m-d H:i:s'),
			'updated_at' => $this->updated_at->format('Y-m-d H:i:s'),
		];

		$fields['user'] = new UserDepartmentResources($this->whenLoaded('user'));

		return $fields;
	}

	/**
	 * @return
	 */
	public function getAuditObjectResource()
	{
		/** @var \App\Models\Audit\Audit $this */
		$className = \App\Models\Audit\Audit::getObjectClassNameByKey($this->auditable_type_key);
		$resourceClass = str_replace('Models', 'Http\\Resources\\Audit', $className);

		$result = null;
		if(class_exists($resourceClass)) {
			try {
				$result =  new $resourceClass($this->whenLoaded('auditable'));
			} catch (\Exception $e) {

			}
		}

		return $result;
	}

	/**
	 * Sorted collection by event types
	 * @param $resource
	 * @return array
	 */
	public static function sortedCollection($resource) {
		$result = [];
		foreach($resource as $audit) {
			/** @var \App\Models\Audit\Audit $audit */
			$result[Events::getEventCodeByKey($audit->event_key)] = new Audit($audit);
		}
		return $result;
	}
}
