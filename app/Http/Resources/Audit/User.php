<?php

namespace App\Http\Resources\Audit;

use Illuminate\Http\Resources\Json\Resource;

class User extends Resource
{
	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return array
	 */
	public function toArray($request)
	{
		/** @var \App\Models\User $this */
		return [
			'id' => $this->id,
			'name' => $this->getName(),
			'email' => $this->email,
			'role_id' => $this->role_id
		];
	}
}
