<?php

namespace App\Http\Resources\Audit;

use Illuminate\Http\Resources\Json\Resource;

class Vehicle extends Resource
{
	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return array
	 */
	public function toArray($request)
	{
		/** @var \App\Models\Vehicle $this */
		return [
			'id' => $this->id,
			'real_number' => $this->real_number,
		];
	}
}
