<?php

namespace App\Http\Resources\Audit;

use Illuminate\Http\Resources\Json\Resource;

class PrivilegeStatus extends Resource
{
	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return array
	 */
	public function toArray($request)
	{
		/** @var \App\Models\PrivilegeStatus $this */
		return [
			'id' => $this->id,
			'title' => $this->title
		];
	}
}
