<?php

namespace App\Http\Resources\Audit;

use Illuminate\Http\Resources\Json\Resource;

class ApplicationAgreement extends Resource
{
	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return array
	 */
	public function toArray($request)
	{
		/** @var \App\Models\ApplicationAgreement $this */
		return [
			'id' => $this->id,
			'department_title' => $this->department ? $this->department->title : '',
			'application_number' => $this->application ? $this->application->getFormattedId() : ''
		];
	}
}
