<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class File extends Resource
{


	/**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var \App\Models\File $this */
        return [
            'id' => $this->id,
            'name' => $this->name,
            'source' => $this->source,
            'url' => $this->getUrl(),
			'old_url' => $this->url
        ];
    }

	/**
	 * Sorted collection by file types
	 * @param $resource
	 * @return array
	 */
    public static function sortedCollection($resource) {
    	$result = [];
    	foreach($resource as $file) {
			$result[\App\Models\File::getResourceKey($file->type_id)][] = new File($file);
		}
		if(empty($result)) {
    		$result = null;
		}
		return $result;
	}
}
