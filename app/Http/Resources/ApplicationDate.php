<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class ApplicationDate extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var \App\Models\ApplicationDate $this */
        $fields = [
            'id' => $this->id,
            'application_id' => $this->application_id,

            'runs_count' => $this->runs_count,
            'runs_used' => $this->runs_used,
            'is_penalty' => $this->is_penalty,
            'penalty_number' => $this->penalty_number,
            'penalty_place' => $this->penalty_place,
            'penalty_post_id' => $this->penalty_post_id,
			'issue_place_id' =>$this->issue_place_id
        ];

		$fields['penalty_post'] = new ControlPost($this->whenLoaded('penalty_post'));
		$fields['issue_place'] = new IssuePlace($this->whenLoaded('issue_place'));

        return  $fields;
    }
}
