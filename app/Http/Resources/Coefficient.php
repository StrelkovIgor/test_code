<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class Coefficient extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var \App\Models\Coefficient $this */
        return [
            'id' => $this->id,
            'key' => $this->key,
            'road_type' => $this->road_type,
            'title' => $this->title,
            'short_title' => $this->short_title,
            'value' => $this->value,
            'spring_value' => $this->spring_value,
            'created_at' => $this->created_at->format('Y-m-d H:i:s'),
            'updated_at' => $this->updated_at->format('Y-m-d H:i:s'),
        ];
    }
}
