<?php

namespace App\Http\Resources;

use App\Models\Audit\Audit;
use App\Models\Audit\Enum\Events;
use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\ApplicationLoad as ApplicationLoadResource;
use App\Http\Resources\ApplicationRoute as ApplicationRouteResource;
use App\Http\Resources\ApplicationDate as ApplicationDateResource;
use App\Http\Resources\FastApplication as FastApplicationResource;
use App\Http\Resources\Vehicle as VehicleResource;
use App\Http\Resources\File as FileResource;
use App\Http\Resources\PrivilegeStatus as PrivilegeStatusResource;
use App\Http\Resources\Permit as PermitResource;
use App\Http\Resources\ControlMark as ControlMarkResource;
use App\Http\Resources\ApplicationAgreement as ApplicationAgreementResource;
use App\Http\Resources\Audit\Audit as AuditResource;

class ApplicationReport extends Resource
{
	/**
	 * @var \App\Models\Coefficient|null
	 */
	private static $koeff = null;

	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @return array
	 */
	public function toArray($request)
	{
		/** @var \App\Models\Application $this */
		$koeff = self::getKoeff();
		$koeffPrice = $this->is_spring ? $koeff->spring_value : $koeff->value;
		$damage = $this->getRealPrice() - $koeffPrice;
		$damage = max(0, $damage);

		$fields = [
			'id' => $this->id,
			'formatted_id' => $this->getFormattedId(),
			'form_id' => $this->form_id,
			'issue_place' => $this->application_dates->issue_place->title ?? null,
            'employee_id' => $this->employee_id,
			'start_date' => $this->start_date,
			'finish_date' => $this->finish_date,
			'user_name' => $this->getUserName(),
			'user_inn' => $this->getUserInn(),
			'real_number' => $this->vehicle ? $this->vehicle->real_number : null,
			'price' => $this->getRealPrice(),
			'damage_price' => $damage
		];

        $fields['employee'] = new UserEmployee($this->whenLoaded('employee'));

		$events = $this->getEvents();

		$toReviewEvent = $events[Events::APPLICANT_APP_TOREVIEW] ?? null;
		$toReviewTime = $toReviewEvent ? $toReviewEvent['created_at']->toDateTimeString() : '';
		$fields['to_review_time'] = $toReviewTime;

		$fields['agreements'] = $this->getAgreements();

		$acceptEvent = $events[Events::ADMINGBU_APP_ACCEPT] ?? null;
		$fields['accept_time'] = ($acceptEvent && isset($acceptEvent['created_at'])) ? $acceptEvent['created_at']->toDateTimeString() : '';
		$fields['accept_username'] = $acceptEvent ? array_get($acceptEvent,'user_name','') : '';

		$activateEvent = $events[Events::ADMINGBU_APP_ACTIVATE] ?? null;
		$fields['activate_time'] = ($activateEvent && isset($activateEvent['created_at'])) ? $activateEvent['created_at']->toDateTimeString() : '';
		$fields['activate_username'] = $activateEvent ? array_get($activateEvent,'user_name','')  : '';

		return $fields;
	}

	/**
	 * @return array
	 */
	private function getEvents()
	{
		$result = [
			Events::DEPAGENT_APP_ACCEPT => []
		];

		/** @var \App\Models\Application $this */
		foreach($this->audits as $audit) {
			if(array_search($audit->event_key, Events::getApplicationEvents()) !== false) {
				$eventCode = Events::getEventCodeByKey($audit->event_key);
				$result[$eventCode] = [
					'created_at' => $audit->created_at ? $audit->created_at : ''
				];
				if($audit->user){
				    $data = [
                        'user_id' => $audit->user->id,
                        'user_name' => $audit->user->name,
                        'role_id' => $audit->user->role_id
                    ];
                    $result[$eventCode] = array_merge($result[$eventCode], $data);
                }
			}

			if($audit->event_key === Events::DEPAGENT_APP_ACCEPT_KEY) {
				$result[Events::DEPAGENT_APP_ACCEPT][] = [
					[
						'created_at' => $audit->created_at ? $audit->created_at : '',
						'user_id' => $audit->user?$audit->user->id:'',
						'user_name' => $audit->user?$audit->user->name:'',
						'department_id' => $audit->audi
					]
				];
			}
		}

		return $result;
	}

	private function getAgreements()
	{
		/** @var \App\Models\Application $this */
		$result = [];
		if(!empty($this->application_agreements)) {
			foreach($this->application_agreements as $applicationAgreement) {
				//only accepted agreements
				if($applicationAgreement->status !== \App\Models\ApplicationAgreement::STATUS_ACCEPTED) {
					continue;
				}
				$agrData = [];
				$agrData['department_title'] = $applicationAgreement->department->title ?? '';
				$agrData['created_at'] = $applicationAgreement->created_at->toDateTimeString() ?? '';

				/** @var Audit $event */
				$events = [];
				foreach($applicationAgreement->audits_active as $audit) {
					if(array_search($audit->event_key, Events::getAgreementEvents()) !== false) {
					    $event = Events::getEventCodeByKey($audit->event_key);
						$events[$event] = [
							'created_at' => $audit->created_at ? $audit->created_at : '',
						];
                        if($audit->user){
                            $data = [
                                'user_id' => $audit->user->id,
                                'user_name' => $audit->user->name,
                                'role_id' => $audit->user->role_id
                            ];
                            $events[$event] = array_merge($events[$event], $data);
                        }
					}
				}

				$acceptEvent = $events[Events::DEPAGENT_APP_ACCEPT] ?? null;
				$agrData['accept_time'] = $acceptEvent && $acceptEvent['created_at'] ? $acceptEvent['created_at']->toDateTimeString() : null;
				$agrData['accept_username'] = $acceptEvent['user_name'] ?? null;
				$result[] = $agrData;
			}
		}

		return $result;
	}


	/**
	 * @return \App\Models\Coefficient|null
	 */
	public static function getKoeff()
	{
		if(!static::$koeff) {
			static::$koeff = \App\Models\Coefficient::find(\App\Models\Coefficient::ID_GP);
		}

		return static::$koeff;
	}
}
