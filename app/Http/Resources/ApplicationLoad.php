<?php

namespace App\Http\Resources;

use App\Http\Resources\ApplicationDate as ApplicationDateResource;
use Illuminate\Http\Resources\Json\Resource;

class ApplicationLoad extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var \App\Models\ApplicationLoad $this */
        $fields = [
            'id' => $this->id,
            'application_id' => $this->application_id,

            'type_id' => $this->type_id,
            'name' => $this->name,

            'length' => $this->length,
            'width' => $this->width,
            'height' => $this->height,
            'weight' => $this->weight,
            'load_weight' => $this->load_weight,
            'load_dimension' => $this->load_dimension,

            'axles_count' => $this->axles_count,
            'axles_info' => $this->axles_info,
            'escort' => $this->getDecodedEscort(),
            'escort_count' => $this->escort_count,

            'radius' => $this->radius
        ];
        try{
//        $fields['dates'] = new ApplicationDateResource($this->whenLoaded('application.application_dates'));
        $fields['dates'] = new ApplicationDateResource($this->application->application_dates);
        }catch (\Exception $e){}

        return  $fields;
    }
}
