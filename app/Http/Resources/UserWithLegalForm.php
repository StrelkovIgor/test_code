<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\Individual as IndividualResource;
use App\Http\Resources\Firm as FirmResource;
use App\Http\Resources\File as FileResource;
use App\Models\User;

class UserWithLegalForm extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var User $this */
        $mainField = [
            'id' => $this->id,
            'role_id' => $this->role_id,
            'name' => $this->name,
            'updated_at' => $this->updatedAtFormatted(),
        ];

        if($this->role_id == \App\Models\Role::ROLE_FIRM){
            $mainField['name'] = $this->name . '( ' . $this->firm->legal_form->short_title . ')';
        }
        return $mainField;
    }
}
