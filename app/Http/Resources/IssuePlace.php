<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class IssuePlace extends Resource
{
	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return array
	 */
	public function toArray($request)
	{
		/** @var \App\Models\IssuePlace $this */
		return [
			'id' => $this->id,
			'title' => $this->title,
			'created_at' => $this->created_at ? $this->created_at->format('Y-m-d H:i:s') : null,
			'updated_at' => $this->updated_at ? $this->updated_at->format('Y-m-d H:i:s') : null,
		];
	}
}
