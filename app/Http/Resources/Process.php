<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class Process extends Resource
{
	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return array
	 */
	public function toArray($request)
	{
		/** @var \App\Models\Process $this */
		return [
			'id' => $this->id,
			'type' => $this->type,
			'params' => $this->params,
			'status' => $this->status,
			'progress' => $this->progress,
			//'user_id' => $this->user_id,
			'result' => $this->result,
			'created_at' => $this->created_at ? $this->created_at->format('Y-m-d H:i:s') : null,
			'updated_at' => $this->updated_at ? $this->updated_at->format('Y-m-d H:i:s') : null,
		];
	}
}
