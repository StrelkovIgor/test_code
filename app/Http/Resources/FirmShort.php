<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
use App\Models\User;

class FirmShort extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var User $this */
        $mainField = [
            'id' => $this->id,
            'name' => $this->name,
            'phone' => $this->phone,
            'email' => $this->email,
            'address' => $this->firm->address ?? null,
            'legal_form_id' => $this->firm->legal_form_id ?? null,
            'legal_form_title' => $this->firm->legal_form->title ?? null,
            'updated_at' => $this->updated_at->format('Y-m-d H:i:s'),
        ];

        return $mainField;
    }
}
