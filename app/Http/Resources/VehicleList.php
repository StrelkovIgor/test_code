<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class VehicleList extends Resource
{
	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return array
	 */
	public function toArray($request)
	{
		/** @var \App\Models\Vehicle $this */
		$fields =  [
			'id' => $this->id,
			'is_trailer' => $this->is_trailer,

			'brand_id' => $this->brand_id,
			'brandTitle' => $this->getBrandTitle(),

			'model_id' => $this->model_id,
			'modelTitle' => $this->getModelTitle(),

			'real_number' => $this->real_number,
			'updated_at' => $this->updated_at->format('Y-m-d H:i:s'),

			'user_id' => $this->user_id,
			'user_name' => $this->user ? $this->user->getName() : $this->owner_name,

			'admin_id' => $this->admin_id,
			'admin_name' => $this->admin ? $this->admin->name : '',

			'note' => $this->vehicle_app ? $this->vehicle_app->note : null,

            'privilege_status_id' => $this->privilege ? $this->privilege->privilege_status_id : null,
            'privilegeTitle' => $this->privilege && $this->privilege->privilege_status ?  $this->privilege->privilege_status->title : null,
		];

		return $fields;
	}
}
