<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class RouteAddress extends Resource
{
	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return array
	 */
	public function toArray($request)
	{
		/** @var \App\Models\RouteAddress $this */
		return [
			'id' => $this->id,
			'title' => $this->title,
			'created_at' => $this->created_at->format('Y-m-d H:i:s'),
			'updated_at' => $this->updated_at->format('Y-m-d H:i:s'),
		];
	}
}
