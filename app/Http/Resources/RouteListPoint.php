<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class RouteListPoint extends Resource
{
	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return array
	 */
	public function toArray($request)
	{
		/** @var \App\Models\RouteList $this */
		return [
			'id' => $this->id,
			'route_list_id' => $this->route_list_id,
			'name' => $this->name,
			'lat' => $this->lat,
			'lon' => $this->lon,
		];
	}
}
