<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
use App\Models\User;
use App\Http\Resources\Firm as FirmResource;

class UserFirmFull extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var User $this */
        $mainField = [
            'id' => $this->id,
            'name' => $this->name,
            'phone' => $this->phone,
            'email' => $this->email,
            'role_id' => $this->role_id,
            'note' => $this->regApp ? $this->regApp->note : null,
            'confirmation_status' => $this->confirmation_status,
            'locked' => $this->locked,
            'updated_at' => $this->updated_at->format('Y-m-d H:i:s'),
            'vehicles' => VehicleShort::collection($this->whenLoaded('vehicles')),
        ];
		$mainField['firm'] = new FirmResource($this->firm);

        return $mainField;
    }
}
