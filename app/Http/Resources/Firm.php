<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\FirmPaymentDetail as FirmPaymentDetailResource;
use App\Http\Resources\File as FileResource;

class Firm extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
		/** @var \App\Models\Firm $this */
    	return [
            'id' => $this->id,
            'inn_org' => $this->inn_org,
            'legal_form_id' => $this->legal_form_id,
            'legal_form_title' => $this->legal_form->title,
            'address' => $this->address,
            'executive_fio' => $this->executive_fio,
            'executive_position' => $this->executive_position,
            'reason_key' => $this->reason_key,
            'contact_phone' => $this->contact_phone,
            'contact_fio' => $this->contact_fio,
			'phone_static' => $this->phone_static,
            'updated_at' => $this->updated_at,

			//@todo check
			'innFiles' => "",
			'inn_files' => [],

            'paymentDetails' => new FirmPaymentDetailResource($this->firm_payment_details)
        ];
    }
}
