<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class VehicleShort extends Resource
{
	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return array
	 */
	public function toArray($request)
	{
		/** @var \App\Models\Vehicle $this */
		$fields =  [
			'id' => $this->id,
			'is_trailer' => $this->is_trailer,

			'brand_id' => $this->brand_id,
			'brandTitle' => $this->getBrandTitle(),

			'model_id' => $this->model_id,
			'modelTitle' => $this->getModelTitle(),

			'real_number' => $this->real_number,
			'updated_at' => $this->updated_at->format('Y-m-d H:i:s'),

			'privilege_status_id' => $this->privilege ? $this->privilege->privilege_status_id : null,
			'privilegeTitle' => $this->privilege && $this->privilege->privilege_status ?  $this->privilege->privilege_status->title : null,
		];

		return $fields;
	}
}
