<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class SpecialCondition extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var \App\Models\SpecialCondition $this */
        return [
            'id' => $this->id,
            'title' => $this->title,
            'value' => intval($this->value),
			'start_date' => $this->start_date,
			'finish_date' => $this->finish_date,
            'has_comment' => intval($this->has_comment),
            'comment' => $this->comment,
            'created_at' => $this->created_at ? $this->created_at->format('Y-m-d H:i:s') : null,
            'updated_at' => $this->created_at ? $this->updated_at->format('Y-m-d H:i:s') : null,
        ];
    }
}
