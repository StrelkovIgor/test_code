<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\PrivilegeVehicle as PrivilegeVehicleResource;

class FrozenVehicle extends Resource
{
	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return array
	 */
	public function toArray($request)
	{
		/** @var \App\Models\ApplicationVehicle $this */
		$fields =  [
			'id' => $this->id,
			'is_trailer' => $this->is_trailer,

			'brand_id' => null,
			'brandTitle' => $this->brand_title,

			'model_id' => null,
			'modelTitle' => $this->model_title,

			'vehicle_type_id' => $this->vehicle_type_id,
			'is_owner' => $this->is_owner,
			'owner_name' => $this->owner_name,

			'real_number' => $this->real_number,
			'number' => null,
			'regions' => null,
			'pts_number' => $this->pts_number,
			'sts_number' => $this->sts_number,
			'pts_weight' => $this->pts_weight,
			'pts_max_weight' => $this->pts_max_weight,

			'length' => $this->length,
			'width' => $this->width,
			'height' => $this->height,

			'axle_count' => $this->axle_count,
			'confirmation_status' => \App\Models\Vehicle::CONFIRM_STATUS_ACCEPT,
			'updated_at' => $this->updated_at->format('Y-m-d H:i:s'),

			'note' => null,

			'vehicle_axles' => $this->axles,
			'firm_users' => [],

			'is_non_standard_number' => 0,
			'non_standard_number' => null,
			'can_edit' => 0,

			'privilege' => new PrivilegeVehicleResource($this->whenLoaded('privilege')),

			'user_id' => null,
			'user_name' => $this->owner_name,

			'files' => $this->files
		];

		return $fields;
	}
}
