<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class ApplicationCafap extends Resource
{
	/**
	 * @var int
	 */
	private $routeLengthLimit = 500;

	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return array
	 */
	public function toArray($request)
	{
		/** @var \App\Models\Application $this */
		$fields = [
			'AUTH_NUM' => $this->getAuthNum($this),
			'AUTH_DATE' => $this->getAuthDate($this),
			'AUTH_START_DATE' => $this->startDateFormatted(),
			'AUTH_FIN_DATE' => $this->finishDateFormatted(),

			'GOSNUM' => $this->generateNumsList($this),

			'VECHICLEMODEL' => $this->vehicle->getBrandModelTitle() ?? null,
			'TRAILERMODEL' => $this->generateTrailersBrandModel($this),

			'ROUTE' => $this->generateRoute($this),
			'KTG_TOTAL_WEIGHT' => $this->tToKg($this->application_load->weight ?? 0),

			'KTG_LENGTHNUM' => $this->mToCm($this->application_load->length ?? 0),
			'KTG_WIDTHNUM' => $this->mToCm($this->application_load->width ?? 0),

			'KTG_HEIGHTHNUM' => $this->mToCm($this->application_load->height ?? 0),
			'STATUS' => $this->privilege_status_id ? 1 : 0,

			'PERM_VALWHEELBASE_LIST' => $this->generateAxlesDistances($this),
			'AXLE_INFO_LIST' => $this->generateAxlesInfo($this)
		];

		return  $fields;
	}

	/**
	 * @param \App\Models\\App\Models\Application $application
	 * @return string
	 */
	private function getAuthNum($application)
	{
		/** @var \App\Models\Application $this */
		$result = '№' . $this->getFormattedId();
		if($application->form_id){
			$result .= ', ' . $this->form_id;
		}

		return $result;
	}


	/**
	 * @param \App\Models\Application $application
	 * @return string
	 */
	private function getAuthDate($application)
	{
		$result = $application->getActivateDate($application->myDateFormat);
		return $result;
	}

	/**
	 * @param \App\Models\Application $application
	 * @return string
	 */
	private function generateNumsList($application){
		$numbers = [];
		if($application->vehicle) {
			$numbers[] = $application->vehicle->real_number;
		}

		if(!empty($application->trailers)) {
			/** @var \App\Models\Vehicle $trailer */
			foreach($application->trailers as $trailer) {
				$numbers[] = $trailer->real_number;
			}
		}

		return implode(',', $numbers);
	}

	/**
	 * @param $application
	 * @return string
	 */
	private function generateTrailersBrandModel($application)
	{
		$titles = [];
		if(!empty($application->trailers)) {
			/** @var \App\Models\Vehicle $trailer */
			foreach($application->trailers as $trailer) {
				$titles[] = $trailer->getBrandModelTitle();
			}
		}

		return implode(',', $titles);
	}

	/**
	 * @param \App\Models\Application $application
	 * @return string
	 */
	private function generateRoute($application)
	{
		$routeText = '';
		if($application->privilege_status){
			$routeText = \App\Models\ApplicationRoute::getPrivilegeTypeInfo() . '. ' . $application->privilege_status->route_info;
		}else{
			if($application->application_route){
				$routeText = $application->application_route->getRouteTypeInfo() . '. ' . $application->application_route->getRouteText();
			}
		}

		$routeLength = mb_strlen($routeText, 'utf8');
		if($routeLength >= $this->routeLengthLimit){
			$routeText = mb_strimwidth($routeText, 0, $this->routeLengthLimit, null, 'utf8');
		}

		return $routeText;
	}

	/**
	 * @param \App\Models\Application $application
	 * @return array
	 */
	private function generateAxlesDistances($application)
	{
		$axlesInfo = [];

		if(!$application->application_load) {
			return [];
		}
		$axlesInfoObj = $application->application_load->axles_info ?? null;
		$encodedInfo = \GuzzleHttp\json_decode($axlesInfoObj, true);
		$i=0;
		foreach($encodedInfo as $axleInfo){
			//exclude distance after last axle
			if($i >= count($encodedInfo) - 1){
				break;
			}
			$axlesInfo[] = $this->generateAxleDistance($axleInfo, $i);
			$i++;
		}

		return $axlesInfo;
	}

	/**
	 * @param $axleInfo
	 * @param $num
	 * @return array
	 */
	private function generateAxleDistance($axleInfo, $num)
	{
		return [
			'NUM' => $num + 1,
			'VALUE' => $this->mToCm($axleInfo['distance'])
		];
	}

	/**
	 * @param \App\Models\Application $application
	 * @return array
	 */
	private function generateAxlesInfo($application)
	{
		$axlesInfo = [];

		if(!$application->application_load) {
			return [];
		}
		$encodedInfo = \GuzzleHttp\json_decode($application->application_load->axles_info, true);
		$i=0;
		foreach($encodedInfo as $axleInfo){
			$i++;
			$axlesInfo[] = $this->generateAxleInfo($axleInfo, $i);
		}

		return $axlesInfo;
	}

	/**
	 * @param $axleInfo
	 * @param $num
	 * @return array
	 */
	private function generateAxleInfo($axleInfo, $num)
	{
		$result = [];

		$result['AXLE_NUM'] = $num;
		$result['PERM_WHEELS_EX'] = $axleInfo['wheel_count'];
		$result['PERM_WHEELS_COUNT'] = $axleInfo['wheels'];
		$result['PERM_AXEL_WEIGHT'] = $this->tToKg($axleInfo['axle_load']);

		return $result;
	}

	/**
	 * @param $meters
	 * @return int
	 */
	private function mToCm($meters)
	{
		return intval(floatval($meters) * 100);
	}

	/**
	 * @param $tons
	 * @return int
	 */
	private function tToKg($tons)
	{
		return intval(floatval($tons) * 1000);
	}
}