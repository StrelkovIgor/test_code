<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\File as FileResource;
use App\Http\Resources\User as UserResource;

class PrivilegeStatus extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var \App\Models\PrivilegeStatus $this */
        $fields = [
            'id' => $this->id,
            'title' => $this->title,
			'route_info' => $this->route_info,
            'start_date' => $this->start_date->format('d.m.Y'),
            'finish_date' => $this->finish_date->format('d.m.Y'),
            'user_id' => $this->user_id,
            'user' => new UserResource($this->whenLoaded('user')),

			'files' => File::sortedCollection($this->whenLoaded('files')),
            'apvgk' => apvgk::collection($this->whenLoaded('apvgk')),

            'created_at' => $this->created_at->format('Y-m-d H:i:s'),
            'updated_at' => $this->updated_at->format('Y-m-d H:i:s'),
        ];

        return $fields;
    }
}
