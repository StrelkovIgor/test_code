<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

/**
 * Class APVGK
 * @package App\Http\Resources
 */
class APVGK_marks extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $fields = [
            'id' => $this->id,
            'platform_id' => $this->direction,
            'date_created' => $this->getEventDate()
        ];
        $this->setApvgk($this, $fields);
        return  $fields;
    }

    protected function getFormatedId($apvgkMark){
        $application = $apvgkMark->application()->first();
        return $application ? $application->getFormattedId() : null;
    }

    protected function setApvgk($apvgkMark, &$fields){
        $apvgk = $apvgkMark->apvgk()->first();
        if($apvgk){
            $fields['sensorname'] = $apvgk->number;
            $fields['name'] = $apvgk->name;
        }
    }
}
