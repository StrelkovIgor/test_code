<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\ApplicationLoad as ApplicationLoadResource;
use App\Http\Resources\ApplicationRoute as ApplicationRouteResource;
use App\Http\Resources\ApplicationDate as ApplicationDateResource;
use App\Http\Resources\Vehicle as VehicleResource;
use App\Http\Resources\File as FileResource;
use App\Http\Resources\PrivilegeStatus as PrivilegeStatusResource;
use App\Http\Resources\Permit as PermitResource;
use App\Http\Resources\ControlMark as ControlMarkResource;
use App\Http\Resources\ApplicationAgreement as ApplicationAgreementResource;

class ApplicationFull extends Resource
{
	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @return array
	 */
	public function toArray($request)
	{
		/** @var \App\Models\Application $this */
		$fields = [
			'id' => $this->id,
			'user_id' => $this->user_id,
			'vehicle_id' => $this->vehicle_id,

			'comment' => $this->comment,
			'price' => $this->price,
			'spring_price' => $this->spring_price,
			'real_price' => $this->getRealPrice(),
			'federal_price' => $this->federal_price,
			'regional_price' => $this->regional_price,
			'start_date' => $this->startDateFormatted(),
			'finish_date' => $this->finishDateFormatted(),
			'status' => $this->status,
			'start_address' => $this->start_address,
			'finish_address' => $this->finish_address,

			'special_condition_id' => $this->special_condition_id,
			'privilege_status_id' => $this->privilege_status_id,
			'is_draft' => $this->is_draft,

			'is_template' => $this->is_template,
			'accept_date' => $this->accept_date,
			'employee_id' => $this->employee_id,
			'form_id' => $this->form_id,
			'created_at' =>  $this->created_at ? $this->created_at->format('Y-m-d H:i:s') : '',
			'updated_at' => $this->updated_at ? $this->updated_at->format('Y-m-d H:i:s') : '',
			'removed' => $this->deleted_at ? 1 : null,
			'is_locked' => $this->is_locked,
			'is_spring' => $this->is_spring
		];

		$fields['vehicle'] = new VehicleResource($this->vehicle);
		$fields['trailer'] = new VehicleResource($this->trailer);
		$fields['trailers'] = VehicleResource::collection(($this->trailers));
		$fields['user'] = new RegAppFull($this->user);
        $fields['employee'] = new UserEmployee($this->employee);

		$fields['load'] = new ApplicationLoadResource($this->application_load);
		$fields['route'] = new ApplicationRouteResource($this->application_route);
		$fields['dates'] = new ApplicationDateResource($this->application_dates);
		$fields['permit'] = new PermitResource($this->permit);

		$fields['privilegeStatus'] = new PrivilegeStatusResource($this->privilege_status);
		$fields['admin'] = new User($this->admin);
		$fields['application_dates'] = new ApplicationDate($this->application_dates);
		$fields['control_marks'] = ControlMarkResource::collection($this->control_marks);

		$fields['application_agreements'] = ApplicationAgreementResource::collection($this->application_agreements);


		$load_files = [];
		$pay_files = [];
		$files = \App\Models\File::where('application_id', $this->id)->get();
		foreach($files as $file){
			/** @var \App\Models\File $file */
			switch($file->type_id){
				case \App\Models\File::TYPE_APPLICATION_LOAD:
					$load_files[] = new FileResource($file);
					break;
				case \App\Models\File::TYPE_APPLICATION_PAY:
					$pay_files[] = new FileResource($file);
					break;
			}
		}

		$fields['load_files'] = $load_files;
		$fields['pay_files'] = $pay_files;

		return $fields;
	}
}
