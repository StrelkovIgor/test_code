<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

/**
 * Class APVGK
 * @package App\Http\Resources
 */
class APVGK extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $fields = [
            'id' => $this->id,
            'number' => $this->number,
            'name' => $this->name,
            'direction' => $this->direction
        ];
        return  $fields;
    }
}
