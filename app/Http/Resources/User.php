<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\Department as DepartmentResource;
use App\Http\Resources\ControlPost as ControlPostResource;

class User extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var \App\Models\User $this */
        return [
            'id' => $this->id,
            'name' => $this->name,
            'phone' => $this->phone,
            'email' => $this->email,
			'inn' => $this->inn,
            'role_id' => $this->role_id,
            'api_token' => $this->api_token,
            'confirmation_status' => $this->confirmation_status,
            'email_confirmed' => $this->email_confirmed,

            'department_id' => $this->department_id,
			
			'department' =>  new DepartmentResource($this->whenLoaded('department')),
			'control_post' =>  new ControlPostResource($this->whenLoaded('control_post')),

			'files' => File::sortedCollection($this->whenLoaded('files')),

			'smev_enabled' => $this->smev_enabled,
			'position' => $this->position,
        ];
    }
}
