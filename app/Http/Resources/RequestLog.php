<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class RequestLog extends Resource
{
	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return array
	 */
	public function toArray($request)
	{
		/** @var \App\Models\RequestLog $this */
		$fields = [
			'id' => $this->id,

			'number' => $this->number,
			'event_datetime' => $this->event_datetime  ? $this->event_datetime->toDateTimeString() : '',

			'duration' => $this->duration,
			'ip_address' => $this->ip_address,
			'url' => $this->url,
			'method' => $this->method,

			'request' => $this->request,
			'response' => $this->response,

			'created_at' => $this->created_at ? $this->created_at->toDateTimeString() : ''
		];

		return  $fields;
	}
}
