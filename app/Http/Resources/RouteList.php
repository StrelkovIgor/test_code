<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class RouteList extends Resource
{
	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return array
	 */
	public function toArray($request)
	{
		/** @var \App\Models\RouteList $this */
        $points = $this->RouteListsPoints;

		return [
			'id' => $this->id,
			'name' => $this->name,
            'name_prefix' => $points->map(function($item, $key){
                return $item->name;
            })->implode(' - '),
            'points' => RouteListPoint::collection($points),
            'distance' => $this->distance,
            'distance_info' => $this->distance_info,
			'created_at' => $this->created_at->format('Y-m-d H:i:s'),
			'updated_at' => $this->updated_at->format('Y-m-d H:i:s'),
		];
	}
}
