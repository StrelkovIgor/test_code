<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\VehicleAxle as VehicleAxleResource;

class VehicleModel extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var \App\Models\VehicleModel $this */
        $fields = [
            'id' => $this->id,
            'title' => $this->title,
            'brand_id' => $this->brand_id,
            'brandTitle' => $this->vehicle_brand ? $this->vehicle_brand->title : null,
            'hidden' => $this->hidden,
            'is_trailer' => $this->is_trailer,
            'vehicle_type_id' => $this->vehicle_type_id,

            'length' => $this->length,
            'width' => $this->width,
            'height' => $this->height,
            'axle_count' => $this->axle_count,
            'axles_wheel_count' => $this->axles_wheel_count,
            'axles_wheels' => $this->axles_wheel_count,
            'axles_distance' => $this->axles_distance,
			'accepted' => $this->accepted
        ];

		$fields['vehicle_axles'] = VehicleAxleResource::collection($this->whenLoaded('vehicle_axles'));

        return $fields;
    }
}
