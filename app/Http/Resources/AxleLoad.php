<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class AxleLoad extends Resource
{
	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return array
	 */
	public function toArray($request)
	{
		/** @var \App\Models\AxleLoad $this */
		return [
			'id' => $this->id,
			'title' => $this->title,

			'is_spring' => $this->is_spring,
			'group_axles_count' => $this->group_axle_count,
			'wheel_count' => $this->wheel_count,
			'distance_index' => $this->distance_index,
			'axle_type' => $this->axle_type,
			'wheels' => $this->wheels,

			'value0' => $this->value0,
			'value1' => $this->value1,
			'value2' => $this->value2,

			'created_at' => $this->created_at ? $this->created_at->format('Y-m-d H:i:s') : null,
			'updated_at' => $this->updated_at ? $this->updated_at->format('Y-m-d H:i:s') : null,
		];
	}
}
