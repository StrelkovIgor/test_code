<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\File as FileResource;
use App\Http\Resources\PrivilegeVehicle as PrivilegeVehicleResource;

class Vehicle extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var \App\Models\Vehicle $this */
        $fields =  [
            'id' => $this->id,
            'is_trailer' => $this->is_trailer,

            'brand_id' => $this->brand_id,
            'brandTitle' => $this->getBrandTitle(),

            'model_id' => $this->model_id,
            'modelTitle' => $this->getModelTitle(),

            'vehicle_type_id' => $this->vehicle_type_id,
            'is_owner' => $this->is_owner,
            'owner_name' => $this->owner_name,

            'real_number' => $this->real_number,
            'number' => $this->number,
            'regions' => $this->regions,
            'pts_number' => $this->pts_number,
            'sts_number' => $this->sts_number,
            'pts_weight' => $this->pts_weight,
            'pts_max_weight' => $this->pts_max_weight,

            'length' => $this->length,
            'width' => $this->width,
            'height' => $this->height,

            'axle_count' => $this->axle_count,
            'confirmation_status' => $this->confirmation_status,
            'updated_at' => $this->updated_at->format('Y-m-d H:i:s'),

            //@todo rebuild to conditional
            'note' => $this->vehicle_app ? $this->vehicle_app->note : null,

            'vehicle_axles' => VehicleAxle::collection($this->whenLoaded('vehicle_axles')),
            'firm_users' => FirmUser::collection($this->whenLoaded('users')),

            'is_non_standard_number' => $this->is_non_standard_number,
            'non_standard_number' => $this->non_standard_number,

            'can_edit' => $this->can_edit ?? '',

            'privilege' => new PrivilegeVehicleResource($this->whenLoaded('privilege')),

			'user_id' => $this->user_id,

			'user_name' => $this->user ? $this->user->name : $this->owner_name,

			'files' => File::sortedCollection($this->whenLoaded('files'))
        ];

        return $fields;
    }
}
