<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class Permit extends Resource
{
	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return array
	 */
	public function toArray($request)
	{
		/** @var \App\Models\Permit $this */
		$fields = [
			'id' => $this->id,
			'application_id' => $this->application_id,
			
			'invoice_link' => $this->invoice_link,
		];
		
		return  $fields;
	}
}
