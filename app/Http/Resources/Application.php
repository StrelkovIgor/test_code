<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\ApplicationLoad as ApplicationLoadResource;
use App\Http\Resources\ApplicationRoute as ApplicationRouteResource;
use App\Http\Resources\ApplicationDate as ApplicationDateResource;
use App\Http\Resources\FastApplication as FastApplicationResource;
use App\Http\Resources\ApplicationSmev as ApplicationSmevResource;
use App\Http\Resources\Vehicle as VehicleResource;
use App\Http\Resources\File as FileResource;
use App\Http\Resources\PrivilegeStatus as PrivilegeStatusResource;
use App\Http\Resources\Permit as PermitResource;
use App\Http\Resources\ControlMark as ControlMarkResource;
use App\Http\Resources\ApplicationAgreement as ApplicationAgreementResource;
use App\Http\Resources\FrozenVehicle as FrozenVehicleResource;
use App\Http\Resources\Audit\Audit as AuditResource;

class Application extends Resource
{
	/**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var \App\Models\Application $this */
        $fields = [
            'id' => $this->id,
            'user_id' => $this->user_id,
            'username' => $this->username,
            'vehicle_id' => $this->vehicle_id,

            'comment' => $this->comment,
            'price' => $this->price,
            'spring_price' => $this->spring_price,
            'real_price' => $this->getRealPrice(),
            'federal_price' => $this->federal_price,
            'regional_price' => $this->regional_price,
            'price_info' => $this->price_info,
            'start_date' => $this->startDateFormatted(),
            'finish_date' => $this->finishDateFormatted(),
            'status' => $this->status,
            'start_address' => $this->start_address,
            'finish_address' => $this->finish_address,

            'special_condition_id' => $this->special_condition_id,
            'privilege_status_id' => $this->privilege_status_id,
            'is_draft' => $this->is_draft,

            'is_template' => $this->is_template,
			'accept_date' => $this->accept_date,
			'employee_id' => $this->employee_id,
			'form_id' => $this->form_id,
            'created_at' =>  $this->getCreatedAtFormatted(),
            'updated_at' => $this->getUpdatedAtFormatted(),
			'removed' => $this->deleted_at ? 1 : null,
			'is_locked' => $this->is_locked,
			'is_spring' => $this->is_spring,
			'is_fast' => $this->is_fast,
			'location_type' => $this->location_type,
			'is_frozen' => $this->isFrozen() ? 1 : 0,
			
			'is_smev' => $this->is_smev,
			'checked_number' => $this->getNumberChecked(1),
            'special_conditions' => $this->special_conditions
        ];

        $fields['vehicle'] = $this->getVehicleResource();
        $fields['trailers'] = $this->getTrailersResources();
        $fields['user'] = new RegAppFull($this->whenLoaded('user'));
        $fields['employee'] = new UserEmployee($this->whenLoaded('employee'));

        $fields['load'] = new ApplicationLoadResource($this->whenLoaded('application_load'));
        $fields['fast'] = new FastApplicationResource($this->whenLoaded('fast_application'));
        $fields['smev'] = new ApplicationSmevResource($this->whenLoaded('smev_application'));
        $fields['route'] = new ApplicationRouteResource($this->whenLoaded('application_route'));
        $fields['dates'] = new ApplicationDateResource($this->whenLoaded('application_dates'));
        $fields['permit'] = new PermitResource($this->whenLoaded('permit'));
        
        $fields['privilegeStatus'] = new PrivilegeStatusResource($this->whenLoaded('privilege_status'));
        $fields['admin'] = new User($this->whenLoaded('admin'));
        $fields['control_marks'] = ControlMarkResource::collection($this->whenLoaded('control_marks'));
        
        $fields['application_agreements'] = ApplicationAgreementResource::collection($this->whenLoaded('application_agreements'));


        $fields['events'] = AuditResource::sortedCollection($this->whenLoaded('audits'));

        $fields['files'] = FileResource::sortedCollection($this->whenLoaded('files'));
        $fields['filesInWork'] = FileNew::collection($this->files()->where('type_id', \App\Models\File::TYPE_APPLICATION_IN_WORK)->get());

        if ($this->is_smev) {
            $gpKoeff = \App\Models\Coefficient::getCachedKoefficient(\App\Models\Coefficient::ID_GP);
            $blankKoeff = \App\Models\Coefficient::getCachedKoefficient(\App\Models\Coefficient::ID_PB);
            $gpPrice = $gpKoeff ? $gpKoeff->value : 0;
            $pbPrice = $blankKoeff ?  $blankKoeff->value : 0;

            /** @var \App\Models\Application $this */
            $fields['damage'] = max(0, $this->price - $gpPrice - $pbPrice);
        }

        return $fields;
    }

	public function getCreatedAtFormatted()
	{
		/** @var \App\Models\Application $this */
		$result = null;
		if(is_string($this->created_at)) {
			return $this->created_at;
		}
		return $this->created_at ? $this->created_at->format('Y-m-d H:i:s') : '';
	}

	public function getUpdatedAtFormatted()
	{
		/** @var \App\Models\Application $this */
		$result = null;
		if(is_string($this->updated_at)) {
			return $this->updated_at;
		}
		return $this->updated_at ? $this->updated_at->format('Y-m-d H:i:s') : '';
	}

	public function getVehicleResource()
	{
		/** @var \App\Models\Application $this */
		if($this->isFrozen() && $this->hasFrozenData()) {
			return new FrozenVehicleResource($this->frozen_vehicle);
		}
		return new VehicleResource($this->whenLoaded('vehicle'));
	}

	public function getTrailersResources()
	{
		/** @var \App\Models\Application $this */
		if($this->isFrozen() && $this->hasFrozenData()) {
			return FrozenVehicleResource::collection(($this->whenLoaded('frozen_trailers')));
		}
		return VehicleResource::collection(($this->whenLoaded('trailers')));
	}
}
