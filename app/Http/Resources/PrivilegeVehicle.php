<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class PrivilegeVehicle extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var \App\Models\PrivilegeVehicle $this */
        return [
            'id' => $this->id,
            'is_trailer' => $this->is_trailer,
            'privilege_status_id' => $this->privilege_status_id,

            'number' => $this->number,
            'real_number' => $this->realNumber(),

            'brandTitle' => $this->brand_title,
            'modelTitle' => $this->model_title,

            'privilegeStatus' => new PrivilegeStatus($this->whenLoaded('privilege_status')),

            'created_at' => $this->created_at->format('Y-m-d H:i:s'),
            'updated_at' => $this->updated_at->format('Y-m-d H:i:s'),
        ];
    }
}
