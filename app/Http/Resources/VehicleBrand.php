<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class VehicleBrand extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var \App\Models\VehicleBrand $this */
        return [
            'id' => $this->id,
            'title' => $this->title,
			'accepted' => $this->accepted
        ];
    }
}
