<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\User as UserResource;
use App\Http\Resources\ControlPost as ControlPostResource;

class ControlMark extends Resource
{
	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return array
	 */
	public function toArray($request)
	{
		/** @var \App\Models\ControlMark $this */
		$fields = [
			'id' => $this->id,
			'reverse' => $this->reverse,
			'active' => $this->active,
			'created_at' => $this->created_at->format('Y-m-d H:i:s'),
			'updated_at' => $this->updated_at->format('Y-m-d H:i:s'),
		];
		
		$fields['user'] = new UserResource($this->whenLoaded('user'));
		$fields['control_post'] = new ControlPostResource($this->whenLoaded('control_post'));
		
		return $fields;
	}
}
