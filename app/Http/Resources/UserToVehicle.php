<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class UserToVehicle extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var \App\Models\UserToVehicle $this */
        return [
            'user_id' => $this->user_id,
            'vehicle_id' => $this->vehicle_id,
        ];
    }
}
