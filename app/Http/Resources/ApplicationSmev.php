<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class ApplicationSmev extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var \App\Models\ApplicationSmev $this */
        $fields = [
            'id' => $this->id,
            'application_id' => $this->application_id,
            
            'approval_id' => $this->approval_id,
            'authority_name' => $this->authority_name,
            'output_number' => $this->output_number,
            'output_date' => $this->formattedDate(),
            'special_conditions' => $this->special_conditions,
            'ecp' => $this->ecp,
            'created_at' => $this->created_at->format('Y-m-d H:i:s'),
            'answer_at' => $this->getAnswerDate()
        ];
        
        
        return  $fields;
    }
}
