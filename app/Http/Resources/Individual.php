<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class Individual extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);
        /** @var \App\Models\Individual $this */
        return [
            'id' => $this->id,
            'inn' => $this->inn,
            'updated_at' => $this->updatedAtFormatted()
        ];
    }
}
