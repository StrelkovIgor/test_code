<?php

namespace App\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * These middleware are run during every request to your application.
     *
     * @var array
     */
    protected $middleware = [
        \Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode::class,
        \Illuminate\Foundation\Http\Middleware\ValidatePostSize::class,
        \App\Http\Middleware\TrimStrings::class,
        \Illuminate\Foundation\Http\Middleware\ConvertEmptyStringsToNull::class,
        \App\Http\Middleware\TrustProxies::class,
        \Barryvdh\Cors\HandleCors::class
    ];

    /**
     * The application's route middleware groups.
     *
     * @var array
     */
    protected $middlewareGroups = [
        'web' => [
            \App\Http\Middleware\EncryptCookies::class,
            \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
            \Illuminate\Session\Middleware\StartSession::class,
            // \Illuminate\Session\Middleware\AuthenticateSession::class,
            \Illuminate\View\Middleware\ShareErrorsFromSession::class,
            \App\Http\Middleware\VerifyCsrfToken::class,
            \Illuminate\Routing\Middleware\SubstituteBindings::class,
        ],

        'api' => [
            //'throttle:300,1',
            'bindings',

            \Barryvdh\Cors\HandleCors::class,
			\App\Http\Middleware\ProfileJsonResponse::class
        ],
        'eputs' => [
//            \Barryvdh\Cors\HandleCors::class,
//            \App\Http\Middleware\ProfileJsonResponse::class,
        ]
    ];

    /**
     * The application's route middleware.
     *
     * These middleware may be assigned to groups or used individually.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'auth' => \Illuminate\Auth\Middleware\Authenticate::class,
        'auth.basic' => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
        'bindings' => \Illuminate\Routing\Middleware\SubstituteBindings::class,
        'can' => \Illuminate\Auth\Middleware\Authorize::class,
        'guest' => \App\Http\Middleware\RedirectIfAuthenticated::class,
        'throttle' => \Illuminate\Routing\Middleware\ThrottleRequests::class,
		'request_logger' => \App\Http\Middleware\RequestLogger\ApiDataLogger::class,

		//auth
		'authClient' => \App\Http\Middleware\Auth\AuthClient::class,
		'authFirm' => \App\Http\Middleware\Auth\AuthFirm::class,
		'authClientEmployee' => \App\Http\Middleware\Auth\AuthClientEmployee::class,
		'authEmployee' => \App\Http\Middleware\Auth\AuthEmployee::class,

		'authAdmin' => \App\Http\Middleware\Auth\AuthAdmin::class,
		'authAdminGbu' => \App\Http\Middleware\Auth\AuthAdminGbu::class,
		'authAdminGbuDepagent' => \App\Http\Middleware\Auth\AuthAdminGbuDepagent::class,

		'authDepagent' => \App\Http\Middleware\Auth\AuthDepagent::class,
		'authControl' => \App\Http\Middleware\Auth\AuthWeightControl::class,
        'cors' => \App\Http\Middleware\Cors::class,
        'NotCookies' => \App\Http\Middleware\NotCookies::class,
        'AuthEputs'=> \App\Http\Middleware\Eputs\AuthEputs::class,
    ];
}
