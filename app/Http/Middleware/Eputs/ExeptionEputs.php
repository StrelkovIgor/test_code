<?php

namespace App\Http\Middleware\Eputs;

use App\Helpers\Eputs as EputsHelpers;
use App\Http\Resources\Eputs\Eputs as EputsResources;
use App\Models\Eputs;
use Closure;

class ExeptionEputs
{
    protected $allowedRoles = [];

	/**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);

        if (!empty($response->exception) && $response->exception instanceof FormValidationException) {
//            return redirect()->back()->withErrors($response->exception->form->getErrors())->withInput();
            var_dump($response->exception->form->getErrors());exit;
        }

        $response = new EputsHelpers();
        if(Eputs::i()->hasToken()) {
			return $next($request);
		}

        return new EputsResources($response->setError("You are not authorized to do this."));
    }
}
