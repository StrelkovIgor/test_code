<?php

namespace App\Http\Middleware\Eputs;

use App\Helpers\Eputs as EputsHelpers;
use App\Http\Resources\Eputs\Eputs as EputsResources;
use App\Models\Eputs;
use Closure;

class AuthEputs
{
    protected $allowedRoles = [];

	/**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = new EputsHelpers();
        if(Eputs::i()->hasToken()) {
			return $next($request);
		}
        return new EputsResources($response->setError("You are not authorized to do this."));
    }
}
