<?php

namespace App\Http\Middleware\RequestLogger;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Log;

class ApiDataLogger
{
	/** @var  int */
	private $startTime;

	/** @var  IRequestLogger */
	private $logger;

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		$this->startTime = microtime(true);
		return $next($request);
	}

	/**
	 * @param Request $request
	 * @param Response $response
	 */
	public function terminate($request, $response)
	{
		if ( env('API_DATALOGGER', true) ) {
			$endTime = microtime(true);
			$duration = $endTime - LARAVEL_START;

			$this->initLogger();
			try {
				$this->logger->store($request, $response, $duration);
			} catch (\Exception $e) {
                Log::info('Logger failed', [$e->getMessage()]);
			}
		}
	}

	private function initLogger()
	{
		$this->logger = new \RequestLogger();
	}
}