<?php
/**
 * Created by PhpStorm.
 * User: gserg13
 * Date: 25.08.2019
 * Time: 10:53
 */

namespace App\Http\Middleware\RequestLogger;


use Illuminate\Http\Request;
use Illuminate\Http\Response;

interface IRequestLogger
{
	/**
	 * @param Request $request
	 * @param Response $response
	 * @param float $duration
	 * @return
	 */
	public function store($request, $response, $duration);
}