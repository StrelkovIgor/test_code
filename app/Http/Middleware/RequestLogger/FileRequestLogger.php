<?php
/**
 * Created by PhpStorm.
 * User: gserg13
 * Date: 25.08.2019
 * Time: 10:55
 */

namespace App\Http\Middleware\RequestLogger;


use Illuminate\Http\Request;
use Illuminate\Http\Response;

class FileRequestLogger implements IRequestLogger
{
	/**
	 * @param Request $request
	 * @param Response $response
	 * @param float $duration
	 * @return
	 */
	public function store($request, $response, $duration)
	{
		$filename = 'api_datalogger_' . date('Y-m-d') . '.log';
		$dataToLog  = 'Time: '   . gmdate("F j, Y, g:i a") . "\n";
		$dataToLog .= 'Duration: ' . number_format($duration, 3) . "\n";
		$dataToLog .= 'IP Address: ' . $request->ip() . "\n";
		$dataToLog .= 'URL: '    . $request->fullUrl() . "\n";
		$dataToLog .= 'Method: ' . $request->method() . "\n";
		$dataToLog .= 'Input: '  . $request->getContent() . "\n";
		$dataToLog .= 'Output: ' . $response->getContent() . "\n";
		\File::append( storage_path( 'app' . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'logs' .DIRECTORY_SEPARATOR . $filename), $dataToLog . "\n" . str_repeat("=", 20) . "\n\n");
	}
}