<?php
/**
 * Created by PhpStorm.
 * User: gserg13
 * Date: 25.08.2019
 * Time: 10:55
 */

namespace App\Http\Middleware\RequestLogger;


use App\Helpers\DateTimeConverter;
use App\Models\RequestLog;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class DbRequestLogger implements IRequestLogger
{
	private $eventDateTimeFormat = 'Y.m.d H:i:s';
	private $dbDateTimeFormat = 'Y-m-d H:i:s';

	/**
	 * @param Request $request
	 * @param Response $response
	 * @param float $duration
	 * @return
	 */
	public function store($request, $response, $duration)
	{
		$data = $request->all();
		$eventDateTime = array_get($data, 'EVENT_DATETIME');
		if($eventDateTime) {
			$eventDateTime = DateTimeConverter::convert($eventDateTime, $this->eventDateTimeFormat, $this->dbDateTimeFormat);
		}
		RequestLog::create([
			'duration' => $duration,
			'ip_address' => $request->ip(),
			'url' => $request->fullUrl(),
			'method' => $request->method(),
			'request' => $request->all(),
			'response' => json_decode($response->getContent(), true),

			'number' => array_get($data, 'GOSNUM'),
			'event_datetime' => $eventDateTime,
		]);
	}

	public function changeDateFormat($date, $sourceFormat, $targetFormat) {
		$date = DateTime::createFromFormat($sourceFormat, $date);
		if(!$date) {
			return null;
		}
		return $date->format($targetFormat);
	}
}