<?php

namespace App\Http\Middleware\Auth;

use App\Models\Role;

class AuthFirm extends AuthRole
{
    protected $allowedRoles = [Role::ROLE_FIRM];
}
