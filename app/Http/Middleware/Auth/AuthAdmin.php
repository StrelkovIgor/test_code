<?php

namespace App\Http\Middleware\Auth;

use App\Models\Role;

class AuthAdmin extends AuthRole
{
	protected $allowedRoles = [Role::ROLE_ADMIN];
}
