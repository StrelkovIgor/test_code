<?php

namespace App\Http\Middleware\Auth;

use App\Models\Role;

class AuthAdminGbu extends AuthRole
{
	protected $allowedRoles = [Role::ROLE_ADMIN, Role::ROLE_OFFICER];
}
