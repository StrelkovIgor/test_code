<?php

namespace App\Http\Middleware\Auth;

use Closure;

class AuthRole
{
    protected $allowedRoles = [];

	/**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = $request->user();
        if(
        	$request->getMethod() === 'GET' ||
			($user && array_search($user->role_id, $this->allowedRoles) !== false)
		) {
			return $next($request);
		}

		return response()->json(['error' => 'У вас нет прав на это действие'], 401);
    }
}
