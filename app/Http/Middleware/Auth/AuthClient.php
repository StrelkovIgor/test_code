<?php

namespace App\Http\Middleware\Auth;

use App\Models\Role;

class AuthClient extends AuthRole
{
	protected $allowedRoles = [Role::ROLE_INDIVIDUAL, Role::ROLE_FIRM];
}
