<?php

namespace App\Http\Middleware\Auth;

use App\Models\Role;

class AuthWeightControl extends AuthRole
{
	protected $allowedRoles = [Role::ROLE_WEIGHT_CONTROL];
}
