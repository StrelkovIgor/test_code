<?php

namespace App\Http\Middleware\Auth;

use App\Models\Role;

class AuthClientEmployee extends AuthRole
{
	protected $allowedRoles = [Role::ROLE_INDIVIDUAL, Role::ROLE_FIRM, Role::ROLE_FIRM_USER];
}
