<?php

namespace App\Http\Middleware\Auth;

use App\Models\Role;

class AuthEmployee extends AuthRole
{
	protected $allowedRoles = [Role::ROLE_FIRM_USER];
}
