<?php

namespace App\Http\Middleware\Auth;

use App\Models\Role;

class AuthDepagent extends AuthRole
{
	protected $allowedRoles = [Role::ROLE_DEPARTMENT_AGENT];
}
