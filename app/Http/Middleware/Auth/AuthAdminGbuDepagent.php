<?php

namespace App\Http\Middleware\Auth;

use App\Models\Role;

class AuthAdminGbuDepagent extends AuthRole
{
	protected $allowedRoles = [Role::ROLE_ADMIN, Role::ROLE_OFFICER, Role::ROLE_DEPARTMENT_AGENT];
}
