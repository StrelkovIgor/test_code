<?php

namespace App\Http\Controllers;

use App\Models\Role;
use App\Models\User;
use App\Models\UserToVehicle;
use App\Models\Vehicle;
use Illuminate\Http\Request;
use App\Http\Resources\FirmUser as FirmUserResource;
use App\Http\Resources\UserToVehicle as UserToVehicleResource;
use App\Http\Requests\FirmUser\FirmUserCreate as FirmUserCreateRequest;
use App\Http\Requests\FirmUser\FirmUserUpdate as FirmUserUpdateRequest;

class FirmUserController extends Controller
{
    /**
     * @var int
     */
    private $perPage = 20;
    
    /**
     * Get user's paginated firm users list
     * @param Request $request
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function page(Request $request)
    {
        /** @var User $user */
        $user = $request->user();
        $query = User::where('owner_id', $user->id)
            ->where('role_id', Role::ROLE_FIRM_USER);
    
        //filter
        $data = $request->all();
        $query = User::ownerDataFilter($query, $data);
    
        $per_page = intval(array_get($data, 'per_page', $this->perPage));
    
        return FirmUserResource::collection(
            $query
                ->orderBy('users.updated_at', 'desc')
                ->paginate($per_page)
                ->appends('paged')
        );
    }
    /**
     * @param User $user
     * @return User
     */
    public function show(User $user)
    {
        return response()->json([
            'data' => new FirmUserResource($user),
        ]);
    }
    
    /**
     * Create new firm user
     * @param FirmUserCreateRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(FirmUserCreateRequest $request)
    {
        $owner = $request->user();
        $data = $request->all();
        
        $firmUser = User::createFirmUser($owner, $data);
        
        return response()->json(new FirmUserResource($firmUser), 201);
    }
    
    /**
     * Update firm user
     * @param FirmUserUpdateRequest $request
     * @param User $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(FirmUserUpdateRequest $request, User $user)
    {
        $owner = $request->user();
        
        if (!$this->checkAccess($owner, $user)) {
            return response()->json(['error' => 'У вас нет прав на это действие'], 401);
        }
    
        $data = [
            'name' => $request->get('name'),
            'email' => $request->get('email')
        ];
        $user->update($data);
        
        return response()->json(new FirmUserResource($user), 200);
    }
    
    /**
     * Remove firm user
     * @param Request $request
     * @param User $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(Request $request, User $user)
    {
        $owner = $request->user();
    
        if (!$this->checkAccess($owner, $user)) {
            return response()->json(['error' => 'У вас нет прав на это действие'], 401);
        }
        
        $user->delete();
        
        return response()->json(null, 204);
    }
    
    /**
     * Set access for user to vehicle
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function access(Request $request) {
        $owner = $request->user();
        $data = $request->all();
        
        $user_id = array_get($data, 'user_id');
        $user = $user_id ? User::find($user_id) : null;
        
        $vehicle_id = array_get($data, 'vehicle_id');
        $vehicle = $vehicle_id ? Vehicle::find($vehicle_id) : null;
        
        $access = intval(array_get($data, 'access', 0));
        $can_edit = intval(array_get($data, 'can_edit', 0));

        if (
            !$user ||
            !$vehicle ||
            !$this->checkAccess($owner, $user) ||
            !$this->checkVehicleAccess($owner, $vehicle)
        ) {
            return response()->json(['error' => 'У вас нет прав на это действие'], 401);
        }
        
        //handle change
        /** @var UserToVehicle $userToVehicle */
        $userToVehicle = UserToVehicle
            ::where('user_id', $user_id)
            ->where('vehicle_id', $vehicle_id)
            ->first();
        
        $success = false;
        if($userToVehicle) {
            if($access) {
                //edit current bind
                $userToVehicle->can_edit = $can_edit;
                $success = $userToVehicle->save();
            } else {
                //remove bind
                $userToVehicle->delete();
            }
        } else {
            if($access) {
                //create bind
                UserToVehicle::create([
                    'user_id' => $user_id,
                    'vehicle_id' => $vehicle_id,
                    'can_edit' => $can_edit,
                ]);
            }else {
                //do nothing
            }
        }
    
        return response()->json(['success' => true], 200);
    }
    
    /**
     * Get user's binds user to vehicles
     * @param Request $request
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function binds(Request $request)
    {
        /** @var User $user */
        $user = $request->user();
        
        $binds = UserToVehicle::select('user_to_vehicle.*')
            ->leftjoin('users', 'users.id', '=', 'user_to_vehicle.user_id')
            ->where('users.owner_id', $user->id)
            ->where('users.role_id', Role::ROLE_FIRM_USER)
            ->get();
        
        return UserToVehicleResource::collection(
            $binds
        );
    }
    
    /**
     * @param User $owner
     * @param User $user
     * @return bool
     */
    private function checkAccess($owner, $user)
    {
        return $user->owner_id === $owner->id;
    }
    
    /**
     * @param User $owner
     * @param Vehicle $vehicle
     * @return bool
     */
    private function checkVehicleAccess($owner, $vehicle)
    {
        return $vehicle->user_id === $owner->id;
    }
}
