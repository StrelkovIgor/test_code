<?php

namespace App\Http\Controllers;

use App\Http\Requests\VehicleModel\VehicleModelCreate;
use App\Http\Requests\VehicleModel\VehicleModelUpdate;
use App\Models\VehicleModel;
use App\Http\Resources\VehicleModel as VehicleModelResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class VehicleModelController extends Controller
{
	private $perPage = 20;
	
	/**
	 * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
	 */
	public function index()
	{
		$brand_id = Input::get('brand_id');
		if ($brand_id) {
			return VehicleModelResource::collection(
				VehicleModel::where('brand_id', $brand_id)
					->where('accepted', VehicleModel::STATUS_ACCEPTED_YES)
					->with(['vehicle_axles'])
					->orderBy('title', 'asc')
					->get()
			);
		}
		return VehicleModelResource::collection(VehicleModel::all());
	}
	
	/**
	 * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
	 */
	public function page(Request $request)
	{
		$data = $request->all();
	    
	    $query = VehicleModel::with('vehicle_brand')
            ->where('vehicle_models.accepted', '<>', VehicleModel::STATUS_ACCEPTED_HIDE);
		
		$query = VehicleModel::adminDataFilter($query, $data);
	    
	    return VehicleModelResource::collection(
			$query
				->paginate($this->perPage)
				->appends('paged')
		);
	}
	
	/**
	 * @param VehicleModel $vehicleModel
	 * @return VehicleModel
	 */
	public function show(VehicleModel $vehicleModel)
	{
		return response()->json([
			'data' => new VehicleModelResource($vehicleModel),
		]);
	}
	
	/**
	 * @param VehicleModelCreate $request
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function store(VehicleModelCreate $request)
	{
		$data = $request->all();
		if (isset($data['size'])) {
			$sizes = explode('*', $data['size']);
			
			$data['height'] = floatval($sizes[0] ?? 0);
			$data['length'] = floatval($sizes[1] ?? 0);
			$data['width'] = floatval($sizes[2] ?? 0);
		}
		
		$vehicleModel = VehicleModel::create($data);
		
		return response()->json(new VehicleModelResource($vehicleModel), 201);
	}
	
	/**
	 * @param VehicleModelUpdate $request
	 * @param VehicleModel $vehicleModel
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function update(VehicleModelUpdate $request, VehicleModel $vehicleModel)
	{
		$data = $request->all();
		if (isset($data['size'])) {
			$sizes = explode('*', $data['size']);
			
			$data['height'] = floatval(trim($sizes[0]) ?? 0);
			$data['length'] = floatval(trim($sizes[1]) ?? 0);
			$data['width'] = floatval(trim($sizes[2]) ?? 0);
		}
		$vehicleModel->update($data);
		
		return response()->json(new VehicleModelResource($vehicleModel), 200);
	}
	
	/**
	 * @param VehicleModel $vehicleModel
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function delete(VehicleModel $vehicleModel)
	{
		$success = false;
		if ($vehicleModel->vehicles->isEmpty()) {
			$success = $vehicleModel->delete();
		}
		
		return response()->json(['success' => $success], 200);
	}
	
	/**
	 * @param VehicleModel $vehicleModel
	 * @return VehicleModel
	 */
	public function accept(VehicleModel $vehicleModel)
	{
		$vehicleModel->accepted = VehicleModel::STATUS_ACCEPTED_YES;
		$success = $vehicleModel->save();
		return response()->json(['success' => $success], 200);
	}

	/**
	 * @param VehicleModel $vehicleModel
	 * @return VehicleModel
	 */
	public function hide(VehicleModel $vehicleModel)
	{
		$vehicleModel->accepted = VehicleModel::STATUS_ACCEPTED_HIDE;
		$success = $vehicleModel->save();
		return response()->json(['success' => $success], 200);
	}
}
