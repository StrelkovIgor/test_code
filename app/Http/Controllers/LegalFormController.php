<?php

namespace App\Http\Controllers;

use App\Http\Requests\LegalForm\LegalFormCreate;
use App\Http\Requests\LegalForm\LegalFormUpdate;
use App\Models\LegalForm;
use App\Http\Resources\LegalForm as LegalFormResource;

class LegalFormController extends Controller
{
    private $perPage = 7;

    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        return LegalFormResource::collection(
            LegalForm::orderBy('title', 'asc')
                ->get()
        );
    }

    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function page()
    {
        return LegalFormResource::collection(
            LegalForm::orderBy('id', 'desc')
                ->paginate($this->perPage)
                ->appends('paged')
        );
    }

    /**
     * @param LegalForm $legalForm
     * @return LegalForm
     */
    public function show(LegalForm $legalForm)
    {
        return response()->json([
            'data' => new LegalFormResource($legalForm),
        ]);
    }

    /**
     * @param LegalFormCreate $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(LegalFormCreate $request)
    {
        $legalForm = LegalForm::create($request->all());

        return response()->json(new LegalFormResource($legalForm), 201);
    }

    /**
     * @param LegalFormUpdate $request
     * @param LegalForm $legalForm
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(LegalFormUpdate $request, LegalForm $legalForm)
    {
        $legalForm->update($request->all());

        return response()->json(new LegalFormResource($legalForm), 200);
    }

    /**
     * @param LegalForm $legalForm
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(LegalForm $legalForm)
    {
        $legalForm->delete();

        return response()->json(null, 204);
    }
}
