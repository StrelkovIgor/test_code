<?php

namespace App\Http\Controllers;

use App\Http\Requests\User\ChangePassword;
use App\Mail\UserCreate;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Resources\RegAppFull as RegAppFullResource;
use App\Http\Resources\FirmShort as FirmShortResource;
use App\Http\Requests\User\Cabinet as CabinetRequest;
use App\Http\Requests\User\AdminCreate as AdminCreateRequest;
use App\Http\Resources\User as UserResource;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\Http\Resources\UserFirmFull as UserFirmFullResource;

class UserController extends Controller
{
    private $perPage = 20;

    /**
	 * @param Request $request
	 *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        $data = $request->all();
        $query = User::adminUserFilterQuery($data);
        
        return UserResource::collection(
            $query
				->orderBy('id', 'desc')
                ->paginate($this->perPage)
                ->appends('paged')
        );
    }

    /**
     * @param User $user
	 *
     * @return User
     */
    public function show(User $user)
    {
        if ($user->role_id === Role::ROLE_INDIVIDUAL) {
            $user->load('individual');
        }
        if ($user->role_id === Role::ROLE_FIRM) {
            $user->load('firm');
        }
        return response()->json([
            'data' => new RegAppFullResource($user),
        ]);
    }

    /**
     * @param User $user
	 *
     * @return User
     */
    public function adminFull(User $user)
    {
    	$user->load('firm');

        return response()->json([
            'data' => new UserFirmFullResource($user)
        ]);
    }
	
	/**
	 * Get all firmusers for user
	 * @param Request $request
	 *
	 * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
	 */
    public function firmUsers(Request $request)
    {
        /** @var User $user */
        $user = $request->user();
        
		$query = User::firmUserQuery($user->id);
		
        return UserResource::collection(
            $query->orderBy('id', 'desc')
				->get()
        );
    }


    /**
	 * @param Request $request
	 *
     * @return User
     */
    public function me(Request $request)
    {
        /** @var User $user */
        $user = $request->user();
        
        if ($user->role_id === Role::ROLE_INDIVIDUAL) {
            $user->load('individual');
        }
        if ($user->role_id === Role::ROLE_FIRM) {
            $user->load(['firm', 'files']);
        }

        return response()->json([
            'data' => new RegAppFullResource($user),
        ]);
    }
	
	/**
	 * @param Request $request
	 *
	 * @return User
     */
    public function myFirm(Request $request)
    {
		/** @var User $user */
		$user = $request->user();

        if ($user->owner) {
            return response()->json([
                'data' => new FirmShortResource($user->owner),
            ]);
        }
        return response()->json([], 404);
    }

    /**
     * Save my info
     * @param CabinetRequest $request
     *
     * @return User
     */
    public function updateMe(CabinetRequest $request)
    {
		/** @var User $user */
		$user = $request->user();
		
    	$data = $request->all();
		
		$user->cabinetUpdate($data);

        return response()->json([
            'data' => new RegAppFullResource($user),
        ], 201);
    }

    /**
     * @param AdminCreateRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(AdminCreateRequest $request)
    {
        $data = $request->all();
        $password = str_random(8);
        $data['password'] = bcrypt($password);

        if ($user = User::create($data)) {
            //send email
            Mail::to($user->email)
                ->send(new UserCreate($user, $password));
        }

        return response()->json($user, 201);
    }

    /**
     * @param AdminCreateRequest $request
     * @param User $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(AdminCreateRequest $request, User $user)
    {
        $user->update($request->all());

        return response()->json($user, 200);
    }

    /**
     * @param User $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(User $user)
    {
        $user->delete();

        return response()->json(null, 204);
    }

    /**
     * @param ChangePassword $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function changePassword(ChangePassword $request)
    {
        /** @var User $user */
        $user = Auth::user();
        $data = $request->all();
        if (Hash::check($data['password_old'], $user->password)) {
            $user->password = bcrypt($data['password']);
            //$user->generateToken();
            if ($user->save()) {
                return response()->json([
                    'success' => true,
                    'data' => new UserResource($user),
                ]);
            } else {
                return response()->json([
                    'success' => false
                ]);
            }
        } else {
            return response()->json([
                'message' => 'Неправильный текущий пароль',
                'errors' => [
                    'password_old' => 'Неверный пароль!'
                ]
            ], 422);
        }
    }
}
