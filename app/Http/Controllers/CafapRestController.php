<?php

namespace App\Http\Controllers;

use App\Http\Requests\Application\CafapRequest;
use App\Http\Requests\AxleLoad\AxleLoadUpdate;
use App\Models\Application;
use App\Models\AxleLoad;
use App\Http\Resources\AxleLoad as AxleLoadResource;
use Illuminate\Http\Request;
use \App\Http\Resources\ApplicationCafap as ApplicationCafapResource;

class CafapRestController extends Controller
{
	private $perPage = 20;

	/**
	 * @param CafapRequest $request
	 *
	 * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
	 */
	public function index(CafapRequest $request)
	{
		$data = $request->parseData();

		//check signature
		if(!$request->checkSignature()) {
			return response()->json(['error' => 'Неправильная подпись!'], 401);
		}

		$query = Application::cafapStatusQuery();
		$query = Application::cafapDataFilter($query, $data);
		$applications = $query
			->select('applications.*')
			->leftjoin('vehicles as v', 'v.id', '=', 'applications.vehicle_id')
			->with([
				'vehicle',
				'trailers',
				'application_dates',
				'permit',
				'application_route',
				'application_load',
				'privilege_status'
			])
			->orderBy('applications.updated_at', 'desc')
			->get();

		return ApplicationCafapResource::collection($applications);
	}
}
