<?php

namespace App\Http\Controllers;

use App\Models\Process;
use App\Http\Resources\Process as ProcessResource;
use App\Models\User;
use Faker\Provider\DateTime;
use Illuminate\Http\Request;

class ProcessController extends Controller
{
	/**
	 * ControlPostController constructor.
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * @param Process $process
	 * @return
	 */
	public function show(Process $process)
	{
		return response()->json([
			'data' => new ProcessResource($process),
		]);
	}

	/**
	 * @param Request $request
	 * @return
	 */
	public function list(Request $request)
	{
		/** @var User $user */
		$user = $request->user();

		$type = $request->query('type', Process::TYPE_ACTIVE_REPORT);

		return  ProcessResource::collection(
			Process
				::where('user_id', $user->id)
				->where('type', $type)
				->where('created_at', '>=', $this->getDateInterval())
				->get()
		);
	}

	/**
	 * @return string
	 */
	private function getDateInterval()
	{
		$now = new \DateTime();
		$now->modify('-1 day');

		return $now->format('Y-m-d H:i:s');
	}
}
