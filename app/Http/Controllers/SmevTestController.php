<?php

namespace App\Http\Controllers;

use App\Helpers\SmevFormatter;
use Illuminate\Http\Request;
use Log;

class SmevTestController extends Controller
{
	public function registerTest(Request $request) {
        Log::info('Smev test', $request->all());
    }
    
    public function register(Request $request)
	{
		$data = $this->parseRequest($request);
		
		$success = SmevFormatter::createSmevApplication($data);

		print_r($success);die;
	}
	
	private function parseRequest(Request $request)
	{
		$result = [];
		
		$xmlStr = $request->getContent();
		$start = strpos($xmlStr, '<data>');
		$finish = strpos( $xmlStr, '</data>');
		$subXml = trim(substr($xmlStr, $start, $finish - $start)) . '</data>';
//		print_r($subXml);die;
		$xml = simplexml_load_string($subXml);
		
		//print_r($xml->data);die;
		foreach($xml->field as $field) {
			$name = (string)($field['name']);
			$value = (string)($field->value[0]);

			$result[$name] = $value;
		}
		
		return $result;
	}
	

}