<?php

namespace App\Http\Controllers;

use App\Helpers\SmevFormatter;
use App\Models\Application;
use Illuminate\Http\Request;

class SmevController extends Controller
{
    /**
     * Register smev application
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request)
    {
        $data = $request->all();
        
        $success = SmevFormatter::createSmevApplication($data);
        if(!$success) {
            return response()->json([
                'Value' => 'ERROR'
            ], 200);
        }
        
        return response()->json([
            'Value' => 'OK',
            'ErrorCode' => "0",
        ]);
    }

    /**
     * Send files to smev application
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function sendFiles(Request $request)
    {
        $data = $request->all();

        $success = SmevFormatter::sendFiles($data);

        if(!$success) {
            return response()->json([
                'Value' => 'ERROR',
            ], 200);
        }

        return response()->json([
            'Value' => 'OK',
            'ErrorCode' => "0",
        ]);
    }

    /**
     * Check status of smev application
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function check(Request $request)
    {
        $data = $request->all();

        $resultData = SmevFormatter::checkApplication($data);

        if(empty($resultData)) {
            return response()->json([
                'Value' => 'ERROR'
            ], 200);
        }

        return response()->json($resultData);
    }

    /**
     * Cancel smev application
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function cancel(Request $request)
    {
        $data = $request->all();

        $success = SmevFormatter::cancelApplication($data);

        if(!$success) {
            return response()->json([
                'Value' => 'ERROR',
                'ErrorCode' => "0",
            ], 200);
        }

        return response()->json([
            'Value' => 'OK',
            'ErrorCode' => "0",
        ]);
    }
    public function stop(Request $request){
        $data = $request->all();
        $approvalId = array_get($data, 'ApprovalID');

        if(!$approvalId) return $this->respons(true);

        $application = Application::findByApprovalId($approvalId);
        if(!$application) return $this->respons(true);
        $status = $application->getSmevStatus();

        if($status == Application::SMEV_STATUS_IN_PROGRESS){
            $application->status = Application::STATUS_NEW;
            $application->save();
            return $this->respons();
        }
    }

    public function respons($error = false){
        return response()->json([
            'Value' => $error ? 'ERROR' : 'OK',
            'ErrorCode' => "0",
        ], 200);
    }
}