<?php

namespace App\Http\Controllers;

use App\Helpers\AxleGroup;
use App\Helpers\CSVHelper;
use App\Helpers\ExcelHelper;
use App\Helpers\GradoserviceDistanceHelper;
use App\Helpers\MapBoxDistanceHelper;
use App\Helpers\PdfGenerator;
use App\Helpers\PriceCalculator;
use App\Http\Requests\Application\PrintRequest;
use App\Http\Resources\Application as ApplicationResource;
use App\Http\Resources\ApplicationLoad as ApplicationLoadResource;
use App\Http\Resources\ApplicationRoute as ApplicationRouteResource;
use App\Http\Resources\ApplicationDate as ApplicationDateResource;
use App\Http\Resources\ControlMark as ControlMarkResource;
use App\Http\Resources\ApplicationAgreement as ApplicationAgreementResource;
use App\Http\Requests\Application\ApplicationVehicle as ApplicationVehicleRequest;
use App\Http\Requests\Application\ApplicationLoad as ApplicationLoadRequest;
use App\Http\Requests\Application\ApplicationRoute as ApplicationRouteRequest;
use App\Http\Requests\Application\ApplicationDate as ApplicationDateRequest;
use App\Models\Application;
use App\Models\ApplicationAgreement;
use App\Models\ApplicationDate;
use App\Models\ApplicationLoad;
use App\Models\ApplicationRoute;
use App\Models\AxleLoad;
use App\Models\ControlMark;
use App\Models\Permit;
use App\Models\Role;
use App\Models\Audit\Enum\Events;
use App\Models\RouteAddress;
use App\Models\RouteList;
use App\Models\RouteListsPoint;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Resources\RouteList as RouteListResource;

class ApplicationController extends Controller
{
	/** @var int Application per page on frontend */
	private $perPage = 5;
	
	/**
	 * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
	 */
	public function index()
	{
		return ApplicationResource::collection(
			User::where('confirmation_status', User::CONFIRM_STATUS_NEW)
				->paginate($this->perPage)
				->appends('paged')
		);
	}
	
	/**
	 * Get user's app templates
	 * @param Request $request
	 * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
	 */
	public function templates(Request $request)
	{
		/** @var User $user */
		$user = $request->user();
		
		$query = Application::userTemplatesQuery($user);
		
		return ApplicationResource::collection(
			$query
				->orderBy('updated_at', 'desc')
				->with(['user', 'vehicle', 'trailer', 'privilege_status'])
				->get()
		);
	}
	
	/**
	 * Use template
	 * @param Request $request
	 * @param Application $application
	 * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
	 */
	public function useTemplate(Request $request, Application $application)
	{
		$newApp = $application->templateToNewApplication();
		
		return response()->json([
			'data' => new ApplicationResource($newApp),
		]);
	}
	
	/**
	 * @param Request $request
	 * @param string $status
	 * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
	 */
	public function userFilter(Request $request, $status)
	{
		$status = intval($status);
		/** @var User $user */
		$user = $request->user();
		
		//Apply status filter
		$query = Application::userStatusQuery($status);
		
		//Apply role filter
		$query = Application::userRoleFilter($query, $user);
		
		//Apply data filter
		$data = $request->all();
		$query = Application::userDataFilter($query, $data);
		
		return ApplicationResource::collection(
			$query
				->select('applications.*')
				->distinct()
				->with([
					'vehicle',
					'trailers',
					'user.firm.legal_form',
					'admin',
					'permit',
					'application_dates',
					'privilege_status'
				])
				->orderBy('applications.updated_at', 'desc')
				->paginate($this->perPage)
				->appends('paged')
		);
	}
	
	/**
	 * @param Request $request
	 * @param int $status
	 * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
	 */
	public function adminFilter(Request $request, $status)
	{
		/** @var User $user */
		$user = $request->user();

		$status = intval($status);
		if (!$status) {
			$status = Application::STATUS_NEW;
		}

        //filter by form data
        $data = $request->all();
        //check fast param
        if($user->role_id !== Role::ROLE_DEPARTMENT_AGENT) {
            $data['is_fast'] = $request->query('is_fast', 0);
            $querySmev = $request->query('is_smev', null);
            if ($querySmev !== null) {
                $data['is_smev'] = $querySmev;
            }
        }else{
            unset($data['is_fast']);
            unset($data['is_smev']);
        }

		//filter by status
		$query = Application::adminStatusQuery($status, $user, $data);
  

		$query = Application::adminDataFilter($query, $data, $user);
		//filter by role
		$query = Application::adminRoleFilter($query, $user, $status, $data);
		
		//sorting
		$query = Application::adminSorting($query, $data);
		
		
		$statusField = $user->role_id === Role::ROLE_DEPARTMENT_AGENT ? 'application_agreements.status' : 'applications.status';

		//get per_page count
		$perPage = intval(array_get($data, 'per_page', $this->perPage));

		return ApplicationResource::collection(
			$query
				->select([
					'applications.*',
					$statusField . ' as status'
				])
				->with([
//					'vehicle.vehicle_model',
//					'vehicle.vehicle_brand',
//					'vehicle',

					'trailers',
					'user.firm.legal_form',
					'employee',
					'admin',
					'application_dates',
					'permit',
					'application_agreements.department',
					'application_agreements.audits_agreements.user',
					'application_load',
					'privilege_status',
					'audits.user',
					'fast_application.user',
					'smev_application',
					'files'
				])
				->paginate($perPage)
				->appends('paged')
		);
	}
	
	/**
	 * Get filtered apps for weight control
	 * @param Request $request
	 * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
	 */
	public function controlFilter(Request $request)
	{
		/** @var User $user */
		$user = $request->user();
		
		//filter by status
		$query = Application::controlStatusQuery();
		
		//filter by form data
		$data = $request->all();
		$query = Application::controlDataFilter($query, $data);
		
		return ApplicationResource::collection(
			$results = $query
				->select('applications.*')
				->with([
					'vehicle',
					'trailers',
					'user.firm.legal_form',
					'employee',
					'application_dates',
					'privilege_status',
					'fast_application.user'
				])
				->orderBy('applications.updated_at', 'desc')
				->get()
		);
	}
	
	/**
	 * Get application full info
	 * @param Request $request
	 * @param Application $application
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function show(Request $request, Application $application)
	{
		/** @var User $user */
		$user = $request->user();
		if (!$this->checkAccess($user, $application)) {
			return response()->json(['error' => 'У вас нет прав на это действие'], 401);
		}
		
		$application->load(['vehicle.privilege.privilege_status', 'trailers.privilege.privilege_status','application_dates']);
		
		return response()->json([
			'data' => new ApplicationResource($application),
		]);
	}
	
	/**
	 * Check is user can access to update application
	 * @param User $user
	 * @param Application $application
	 * @return bool
	 */
	private function checkAccess($user, $application)
	{
		if (
			$user->role_id === Role::ROLE_ADMIN ||
			$user->role_id === Role::ROLE_OFFICER ||
			$user->role_id === Role::ROLE_WEIGHT_CONTROL ||
			($user->role_id === Role::ROLE_DEPARTMENT_AGENT) ||//@todo && $user->department_id === $application->department_id) || //@todo fix
			($user->role_id === Role::ROLE_FIRM_USER && $user->owner_id == $application->user_id)// && $application->employee_id === $user->id)
		) {
			return true;
		}
		if ($user->role_id === Role::ROLE_INDIVIDUAL || Role::ROLE_FIRM) {
			if ($application->user_id !== $user->id) {
				return false;
			}
			if (!$user->isConfirmed()) {
				return false;
			}
			return true;
		}
		return true;
	}
	
	/**
	 * Get application full info
	 * @param Request $request
	 * @param int $id
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function showFull(Request $request, $id)
	{
		/** @var Application $application */
		$application = Application::withTrashed()->find($id);
		if(!$application) {
			return response()->json(['error' => 'Заявление не найдено'], 404);
		}
		/** @var User $user */
		$user = $request->user();
		if (!$this->checkAccess($user, $application)) {
			return response()->json(['error' => 'У вас нет прав на это действие'], 401);
		}
		
		$application->load([
			'admin',
			'user.firm',
			'employee',
			'frozen_trailers',
			'vehicle.vehicle_brand',
			'vehicle.vehicle_model',
			'trailers.vehicle_brand',
			'trailers.vehicle_model',
			'application_load',
			'application_route',
			'application_dates.penalty_post',
			'privilege_status',
			'permit',
			'control_marks.user',
			'control_marks.control_post',
			'application_agreements.department',
			'fast_application.user',
			'smev_application',
			'files',
            'audits'
		]);
		
		return response()->json([
			'data' => new ApplicationResource($application),
		]);
	}
	
	/**
	 * Get application full info
	 * @param Request $request
	 * @param Application $application
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function showLoad(Request $request, Application $application)
	{
		/** @var User $user */
		$user = $request->user();
		if (!$this->checkAccess($user, $application)) {
			return response()->json(['error' => 'У вас нет прав на это действие'], 401);
		}
		
		$application->load(['vehicle.vehicle_axles', 'vehicle.files', 'trailers.vehicle_axles', 'files',"application_dates"]);
		$applicationLoad = ApplicationLoad::where('application_id', $application->id)->first();
		
		return response()->json([
			'data' => [
				'application' => new ApplicationResource($application),
				'load' => $applicationLoad ? new ApplicationLoadResource($applicationLoad) : null,
			]
		]);
	}
	
	/**
	 * Get application route info
	 * @param Request $request
	 * @param Application $application
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function showRoute(Request $request, Application $application)
	{
		/** @var User $user */
		$user = $request->user();
		if (!$this->checkAccess($user, $application)) {
			return response()->json(['error' => 'У вас нет прав на это действие'], 401);
		}
		
		$application->load(['vehicle.vehicle_axles', 'vehicle.files', 'trailers.vehicle_axles', 'privilege_status', 'application_dates']);
		$applicationRoute = ApplicationRoute::where('application_id', $application->id)->first();
		
		return response()->json([
			'data' => [
				'application' => new ApplicationResource($application),
				'route' => $applicationRoute ? new ApplicationRouteResource($applicationRoute) : null,
			]
		]);
	}
	
	/**
	 * Get application dates info
	 * @param Request $request
	 * @param Application $application
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function showDates(Request $request, Application $application)
	{
		/** @var User $user */
		$user = $request->user();
		if (!$this->checkAccess($user, $application)) {
			return response()->json(['error' => 'У вас нет прав на это действие'], 401);
		}
		
		$application->load([
			'vehicle.vehicle_axles',
			'vehicle.files',
			'trailers.vehicle_axles',
			'files',
            'privilege_status'
		]);
		
		$applicationLoad = ApplicationLoad::where('application_id', $application->id)->first();
		$applicationRoute = ApplicationRoute::where('application_id', $application->id)->first();
		$applicationDates = ApplicationDate::where('application_id', $application->id)->first();
		
		return response()->json([
			'data' => [
				'application' => $application ? new ApplicationResource($application) : null,
				'load' => $applicationLoad ? new ApplicationLoadResource($applicationLoad) : null,
				'route' => $applicationRoute ? new ApplicationRouteResource($applicationRoute) : null,
				'dates' => $applicationDates ? new ApplicationDateResource($applicationDates) : null,
			]
		]);
	}
	
	/**
	 * Withdraw application by user
	 * @param Request $request
	 * @param Application $application
	 * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
	 */
	public function withdraw(Request $request, Application $application)
	{
		/** @var User $user */
		$user = $request->user();
		if (!$this->checkAccess($user, $application)) {
			return response()->json(['error' => 'У вас нет прав на это действие'], 401);
		}
		
		$success = $application->withdraw();
		//@todo optimize, not reload
		$application->load([
			'user.firm',
			'vehicle',
			'trailers',
			'application_load',
			'application_route',
			'application_dates.penalty_post',
			'privilege_status',
			'files'
		]);
		
		return response()->json([
			'data' => new ApplicationResource($application),
		]);
	}
	
	/**
	 * Send app to admin (change status)
	 * @param Request $request
	 * @param Application $application
	 * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
	 */
	public function toAdmin(Request $request, Application $application)
	{
		/** @var User $user */
		$user = $request->user();
		if (!$this->checkAccess($user, $application)) {
			return response()->json(['error' => 'У вас нет прав на это действие'], 401);
		}
		
		$success = $application->toReview();
		//@todo optimize, not reload
		$application->load([
			'user.firm',
			'vehicle',
			'trailers',
			'application_load',
			'application_route',
			'application_dates.penalty_post',
			'privilege_status',
			'files'
		]);
		
		return response()->json([
			'data' => new ApplicationResource($application),
		]);
	}
	
	/**
	 * Send application to work by admin, department agent
	 * @param Request $request
	 * @param Application $application
	 * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
	 */
	public function toWork(Request $request, Application $application)
	{
		/** @var User $user */
		$user = $request->user();
		if (!$this->checkAdminAccess($user, $application)) {
			return response()->json(['error' => 'У вас нет прав на это действие'], 401);
		}
		$success = $application->load(['files'])->toWork($user);
		if($success) {
			return response()->json([
				'data' => new ApplicationResource($application),
			]);
		}

		return response()->json(['error' => 'Заявка не может быть взята в работу!'], 422);
	}

	/**
	 * Lock application
	 * @param Request $request
	 * @param Application $application
	 * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
	 */
	public function lock(Request $request, Application $application)
	{
		/** @var User $user */
		$admin = $request->user();
		if (!$this->checkAdminAccess($admin, $application)) {
			return response()->json(['error' => 'У вас нет прав на это действие'], 401);
		}

		$success = $application->lock();

		return response()->json([
			'data' => new ApplicationResource($application),
		]);
	}

	/**
	 * Unlock application
	 * @param Request $request
	 * @param Application $application
	 * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
	 */
	public function unlock(Request $request, Application $application)
	{
		/** @var User $user */
		$admin = $request->user();
		if (!$this->checkAdminAccess($admin, $application)) {
			return response()->json(['error' => 'У вас нет прав на это действие'], 401);
		}

		$success = $application->unlock();

		return response()->json([
			'data' => new ApplicationResource($application),
		]);
	}
	
	/**
	 * Check is user can access to update application
	 * @param User $user
	 * @param Application $application
	 * @return bool
	 */
	private function checkAdminAccess($user, $application)
	{
		return
			$user->role_id === Role::ROLE_ADMIN ||
			$user->role_id === Role::ROLE_OFFICER ||
			$user->role_id === Role::ROLE_DEPARTMENT_AGENT;
	}
	
	/**
	 * Send applications to work by admin, department agent
	 * @param Request $request
	 * @param Application $application
	 * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
	 */
	public function toWorkMultiple(Request $request)
	{
		/** @var User $user */
		$user = $request->user();

		$data = $request->all();
		$idsStr = array_get($data, 'towork', []);

		if (!is_array($idsStr)) {
            $ids = json_decode($idsStr, true);
        } else {
		    $ids = $idsStr;
        }

		if (!empty($ids)) {
			$applications = Application::whereIn('id', $ids)->get();
			if (!empty($applications)) {
				foreach ($applications as $application) {
					if (!$this->checkAdminAccess($user, $application)) {
						return response()->json(['error' => 'У вас нет прав на это действие'], 401);
					}
				}
			}

			Application::multipleToWork($ids, $user);
		}

		return response()->json([
			'data' => $ids,
		]);
	}

    /**
     * Accept application
     * @param Request $request
     * @param Application $application
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
	public function accept(Request $request, Application $application)
	{
		/** @var User $user */
		$user = $request->user();
		if (!$this->checkAdminAccess($user, $application)) {
			return response()->json(['error' => 'У вас нет прав на это действие'], 401);
		}
		
		//check if route was changed by admin
		$data = $request->all();
		$points = array_get($data, 'waypoints');
		$changed = intval(array_get($data, 'routeChanged', 0));
		$changeStatus = intval(array_get($data, 'changeStatus', 1));
		$oldStatus = $application->status;
		$is_spring = array_get($data, 'is_spring', 0);
		$special_conditions = array_get($data, 'special_conditions');

		$status = Application::STATUS_ACCEPTED;
		if (!$application->privilege_status_id && !empty($points)) {
			$federalDistance = array_get($data, 'federal_distance', 0);
			$regionalDistance = array_get($data, 'regional_distance', 0);
			$application->application_route->applyRouteChange($points, $federalDistance, $regionalDistance);

			$priceCalculator = new PriceCalculator($application);
            $application->federal_price = 0;
            $application->regional_price = 0;
			if(!$is_spring){
                $application->price = $priceCalculator->calculatePrice();
                $priceInfo = $priceCalculator->getPriceInfo();
                $application->spring_price = $priceCalculator->calculatePrice(1);
                $springPriceInfo = $priceCalculator->getPriceInfo();
                $priceInfo = array_merge($priceInfo, $springPriceInfo);
            }else{
                $spring_price = $priceCalculator->calculatePrice(1);
                $priceInfo =$priceCalculator->getPriceInfo();
                $application->price = $spring_price;
                $application->spring_price = $spring_price;
                $application->special_conditions = $special_conditions;
            }

            $application->price_info = $priceInfo;
			$application->is_spring = $is_spring;

			if($changed){
				$status = Application::STATUS_ACCEPTED_WITH_CHANGES;
			}
		}

        if($application_route = $application->application_route)
            $application_route->descriptionSave();

		if(!$changeStatus){
			$status = $oldStatus;
		}
		$success = $application->accept($user, $data, $status);

		$application->load(['files', 'permit']);
		if($success) {
			RouteAddress::addNewAddresses($points);
		}
		
		return response()->json([
			'data' => new ApplicationResource($application),
		]);
	}
	
	/**
	 * Activate application
	 * @param Request $request
	 * @param Application $application
	 * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
	 */
	public function activate(Request $request, Application $application)
	{
		/** @var User $user */
		$user = $request->user();
		if (!$this->checkAdminAccess($user, $application)) {
			return response()->json(['error' => 'У вас нет прав на это действие'], 401);
		}
		
		$application->activate($user);
		
		return response()->json([
			'data' => new ApplicationResource($application),
		]);
	}
	
	/**
	 * Decline application
	 * @param Request $request
	 * @param Application $application
	 * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
	 */
	public function decline(Request $request, Application $application)
	{
		/** @var User $user */
		$user = $request->user();
		if (!$this->checkAdminAccess($user, $application)) {
			return response()->json(['error' => 'У вас нет прав на это действие'], 401);
		}
		
		$data = $request->all();
		$success = $application->decline($user, $data);
		return response()->json([
			'data' => new ApplicationResource($application),
		]);
	}

    /**
     * Set status for application = awaiting attachments
     * @param Request $request
     * @param Application $application
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function awaitingAttachments(Request $request, Application $application)
    {
        /** @var User $user */
        $user = $request->user();
        if (!$this->checkAdminAccess($user, $application)) {
            return response()->json(['error' => 'У вас нет прав на это действие'], 401);
        }

        $data = $request->all();
        $success = $application->awaitingAttachments($user, $data);
        return response()->json([
            'data' => new ApplicationResource($application),
        ]);
    }
	
	/**
	 * Restore application
	 * @param Request $request
	 * @param int $id
	 * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
	 */
	public function restore(Request $request, $id)
	{
        $application = Application::withTrashed()->where('id', $id)->whereNotNull('deleted_at')->first();
		/** @var User $user */
		$user = $request->user();
		if (!$this->checkAdminAccess($user, $application)) {
			return response()->json(['error' => 'У вас нет прав на это действие'], 401);
		}
		
		if($application){
			$application->restore();
		}
		
		return response()->json([
			'data' => new ApplicationResource($application),
		]);
	}

	/**
	 * @param ApplicationVehicleRequest $request
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function create(ApplicationVehicleRequest $request)
	{
		/** @var User $user */
		$user = $request->user();
		$data = $request->all();
		
		$application = Application::createByVehicles($user, $data);
        $dates = ApplicationDate::updateByData($application, $data);
        $application->load('application_dates');
		
		return response()->json(new ApplicationResource($application), 201);
	}
	
	/**
	 * Update vehicle section
	 * @param ApplicationVehicleRequest $request
	 * @param Application $application
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function vehicle(ApplicationVehicleRequest $request, Application $application)
	{
		/** @var User $user */
		$user = $request->user();
		
		if (!$this->checkAccess($user, $application)) {
			return response()->json(['error' => 'У вас нет прав на это действие'], 401);
		}
		
		$data = $request->all();
		
		$application->updateByVehicles($data);
        $dates = ApplicationDate::updateByData($application, $data);
        $application->load("application_dates");
		
		return response()->json(new ApplicationResource($application), 201);
	}
	
	/**
	 * Update load section
	 * @param ApplicationLoadRequest $request
	 * @param Application $application
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function load(ApplicationLoadRequest $request, Application $application)
	{
		/** @var User $user */
		$user = $request->user();
		if (!$this->checkAccess($user, $application)) {
			return response()->json(['error' => 'У вас нет прав на это действие'], 401);
		}
		
		$data = $request->all();
		$load = ApplicationLoad::updateByData($application, $data);
        $dates = ApplicationDate::updateByData($application, $data);
		
		return response()->json(new ApplicationLoadResource($load), 201);
	}
	
	/**
	 * Update route section
	 * @param ApplicationRouteRequest $request
	 * @param Application $application
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function route(ApplicationRouteRequest $request, Application $application)
	{
		/** @var User $user */
		$user = $request->user();
		if (!$this->checkAccess($user, $application)) {
			return response()->json(['error' => 'У вас нет прав на это действие'], 401);
		}
		
		$data = $request->all();
		
		if ($privilege_status_id = array_get($data, 'privilege_status_id')) {
            $application->load("application_dates");
			return response()->json(new ApplicationResource($application), 201);
		}

		$route = ApplicationRoute::updateByData($application, $data);
		$application->updateRoute($data);
		
		return response()->json(new ApplicationRouteResource($route), 201);
	}
	
	/**
	 * Update dates section
	 * @param ApplicationDateRequest $request
	 * @param Application $application
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function dates(ApplicationDateRequest $request, Application $application)
	{
		/** @var User $user */
		$user = $request->user();
		if (!$this->checkAccess($user, $application)) {
			return response()->json(['error' => 'У вас нет прав на это действие'], 401);
		}
		
		$data = $request->all();
		$dates = ApplicationDate::updateByData($application, $data);
		$application->updateDates($data);
		$application->load(['files']);

		
		//recalculate price
		$prices = Permit::createForApp($application);
		$application->price = $prices['price'] ?? 0;
		$application->spring_price = $prices['spring_price'] ?? 0;
		$application->save();
		
		//reset agreements statuses
		ApplicationAgreement::resetForApp($application->id);
		
		//save new template, if template not exists
		if ($data['is_template']) {
			$application->saveToTemplate();
		}
		
		return response()->json(new ApplicationDateResource($dates), 201);
	}
	
	/**
	 * @param Request $request
	 * @param Application $application
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function delete(Request $request, Application $application)
	{
		/** @var User $user */
		$user = $request->user();
		if ($application->user_id !== $user->id) {
			return response()->json(['error' => 'У вас нет прав на это действие'], 401);
		}
		if (!$user->isConfirmed()) {
			return response()->json(['error' => 'У вас нет прав на это действие'], 401);
		}
		if (
			$application->status === Application::STATUS_NEW ||
			$application->status === Application::STATUS_DECLINE ||
			$application->status === Application::STATUS_ACCEPTED_WITH_CHANGES ||
			$application->status === Application::STATUS_ACCEPTED ||
			(
				$application->status === Application::STATUS_ACTIVE &&
				(
					$application->isExpired() ||
					$application->runsUsed()
				)
			)
		) {
			$application->delete();
			
			return response()->json(null, 204);
			
		}
		return response()->json(['error' => 'Вы можете удалить заявление в этом статусе!'], 401);
	}
	
	public function testPrice(Request $request, Application $application)
	{
	    if($application_route = $application->application_route)
            $application_route->getLegsByPoints();
		/** @var Application $application */
//		$application = Application::find($id);
//		if(!$application){
//			return 'No application with id = '. $id;
//		}
//
//		if($application->application_route) {
//            //refresh legs if necessary
//		    $points = \GuzzleHttp\json_decode($application->application_route->points, true);
//            if (!empty($points)) {
//                $coords = [];
//
//                foreach ($points as $point) {
//                    $coords[] = $point['coords'];
//                }
//
//                $distanceHelper = new GradoserviceDistanceHelper();
//                $routeInfo = $distanceHelper->getRouteInfo($coords);
//
//                if ($routeInfo['distance']) {
//                    $application->application_route->legs = \GuzzleHttp\json_encode($routeInfo['legs'], true);
//                    $application->application_route->distance = $routeInfo['distance'];
//                    $application->application_route->save();
//                }
//            }
//        }


		$priceCalculator = new PriceCalculator($application);
        $price = $priceCalculator->calculatePrice();
        $priceInfo = $priceCalculator->getPriceInfo();
		$params['default'] = $priceCalculator->getLastCalculationInfo();

		$springPrice = $priceCalculator->calculatePrice(1);
        $springPriceInfo = $priceCalculator->getPriceInfo();
        $priceInfo = array_merge($priceInfo, $springPriceInfo);
		$params['spring'] = $priceCalculator->getLastCalculationInfo();
		$params['price_info'] = $priceInfo;
		$params['result_price'] = $price;
		$params['result_spring_price'] = $springPrice;

        $application->price_info = $priceInfo;

		if($request->query('save', false)){
			$application->save();
		}

		print_r($params);
		die;
	}

	public function testLoads(Request $request, $id){
		/** @var Application $application */
		$application = Application::find($id);
		if(!$application){
			return 'No application with id = '. $id;
		}
		$applicationLoad = $application->application_load ?? null;
		if(!$applicationLoad){
			return 'No application load';
		}
		$axlesInfo = \GuzzleHttp\json_decode($applicationLoad->axles_info, true);
		$axlesGroups = AxleGroup::splitAxles($axlesInfo);
		//print_r($axlesGroups);die;

		$allowedLoads = [];
		$loads = AxleLoad::all();
		//print_r($loads);die;

		$index = 0;
		$result = [];
		foreach($axlesGroups as $axlesGroup){
			$load = $axlesGroup->calculatePermissibleLoad($loads, 0);
			$spring_load = $axlesGroup->calculatePermissibleLoad($loads, 1);
			$groupInfo = $axlesGroup->getLastGroupInfo();
			$axles = $axlesGroup->getAxlesIndexesArray();
			$result[] = [
				'load' => $load,
				'spring_load' => $spring_load,
				'axles' => $axles,
				'groupInfo' => $groupInfo
			];

		}


		if($request->query('save', false)){
			$oldAxlesInfo = json_decode($applicationLoad->axles_info, true);
			$axles_info = AxleGroup::recalculatePermissibleLoads($oldAxlesInfo);
			$applicationLoad->axles_info = json_encode($axles_info);
			$applicationLoad->save();

			print_r($axles_info);
			print_r(json_encode($axles_info));
			die;
		}

		print_r($result);
		die;
	}
	
	public function testDistance($id)
	{
		$distance = 0;
		/** @var ApplicationRoute $applicationRoute */
		$applicationRoute = ApplicationRoute::where('application_id', $id)->first();
		
		if ($applicationRoute && !empty($applicationRoute)) {
			$points = \GuzzleHttp\json_decode($applicationRoute->points, true);
			if (!empty($points)) {
				$coords = [];
				
				foreach ($points as $point) {
					$coords[] = $point['coords'];
				}

				$distanceHelper = new GradoserviceDistanceHelper();
				$routeInfo = $distanceHelper->getRouteInfo($coords);
				$distance = array_get($routeInfo, 'distance');
				$legs = array_get($routeInfo, 'legs');

				$applicationRoute->distance = $distance;
				$applicationRoute->legs = \GuzzleHttp\json_encode($legs);
				
				if ($applicationRoute->distance) {
					$applicationRoute->save();
					$result = [
					    'url' => $routeInfo['url'],
					    'distance' => $applicationRoute->distance,
                        'legs' => PriceCalculator::splitLegsToTypes($legs),
                        'api_response' => $legs
                    ];
					print_r($result);
					die;
				}
			}
		};
	}
	
	public function generateInvoice($id)
	{
		$application = Application::find($id);
		
		$excelHelper = new ExcelHelper();
		$url = $excelHelper->generateInvoice($application);
		
		return $url;
	}
	
	public function addControlMark(Application $application, Request $request)
	{
		/** @var User $user */
		$user = $request->user();
		$data = $request->all();
		
		$marksInLast10Minutes = ControlMark::find10MinutesMarks($application->id);
		if($marksInLast10Minutes){
			return response()->json([
				'data' => [
					'success' => false,
					'error' => 'Нельзя добавить несколько проездов с интервалом менее 10 минут!'
				]
			]);
		}
		
		$controlMark = ControlMark::addByAdmin($user, $application, $data);
		$controlMark->load(['user', 'control_post']);
		
		//recalculate used runs
		/** @var ApplicationDate $applicationDates */
		$applicationDates = $application->application_dates;
		$applicationDates->addMark($controlMark);
		
		if (Role::isWeightControl()) {
			$application->newAuditEvent(Events::WCONTROL_APP_ADD_CONTROL_MARK, null, $controlMark->control_post_id);
		}

		return response()->json([
			'data' => [
				'success' => true,
				'mark' => new ControlMarkResource($controlMark),
				'runs_used' => $applicationDates->runs_used,
				'runs_count' => $applicationDates->runs_count,
			]
		], 201);
	}
	
	public function sendToDepartment(Request $request, Application $application)
	{
		$applicationAgreement = null;
		$departmentId = $request->get('department_id');
		
		if ($departmentId) {
			$existingItem = ApplicationAgreement
				::where('application_id', $application->id)
				->where('department_id', $departmentId)
				->first();
			if ($existingItem) {
				return response()->json(['data' => null], 201);
			}
			
			$applicationAgreement = ApplicationAgreement::create($application->id, $departmentId);
		}
		
		if ($applicationAgreement) {
			$applicationAgreement->load('department');
			if (Role::isAdminOrGbu()) {
				$applicationAgreement->newAuditEvent(Events::ADMINGBU_APP_TODEPARTMENT);
				//$application->newAuditEvent(Events::ADMINGBU_APP_TODEPARTMENT);
			}
			return response()->json(['data' => new ApplicationAgreementResource($applicationAgreement)], 201);
		}
		
		return response()->json(['data' => null], 201);
	}
	
	public function removeAgreement(Request $request, ApplicationAgreement $agreement)
	{
		$agreement->delete();
		
		return response()->json(null, 204);
	}

    /**
     * Change route by admin by adding new coords
     * @param Request $request
     * @param Application $application
     * @return \Illuminate\Http\JsonResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
	public function changeRouteInfo(Request $request, Application $application)
	{
		//@todo refactoring
		$result = [
			'distance' => 0,
			'price' => 0,
			'success' => false
		];
		
		$data = $request->all();
		$points = $this->parseRouteWaypoints(array_get($data, 'waypoints'));
        $segments = array_get($data, 'segments', null);
		$textMarkers = array_get($data, 'routeMarkers');
		if (!empty($points)) {

			$distanceHelper = new GradoserviceDistanceHelper();
			$distanceInfo = $distanceHelper->getRouteInfo($points);
			$distance = array_get($distanceInfo, 'distance');
			$legs = array_get($distanceInfo, 'legs', []);

			$result['distance'] = number_format($distance, 2);
			$result['distance_info'] = PriceCalculator::splitLegsToTypes($legs);

			$priceCalculator = new PriceCalculator($application);
			$priceCalculator->setOverride('distance', $distance);
			$priceCalculator->setOverride('legs', $legs);

			$result['price'] = number_format($priceCalculator->calculatePrice(), 2);
			$result['spring_price'] = number_format($priceCalculator->calculatePrice(1), 2);
            $result['info'] = $priceCalculator->getPriceInfo();

			$result['success'] = true;

		
            //refresh markers, geolocation info
            $markersResult = [];
            for ($j = 0; $j < count($points); $j++) {
                //find marker
                $textMarker = null;
                for ($i = 0; $i < count($textMarkers) && !$textMarker; $i++) {
                    $textCoords = $textMarkers[$i]['coords'] ?? [];
                    if(!empty($textCoords)) {
                        $textLat = $textCoords['lat'];
                        $textLon = $textCoords['lon'];
                    }else{
                        $textLat = $textMarkers[$i]['lat'];
                        $textLon = $textMarkers[$i]['lon'];
                    }

                    if (
                        abs($points[$j]['lat'] - $textLat) < 0.005 &&
                        abs($points[$j]['lon'] - $textLon) < 0.005
                    ) {
                        $textMarker = $textMarkers[$i];
                    }
                }

                if ($textMarker) {
                    $points[$j]['text'] = $textMarker['text'] ?? null;
                } else {
                    $points[$j]['text'] = '';
                }
            }
            $result['points'] = $points;
        }elseif(!empty($segments)){

            $segmentsResult = $this->routeList($segments);

            $distance = array_sum(array_get($segmentsResult,'distance',0));
            $distance_info = RouteList::sumInfo(array_get($segmentsResult,'distance_info',[]));
            $legs = array_get($segmentsResult,'legs',null);
            $points = array_get($segmentsResult,'points',null);

            $result['success'] = true;
            $result['distance'] = number_format($distance, 2);
            $result['distance_info'] = $distance_info;
            $result['points'] = $points;

            $priceCalculator = new PriceCalculator($application);
            $priceCalculator->setOverride('distance', $distance);
            $priceCalculator->setOverride('legs', $legs);

            $result['price'] = number_format($priceCalculator->calculatePrice(), 2);
            $result['spring_price'] = number_format($priceCalculator->calculatePrice(1), 2);
            $result['info'] = $priceCalculator->getPriceInfo();

        }
		
		return response()->json(['data' => $result]);
	}
	
	private function parseRouteWaypoints($waypoints)
	{
		$result = [];
		
		if (!empty($waypoints)) {
			foreach ($waypoints as $waypoint) {
				$result[] = [
					'lat' => $waypoint['latLng']['lat'],
					'lon' => $waypoint['latLng']['lng']
				];
			}
		}
		
		return $result;
	}
	
	public function location(Request $request)
	{
		$data = $request->all();
		$lat = array_get($data, 'lat');
		$lon = array_get($data, 'lon');
		
		$result = [
			'coords' => [
				'lat' => $lat,
				'lon' => $lon
			]
		];
		if ($lat && $lon) {
            $mapHelper = new GradoserviceDistanceHelper();
			$result['text'] = $mapHelper->getLocationNameByCoords($lat, $lon);
		}
		
		return response()->json(['data' => $result]);
	}
	
	public function brands()
	{
		$excelImporter = new ExcelHelper();
		$newBrandTitles = $excelImporter->importVehicleBrands(storage_path($excelImporter->brandsListPath));
		$success = \App\Models\VehicleBrand::importList($newBrandTitles);
		
		return response()->json(['data' => $success]);
	}
	
	public function privilege(Request $request)
	{
		$data = $request->all();
		$privilege_status_id = array_get($data, 'status', 1);
		
		$excelImporter = new ExcelHelper();
		$newVehiclesData = $excelImporter->importPrivilegeVehicles(storage_path($excelImporter->privilegeListPath));
		$success = \App\Models\PrivilegeVehicle::importList($newVehiclesData, $privilege_status_id);
		
		return response()->json(['data' => $success]);
	}
	
	public function pdf(Request $request, Application $application)
	{
		$number = Input::get('number');
		$data = $request->all();
		$privilege_status_id = array_get($data, 'status', 1);
		
		$pdfGenerator = new PdfGenerator();
		$dirPath = storage_path('app/public/application/' . $application->id);
		\File::makeDirectory($dirPath, $mode = 0777, true, true);
		$path = $dirPath . '/permit.pdf';
		$success = $pdfGenerator->generatePermit($application, $path, $number);
		
		return $success ? env('APP_URL') . '/storage/application/' . $application->id . '/permit.pdf' : 'failed';
	}

	/**
	 * @param Request $request
	 * @param Application $application
	 * @return string
	 */
	public function usernameFix(Request $request, Application $application)
	{
		$username = null;
		if($application->isFast()) {
			$username = $application->fast_application ? $application->fast_application->name : null;
		}else{
			$username = $application->user ? $application->user->name : null;
		}
		$application->username = $username;
		$application->timestamps = false;
		$success = $application->save();

		return $success ? 'success' : 'failed';
	}
	
	public function printPdf(PrintRequest $request, Application $application)
	{
		/** @var User $user */
		$user = $request->user();
		if (!$this->checkAdminAccess($user, $application)) {
			return response()->json(['error' => 'У вас нет прав на это действие'], 401);
		}
		
		$data = $request->all();
		$form_id = array_get($data, 'form_id');
		
		$application->form_id = $form_id;
		$application->form_year = date('Y');
		$application->save();
		
		$pdfGenerator = new PdfGenerator();
		$dirPath = storage_path('app/public/application/' . $application->id);
		\File::makeDirectory($dirPath, $mode = 0777, true, true);
		$path = $dirPath . '/permit.pdf';

		$success = $pdfGenerator->generatePermit($application, $path);

		if ($success && Role::isAdminOrGbu()) {
			$application->newAuditEvent(Events::ADMINGBU_APP_PDF);
		}


		$data = $success ?
			[
				'url' => env('APP_URL') . '/storage/application/' . $application->id . '/permit.pdf',
			] :
			[];
		return response()->json(['data' => $data]);
	}

	public function csv(Request $request)
	{
		//@todo move to helper
		$data = $request->all();
		$maxId = array_get($data, 'max', 500000);
		$minId = array_get($data, 'min', 0);

		//set data array
		$applications = Application::where('is_template', 0)
			->where('id', '>', $minId)
			->where('id', '<', $maxId)
			->where(function ($q) {
				$q->where('is_draft', 0)
					->orWhere('status', '<>', Application::STATUS_NEW);
			})
		->with([
			'application_dates.issue_place',
			'user',
			'vehicle'
		])->get();


		$data = [];
		$i = 1;

		/** @var Application[] $applications */
		foreach($applications as $application) {
			$rowData = $application->toCsvExportData();
			array_unshift($rowData, $i);
			$data[] = $rowData;
			$i++;
		}

		$dirPath = storage_path('app/public/application/reports');
		\File::makeDirectory($dirPath, $mode = 0777, true, true);
		$filename = $dirPath . '/all.csv';

		$csvExporter = new CSVHelper();
		$file = $csvExporter->export($filename, Application::getCsvExportHeaders(), $data);

		return $file ? env('APP_URL') . '/storage/application/reports/all.csv' : 'failed';
	}

	public function test_calc(Application $application){
        $priceCalculator = new PriceCalculator($application);
        $result = [];

        $result['price'] = $priceCalculator->calculatePrice();
        $result['info'] = $priceCalculator->getPriceInfo();
        $result['spring_price'] = $priceCalculator->calculatePrice(1);
        $result['spring_info'] = $priceCalculator->getPriceInfo();

        print_r($result);exit;
    }

    public function track(Application $application, Request $request){
        #623
        #1175
        $maxTrack = 30;

        if(count($application->application_route->getTracks()) > $maxTrack){
            return response()->json(['error' => 'Превышение максимального количество отрезков'], 401);
        }

        $data = $request->all();
        $points = array_get($data,'points', null);
        $save = $request->route('save');
        $legs = $application->application_route->track($points, $save, $data);

        $priceCalculator = new PriceCalculator($application);
        $priceCalculator->setOverride('legs', array_get($legs, "legs"));

//        $info = $priceCalculator->getLastCalculationInfo();

        $application->federal_price = 0;
        $application->regional_price = 0;

        $application->price = $priceCalculator->calculatePrice();
        $application->spring_price = $priceCalculator->calculatePrice(1);

        return response()->json([
            'data' => new ApplicationResource($application),
        ]);

    }

    public function trackRemove(Application $application, Request $request){
        $result = $application->application_route->trackRemove($request);
        if($result)
            return response()->json(null, 200);
        return response()->json(['error' => 'Отрезак по такому tripId не найдено'], 401);
    }

    private function routeList($segments, $save = false){
        $setData = [
            'legs' => [],
            'points' => [],
            'distance' => []
        ];

        foreach($segments as $key => $segment){
            if(!empty($segment['waypoints'])){
                $coords = [];

                foreach ($segment['waypoints'] as $waypoints){
                    $c = array_get($waypoints, 'coords',[]);
                    $lat = array_get($c, 'lat',null);
                    $lon = array_get($c, 'lng',null);
                    if($lat && $lon)
                        $coords[] = [
                            'lat' => $lat,
                            'lon' => $lon,
                            'name' => array_get($waypoints, 'name','')
                        ];
                }
                $distanceHelper = new GradoserviceDistanceHelper();
                $routeInfo = $distanceHelper->getRouteInfo($coords);

                $setData['distance'][$key] = array_get($routeInfo, 'distance');
                $setData['distance_info'][$key] = array_get($routeInfo, 'distance_info');
                $legs = array_get($routeInfo, 'legs', []);

                if($save)
                    RouteList::checkPointsAndSave(
                        array_get($segment, 'name', null),
//                        \GuzzleHttp\json_encode($legs),
                        $routeInfo,
                        $coords
                    );

                $setData['legs'][$key] = $legs;
                $setData['points'][$key] = $segment['waypoints'];

            }elseif(!empty($segment['id_list'])){

                $rl = RouteList::find($segment['id_list']);
                if($rl){
                    $setData['legs'][$key] = \GuzzleHttp\json_decode($rl->legs, true);
                    $setData['points'][$key] = ['id_list' => $segment['id_list']];
                    $setData['distance'][$key] = (float) $rl->distance;
                    $setData['distance_info'][$key] = \GuzzleHttp\json_decode($rl->distance_info ?? '[]', true);
                }
            }

        }

        return $setData;
    }

    public function routeSave(Application $application, Request $request){

	    $data = $request->all();
        $segments = array_get($data, 'segments', null);

        if($segments){
            $segmentsResult = $this->routeList($segments, true);

            $route = $application->application_route;
            $route->tracks = 1;
            $route->points = \GuzzleHttp\json_encode(array_get($segmentsResult,'points',[]));
            $route->legs = \GuzzleHttp\json_encode(array_get($segmentsResult,'legs',[]));
            $route->distance = array_sum(array_get($segmentsResult,'distance',null));
            $route->distance_info = \GuzzleHttp\json_encode(RouteList::sumInfo(array_get($segmentsResult,'distance_info',[])));
            $route->save();

            $priceCalculator = new PriceCalculator($application);
            $application->federal_price = 0;
            $application->regional_price = 0;

            $application->price = $priceCalculator->calculatePrice();
            $application->spring_price = $priceCalculator->calculatePrice(1);
            $application->price_info = $priceCalculator->getPriceInfo();
            $application->save();
        }

        return response()->json([
            'data' => new ApplicationResource($application),
        ]);
    }

    public function getRouteList(){
	    return response()->json(RouteListResource::collection(RouteList::all()),200);
    }

    public function reset_status(Application $application){

        if(!$application->cancelStatus()) return response()->json(['success' => true, "message" => "Статус этого разрешения невозможно отменить"]);

        $application->status = Application::STATUS_NEW;
        $application->save();

        $application->newAuditEvent(Events::ADMINGBU_CANCEL_STATUS_NEW);

        return response()->json(['success' => true]);
    }
}
