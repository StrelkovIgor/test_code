<?php

namespace App\Http\Controllers;

use App\Http\Requests\APVGKRequest;
use App\Http\Resources\APVGK as APVGKResources;
use App\Models\APVGK;
use Illuminate\Http\Request;

class APVGKController extends Controller
{

    private $perPage = 10;

    public function create(APVGKRequest $request){

        $apvgk = APVGK::create($request->all());

        return response()->json(new APVGKResources($apvgk),201);

    }

    public function page(Request $request){

        $perPage = (int) $request->get('per_page') ?? $this->perPage;
        $perPage = $perPage ? $perPage : $this->perPage ;

        return APVGKResources::collection(
            APVGK::orderBy('id', 'desc')
                ->paginate($perPage)
                ->appends('paged')
        );
    }

    public function update(APVGKRequest $request, APVGK $apvgk){
        
        $apvgk->update($request->all());

        return response()->json(new APVGKResources($apvgk), 200);
    }

    public function delete(Request $request, APVGK $apvgk){
        $apvgk->delete();
        return response()->json(['success' => true], 200);
    }
}
