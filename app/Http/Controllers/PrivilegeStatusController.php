<?php

namespace App\Http\Controllers;

use App\Http\Requests\PrivilegeStatus\PrivilegeStatusCreate;
use App\Http\Requests\PrivilegeStatus\PrivilegeStatusUpdate;
use App\Models\File;
use App\Models\PrivilegeStatus;
use App\Http\Resources\PrivilegeStatus as PrivilegeStatusResource;
use App\Http\Resources\PrivilegeVehicle as PrivilegeVehicleResource;
use App\Models\PrivilegeVehicle;
use Illuminate\Http\Request;

class PrivilegeStatusController extends Controller
{
    private $perPage = 7;

    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        return PrivilegeStatusResource::collection(
            PrivilegeStatus::orderBy('title', 'asc')
                ->get()
        );
    }

    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function page()
    {
        return PrivilegeStatusResource::collection(
            PrivilegeStatus
                ::with(['user', 'files'])
                ->orderBy('id', 'desc')
                ->paginate($this->perPage)
                ->appends('paged')
        );
    }

    /**
     * @param PrivilegeStatus $privilegeStatus
     * @return PrivilegeStatus
     */
    public function show(PrivilegeStatus $privilegeStatus)
    {
    	$privilegeStatus->load(['files','apvgk']);
        return response()->json([
            'data' => new PrivilegeStatusResource($privilegeStatus),
        ]);
    }

    /**
     * @param PrivilegeStatusCreate $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(PrivilegeStatusCreate $request)
    {
		$data = $request->all();
    	
    	/** @var PrivilegeStatus $privilegeStatus */
        $privilegeStatus = PrivilegeStatus::create($request->all());
	
		$filesData = array_get($data, 'files', []);
		if($privilegeStatus && !empty($filesData)){
			$privilegeStatus->updateFiles($filesData, File::TYPE_PRIVILEGE_STATUS, false);
		}

		if($data['apvgk'])
            $privilegeStatus->createApvgk((array) $data['apvgk']);
        
        $privilegeStatus->load(['user', 'files','apvgk']);

        return response()->json(new PrivilegeStatusResource($privilegeStatus), 201);
    }

    /**
     * @param PrivilegeStatusUpdate $request
     * @param PrivilegeStatus $privilegeStatus
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(PrivilegeStatusUpdate $request, PrivilegeStatus $privilegeStatus)
    {
        $privilegeStatus->update($request->all());
        $data = $request->all();

        //update files
		$filesData = array_get($data, 'files', []);
		$privilegeStatus->updateFiles($filesData, File::TYPE_PRIVILEGE_STATUS, true);

        if($data['apvgk'])
            $privilegeStatus->createApvgk((array) $data['apvgk']);

		$privilegeStatus->load(['user', 'files', 'apvgk']);

        return response()->json(new PrivilegeStatusResource($privilegeStatus), 200);
    }

    /**
     * @param PrivilegeStatus $privilegeStatus
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(PrivilegeStatus $privilegeStatus)
    {
        $privilegeStatus->delete();

        return response()->json(null, 204);
    }


    /**
     * Get page of privilege status vehicles
     * @param Request $request
     * @param $id
     * @return mixed
     */
    public function vehicles(Request $request, $id)
    {
        return PrivilegeVehicleResource::collection(
            PrivilegeVehicle
                ::where('privilege_status_id', $id)
                ->orderBy('updated_at', 'desc')
                ->paginate($this->perPage)
                ->appends('paged')
        );
    }


    /**
     * @param Request $request
     * @return \App\Http\Resources\PrivilegeStatus|\Illuminate\Http\JsonResponse
     */
    public function check(Request $request)
    {
        //@todo change check
        $number = trim($request->get('number'));
        $parts = explode(' ', $number);
        $number = $parts[0];

        $privilegeStatus = PrivilegeStatus::findByNumber($number);


        return $privilegeStatus ?
            new \App\Http\Resources\PrivilegeStatus($privilegeStatus) :
            response()->json(['data' => ['id' => 0]]);
    }
}
