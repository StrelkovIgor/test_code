<?php

namespace App\Http\Controllers;

use App\Http\Requests\RouteAddress\RouteAddressCreate;
use App\Http\Requests\RouteAddress\RouteAddressUpdate;
use App\Models\RouteAddress;
use App\Http\Resources\RouteAddress as RouteAddressResource;
use Illuminate\Http\Request;

class RouteAddressController extends Controller
{
	private $perPage = 7;

	/**
	 * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
	 */
	public function index()
	{
		return RouteAddressResource::collection(
			RouteAddress::orderBy('title', 'asc')
				->get()
		);
	}

	/**
	 * @param Request $request
	 * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
	 */
	public function search(Request $request, $search)
	{
		//$data = $request->all();
		//$search = array_get($data, 'search', '');

		return RouteAddressResource::collection(
			RouteAddress::orderBy('title', 'asc')
				->where('title', 'like', '%' . $search . '%')
				->limit(20)
				->get()
		);
	}

	/**
     * @param Request $request
     *
	 * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
	 */
	public function page(Request $request)
	{
	    $data = $request->all();
	    
		$query = RouteAddress::orderBy('id', 'desc');
		if($title = array_get($data, 'title')) {
		    $query->where('title', 'like', '%' . $title . '%');
        }
	    
	    return RouteAddressResource::collection(
			$query
				->paginate($this->perPage)
				->appends('paged')
		);
	}

	/**
	 * @param RouteAddress $routeAddress
	 * @return RouteAddress
	 */
	public function show(RouteAddress $routeAddress)
	{
		return response()->json([
			'data' => new RouteAddressResource($routeAddress),
		]);
	}

	/**
	 * @param RouteAddressCreate $request
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function store(RouteAddressCreate $request)
	{
		$routeAddress = RouteAddress::create($request->all());

		return response()->json(new RouteAddressResource($routeAddress), 201);
	}

	/**
	 * @param RouteAddressUpdate $request
	 * @param RouteAddress $routeAddress
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function update(RouteAddressUpdate $request, RouteAddress $routeAddress)
	{
		$routeAddress->update($request->all());

		return response()->json(new RouteAddressResource($routeAddress), 200);
	}

	/**
	 * @param RouteAddress $routeAddress
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function delete(RouteAddress $routeAddress)
	{
		$routeAddress->delete();

		return response()->json(null, 204);
	}
}
