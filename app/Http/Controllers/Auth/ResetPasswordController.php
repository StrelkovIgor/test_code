<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\ChangePassword;
use App\Models\PasswordReset;
use App\Models\User;
use Illuminate\Foundation\Auth\ResetsPasswords;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function reset(ChangePassword $request){
        $success = false;
        /** @var PasswordReset $passwordReset */
        $passwordReset = PasswordReset::where('token', $request->input('token'))->first();
        if(!$passwordReset){
            return response()->json([
                'message' => 'Неверный код для сброса пароля!',
                'errors' => [
                    'token' => 'Неверный код для сброса пароля!'
                ]
            ],422);
        }

        /** @var User $user */
        $user = User::where('email', $passwordReset->email)->first();
        if (!$user) {
            return response()->json([
                'message' => 'Пользователь с таким Email не найден!',
                'errors' => [
                    'email' => 'Пользователь с таким номером телефона  не найден!'
                ]
            ],422);
        }

        $user->password = bcrypt($request->input('password'));
        $token = $user->generateToken(true);
        if($token){
            //Reset api token
            $success = true;
            $passwordReset->delete();
        }

        //return response()->json(['data' => $user]);
        return response()->json(['data' => ['success' => $success]]);
    }
}
