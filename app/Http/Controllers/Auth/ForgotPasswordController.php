<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\RestorePassword;
use App\Http\Requests\Auth\RestoreCodeConfirmation;
use App\Mail\PasswordRestore;
use App\Models\PasswordReset;
use App\Models\User;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Support\Facades\Mail;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Send a confirmation code via sms.
     *
     * @param  RestorePassword $request
     * @return \Illuminate\Http\Response
     */
    public function changeRequest(RestorePassword $request)
    {
        /** @var User $user */
        $user = User::where('email', $request->input('email'))->first();
        if (!$user) {
            return response()->json([
                'message' => 'Пользователь с таким Email не найден!',
                'errors' => [
                    'email' => 'Пользователь с таким Email не найден!'
                ]
            ], 422);
        }
        //generate code and send via sms
        //$success = SmsSender::generateAndSendCode($user);

        $token = $this->createPasswordResetToken($user);

        $success = true;
        Mail::to($user->email)
            ->send(new PasswordRestore($user, $token));

        return response()->json([
            'data' => [
                'success' => $success,
            ]
        ]);
    }

    /**
     * Send a confirmation code via sms.
     *
     * @param  RestoreCodeConfirmation $request
     * @return \Illuminate\Http\Response
     */
    public function codeConfirmation(RestoreCodeConfirmation $request)
    {
        $user = User::where('phone', $request->input('phone'))->first();
        if (!$user) {
            return response()->json([
                'message' => 'Пользователь с таким номером телефона  не найден!',
                'errors' => [
                    'phone' => 'Пользователь с таким номером телефона  не найден!'
                ]
            ], 422);
        }

        if ($user->confirmation_code != $request->input('code')) {
            return response()->json([
                'message' => 'Неверный код подтверждения!',
                'errors' => [
                    'code' => 'Неверный код подтверждения!'
                ]
            ], 422);
        }

        //$token = $this->broker()->createToken($user);
        $token = $this->createPasswordResetToken($user);

        return response()->json(['data' => ['token' => $token]]);
    }

    /**
     * @param User $user
     *
     * @return string|null
     */
    private function createPasswordResetToken($user)
    {
        $passwordReset = PasswordReset::where('email', $user->email)->first();
        if (!$passwordReset) {
            $passwordReset = new PasswordReset;
            $passwordReset->email = $user->email;
        }
        return $passwordReset->generateToken() ? $passwordReset->token : null;
    }
}
