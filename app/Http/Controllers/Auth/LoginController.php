<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\Login;
use App\Models\Role;
use App\Models\Audit\Enum\Events;
use App\Models\User;
use App\Http\Resources\User as UserResource;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(Login $request)
    {
        if ($this->attemptLogin($request)) {
            /** @var User $user */
            $user = $this->guard()->user();
            if(!$user->isLocked()){
                $user->generateToken();
				if($user->role_id === Role::ROLE_DEPARTMENT_AGENT){
					$user->load('department');
				}
				if($user->role_id === Role::ROLE_WEIGHT_CONTROL){
					$user->load('control_post');
                }
                if (!Role::isApplicant()) {
                    $user->newAuditEvent(Events::USER_LOGIN_EXEPT_APPLICANT, $user);
                }
                return response()->json([
                    'data' => new UserResource($user),
                ]);
            }else{
                return response()->json([
                    'message' => 'Пользователь удален администратором!',
                    'errors' => [
                        'email' => 'Пользователь удален администратором!'
                    ]
                ],422);
            }
        }

        return $this->sendFailedLoginResponse($request);
    }

    public function logout(Request $request)
    {
        $user = Auth::guard('api')->user();

        if ($user) {
            $user->api_token = null;
            $user->save();
        }

        return response()->json(['data' => 'User logged out.'], 200);
    }

    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
    public function username()
    {
        return 'email';
    }
}
