<?php

namespace App\Http\Controllers\Auth;

use App\Http\Requests\Auth\Registration;
use App\Http\Requests\Auth\RestoreCodeConfirmation;
use App\Mail\UserRegistration;
use App\Models\RegApp;
use App\Models\User;
use App\Http\Controllers\Controller;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Http\Resources\User as UserResource;

class RegisterController extends Controller
{
	/*
	|--------------------------------------------------------------------------
	| Register Controller
	|--------------------------------------------------------------------------
	|
	| This controller handles the registration of new users as well as their
	| validation and creation. By default this controller uses a trait to
	| provide this functionality without requiring any additional code.
	|
	*/
	
	use RegistersUsers;
	
	/**
	 * Where to redirect users after registration.
	 *
	 * @var string
	 */
	protected $redirectTo = '/home';
	
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('guest');
	}
	
	public function register(Registration $request)
	{
		event(new Registered($user = $this->create($request->all())));
		
		$this->guard()->login($user);
		
		return $this->registered($request, $user)
			?: redirect($this->redirectPath());
	}
	
	/**
	 * Create a new user instance after a valid registration.
	 *
	 * @param  array $data
	 * @return \App\Models\User
	 */
	protected function create(array $data)
	{
		$user = User::createByRegData($data);
		
		$password = array_get($data, 'password');
		Mail::to($user->email)
			->send(new UserRegistration($user, $password));
		
		return $user;
	}
	
	
	/**
	 * @param Request $request
	 * @param User $user
	 * @return \Illuminate\Http\JsonResponse
	 */
	protected function registered(Request $request, $user)
	{
		$user->generateToken();

		//create application
		$regApp = RegApp::create([
			'user_id' => $user->id,
		]);
		
		return response()->json([
			'data' => new UserResource($user),
		], 201);
	}
	
	/**
	 * Send a confirmation code via sms.
	 *
	 * @param  RestoreCodeConfirmation $request
	 * @return \Illuminate\Http\Response
	 */
	public function codeConfirmation(RestoreCodeConfirmation $request)
	{
		$user = User::where('phone', $request->input('phone'))->first();
		if (!$user) {
			return response()->json([
				'message' => 'Пользователь с таким номером телефона не найден!',
				'errors' => [
					'phone' => 'Пользователь с таким номером телефона  не найден!'
				]
			], 422);
		}
		
		if ($user->confirmation_code != $request->input('code')) {
			return response()->json([
				'message' => 'Неверный код подтверждения!',
				'errors' => [
					'code' => 'Неверный код подтверждения!'
				]
			], 422);
		}
		
		//$token = $this->broker()->createToken($user);
		//$token = $this->createPasswordResetToken($user);
		
		$success = $this->confirmPhone($user);
		
		return response()->json([
			'data' => new UserResource($user),
		], 201);
	}
	
	/**
	 * @param $user
	 * @return mixed
	 */
	protected function confirmPhone($user)
	{
		$user->phone_confirmed = 1;
		return $user->save();
	}
	
	public function emailConfirmation(Request $request)
	{
		/** @var User $user */
		$user = User::where('confirmation_code', $request->post('confirmation_code'))->first();
		
		if ($user) {
			$user->email_confirmed = 1;
			$user->save();
		}
		
		if (!$user) {
			return response()->json([
				'user' => null,
				'message' => 'Пользователь с таким кодом не найден!',
			]);
		}
		
		return response()->json([
			'data' => new UserResource($user),
		], 201);
	}
	
	/**
	 * Get a validator for an incoming registration request.
	 *
	 * @param  array $data
	 * @return \Illuminate\Contracts\Validation\Validator
	 */
	protected function validator(array $data)
	{
		return Validator::make($data, [
			'name' => 'required|string|max:255',
			'phone' => 'required|string|max:255|unique:users',
			'email' => 'required|string|email|max:255|unique:users',
			'password' => 'required|string|min:6|confirmed',
			'role_id' => 'required|integer',
		]);
	}
}
