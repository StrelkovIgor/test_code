<?php

namespace App\Http\Controllers;

use App\Http\Resources\RegAppFull;
use App\Http\Resources\User as UserResource;
use App\Mail\RegAppAccept;
use App\Mail\RegAppDecline;
use App\Models\Role;
use App\Models\User;
use App\Http\Resources\RegAppFull as RegAppFullResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class RegAppController extends Controller
{
	/**
	 * @var int
	 */
	private $perPage = 20;
	
	/**
	 * @param Request $request
	 *
	 * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
	 */
	public function index(Request $request)
	{
		/** @var User $user */
		$user = $request->user();
		if (!$this->checkAdminAccess($user)) {
			return response()->json(['error' => 'У вас нет прав на это действие'], 401);
		}
		
		return UserResource::collection(
			User::where('confirmation_status', User::CONFIRM_STATUS_NEW)
				->paginate($this->perPage)
				->appends('paged')
		);
	}
	
	/**
	 * Check is user can access to update application
	 * @param User $admin
	 * @return bool
	 */
	private function checkAdminAccess($admin)
	{
		return
			$admin->role_id === Role::ROLE_ADMIN ||
			$admin->role_id === Role::ROLE_OFFICER;
	}
	
	/**
	 * Get page items by status, page
	 * @param Request $request
	 * @param int $status
	 * @param int $role
	 * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
	 */
	public function byStatus(Request $request, $status, $role)
	{
		$query = User::statusQuery($status, $role);
		
		return RegAppFull::collection(
			$query
				->orderBy('updated_at', 'desc')
				->with(['firm.legal_form', 'individual', 'regApp', 'firm.firm_payment_details'])
				->paginate($this->perPage)
				->appends('paged')
		);
	}
	
	/**
	 * Show full info about user
	 * @param Request $request
	 * @param User $user
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function show(Request $request, User $user)
	{
		/** @var User $user */
		$user = $request->user();
		if (!$this->checkAdminAccess($user)) {
			return response()->json(['error' => 'У вас нет прав на это действие'], 401);
		}
		
		if ($user->role_id === Role::ROLE_INDIVIDUAL) {
			$user->load('individual');
		}
		if ($user->role_id === Role::ROLE_FIRM) {
			$user->load('firm');
		}
		
		return response()->json([
			'data' => new RegAppFullResource($user),
		]);
	}
	
	/**
	 * Accept regApp
	 * @param Request $request
	 * @param User $user
	 * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
	 */
	public function accept(Request $request, User $user)
	{
		/** @var User $admin */
		$admin = $request->user();
		
		if (!$this->checkAdminAccess($admin)) {
			return response()->json(['error' => 'У вас нет прав на это действие'], 401);
		}
		
		if ($user->accept($admin)) {
			Mail::to($user->email)
				->send(new RegAppAccept($user));
		};
		
		return response()->json([
			'data' => new RegAppFullResource($user),
		]);
	}
	
	/**
	 * Accept regApp
	 * @param Request $request
	 * @param User $user
	 * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
	 */
	public function decline(Request $request, User $user)
	{
		/** @var User $admin */
		$admin = $request->user();
		
		if (!$this->checkAdminAccess($admin)) {
			return response()->json(['error' => 'У вас нет прав на это действие'], 401);
		}
		
		$data = $request->all();
		$note = array_get($data, 'note', '');
		
		if ($user->decline($admin, $note)) {
			//send accept email
			Mail::to($user->email)
				->send(new RegAppDecline($user, $note));
		}
		
		return response()->json([
			'data' => new RegAppFullResource($user),
		]);
	}
	
	/**
	 * Lock user
	 * @param Request $request
	 * @param User $user
	 * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
	 */
	public function lock(Request $request, User $user)
	{
		/** @var User $user */
		$admin = $request->user();
		if (!$this->checkAdminAccess($admin)) {
			return response()->json(['error' => 'У вас нет прав на это действие'], 401);
		}
		
		$success = $user->lock();
		
		return response()->json([
			'data' => new RegAppFullResource($user),
		]);
	}
	
	/**
	 * Lock user
	 * @param Request $request
	 * @param User $user
	 * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
	 */
	public function unlock(Request $request, User $user)
	{
		/** @var User $user */
		$admin = $request->user();
		if (!$this->checkAdminAccess($admin)) {
			return response()->json(['error' => 'У вас нет прав на это действие'], 401);
		}

		$success = $user->unlock();

		// $user->locked = User::LOCKED_NO;
		// if ($user->save()) {
		
		// };
		
		return response()->json([
			'data' => new RegAppFullResource($user),
		]);
	}
	
	/**
	 * @param Request $request
	 * @param int $status
	 * @param int $role
	 * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
	 */
	public function adminFilter(Request $request, $status, $role)
	{
		/** @var User $user */
		$user = $request->user();
		if (!$this->checkAdminAccess($user)) {
			return response()->json(['error' => 'У вас нет прав на это действие'], 401);
		}
		
		$query = User::statusQuery($status, $role);
		//filter by form data
		$data = $request->all();
		$query = User::adminDataFilter($query, $data, intval($role));
		
		return RegAppFull::collection(
			$query
				->select('users.*')
				->orderBy('updated_at', 'desc')
				->with(['firm.legal_form', 'individual', 'regApp'])
				->paginate($this->perPage)
				->appends('paged')
		
		);
	}
}
