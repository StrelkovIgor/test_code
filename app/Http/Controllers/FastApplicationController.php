<?php
namespace App\Http\Controllers;


use App\Http\Resources\Application as ApplicationResource;

use App\Http\Requests\Application\FastApplication as FastApplicationRequest;
use App\Models\Application;
use App\Models\ApplicationDate;
use App\Models\ApplicationLoad;
use App\Models\ApplicationRoute;
use App\Models\ApplicationTrailer;
use App\Models\FastApplication;
use App\Models\File;
use App\Models\Permit;
use App\Models\Role;
use App\Models\User;
use App\Models\Vehicle;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class FastApplicationController extends Controller
{
	/** @var int FastApplication per page on frontend */
	private $perPage = 5;

	/**
	 * Create fast application by admin
	 * @param FastApplicationRequest $request
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function store(FastApplicationRequest $request)
	{
		/** @var User $user */
		$user = $request->user();
		if (!$this->checkAdminAccess($user, null)) {
			return response()->json(['error' => 'У вас нет прав на это действие'], 401);
		}

		$user = null; //For fast apps user = null

		/** @var User $admin */
		$admin = $request->user();
		$data = $request->all();
		if(!isset($data['load_files'])) {
			$data['load_files'] = [];
		}
		if(!isset($data['markers'])) {
			$data['markers'] = [];
		}

		$application = null;
		$success = false;
		DB::transaction(function() use ($data, $admin, &$success, &$application) {
			//create vehicle
			$vehicleData = array_get($data, 'vehicle');
			$vehicle = Vehicle::createFast($vehicleData, $data, 0);
			if (!empty($vehicle)) {
				$data['vehicle'] = [
					'id' => $vehicle->id
				];
			}

			//create trailers
			$trailers = [];
			$trailersData = array_get($data, 'trailers');
			if (!empty($trailersData)) {
				foreach ($trailersData as $trailerData) {
					$trailers[] = Vehicle::createFast($trailerData, $data, 1);
				}
			}

			//create app
			$application = Application::createByVehicles(null, $data, $admin);
			$fastApplication = FastApplication::updateByData($application, $data, $admin);

			//bind  trailers to app
			$trailersData = array_get($data, 'trailers');
			if (!empty($trailersData)) {
				ApplicationTrailer::createFastApplicationBinds($application, $trailers);
			}

			//rename load_type_id to type_id to avoid  'type_id' field duplication
			$data['type_id'] = $data['load_type_id'];
			$load = ApplicationLoad::updateByData($application, $data);

			//save route if not privilege
			if (!$privilege_status_id = array_get($data, 'privilege_status_id')) {
				//rename route_type_id to type_id to avoid  'type_id' field duplication
				$data['type_id'] = $data['route_type_id'];
				$route = ApplicationRoute::updateByData($application, $data);
			}
			$application->updateRoute($data);

			//save app dates
			$dates = ApplicationDate::updateByData($application, $data);
			$application->updateDates($data);

			//bind files
			$penaltyFilesData = array_get($data, 'penalty_files');
			$application->updateFiles($penaltyFilesData, File::TYPE_APPLICATION_PENALTY);

			//recalculate price
			$prices = Permit::createForApp($application);
			$application->price = $prices['price'] ?? 0;
			$application->spring_price = $prices['spring_price'] ?? 0;

			$application->username = $fastApplication->name;
			$application->save();

			$success = true;
		});

		if($success && $application) {
			return response()->json(new ApplicationResource($application), 201);
		}
		return response()->json(['success' => false], 201);
	}

	/**
	 * @param Request $request
	 * @param int $status
	 * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
	 */
	public function adminFilter(Request $request, $status)
	{
		/** @var User $user */
		$user = $request->user();
		if (!$this->checkAdminAccess($user, null)) {
			return response()->json(['error' => 'У вас нет прав на это действие'], 401);
		}

		$status = intval($status);
		if (!$status) {
			$status = Application::STATUS_NEW;
		}

		//filter by status
		$query = Application::adminStatusQuery($status, $user);

		//filter by form data
		$data = $request->all();
		$query = Application::adminDataFilter($query, $data);
		$query->where('is_fast', 1);

		//filter by role
		$query = Application::adminRoleFilter($query, $user, $status, $data);

		//sorting
		$query = Application::adminSorting($query, $data);


		$statusField = $user->role_id === Role::ROLE_DEPARTMENT_AGENT ? 'application_agreements.status' : 'applications.status';

		//get per_page count
		$perPage = array_get($data, 'per_page', $this->perPage);

		return ApplicationResource::collection(
			$query
				->select([
					'applications.*',
					$statusField . ' as status'
				])
				->with([
					'vehicle',
					'trailers',
					'admin',
					'application_dates',
					'permit',
					'application_agreements.department',
					'application_load',
					'privilege_status',
					'fast_application'
				])
				->paginate($perPage)
				->appends('paged')
		);
	}

	/**
	 * Check is user can access to update application
	 * @param User $user
	 * @param Application $application
	 * @return bool
	 */
	private function checkAdminAccess($user, $application)
	{
		return
			$user->role_id === Role::ROLE_ADMIN ||
			$user->role_id === Role::ROLE_OFFICER;
	}
}