<?php

namespace App\Http\Controllers;

use SoapServer;
use WSDL\Annotation\BindingType;
use WSDL\Annotation\SoapBinding;
use WSDL\Builder\WSDLBuilder;
use WSDL\WSDL;

class CafapController extends Controller
{
	public function __construct() {
		ini_set('soap.wsdl_cache_enabled', 0);
		ini_set('soap.wsdl_cache_ttl', 0);
		ini_set('default_socket_timeout', 300);
		ini_set('max_execution_time', 0);
	}

	public function server() {
		//generate wsdl
		$location = env('APP_DOMAIN', 'http://127.0.0.1') . '/cafap/server';
		$namespace = $location;

		//ini_set("soap.wsdl_cache_enabled", 0);
		$builder = WSDLBuilder::instance()
			->setName('DocumentLiteralService')
			->setTargetNamespace('http://foo.bar/documentliteralservice')
			->setNs('http://foo.bar/documentliteralservice/types')
			->setLocation($location)
			->setStyle(SoapBinding::DOCUMENT)
			->setUse(SoapBinding::LITERAL)
			->setSoapVersion(BindingType::SOAP_11)
			->setMethods(MethodsProvider::get());

		$wsdl = WSDL::fromBuilder($builder);

		if (isset($_GET['wsdl'])) {
			//$wsdl->renderWSDL();
			header("Content-Type: text/xml");
			echo $wsdl->create();
			exit;
		}

		$wsdlUrl = $location . '?wsdl';
		$server = new SoapServer($wsdlUrl, [
			'uri' => $builder->getTargetNamespace(),
			'location' => $builder->getLocation(),
			'style' => SOAP_RPC,
			'use' => SOAP_LITERAL
		]);
//		OLD CafapService
//		$server->setClass(CafapService::class);
		$server->setClass(\stdClass::class);
		$server->handle();
		exit;
	}

	public function client() {
		$wsdl = url('api/cafap/server?wsdl');
		$client = new \SoapClient($wsdl, [
			"trace"      => 1,
			"exceptions" => 0
		]);

		try {
			$params = array(
				'Search' => array(
					'userName' => '1',
					'name' => '2',
				));

			$gosnum = 'В567ЕС 123';
			$eventDate = '2019.01.30 12:12:12';
			$params1 = array(
				'SEARCH' => array(
					'ID_BETAMOUNT' => '1',
					'ID_VIOLATION' => 2,
					'GOSNUM' => $gosnum,
					'SENSORNAME' => '4',
					'EVENT_DATETIME' => $eventDate,
				));

			$data = ['1', 2, $gosnum, '4', $eventDate, 'a37zQ8wS2Ufrd80E'];
			$strToSign = implode(';', $data);
			$sign = md5($strToSign);
			$params1['SEARCH']['SIGNATURE'] = $sign;

			$res = $client->applications($params1);

			print "<pre>\n";
			print "Запрос :\n".htmlspecialchars($client->__getLastRequest()) ."\n";
			print "Ответ:\n".htmlspecialchars($client->__getLastResponse())."\n";
			print "</pre>";
			die;

			print_r($res);die;
		} catch (\Exception $ex) {
			dd($ex);
		}
	}
}