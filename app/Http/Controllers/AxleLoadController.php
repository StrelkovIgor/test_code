<?php

namespace App\Http\Controllers;

use App\Http\Requests\AxleLoad\AxleLoadUpdate;
use App\Models\AxleLoad;
use App\Http\Resources\AxleLoad as AxleLoadResource;
use Illuminate\Http\Request;

class AxleLoadController extends Controller
{
	private $perPage = 7;

	/**
	 * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
	 */
	public function index()
	{
		return AxleLoadResource::collection(
			AxleLoad::orderBy('id', 'asc')
				->get()
		);
	}

	/**
	 * @param Request $request
	 * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
	 */
	public function page(Request $request)
	{
		$data = $request->all();
		$isSpring = array_get($data, 'is_spring', 0);

		return AxleLoadResource::collection(
			AxleLoad
				::where('is_spring', $isSpring)
				->orderBy('id', 'asc')
				->paginate($this->perPage)
				->appends('paged')
		);
	}

	/**
	 * @param AxleLoad $axleLoad
	 * @return AxleLoad
	 */
	public function show(AxleLoad $axleLoad)
	{
		return response()->json([
			'data' => new AxleLoadResource($axleLoad),
		]);
	}

	/**
	 * @param AxleLoadUpdate $request
	 * @param AxleLoad $axleLoad
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function update(AxleLoadUpdate $request, AxleLoad $axleLoad)
	{
		$axleLoad->update($request->all());

		return response()->json(new AxleLoadResource($axleLoad), 200);
	}
}
