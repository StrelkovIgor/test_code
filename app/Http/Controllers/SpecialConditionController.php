<?php

namespace App\Http\Controllers;

use App\Http\Requests\SpecialCondition\SpecialConditionUpdate;
use App\Models\SpecialCondition;
use App\Http\Resources\SpecialCondition as SpecialConditionResource;
use Illuminate\Http\Request;

class SpecialConditionController extends Controller
{
    /**
     * @param int $id
     * @return SpecialCondition
     */
    public function show($id)
    {
        $specialCondition = SpecialCondition::find($id);
    	if(!$specialCondition && $id == 1){
        	$specialCondition = SpecialCondition::createDefaultSpring();
		}
    	return response()->json([
            'data' => new SpecialConditionResource($specialCondition),
        ]);
    }

    /**
     * @param SpecialConditionUpdate $request
     * @param SpecialCondition $specialCondition
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(SpecialConditionUpdate $request, SpecialCondition $specialCondition)
    {
        $specialCondition->update($request->all());

        return response()->json(new SpecialConditionResource($specialCondition), 200);
    }
}
