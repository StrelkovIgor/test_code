<?php

namespace App\Http\Controllers;

use App\Helpers\ExcelHelper;
use App\Jobs\ExportAuditionReport;
use App\Models\Process;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use App\Http\Requests\Audit\ListRequest;
use App\Models\Audit\Audit;
use App\Models\Audit\Enum\Events;
use App\Http\Resources\Audit\Audit as AuditResource;

class AuditionController extends Controller
{
	/**
	 * @var int
	 */
	private $perPage = 20;

	public function list(ListRequest $request, Audit $audit)
	{
		$data = $request->all();
		$per_page = intval(array_get($data, 'per_page', $this->perPage));

		return  AuditResource::collection(
			$audit
				->setFilter($request->all())
				->getList()
				->with(['auditable'])
				->orderBy('id', 'desc')
				->paginate($per_page)
				->appends('paged')
		);
	}

	public function directory()
	{
		return new JsonResponse([
			'events' => Events::$toSelect,
		]);
	}

	/**
	 * @param ListRequest $request
	 * @param Audit $audit
	 * @return JsonResponse
	 */
	public function export(ListRequest $request, Audit $audit) {
		/*
		$data =  new JsonResponse(
			AuditResource::collection(
				$audit
				->setFilter($request->all())
				->getList()
				->with(['auditable'])
				->get()
			)
		);

		$excelImporter = new ExcelHelper();
		$url = $excelImporter->exportLogs($data->getData(true));
		return new JsonResponse([
			'link' => $url,
		]);
		*/
		/** @var User $user */
		$user = $request->user();

		/** @var Process $process */
		$process = Process::createForType($user->id, $request->all(), Process::TYPE_LOGS);


		dispatch(new ExportAuditionReport(
			$user->id,
			$request->all(),
			$process->id
		));

		return new JsonResponse([
			'process_id' => $process->id,
		]);
	}
}
