<?php

namespace App\Http\Controllers;

use App\Http\Requests\OfficerRequest;
use App\Http\Requests\Vehicle\VehicleCreate;
use App\Http\Requests\Vehicle\VehicleUpdate;
use App\Models\Application;
use App\Models\File;
use App\Models\PrivilegeVehicle;
use App\Models\Role;
use App\Models\User;
use App\Models\UserToVehicle;
use App\Models\Vehicle;
use App\Http\Resources\Vehicle as VehicleResource;
use App\Http\Resources\VehicleList as VehicleListResource;
use App\Http\Resources\VehicleForApp as VehicleForAppResource;
use App\Http\Resources\PrivilegeVehicle as PrivilegeVehicleResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class VehicleController extends Controller
{
    private $perPage = 20;

    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        return VehicleResource::collection(
            Vehicle
				::orderBy('vehicles.updated_at', 'desc')
				->paginate($this->perPage)
                ->appends('paged')
        );
    }

	/**
	 * Get vehicles by owner user
	 * @param Request $request
	 * @param string $status
	 * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
	 */
	public function userAll(Request $request, $status)
	{
		/** @var User $user */
		$user = $request->user();

		$query = Vehicle::statusQuery($status);
		$query = Vehicle::userQuery($query, $user);

		$with = ['vehicle_brand', 'vehicle_model', 'privilege.privilege_status'];

		return VehicleForAppResource::collection(
			$query
				->with($with)
				->orderBy('vehicles.updated_at', 'desc')
				->get()
		);
	}

	/**
	 * Get vehicles by owner user with pagination
	 * @param Request $request
	 * @param string $status
	 * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
	 */
    public function user(Request $request, $status)
    {
        /** @var User $user */
        $user = $request->user();
    
        $status = intval($status);
        $query = Vehicle::statusQuery($status);
        $query = Vehicle::userQuery($query, $user);

        //filter
		$data = $request->all();
		$query = Vehicle::dataFilter($query, $data, $user);
  
		$with = ['privilege.privilege_status'];
        if($status === Vehicle::CONFIRM_STATUS_ALL) {
            $with[] = 'users';
        }
		
        return VehicleResource::collection(
            $query
				->with($with)
				->orderBy('vehicles.updated_at', 'desc')
				->paginate($this->perPage)
				->appends('paged')
        );
    }
	
	/**
	 * Admin vehicles page
	 * @param OfficerRequest $request
	 * @param $status
	 * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
	 */
    public function admin(OfficerRequest $request, $status)
    {
        /** @var User $user */
    	$user = $request->user();

    	return VehicleListResource::collection(
            Vehicle::adminStatusQuery($status, $user)
				->select('vehicles.*')
				->with(['user', 'vehicle_model', 'vehicle_brand', 'admin', 'vehicle_app'])
				->orderBy('vehicles.updated_at', 'asc')
                ->paginate($this->perPage)
                ->appends('paged')
        );
    }
	
	/**
	 * Admin filter
	 * @param OfficerRequest $request
	 * @param $status
	 * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
	 */
	public function adminFilter(OfficerRequest $request, $status)
	{
		/** @var User $user */
		$user = $request->user();

		$query = Vehicle::adminStatusQuery($status, $user);
		//filter by form data
		$data = $request->all();
		$query = Vehicle::dataFilter($query, $data, $user);

		return VehicleListResource::collection(
			$query
				->select('vehicles.*')
				->with(['user', 'vehicle_model', 'vehicle_brand', 'admin', 'vehicle_app'])
				->orderBy('vehicles.updated_at', 'asc')
				->paginate($this->perPage)
				->appends('paged')
		);
	}

	/**
	 * Filter firm's vehicles
	 * @param OfficerRequest $request
	 * @param User $user
	 * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
	 */
	public function adminFirm(OfficerRequest $request, User $user)
	{
		$query = Vehicle::where('vehicles.user_id', $user->id)
			->where('confirmation_status', Vehicle::CONFIRM_STATUS_ACCEPT)
			->where('is_fast', '<>', 1);

		//filter by form data
		$data = $request->all();
		$query = Vehicle::dataFilter($query, $data);

		return VehicleListResource::collection(
			$query
				->select('vehicles.*')
				->with(['vehicle_model', 'vehicle_brand', 'privilege.privilege_status'])
				->orderBy('vehicles.updated_at', 'asc')
				->get()
		);
	}

	/**
	 * Send vehicle to work by admin, department agent
	 * @param Request $request
	 * @param Vehicle $vehicle
	 * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
	 */
	public function toWork(Request $request, Vehicle $vehicle)
	{
		/** @var User $user */
		$user = $request->user();
		if (!$this->checkAdminAccess($user, $vehicle)) {
			return response()->json(['error' => 'У вас нет прав на это действие'], 401);
		}

		$vehicle->toWork($user);

		return response()->json([
			'data' => new VehicleResource($vehicle),
		]);
	}

    /**
     * Accept vehicleApp
     * @param Request $request
     * @param Vehicle $vehicle
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function accept(Request $request, Vehicle $vehicle)
    {
		/** @var User $admin */
		$admin = $request->user();
	
		if (!$this->checkAdminAccess($admin)) {
			return response()->json(['error' => 'У вас нет прав на это действие'], 401);
		}
	
		$success = $vehicle->accept($admin);
	
		return response()->json([
			'data' => new VehicleResource($vehicle),
		]);
    }

    /**
     * Accept vehicleApp
     * @param Request $request
     * @param Vehicle $vehicle
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function decline(Request $request, Vehicle $vehicle)
    {
		/** @var User $admin */
		$admin = $request->user();
	
		if (!$this->checkAdminAccess($admin)) {
			return response()->json(['error' => 'У вас нет прав на это действие'], 401);
		}
	
		$data = $request->all();
		$note = array_get($data, 'note', '');
	
		$success = $vehicle->decline($admin, $note);
	
		return response()->json([
			'data' => new VehicleResource($vehicle),
		]);
    }

    /**
     * @param Request $request
     * @param Vehicle $vehicle
     * @return Vehicle
     */
    public function show(Request $request, Vehicle $vehicle)
    {
        /** @var User $user */
        $user = $request->user();
        if (!$this->checkAccess($user, $vehicle)) {
            return response()->json(['error' => 'У вас нет прав на это действие'], 401);
        }

        $vehicle->load(['vehicle_axles', 'users', 'files']);

        return response()->json([
            'data' => new VehicleResource($vehicle),
        ]);
    }
	
	/**
	 * Check is user can access to manage vehicle
	 * @param User $admin
	 * @param Vehicle $vehicle
	 * @return bool
	 */
	private function checkAdminAccess($admin, $vehicle = null)
	{
		return
			$admin->role_id === Role::ROLE_ADMIN ||
			$admin->role_id === Role::ROLE_OFFICER;
	}
	
	/**
	 * @param $user
	 * @param $vehicle
	 * @return bool
	 */
    private function checkAccess($user, $vehicle)
    {
        if ($user->role_id !== Role::ROLE_ADMIN && $user->role_id !== Role::ROLE_OFFICER) {
            if ($user->role_id === Role::ROLE_FIRM_USER) {
                $bind = UserToVehicle::where('user_id', $user->id)
                    ->where('vehicle_id', $vehicle->id)
                    ->get();
                if (!$bind) {
                    return false;
                }
            } else {
                if ($vehicle->user_id !== $user->id) {
                    return false;
                }
                if (!$user->isConfirmed()) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * @param VehicleCreate $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(VehicleCreate $request)
    {
		/** @var User $user */
		$user = $request->user();
		if (!$user->isConfirmed()) {
			return response()->json(['error' => 'У вас нет прав на это действие'], 401);
		}
	
		$data = $request->all();
		
		/** @var Vehicle $vehicle */
		$vehicle = Vehicle::createByUser($data, $user);
	
		//bind files
		$licenseFilesData = array_get($data, 'license_files');
		$vehicle->updateFiles($licenseFilesData, File::TYPE_LICENSE);
		
		$ptsFilesData = array_get($data, 'pts_files');
		$vehicle->updateFiles($ptsFilesData, File::TYPE_PTS);
		
		$leaseFilesData = array_get($data, 'lease_files');
		$vehicle->updateFiles($leaseFilesData, File::TYPE_LEASE);
		
		//bind users
		UserToVehicle::createUsers($data, $user, $vehicle);
	
		return response()->json(new VehicleResource($vehicle), 201);
    }

    /**
     * @param VehicleUpdate $request
     * @param Vehicle $vehicle
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(VehicleUpdate $request, Vehicle $vehicle)
    {
		/** @var User $user */
		$user = $request->user();
		if (!$this->checkAccess($user, $vehicle)) {
			return response()->json(['error' => 'У вас нет прав на это действие'], 401);
		}
	
		$data = $request->all();
	
		/** @var Vehicle $vehicle */
		$vehicle->updateByUser($data, $user);
	
		//bind files
		$licenseFilesData = array_get($data, 'license_files');
		$vehicle->updateFiles($licenseFilesData, File::TYPE_LICENSE);
	
		$ptsFilesData = array_get($data, 'pts_files');
		$vehicle->updateFiles($ptsFilesData, File::TYPE_PTS);
	
		$leaseFilesData = array_get($data, 'lease_files');
		$vehicle->updateFiles($leaseFilesData, File::TYPE_LEASE);
	
		//bind users
		UserToVehicle::createUsers($data, $user, $vehicle);

        return response()->json(new VehicleResource($vehicle), 200);
    }

    /**
     * @param Request $request
     * @param Vehicle $vehicle
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(Request $request, Vehicle $vehicle)
    {
        /** @var User $user */
        $user = $request->user();
        if ($vehicle->user_id !== $user->id) {
            return response()->json(['error' => 'У вас нет прав на это действие'], 401);
        }
        if (!$user->isConfirmed()) {
            return response()->json(['error' => 'У вас нет прав на это действие'], 401);
        }
        
        //check apps
		$vehiclesUsed = Application::checkVehicleUsed($user->id, $vehicle);
        if($vehiclesUsed){
			return response()->json(['success' => false, 'error' => 'Невозможно удалить! Тс используется в разрешениях!']);
		}
		
        $vehicle->delete();

        return response()->json(['success' => true]);
    }
	
	/**
	 * Get privilege vehicles by vehicle numbers list
	 * @param Request $request
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function checkStatuses(Request $request){
		$numbers = $request->post('numbers');
		
		if(!empty($numbers) && is_array($numbers)){
			$todayDate = date('Y-m-d');
			return PrivilegeVehicleResource::collection(
				PrivilegeVehicle
					::whereIn('real_number', $numbers)
					->leftjoin('privilege_statuses', 'privilege_statuses.id', '=', 'privilege_vehicles.privilege_status_id')
					->where('privilege_statuses.start_date', '<=', $todayDate)
					->where('privilege_statuses.finish_date', '>=', $todayDate)
					->whereNull('privilege_statuses.deleted_at')
					->with('privilege_status')
					->get()
			);
		}
		
		return response()->json([
			'data' => [],
		]);
	}

	public function wheelCount(){
		header('Content-Type: text/html; charset=utf-8');
		ini_set("default_charset", "UTF-8");
		mb_internal_encoding("UTF-8");

		/** @var Vehicle[] $vehicles */
		$vehicles = Vehicle::whereExists(function ($query) {
			$query->select(DB::raw(1))
				->from('vehicle_axles')
				->whereRaw('vehicles.id = vehicle_axles.vehicle_id AND vehicle_axles.wheel_count > 2');
		})
			->with(['vehicle_axles', 'vehicle_brand', 'vehicle_model'])
			->get();

		$result = [];
		foreach($vehicles as $vehicle){
			$vehicle_info = [
				'id' => $vehicle->id,
				'brand' => $vehicle->getBrandTitle(),
				'model' => $vehicle->getModelTitle(),
				'number' => $vehicle->real_number,
				'type' => $vehicle->is_trailer ? 'Прицеп' : 'Тс',
				'wheel_counts' => []
			];
			if(!empty($vehicle->vehicle_axles)){
				foreach($vehicle->vehicle_axles as $axle){
					$vehicle_info['wheel_counts'][] = $axle->wheel_count;
				}
			}
			$result[] = $vehicle_info;
		}
		return view('vehicles', ['vehicles' => $result]);
	}
}
