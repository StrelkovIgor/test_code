<?php

namespace App\Http\Controllers;

use App\Helpers\GradoserviceDistanceHelper;
use App\Http\Requests\RequestLog\RequestLogSearch;
use Illuminate\Http\Request;

class GradoserviceController extends Controller
{
    /**
     * Получить данные о токене
     * @param RequestLogSearch $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function auth(Request $request)
    {
        $gradoserviceHelper = new GradoserviceDistanceHelper();

        $tokenData = $gradoserviceHelper->getTokenWithAuthRequest();

        if (!empty($tokenData)) {
            return response()->json([
                'data' => $tokenData
            ], 200);
        }

        return response()->json([
            'error' => $gradoserviceHelper->getLastError()
        ], 200);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function distance(Request $request)
    {
        $coords = $request->get('coords');

        $gradoserviceHelper = new GradoserviceDistanceHelper();

        $distanceInfo = $gradoserviceHelper->getRouteInfo($coords);

        return response()->json([
            'response' => $distanceInfo
        ], 200);
    }
}
