<?php

namespace App\Http\Controllers;

use App\Helpers\ExcelHelper;
use App\Helpers\FileUploader;
use App\Http\Requests\FileRequest;
use App\Http\Requests\PrivilegeVehicle\PrivilegeVehicleCreate;
use App\Http\Requests\PrivilegeVehicle\PrivilegeVehicleUpdate;
use App\Models\Application;
use App\Models\PrivilegeVehicle;
use App\Http\Resources\PrivilegeVehicle as PrivilegeVehicleResource;
use Illuminate\Http\Request;

class PrivilegeVehicleController extends Controller
{
    private $perPage = 20;

    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        return PrivilegeVehicleResource::collection(
            PrivilegeVehicle::orderBy('id', 'desc')
                ->get()
        );
    }

    /**
	 * @param Request $request
	 *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function page(Request $request)
    {
        $privilege_status_id = $request->get('privilege_status_id');

        $data = $request->all();

        $query = PrivilegeVehicle::where('privilege_status_id', $privilege_status_id);

		$type = array_get($data, 'type');
		if ($type !== null) {
			$query->where('is_trailer', $type);
		}
        if($number = array_get($data, 'number')){
        	$query->where('real_number', 'like', '%' . $number . '%');
		}
		if($brand = array_get($data, 'brand')){
			$query->where('brand_title', 'like', '%' . $brand . '%');
		}
		if($model = array_get($data, 'model')){
			$query->where('model_title', 'like', '%' . $model . '%');
		}

        return PrivilegeVehicleResource::collection(
            $query
                ->orderBy('id', 'desc')
                ->paginate($this->perPage)
                ->appends('paged')
        );
    }

    /**
     * @param PrivilegeVehicle $privilegeVehicle
     * @return PrivilegeVehicle
     */
    public function show(PrivilegeVehicle $privilegeVehicle)
    {
        return response()->json([
            'data' => new PrivilegeVehicleResource($privilegeVehicle),
        ]);
    }

    /**
     * @param PrivilegeVehicleCreate $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(PrivilegeVehicleCreate $request)
    {
        $privilegeVehicle = PrivilegeVehicle::create($request->all());

        return response()->json(new PrivilegeVehicleResource($privilegeVehicle), 201);
    }

    /**
     * @param PrivilegeVehicleUpdate $request
     * @param PrivilegeVehicle $privilegeVehicle
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(PrivilegeVehicleUpdate $request, PrivilegeVehicle $privilegeVehicle)
    {
        $privilegeVehicle->update($request->all());

        return response()->json(new PrivilegeVehicleResource($privilegeVehicle), 200);
    }

    /**
     * @param PrivilegeVehicle $privilegeVehicle
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(PrivilegeVehicle $privilegeVehicle)
    {
        Application::handlePrivilegeVehicleRemove($privilegeVehicle);
    	
    	$privilegeVehicle->delete();

        return response()->json(null, 204);
    }
	
	/**
	 * Get privilege vehicles by vehicle numbers list
	 * @param Request $request
	 * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Resources\Json\ResourceCollection
	 */
	public function checkStatuses(Request $request){
		$numbers = $request->post('numbers');
		
		if(!empty($numbers) && is_array($numbers)){
			return PrivilegeVehicleResource::collection(
				PrivilegeVehicle::numberQuery($numbers)
					->with('privilege_status')
					->get()
			);
		}
		
		return response()->json([
			'data' => [],
		]);
	}
	
	public function export(FileRequest $request, $status)
	{
		$imageSource = $request->input('myfile');
		
		$imageData = FileUploader::prepareUploadedFile($imageSource, ['path' => ['users', $request->user()->id, 'brands-export']]);
		
		$file = null;
		//print_r($imageData);die;
		if(!empty($imageData['source'])){
			$excelImporter = new ExcelHelper();
			$newBrandTitles = $excelImporter->importPrivilegeVehicles(storage_path('app/public/' . $imageData['source']));
			$success = \App\Models\PrivilegeVehicle::importList($newBrandTitles, $status);
		}
		
		return PrivilegeVehicleResource::collection(
			PrivilegeVehicle
				::where('privilege_status_id', $status)
				->orderBy('id', 'desc')
				->paginate($this->perPage)
				->appends('paged')
		);
	}
}
