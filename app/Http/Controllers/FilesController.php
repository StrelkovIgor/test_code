<?php

namespace App\Http\Controllers;

use App\Helpers\FileUploader;
use App\Http\Requests\FileRequest;
use App\Models\Application;
use App\Models\File;
use App\Models\Audit\Enum\Events;
use App\Models\Role;
use App\Http\Resources\File as FileResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class FilesController extends Controller
{

    public function __construct()
    {
        ini_set('upload_max_filesize', '10M');
    }

    public static $error = [
        1 => "Размер принятого файла превысил максимально допустимый размер",
        2 => "Размер загружаемого файла превысил значение MAX_FILE_SIZE",
        3 => "Загружаемый файл был получен только частично",
        4 => "Файл не был загружен",
        6 => "Отсутствует временная папка",
        7 => "Не удалось записать файл на диск",
        8 => "PHP-расширение остановило загрузку файла"
    ];

	/**
	 * @param Request $request
	 * @param File $file
	 * @return \Illuminate\Http\JsonResponse|\Symfony\Component\HttpFoundation\BinaryFileResponse
	 */
	public function download(Request $request, File $file)
	{
		$token = $request->query('token');
		if(!$file->checkHash($token)) {
			return response()->json(['error' => 'Invalid Access token!'], 404);
		}

		if(!$file->fileExists()) {
			return response()->json(['error' => 'File not found!'], 404);
		}
		$filepath = $file->getPath();
		$filename = $file->getName();
		$contentType = $file->getContentType();
		$isImage = $file->isImage();

		if(!$contentType) {
			return response()->json(['error' => 'File not found'], 404);
		}

		$headers = array(
			'Content-Type: ' . $contentType,
		);

		$disposition = $isImage ? 'inline' : 'attachment';
		return response()->download($filepath, $filename, $headers, $disposition);
	}

	/**
     * @param FileRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function uploadInn(FileRequest $request)
    {
    	return $this->uploadFile($request, File::TYPE_FIRM_INN, 'inn');
    }

    /**
     * @param FileRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function uploadLicense(FileRequest $request)
    {
		return $this->uploadFile($request, File::TYPE_LICENSE, 'license');
    }

    /**
     * @param FileRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function uploadPts(FileRequest $request)
    {
		return $this->uploadFile($request, File::TYPE_PTS, 'pts');
    }

    /**
     * @param FileRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function uploadLease(FileRequest $request)
    {
		return $this->uploadFile($request, File::TYPE_LEASE, 'lease');
    }

    /**
     * @param FileRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function uploadStatus(FileRequest $request)
    {
		return $this->uploadFile($request, File::TYPE_PRIVILEGE_STATUS, 'status');
    }

    /**
     * @param FileRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function uploadLoad(FileRequest $request)
    {
		return $this->uploadFile($request, File::TYPE_APPLICATION_LOAD, 'load');
    }
	
	/**
	 * @param FileRequest $request
	 * @param Application $application
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function uploadPay(FileRequest $request, Application $application)
	{
		$response = $this->uploadFile($request, File::TYPE_APPLICATION_PAY, 'pay', $application->id, false);

		return $response;
	}

	/**
	 * @param FileRequest $request
	 * @param Application $application
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function uploadPenalty(FileRequest $request, Application $application)
	{
		$response = $this->uploadFile($request, File::TYPE_APPLICATION_PENALTY, 'penalty', $application->id);
		return $response;
	}

	/**
	 * @param FileRequest $request
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function uploadFastPenalty(FileRequest $request)
	{
		$response = $this->uploadFile($request, File::TYPE_APPLICATION_PENALTY, 'penalty');
		return $response;
	}
	
	/**
	 * @param FileRequest $request
	 * @param File $file
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function remove(FileRequest $request, File $file)
	{
		if($file->application->user->id === $request->user()->id){
			$file->delete();
			return response()->json(['data' => $file->id]);
		}
		
		return response()->json(['data' => $file->id]);
	}
	
	/**
	 * @param $request
	 * @param int $type
	 * @param string $folderName
	 * @param int|null $application_id
	 * @param bool $removeExisting
	 * @return \Illuminate\Http\JsonResponse
	 */
	private function uploadFile($request, $type, $folderName, $application_id = null, $removeExisting = false)
	{
		//@todo check access to upload files
        $data = $request->all();

        switch($type){
            case File::TYPE_APPLICATION_IN_WORK:
                $fileData = $request->file('file');
                $fileName = $fileData->getClientOriginalName();
                break;
            default:
                $fileData = array_get($data, 'myfile', '');
                $fileName = array_get($data, 'name', '');
        }

		
		if(!empty($fileData)){
			$imageData = FileUploader::prepareUploadedFile(
				$fileData,
				[
					'path' => ['users', $request->user()->id, $folderName],
					'fileName' => $fileName
				]
			);
			
			if(!empty($imageData)){
				$fileData = [
					'type_id' => $type,
					'name' => $imageData['name'],
					'source' => $imageData['source'],
					'url' => $imageData['url'],
				];
				if($application_id){
					$fileData['application_id'] = $application_id;
					//remove existing one of necessary
					if($removeExisting) {
						$filesRemoved = File::removeWithFiles($type, $application_id);
					}
				}
				
				$file = File::create($fileData);
				
				if(!empty($file)){
					//save to logs
					if ($type == File::TYPE_APPLICATION_PAY && Role::isApplicant()) {
						$application = Application::find($application_id);
						if($application) {
							$application->newAuditEvent(Events::APPLICANT_APP_INVOICE);
						}
					}

					return response()->json([
						'success' => true,
						'data' => new FileResource($file)
					]);
				}
			}
		}
		return response()->json(['success' => false], 422);
	}

    public function inWork(Request $request, Application $application){
        $file = $request->file('file');
        if($valedateFile = $this->validateFile($file, $application)){
            return response()->json(['error' => $valedateFile], 404);
        }

        $response = $this->uploadFile($request, File::TYPE_APPLICATION_IN_WORK, 'inWork', $application->id);
        return $response;

    }

    private function validateFile($file, Application $application)
    {
        if(!$file) return "Свойства 'file' не найдено";
        $mimeType = ['png','jpeg','jpg','pdf','doc','docx','xls','xlsx',];
        $f = $file->getClientOriginalName();
        $f = explode('.',$f);
        $f = $f[count($f) - 1];
        $count = File::where('type_id', File::TYPE_APPLICATION_IN_WORK)->where('application_id',$application->id)->count();
        if($error = $file->getError()){
            if(isset(self::$error[$error]))
                return self::$error[$error];
            else return 'error ' . $error;
        }
        if(!in_array($f, $mimeType)) return "Файл не соответствует типу";
        if($file->getSize() > 5*1024*1024) return "Файл превышает 5 Мб";
        if($count > 5) return "Нельзя загрузить более 5 файлов";
        return null;
    }

    public function downloadNew(Request $request){
        $file = $request->route('file');

        $fileData = File::where('source',$file)->where('type_id',File::TYPE_APPLICATION_IN_WORK)->first();

        if(!$fileData){
//            if(Storage::exists($file)) Storage::delete($file);
//            if($fileData) $fileData->delete();

            return response()->json(['message' => 'Not found'],404);
        }

//        return Storage::download($file, $fileData->name);


        $filepath = $fileData->getPath();
        $filename = $fileData->getName();
        $contentType = $fileData->getContentType();
        $isImage = $fileData->isImage();

        if(!$contentType) {
            return response()->json(['error' => 'File not found'], 404);
        }

        $headers = array(
            'Content-Type: ' . $contentType,
        );

        $disposition = $isImage ? 'inline' : 'attachment';
        return response()->download($filepath, $filename, $headers, $disposition);

    }

    public function deleteNew(Request $request){
        $file = $request->route('file');
        $fileData = File::where('source',$file)->first();
        if(!$fileData) return response()->json(['success' => false, 'message' => 'Файл не найден'],200);
        Storage::disk('public')->delete($file);
        if($fileData) $fileData->delete();

        return response()->json(['success' => true],200);

    }
}
