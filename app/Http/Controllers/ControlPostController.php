<?php

namespace App\Http\Controllers;

use App\Http\Requests\ControlPost\ControlPostCreate;
use App\Http\Requests\ControlPost\ControlPostUpdate;
use App\Models\ControlPost;
use App\Http\Resources\ControlPost as ControlPostResource;
use App\Repositories\Interfaces\ControlPostRepository;

class ControlPostController extends Controller
{
	/** @var int default per page value  */
	protected $perPage = 7;
	
	/**
	 * @var ControlPostRepository
	 */
	protected $controlPosts;
	
	/**
	 * ControlPostController constructor.
	 * @param ControlPostRepository $controlPosts
	 */
	public function __construct(ControlPostRepository $controlPosts)
	{
		$this->controlPosts = $controlPosts;
		
		$this->middleware('auth');
	}
	
	/**
	 * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
	 */
	public function index()
	{
		return response()->json([
			'data' => ControlPostResource::collection(
				$this->controlPosts->list()
			)]
		);
	}
	
	/**
	 * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
	 */
	public function page()
	{
		return ControlPostResource::collection(
			$this->controlPosts->page($this->perPage)
		);
	}
	
	/**
	 * @param ControlPost $controlPost
	 * @return ControlPost
	 */
	public function show(ControlPost $controlPost)
	{
		return response()->json([
			'data' => new ControlPostResource($controlPost),
		]);
	}
	
	/**
	 * @param ControlPostCreate $request
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function store(ControlPostCreate $request)
	{
		$controlPost = $this->controlPosts->create($request->all());
		
		return response()->json(new ControlPostResource($controlPost), 201);
	}
	
	/**
	 * @param ControlPostUpdate $request
	 * @param ControlPost $controlPost
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function update(ControlPostUpdate $request, ControlPost $controlPost)
	{
		$controlPost = $this->controlPosts->update($request->all(), $controlPost);
		
		return response()->json(new ControlPostResource($controlPost), 200);
	}
	
	/**
	 * @param ControlPost $controlPost
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function delete(ControlPost $controlPost)
	{
		return response()->json(['success' => $this->controlPosts->delete($controlPost)], 200);
	}
}
