<?php

namespace App\Http\Controllers;

use App\Models\Application;
use App\Models\ApplicationAgreement;
use App\Models\Role;
use App\Models\Audit\Enum\Events;
use App\Models\User;
use App\Http\Resources\ApplicationAgreement as ApplicationAgreementResource;
use Illuminate\Http\Request;
use Log;

class ApplicationAgreementController extends Controller
{
	/**
	 * Accept application agreement
	 * @param Request $request
	 * @param Application $application
	 * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
	 */
	public function accept(Request $request, Application $application)
	{
		/** @var User $user */
		$user = $request->user();
		
		/** @var ApplicationAgreement $agreement */
		$agreement = ApplicationAgreement
			::where('application_id', $application->id)
			->where('department_id', $user->department_id)
			->first();

		if(!$agreement) {
			Log::info('Failed to find agreement', ['application_id' => $application->id, 'department_id' => $user->department_id]);
			return response()->json(['error' => 'Согласование не найдено'], 404);
		}
		
		if (!$this->checkManageAccess($user, $agreement)) {
			return response()->json(['error' => 'У вас нет прав на это действие'], 401);
		}
		
		$agreement->accept($user);
		$agreement->load('department');

		if (Role::isDepAgent()) {
			$agreement->newAuditEvent(Events::DEPAGENT_APP_ACCEPT);
		}

		return response()->json([
			'data' => new ApplicationAgreementResource($agreement),
		]);
	}
	
	/**
	 * Decline application agreement
	 * @param Request $request
	 * @param Application $application
	 * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
	 */
	public function decline(Request $request, Application $application)
	{
		/** @var User $user */
		$user = $request->user();
		
		/** @var ApplicationAgreement $agreement */
		$agreement = ApplicationAgreement
			::where('application_id', $application->id)
			->where('department_id', $user->department_id)
			->first();

		if (!$this->checkManageAccess($user, $agreement)) {
			return response()->json(['error' => 'У вас нет прав на это действие'], 401);
		}
		
		$success = $agreement->decline($user, $request->get('note'));
		
		if ($success) {
			/** @var Application $application */
			$application = $agreement->application;
			$application->comment = $request->get('note');
			$application->admin_id = $user->id;
			$application->recalculateStatusByAgreements();
			$application->save();
		}
		
		$agreement->load('department');
		
		if ($success && Role::isDepAgent()) {
			$agreement->newAuditEvent(Events::DEPAGENT_APP_DECLINE);
		}

		return response()->json([
			'data' => new ApplicationAgreementResource($agreement),
		]);
	}
	
	/**
	 * Check is user can access to update application
	 * @param User $user
	 * @param ApplicationAgreement $agreement
	 * @return bool
	 */
	private function checkAdminAccess($user, ApplicationAgreement $agreement)
	{
		return
			$user->role_id === Role::ROLE_ADMIN ||
			$user->role_id === Role::ROLE_OFFICER;
	}
	
	/**
	 * Check is user can accept/deny agreement
	 * @param User $user
	 * @param ApplicationAgreement $agreement
	 * @return bool
	 */
	private function checkManageAccess($user, $agreement)
	{
		return $user->role_id === Role::ROLE_DEPARTMENT_AGENT && $user->department_id === $agreement->department_id;
	}
}