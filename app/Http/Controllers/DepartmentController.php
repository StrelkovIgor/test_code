<?php

namespace App\Http\Controllers;

use App\Http\Requests\Department\DepartmentCreate;
use App\Http\Requests\Department\DepartmentUpdate;
use App\Models\Department;
use App\Http\Resources\Department as DepartmentResource;

class DepartmentController extends Controller
{
    private $perPage = 7;

    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        return DepartmentResource::collection(
            Department::orderBy('title', 'asc')
                ->get()
        );
    }

    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function page()
    {
        return DepartmentResource::collection(
            Department::orderBy('id', 'desc')
                ->paginate($this->perPage)
                ->appends('paged')
        );
    }

    /**
     * @param Department $department
     * @return Department
     */
    public function show(Department $department)
    {
        return response()->json([
            'data' => new DepartmentResource($department),
        ]);
    }

    /**
     * @param DepartmentCreate $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(DepartmentCreate $request)
    {
        $department = Department::create($request->all());

        return response()->json(new DepartmentResource($department), 201);
    }

    /**
     * @param DepartmentUpdate $request
     * @param Department $department
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(DepartmentUpdate $request, Department $department)
    {
        $department->update($request->all());

        return response()->json(new DepartmentResource($department), 200);
    }

    /**
     * @param Department $department
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(Department $department)
    {
        $success = false;
        if($department->users->isEmpty()){
            $success = $department->delete();
        }



        return response()->json(['success' => $success], 200);
    }
}
