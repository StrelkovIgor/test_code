<?php

namespace App\Http\Controllers;

use App\Http\Requests\Coefficient\CoefficientCreate;
use App\Http\Requests\Coefficient\CoefficientUpdate;
use App\Models\Coefficient;
use App\Http\Resources\Coefficient as CoefficientResource;

class CoefficientController extends Controller
{
    private $perPage = 7;

    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        return CoefficientResource::collection(
            Coefficient::orderBy('title', 'asc')
                ->get()
        );
    }

    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function page()
    {
        return CoefficientResource::collection(
            Coefficient::orderBy('id', 'desc')
                ->paginate($this->perPage)
                ->appends('paged')
        );
    }

    /**
     * @param Coefficient $coefficient
     * @return Coefficient
     */
    public function show(Coefficient $coefficient)
    {
        return response()->json([
            'data' => new CoefficientResource($coefficient),
        ]);
    }

    /**
     * @param CoefficientCreate $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(CoefficientCreate $request)
    {
        $coefficient = Coefficient::create($request->all());

        return response()->json(new CoefficientResource($coefficient), 201);
    }

    /**
     * @param CoefficientUpdate $request
     * @param Coefficient $coefficient
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(CoefficientUpdate $request, Coefficient $coefficient)
    {
        $coefficient->update($request->all());

        return response()->json(new CoefficientResource($coefficient), 200);
    }

    /**
     * @param Coefficient $coefficient
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(Coefficient $coefficient)
    {
        $coefficient->delete();

        return response()->json(null, 204);
    }
}
