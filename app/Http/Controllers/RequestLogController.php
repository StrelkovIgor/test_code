<?php

namespace App\Http\Controllers;

use App\Http\Resources\RequestLog as RequestLogResource;
use App\Http\Requests\RequestLog\RequestLogSearch;
use App\Models\RequestLog;

class RequestLogController extends Controller
{
	/** @var int  */
	private $limit = 10;

	/**
	 * @param RequestLogSearch $request
	 * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
	 */
	public function search(RequestLogSearch $request)
	{
		$query = RequestLog::orderBy('id', 'desc')
			->limit($this->limit);

		$data = $request->all();

		if($number = array_get($data, 'number')) {
			$query->where('number', 'like', '%' . $number . '%');
		}

		//request date
		if($date_from = array_get($data, 'date_from')) {
			$query->where('created_at', '>=', $date_from);
		}
		if($date_to = array_get($data, 'date_to')) {
			$query->where('created_at', '<=', $date_to);
		}

		//event_date
		if($event_date_from = array_get($data, 'event_date_from')) {
			$query->where('event_datetime', '>=', $event_date_from);
		}
		if($event_date_to = array_get($data, 'event_date_to')) {
			$query->where('event_datetime', '<=', $event_date_to);
		}

		return RequestLogResource::collection(
			$query->get()
		);
	}
}
