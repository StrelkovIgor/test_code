<?php

namespace App\Http\Controllers;

use App\Http\Requests\Reports\ActiveFilter as ReportsActiveFilterRequest;
use App\Http\Resources\ApplicationReport as ApplicationReportResource;
use App\Jobs\ExportActiveAppsCsv;
use App\Jobs\ExportAllAppsCsv;
use App\Models\Application;
use App\Models\Process;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

/**
 * Class ReportsController
 * @package App\Http\Controllers
 */
class ReportsController extends Controller
{
	const PORTION_SIZE = 2000;

	private $perPage = 10;

	/**
	 * General apps statistic
	 * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
	 */
	public function statistic(Request $request)
	{
		$statistic = Application::getStatistic($request->all());

		return response()->json([
			'data' => $statistic,
		]);
	}

	/**
	 * Active apps report
	 * @param ReportsActiveFilterRequest $request
	 *
	 * @return mixed
	 */
	public function active(ReportsActiveFilterRequest $request)
	{
		/** @var User $user */
		$user = $request->user();

		//filter by status
		//$query = Application::adminStatusQuery(Application::STATUS_ACTIVE, $user, true);
		$query = Application::acivatedStatusQuery();

		//filter by form data
		$data = $request->all();

		$query = Application::adminDataFilter($query, $data, $user);

		$query = Application::activeReportsFilter($query, $data);

		//get per_page count
		$perPage = array_get($data, 'per_page', $this->perPage);

		return
			ApplicationReportResource::collection(
			$query
				->select([
					'applications.*',
					'applications.status as status'
				])
				->with([
					'user',
					'vehicle',
					'admin',
                    'employee',
					'application_dates.issue_place',
					'application_agreements.department',
					'application_agreements.user',
					'application_agreements.audits_active.user',
					'audits.user',
					'fast_application'
				])
				->paginate($perPage)
				->appends('paged')
		);
	}

	/**
	 * Export active reports to excel
	 * @param ReportsActiveFilterRequest $request
	 *
	 * @return mixed
	 */
	public function activeExport(ReportsActiveFilterRequest $request)
	{
		/** @var User $user */
		$user = $request->user();

		/** @var Process $process */
        $data = $request->all();
        $isSmev = array_get($data, 'is_smev');
        $type = $isSmev ? Process::TYPE_ACTIVE_REPORT_SMEV : Process::TYPE_ACTIVE_REPORT;
		$process = Process::createForType($user->id, $request->all(), $type);

		dispatch(new ExportActiveAppsCsv(
			$user->id,
			$data,
			$process->id
		));

		return new JsonResponse([
			'process_id' => $process->id,
		]);
	}

    /**
     * Export active reports to excel
     * @param ReportsActiveFilterRequest $request
     *
     * @return mixed
     */
    public function allExport(ReportsActiveFilterRequest $request)
    {
        /** @var User $user */
        $user = $request->user();

        /** @var Process $process */
        $process = Process::createForType($user->id, $request->all(), Process::TYPE_ACTIVE_REPORT);

        dispatch(new ExportAllAppsCsv(
            $user->id,
            $request->all(),
            $process->id
        ));

        return new JsonResponse([
            'process_id' => $process->id,
        ]);
    }
}
