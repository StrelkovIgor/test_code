<?php

namespace App\Http\Controllers\Eputs;

use App\Helpers\Eputs as EputsHelpers;
use App\Http\Controllers\Controller;
use App\Models\Eputs;
use App\Http\Resources\Eputs\Eputs as EputsResources;
use Illuminate\Support\Facades\Session;


class EputsController extends Controller
{
    public $resp = null;

    public function __construct()
    {
        $this->resp = new EputsHelpers();
    }


    public function response($data = null){
        if($data)
            $this->resp->setModelResponse($data);
        return new EputsResources($this->resp);
    }

}
