<?php

namespace App\Http\Controllers\Eputs;

use Illuminate\Http\Request;
use App\Models\Eputs;


class EputsAuthController extends EputsController
{

    public function __construct()
    {
        parent::__construct();
    }

    public function login(Request $request){

        try{
            $data = $request->all();
            $api_token = Eputs::i()->login(array_get($data,'login',null),array_get($data,'password',null));
            if($api_token !== false){
                $this->resp->setModelResponse(['auth_token' => $api_token]);
            }else{
                $this->resp->setError("login or password is not correct", \App\Helpers\Eputs::CODE_SCCESS);
            }
        }catch (\Exception $e){
            $this->resp->setError($e->getMessage());
        }
        return $this->response();
    }

}
