<?php

namespace App\Http\Controllers\Eputs;

use App\Http\Resources\Eputs\Department as DepartmentResource;
use App\Models\Department;
use Illuminate\Http\Request;
use App\Models\Eputs;
use App\Http\Requests\Eputs\ApplicationList as ApplicationListRequest;
use App\Http\Resources\Eputs\ListApplication as ApplicationListResources;
use App\Models\Application;
use Log;


class EputsApplicationController extends EputsController
{
    // status, location_type,  issue_place_id, type_id, department_id
    private $listDirectory = [
        "list" =>[
            "title" => "Все доступные типы справочника",
            "exec" => "listDirectory"
        ],
        "status" =>[
            "title" => "Статусы заявок",
            "exec" => "defDirectory"
        ],
        "location_type" =>[
            "title" => "Тип локации",
            "exec" => "defDirectory"
        ],
        "issue_place_id" =>[
            "title" => "Места выдачи",
            "exec" => "defDirectory"
        ],
        "type_id" =>[
            "title" => "Типы нагрузок",
            "exec" => "defDirectory"
        ],
        "department_id" =>[
            "title" => "Ведомство для согласования",
            "exec" => "departamentDirectory"
        ],
    ];
    CONST DIRECTORY = [
            "status" => [
                0 => "New",
                1 => "Review",
                2 => "Decline",
                3 => "Accepted with changes",
                4 => "Accepted",
                5 => "Active",
                6 => "Repeat",
                7 => "Expired",
                8 => "Removed",
                9 => "Awaiting attachments",
            ],
            "location_type" =>[
                  0 => 'Межрегиональная',
                  1 => 'Межрегиональная',
                  2 => 'Местная'
            ],
            "issue_place_id" => [
                1 =>'г.Казань, Оренбургский тракт, 5',
                2 =>'г.Набережные Челны, проспект Казанский, 72',
                3 =>'г.Альметьевск, ул. Советская,184г',
                4 =>'г.Нурлат, ул.Складская, 2'
            ],
            "type_id" => [
                1 => 'Инертные материалы',
                2 => 'Пищевые продукты',
                3 => 'Металлоконструкция',
                4 => 'Сельхозпродукция',
                5 => 'Горюче-смазочные материалы',
                6 => 'Железо-бетонные изделия',
                7 => 'Пиломатериал',
                8 => 'Опасный груз',
                9 => 'Товары бытового назначения',
                10 => 'Промышленные товары',
                11 => 'Вагончик',
                12 => 'Бытовка',
                13 => 'Специализированная техника',
                15 => 'Животные',
                16 => 'Лекарственные препараты',
                17 => 'Семенной фонд',
                18 => 'Удобрения',
                19 => 'Почта и почтовые грузы',
                14 => 'Другое'
            ]
        ];


    public function __construct()
    {
        parent::__construct();
    }

    public function list(ApplicationListRequest $request){
        $data = $request->all();
        $query = Application::EputsList($data);
        $query = $query
            ->select('applications.*')
            ->distinct()
            ->with([
                'vehicle',
                'trailers',
//                'department',
                'application_agreements.department',
                'application_load',
                'application_route',
                'application_agreements',
                'frozen_trailers',
                'application_dates'
            ])
            ->limit(10000);
        try{
            return $this->response(ApplicationListResources::collection(
                $query->get()
            )
            );
        }catch (\Exception $e){
            try{
                Log::info($e->getMessage());
                Log::info($query->toSql());
                Log::info(json_encode($query->getBindings()));
            }catch (\Exception $e_ty){}
            $this->resp->setError("Error in request");
            return $this->response();
        }

    }

    public function directory(Request $request){
        $data = $request->all();
        if(!isset($data['type'])) {
            $this->resp->setError("The field \"type\" was not found. To display the entire directory, send \"type list\"", \App\Helpers\Eputs::CODE_BAD_REQUEST);
            return $this->response();
        }

        if(!isset($this->listDirectory[$data['type']])) {
            $this->resp->setError("The \"" . $data['type'] . "\" type was not found in the reference book", \App\Helpers\Eputs::CODE_BAD_REQUEST);
            return $this->response();
        }

        if(isset($this->listDirectory[$data['type']]) && method_exists($this, $this->listDirectory[$data['type']]['exec']))
            return $this->{$this->listDirectory[$data['type']]['exec']}($request);

        return $this->response();

    }

    private function listDirectory(Request $request){
        $result =[];
        foreach ($this->listDirectory as $type => $value)
            $result[$type] = $value['title'];
        return $this->response($result);
    }

    private function defDirectory(Request $request){
        $data = $request->all();
        $d = [];
        try{
            $d = self::DIRECTORY[$data['type']];
        }catch (\Exception $e){
            $this->resp->setError("Can't find parameters directory");
            return $this->response();
        }
        if(isset($data['id'])){
            if(isset($d[$data['id']])){
                return $this->response(["value" => $d[$data['id']]]);
            }else{
                $this->resp->setError("I can not find on this id");
                return $this->response();
            }
        }else{
            return $this->response($this->responseDefault($d));
        }
    }

    private function departamentDirectory(Request $request){
        $data = $request->all();
        if(isset($request['id'])){
            $q = Department::where('id',$request['id'])->orderBy('id', 'desc');
        }else{
            $q = Department::orderBy('id', 'desc');
        }
        return $this->response(DepartmentResource::collection(
            $q->get()
        ));
    }

    private function responseDefault($data){
        $result =[];
        foreach($data as $k => $v){
            $result[] = ["id" => $k, "value" => $v];
        }
        return $result;
    }

}
