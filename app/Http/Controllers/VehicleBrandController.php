<?php

namespace App\Http\Controllers;

use App\Helpers\ExcelHelper;
use App\Helpers\FileUploader;
use App\Http\Requests\FileRequest;
use App\Http\Requests\VehicleBrand\VehicleBrandCreate;
use App\Http\Requests\VehicleBrand\VehicleBrandUpdate;
use App\Http\Resources\Vehicle;
use App\Models\VehicleBrand;
use App\Http\Resources\VehicleBrand as VehicleBrandResource;
use Illuminate\Http\Request;

class VehicleBrandController extends Controller
{
    private $perPage = 7;

    private $orderBy = null;
    
    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        return VehicleBrandResource::collection(
            VehicleBrand
				::where('accepted', VehicleBrand::STATUS_ACCEPTED_YES)
				->orderBy('title', 'asc')
                ->get()
        );
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function page(Request $request)
    {
        $data = $request->all();
        
        $query = VehicleBrand
            ::where('vehicle_brands.accepted', '<>', VehicleBrand::STATUS_ACCEPTED_HIDE);
        
        $query = VehicleBrand::adminDataFilter($query, $data);
        $query = VehicleBrand::adminDataSort($query, $data);
        
        return VehicleBrandResource::collection(
            $query
                ->paginate($this->perPage)
                ->appends('paged')
        );
    }

    /**
     * @param VehicleBrand $vehicleBrand
     * @return VehicleBrand
     */
    public function show(VehicleBrand $vehicleBrand)
    {
        return response()->json([
            'data' => new VehicleBrandResource($vehicleBrand),
        ]);
    }

    /**
     * @param VehicleBrandCreate $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(VehicleBrandCreate $request)
    {
        $vehicleBrand = VehicleBrand::create($request->all());

        return response()->json(new VehicleBrandResource($vehicleBrand), 201);
    }

    /**
     * @param VehicleBrandUpdate $request
     * @param VehicleBrand $vehicleBrand
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(VehicleBrandUpdate $request, VehicleBrand $vehicleBrand)
    {
        $vehicleBrand->update($request->all());

        return response()->json(new VehicleBrandResource($vehicleBrand), 200);
    }

    /**
     * @param VehicleBrand $vehicleBrand
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(VehicleBrand $vehicleBrand)
    {
        $success = false;
        if($vehicleBrand->vehicle_models->isEmpty() && $vehicleBrand->vehicles->isEmpty()){
            $success = $vehicleBrand->delete();
        }

        return response()->json(['success' => $success], 200);
    }
	
	/**
	 * @param VehicleBrand $vehicleBrand
	 * @return VehicleBrand
	 */
	public function accept(VehicleBrand $vehicleBrand)
	{
		$vehicleBrand->accepted = 1;
		$success = $vehicleBrand->save();
		return response()->json(['success' => $success], 200);
	}
	
	public function export(FileRequest $request)
	{
		$imageSource = $request->input('myfile');
		
		$imageData = FileUploader::prepareUploadedFile($imageSource, ['path' => ['users', $request->user()->id, 'brands-export']]);
		
		$file = null;
		//print_r($imageData);die;
		if(!empty($imageData['source'])){
			$excelImporter = new ExcelHelper();
			$newBrandTitles = $excelImporter->importVehicleBrands(storage_path('app/public/' . $imageData['source']));
			$success = \App\Models\VehicleBrand::importList($newBrandTitles);
		}
		
		return VehicleBrandResource::collection(
			VehicleBrand
				::orderBy('id', 'desc')
				->paginate($this->perPage)
				->appends('paged')
		);
	}

	/**
	 * @param VehicleBrand $vehicleBrand
	 * @return VehicleBrand
	 */
	public function hide(VehicleBrand $vehicleBrand)
	{
		$vehicleBrand->accepted = VehicleBrand::STATUS_ACCEPTED_HIDE;
		$success = $vehicleBrand->save();
		return response()->json(['success' => $success], 200);
	}
}
