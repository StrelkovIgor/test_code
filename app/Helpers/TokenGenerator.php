<?php

namespace App\Helpers;

use App\Models\File;

class TokenGenerator
{
	/**
	 * @var string
	 */
	private static $key = 'koi10sw4iIXQdPOxQ1h1O4PgMEEjQCzy';

	/**
	 * Generate code for user and send it
	 * @param File $file
	 * @return bool is code sent successfully
	 */
	public static function generateForFile($file)
	{
		return md5($file->source . self::$key);
	}
}