<?php
/**
 * Created by PhpStorm.
 * User: gserg13
 * Date: 04.06.2018
 * Time: 11:57
 */

namespace App\Helpers;

use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;

class OrsmHelper
{
    private $routeApiUrl = 'http://router.project-osrm.org/route/v1/driving/';

    /**
     * Calculate route length by points coords list
     * @param array $points
     * @return float
     */
    public function calculateRouteLength($points)
    {
        $url = $this->buildRouteUrl($points);
        print_r($url);die;
        $response = $this->executeRequest($url);
        print_r($response);die;
        $routeLength = $this->parseRouteResponse($response);

        return $routeLength;
    }

    /**
     * @param array $points
     * @return null|string
     */
    private function buildRouteUrl($points)
    {
        if (!empty($points)) {
            $pointsStr = [];
            foreach ($points as $point) {
                $pointsStr[] = $point['lat'] . ',' . $point['lon'];
            }
            $urlParams = implode(';', $pointsStr) . '?overview=false';
            return $this->routeApiUrl . $urlParams;
        }

        return null;
    }

    /**
     * @param string $url
     * @param string $method
     * @param array $data
     * @return mixed|null
     */
    private function executeRequest($url, $method = 'GET', $data = [])
    {
        $result = null;
        try {
            $client = new \GuzzleHttp\Client();
            $params = [
                'query' => $data
            ];

            $response = $client->request($method, $url, $params);
            $stream = $response->getBody();
            $result = json_decode($stream->getContents(), true);

        } catch (ClientException | RequestException $e) {
            print_r(1);
            print_r($e->getMessage());die;
        }

        return $result;
    }

    /**
     * @param array $routeResponse
     * @return float|null
     */
    private function parseRouteResponse($routeResponse)
    {
        $result = null;
        if (!empty($routeResponse['routes'])) {
            for ($i = 0; $i < count($routeResponse['routes']); $i++) {
                if (isset($routeResponse['routes'][$i]['distance'])) {
                    return floatval($routeResponse['routes'][$i]['distance']);
                }
            }
        }

        return null;
    }
}