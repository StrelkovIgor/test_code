<?php

namespace App\Helpers;

class DateTimeConverter {
	/**
	 * @param string $date
	 * @param string $sourceFormat
	 * @param string $targetFormat
	 * @return null|string
	 */
	public static function convert($date, $sourceFormat, $targetFormat) {
		$date = \DateTime::createFromFormat($sourceFormat, $date);
		if(!$date) {
			return null;
		}
		return $date->format($targetFormat);
	}
}