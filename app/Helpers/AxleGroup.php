<?php
/**
 * Created by PhpStorm.
 * User: gserg13
 * Date: 04.06.2018
 * Time: 11:57
 */

namespace App\Helpers;

use App\Models\ApplicationRoute;
use App\Models\AxleLoad;

class AxleGroup
{
	const GROUP_MAX_DISTANCE = 2.5;

	private $lastData = [];

	private static $allowedWeights = [
		0 => 0,//not using
		1 => 9,
		2 => 10,
		3 => 10,//not using
		4 => 10,
	];

	/** @var array axles info data */
	private $axles = [];

	/** @var  int absolute index of axles for group's first axle */
	private $firstNum;

	/**
	 * @return int
	 */
	public function getFirstIndex()
	{
		return $this->firstNum;
	}

	/**
	 * @return int
	 */
	public function getAxlesCount()
	{
		return count($this->axles);
	}

	/**
	 * @return int
	 */
	public function getWheelCount()
	{
		$maximum = 4;
		$minimum = 1;

		$min = $maximum;
		for ($i = 0; $i < count($this->axles); $i++) {
			$wheelCount = intval($this->axles[$i]['wheel_count']);
			if ($wheelCount < $min) {
				$min = $wheelCount;
			}
		}

		return max($minimum, min($maximum, $min));
	}

	/**
	 * @return int
	 */
	public function getWheels(){
		$maximum = 8;
		$minimum = 1;

		$max = $minimum;
		for ($i = 0;  $i < count($this->axles); $i++) {
			$wheel = intval($this->axles[$i]['wheels']);
			if ($wheel > $max) {
				$max = $wheel;
			}
		}

		//check min, max
		return max($minimum, min($maximum, $max));
	}

	/**
	 * @return int
	 */
	public function getAxleType(){
		$result = AxleLoad::AXLE_TYPE_SPRING;

		for ($i = 0;  $i < count($this->axles); $i++) {
			$axleType = intval($this->axles[$i]['type_id']);
			if ($axleType !== AxleLoad::AXLE_TYPE_SPRING) {
				$result = AxleLoad::AXLE_TYPE_NON_SPRING;
			}
		}

		return $result;
	}

	/**
	 * Calculate overage distance
	 */
	public function getDistanceId(){
		$distance = $this->calculateAverageDistance();
		return AxleLoad::getDistanceIndexByValue($distance);
	}

	/**
	 * @return float|int
	 */
	private function calculateAverageDistance(){
		if($this->getAxlesCount() === 1){
			return AxleLoad::DISTANCE_INDEX_MORE_25;
		}

		$distancesCount = count($this->axles) -1;
		$distanceSum = 0;
		for ($i = 0;  $i < $distancesCount; $i++) {
			$distance = floatval($this->axles[$i]['distance']);
			$distanceSum+=$distance;
		}

		if($distancesCount == 0 ){
			return 0;
		}

		return $distanceSum / $distancesCount;
	}

	/**
	 * Add axle to group
	 * @param array $axleInfo
	 */
	public function addAxle ($axleInfo) {
		$this->axles[] = $axleInfo;
	}

	/**
	 * @param int $index
	 */
	public function setFirstIndex($index)
	{
		$this->firstNum = $index;
	}

	/**
	 * Calculate group's total weight
	 * @return float
	 */
	public function getTotalWeight()
	{
		$totalWeight = 0;
		for($i = 0; $i < count($this->axles); $i++) {
			$totalWeight += floatval($this->axles[$i]['axle_load']);
		}

		return $totalWeight;
	}

	/**
	 * Calculate group's total weight
	 * @return float
	 */
	public function getTotalAllowedWeight($roadType, $isSpring)
	{
		$totalWeight = 0;
		$prefix = $isSpring ? '_spring' : '';
		for($i = 0; $i < count($this->axles); $i++) {
			$totalWeight += floatval($this->axles[$i]['permissible_load'.$prefix][$roadType]);
		}

		return $totalWeight;
	}

	/**
	 * @return bool
	 */
	public function hasOverweight ($roadType, $isSpring)
	{
		$totalWeight = $this->getTotalWeight();
		$totalAllowedWeight = $this->getTotalAllowedWeight($roadType, $isSpring);
		return $totalWeight > $totalAllowedWeight;
	}

	/**
	 * @return int
	 */
	public function getMinWheelCount()
	{
		if(empty($this->axles)) {
			return 0;
		}
		$minStr = $this->axles[0]['wheel_count'] ?? 1;
		$min = intval($minStr);

		for($i =1; $i < count($this->axles); $i++){
			$wheelStr =  $this->axles[$i]['wheel_count'] ?? 1;
			$wheelCount = intval($wheelStr);

			if($wheelCount < $min){
				$min = $wheelCount;
			}
		}

		return $min;
	}

	/**
	 * @return float
	 */
	public function getAllowedLoadForSingleAxle()
	{
		$wheelCount = $this->getWheelCount();

		if($wheelCount < 4){
			return self::$allowedWeights[$wheelCount];
		}

		//calculate average
		return self::$allowedWeights[$wheelCount];
	}

	/**
	 * @param AxleLoad[] $axleLoads
	 * @param bool $isSpring
	 * @param int $raadType
	 * @return float
	 */
	public function calculatePermissibleLoad($axleLoads, $isSpring, $roadType = 0){
		$axlesCount = $this->getAxlesCount();
		$distanceId = $this->getDistanceId();
		$wheelCount = $this->getWheelCount();
		$wheels = $this->getWheels();
		$axleType = $this->getAxleType();
		$isSpring = $isSpring ? 1 : 0;

		$permissibleLoad = AxleLoad::findByOptions($axleLoads, $axlesCount, $distanceId, $wheelCount, $wheels, $axleType, $isSpring, $this->lastData);

		return $permissibleLoad ? $permissibleLoad->getValue($roadType) : 0;
	}

	/**
	 * Split axles to groups
	 * @param array $axlesInfo
	 * @return AxleGroup[]
	 */
	public static function splitAxles ($axlesInfo)
	{
		$distances = [];
		foreach($axlesInfo as $axleInfo) {
			$distances[] = $axleInfo['distance'];
		}

		/** @var AxleGroup[] $groups */
		$groups = [];
		$currentGroup = 0;

		$groups[0] = new static();
		$groups[0]->setFirstIndex(0);
		$groups[0]->addAxle($axlesInfo[0]);
		for ($i = 0; $i < count($distances) - 1; $i++) {
			//if distance > max distance - start new group
			if ($distances[$i] > self::GROUP_MAX_DISTANCE) {
				$currentGroup++;
				$groups[$currentGroup] = new AxleGroup();
				$groups[$currentGroup]->setFirstIndex($i + 1);
			}

			//add next axle to group
			$groups[$currentGroup]->addAxle($axlesInfo[$i + 1]);
		}

		return $groups;
	}

	/**
	 * @param $axlesInfo
	 * @param int $isSpring
	 * @return mixed
	 */
	public static function recalculatePermissibleLoads($axlesInfo, $isSpring = 0){
		$axleLoads = AxleLoad::all();
		$groups = self::splitAxles($axlesInfo);
		foreach($groups as $group){
			$index = $group->getFirstIndex();
			for($i =0; $i < $group->getAxlesCount(); $i++){
			    $roadTypes = ApplicationRoute::getRoadTypes();
                $axlesInfo[$index + $i]['permissible_load'] = [];
                $axlesInfo[$index + $i]['permissible_load_spring'] = [];
			    foreach($roadTypes as $roadType) {
                    $load = $group->calculatePermissibleLoad($axleLoads, 0, $roadType);
                    $loadSpring = $group->calculatePermissibleLoad($axleLoads, 1, $roadType);

                    $axlesInfo[$index + $i]['permissible_load'][$roadType] = $load;
                    $axlesInfo[$index + $i]['permissible_load_spring'][$roadType] = $loadSpring;
                }

			}
		}

		return $axlesInfo;
	}

	public function getLastGroupInfo(){
		return $this->lastData;
	}
	public function getAxlesIndexesArray(){
		$result = [];
		$index = $this->getFirstIndex();
		for($i = 0; $i < count($this->axles); $i++){
			$result[] = $i + $index;
		}

		return $result;
	}
}