<?php

namespace App\Helpers;

use App\Models\Application;
use App\Http\Resources\ApplicationFull as ApplicationFullResource;

class DataConverter
{
	/**
	 * Convert full app object to array
	 * @param Application $application
	 * @return array
	 */
	public static function getApplicationData($application)
	{
		$applicationResource = new ApplicationFullResource($application);
		$appData = response()->json($applicationResource);
		$data = $appData->getData(true);

		return $data;
	}
}