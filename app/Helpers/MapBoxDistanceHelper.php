<?php

namespace App\Helpers;

use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;

class MapBoxDistanceHelper
{
	private $routeApiUrl = 'https://api.mapbox.com/directions/v5/mapbox/driving/';
	private $reverseGeocodeUrl = 'https://api.mapbox.com/geocoding/v5/mapbox.places/';
	
	public function getLocationNameByCoords($lat, $lng){
		$url = $this->buildReverseGeocodeUrl($lat, $lng);
		$response = $this->executeRequest($url, 'GET',[
			'access_token' => $this->getToken(),
			'language' => 'ru'
		]);
		$locationName = $this->parseReverseGeocodeResponse($response);
		
		return $locationName;
	}
	
	private function buildReverseGeocodeUrl($lat, $lng){
		return $this->reverseGeocodeUrl . $lng . ',' . $lat . '.json';
	}
	
	private function parseReverseGeocodeResponse($response){
		$result = null;
		if (!empty(($response['features']))) {
			return $response['features'][0]['place_name_ru'] ?? null;
		}
		
		return null;
	}
	
	/**
	 * Calculate route length by points coords list
	 * @param array $points
	 * @return float
	 */
	public function calculateRouteLength($points)
	{
		$url = $this->buildRouteUrl($points);
		$response = $this->executeRequest($url, 'GET',[
			'access_token' => $this->getToken()
		]);
		$routeLength = $this->parseRouteResponse($response);
		
		return $routeLength;
	}
	
	/**
	 * @param array $points
	 * @return null|string
	 */
	private function buildRouteUrl($points)
	{
		if (!empty($points)) {
			$pointsStr = [];
			foreach ($points as $point) {
				if(isset($point['lon']) && isset($point['lat'])) {
					$pointsStr[] = $point['lon'] . ',' . $point['lat'];
				}
			}
			$urlParams = implode(';', $pointsStr);
			return $this->routeApiUrl . $urlParams;
		}
		
		return null;
	}
	
	/**
	 * @param string $url
	 * @param string $method
	 * @param array $data
	 * @return mixed|null
	 */
	private function executeRequest($url, $method = 'GET', $data = [])
	{
		$result = null;
		try {
			$client = new \GuzzleHttp\Client();
			$params = [
				'query' => $data
			];
			
			$response = $client->request($method, $url, $params);
			$stream = $response->getBody();
			$result = json_decode($stream->getContents(), true);
			
		} catch (ClientException | RequestException $e) {
			print_r(1);
			print_r($e->getMessage());die;
		}
		
		return $result;
	}
	
	/**
	 * @param array $routeResponse
	 * @return float|null
	 */
	private function parseRouteResponse($routeResponse)
	{
		$result = null;
		if (!empty($routeResponse['routes'])) {
			for ($i = 0; $i < count($routeResponse['routes']); $i++) {
				if (isset($routeResponse['routes'][$i]['distance'])) {
					$meters = floatval($routeResponse['routes'][$i]['distance']);
					return $meters / 1000;
				}
			}
		}
		
		return null;
	}
	
	private function getToken()
	{
		return env('MAPBOX_DIRECTIONS_TOKEN');
	}
}