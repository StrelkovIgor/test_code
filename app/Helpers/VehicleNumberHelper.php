<?php

/**
 * Created by PhpStorm.
 * User: gserg13
 * Date: 27.10.2018
 * Time: 14:33
 */

namespace App\Helpers;

class VehicleNumberHelper
{
	private static $replaces = [
		'A' => 'А',
		'B' => 'В',
		'E' => 'Е',
		'K' => 'К',
		'M' => 'М',
		'H' => 'Н',
		'O' => 'О',
		'P' => 'Р',
		'C' => 'С',
		'T' => 'Т',
		'Y' => 'У',
		'X' => 'Х'
	];

	/**
	 * Format number, replace english symbols with russians, upper case
	 * @param string $number
	 * @param string $enc
	 * @return string
	 */
	public static function formatNumber($number, $enc = null)
	{
		if(!$number){
			return $number;
		}
		if(!function_exists('mb_strtoupper')){
			return $number;
		}

		$upper = mb_strtoupper($number);
		$russian = self::replaceEngToRu($upper);

		return $russian;
	}

	/**
	 * @param $number
	 * @return string
	 */
	private static function replaceEngToRu($number) {
		return strtr($number, self::$replaces);
	}
}