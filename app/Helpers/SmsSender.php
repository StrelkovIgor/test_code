<?php
/**
 * Created by PhpStorm.
 * User: gserg13
 * Date: 04.06.2018
 * Time: 11:57
 */

namespace App\Helpers;

use App\Models\User;

class SmsSender
{
    /**
     * Generate code for user and send it
     * @param User $user
     * @return bool is code sent successfully
     */
    public static function generateAndSendCode($user)
    {
        $code = self::generateCode();
        $user->confirmation_code = $code;
        if ($user->save()) {
            return self::sendCode($user->phone, $code);
        }
        return false;
    }

    /**
     * Generate new code for sending
     * @return string
     */
    private static function generateCode()
    {
        return '1234';
    }

    /**
     * Send code to user's phone
     * @param string $phone
     * @param string $code
     *
     * @return bool is code sent successfully
     */
    public static function sendCode($phone, $code)
    {
        //call component's method to send code
        return true;
    }
}