<?php

namespace App\Helpers;

use App\Models\VarsStorage;

class GradoserviceDistanceHelper
{
    private $userLogin = null;
    private $userPassword = null;

    private $lastError = null;
    private $tokenExpired = false;

    private $apiRoot = 'https://activemap.aisktg.ru';
    private $apiTileRoot = 'https://tile.aisktg.ru';

    private $tokenUrl = '/auth/token';
    private $refreshTokenUrl = '/auth/token/refresh';
    private $routingUrl = '/api/routing/';
    private $reverseGeocodingUrl = '/geocoding';

    private static $tokenData = [];

    public function __construct()
    {
        $this->userLogin = env('GRADOSERVICE_USER');
        $this->userPassword = env('GRADOSERVICE_PASSWORD');
    }

    //Getters
    /**
     * @return string|null
     */
    public function getLastError()
    {
        return $this->lastError;
    }


    //Tokens
    /**
     * @return string|null
     */
    public function getToken()
    {
        $tokenData = self::$tokenData;

        if (!$tokenData) {
            $tokenData = $this->getTokenDataFromDb();
            self::$tokenData = $tokenData;
        }

        if (self::checkTokenExpired($tokenData)) {
            $this->tokenExpired = true;
            return $this->refreshToken($tokenData);
        }

        return self::getTokenFromTokenData($tokenData);
    }

    /**
     * @param string|null $tokenData
     * @return string|null
     */
    public function refreshToken($tokenData = null) {

        $newTokenData = null;
        $refreshToken = self::getRefreshTokenFromTokenData($tokenData);

        if($refreshToken) {
            $newTokenData = $this->getTokenWithRefreshRequest($refreshToken);
        }

        if(!$newTokenData) {
            $newTokenData = $this->getTokenWithAuthRequest();
        }

        if($newTokenData) {
            $token = self::getTokenFromTokenData($newTokenData);

            //store token to static var, to db
            if ($token) {
                $newTokenData = self::addExpireTimeToTokenData($newTokenData);
                VarsStorage::setValue(VarsStorage::GRADOSERVICE_TOKEN, $newTokenData);
                self::$tokenData = $newTokenData;

                return $token;
            }
        }

        $this->lastError = 'Не получается получить токен через запрос авторизации!';
        return null;
    }

    /**
     * Get token from db
     * @return string
     */
    private function getTokenDataFromDb()
    {
        return VarsStorage::findByKey(VarsStorage::GRADOSERVICE_TOKEN);
    }

    /**
     * Check if token expired
     * @param string $tokenData
     * @return bool
     */
    private static function checkTokenExpired($tokenData)
    {
        $expireTime = self::getValueFromTokenDataByKey($tokenData, 'expireTime');
        if (!$expireTime) {
            return true;
        }
        return time() < intval($expireTime);
    }

    /**
     * @param string $tokenData
     * @return string|null
     */
    private static function getTokenFromTokenData($tokenData)
    {
        return self::getValueFromTokenDataByKey($tokenData, 'token');
    }
    /**
     * @param string $tokenData
     * @return string|null
     */
    private static function getRefreshTokenFromTokenData($tokenData)
    {
        return self::getValueFromTokenDataByKey($tokenData, 'refreshToken');
    }

    /**
     * @param string $tokenData
     * @param string $key
     * @return string|null
     */
    private static function getValueFromTokenDataByKey($tokenData, $key) {
        $value = null;

        try {
            $data = \GuzzleHttp\json_decode($tokenData, true);
            $value = array_get($data, $key);
        } catch (\Exception $e) {

        }

        return $value;
    }

    /**
     * @param $tokenData
     * @return string|null
     */
    private static function addExpireTimeToTokenData($tokenData) {
        $result = null;
        $ttl = static::getValueFromTokenDataByKey($tokenData, 'ttl');

        if($ttl) {
            $expireTime = time() + intval($ttl);
            $data = \GuzzleHttp\json_decode($tokenData, true);
            if($data) {
                $data['expireTime'] = $expireTime;
                $result = \GuzzleHttp\json_encode($data);
            }
        }

        return $result ? $result : $tokenData;
    }

    /**
     * Get token for requests to api
     * @return string
     */
    public function getTokenWithAuthRequest()
    {
        $client = new \GuzzleHttp\Client();
        $token = null;
        $tokenData = null;

        if ($this->userLogin && $this->userPassword) {
            try {
                $res = $client->post(
                    $this->getTokenUrl(), [
                        'form_params' => [
                            'login' => $this->userLogin,
                            'password' => $this->userPassword,
                        ],
                    ]
                );
                if ($res->getStatusCode() == 200) {
                    $tokenData = $res->getBody()->getContents();
                }

            } catch (\Exception $e) {
                $this->lastError = $e->getMessage();
            }
        } else {
            $this->lastError = 'Не заданы имя пользователя или пароль градосервиса в конфигурации приложения';
        }

        return $tokenData;
    }

    /**
     * @param string|null $tokenData
     * @return mixed|null
     */
    public function getTokenWithRefreshRequest($tokenData = null) {
        if(empty($tokenData)) {
            return null;
        }

        $refreshToken = self::getRefreshTokenFromTokenData($tokenData);
        if ($refreshToken) {
            $client = new \GuzzleHttp\Client();
            $token = null;
            $tokenData = null;

            if ($this->userLogin && $this->userPassword) {
                try {
                    $res = $client->get(
                        $this->getRefreshTokenUrl(), [
                            'query' => [
                                'refreshToken' => $refreshToken
                            ],
                        ]
                    );
                    if ($res->getStatusCode() == 200) {
                        $tokenData = $res->getBody()->getContents();
                    }

                } catch (\Exception $e) {
                    $this->lastError = $e->getMessage();
                }
            } else {
                $this->lastError = 'Не заданы имя пользователя или пароль градосервиса в конфигурации приложения';
            }

            return $tokenData;
        }
        return null;
    }

    //Urls

    /**
     * @return string
     */
    private function getTokenUrl()
    {
        return $this->apiRoot . $this->tokenUrl;
    }

    /**
     * @return string
     */
    private function getRefreshTokenUrl()
    {
        return $this->apiRoot . $this->refreshTokenUrl;
    }

    /**
     * @return string
     */
    private function getReverseGeocodingUrl()
    {
        return $this->apiRoot . $this->reverseGeocodingUrl;
    }

    /**
     * @return string
     */
    private function getRoutingUrl()
    {
        return $this->apiTileRoot . $this->routingUrl;
    }


    //Requests

    /**
     * @param float $lat spot latitude
     * @param float $lng spot langitude
     * @return string|null spot name
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getLocationNameByCoords($lat, $lng)
    {
        $url = $this->getReverseGeocodingUrl();

        $token = $this->getToken();
        $response = $this->executeRequest($url, 'GET', [
            'token' => $token,
            'lat' => $lat,
            'lon' => $lng
        ]);

        return $this->parseReverseGeocodeResponse($response);
    }

    /**
     * @param array $response
     * @return string
     */
    private function parseReverseGeocodeResponse($response)
    {
        return $addressItem = isset($response[0]['items'][0]['label']) ? $response[0]['items'][0]['label'] : '';
    }

    /**
     * Calculate route length by points coords list
     * @param array $points
     * @return null|float
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function calculateRouteLength($points)
    {
        $routeInfo = $this->getRouteInfo($points);

        return isset($routeInfo['distance']) ? $routeInfo['distance'] : null;
    }


    /**
     * @param array $points
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getRouteInfo($points) {
        $url = $this->buildRouteUrl($points);


        $response = $this->executeRequest($url, 'GET', [
            //'token' => $this->getToken()
        ]);

        $routeInfo = $this->parseRouteResponse($response);
        $routeInfo['url'] = $url;

        return $routeInfo;
    }
    /**
     * @param array $points
     * @return null|string
     */
    private function buildRouteUrl($points)
    {
        if (!empty($points)) {
            $pointsStr = [];
            foreach ($points as $point) {
                if (isset($point['lon']) && isset($point['lat'])) {
                    $pointsStr[] = $point['lon'] . ',' . $point['lat'];
                }
            }
            $urlParams = implode(';', $pointsStr);
            return $this->getRoutingUrl() . $urlParams;
        }

        return null;
    }

    /**
     * @param string $url
     * @param string $method
     * @param array $data
     * @return mixed|null
     * @throws \Exception
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    private function executeRequest($url, $method = 'GET', $data = [])
    {
        $result = null;
        try {
            $client = new \GuzzleHttp\Client();
            $params = [
                'query' => $data,
                'headers' => [
                    'Accept-Language' => 'ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7'
                ]
            ];

            $response = $client->request($method, $url, $params);
            $stream = $response->getBody();
            $result = \GuzzleHttp\json_decode($stream->getContents(), true);

        } catch (\Exception $e) {
            print_r($e->getMessage());
            die;
        }

        return $result;
    }

    /**
     * @param array $routeResponse
     * @return array
     */
    private function parseRouteResponse($routeResponse)
    {
        $result = [];
        $legs = [];
        $totalDistance = 0;

        //find item with minimal route
        if (!empty($routeResponse)) {
            foreach($routeResponse as $routeItem) {
                $distance = floatval(array_get($routeItem, 'RouteLength'));
                if ($distance) {
                    $totalDistance += $distance;
                    $axleLoad = array_get($routeItem, 'AxleLoad');
                    $roadType = intval(array_get($routeItem, 'TypeRoute'));
                    $commentText = intval(array_get($routeItem, 'CommentText', 0));
                    if($commentText == 6) $roadType = 2;
                    $legs[] = [
                        'd' => $distance,
                        't' => $roadType,
                        'l' => $axleLoad
                    ];
                }
            }
        }

        $result = [
            'distance' => $totalDistance / 1000, //перевод из метров в километры
            'legs' => $legs,
            'distance_info' => PriceCalculator::splitLegsToTypes($legs)
        ];

        return $result;
    }
}