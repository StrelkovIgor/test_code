<?php

namespace App\Helpers;

class CSVHelper
{
    /**
     * Delimeter for CSV row
     * @var string
     */
    public $delimeter = ',';

    /**
     * CSV File Resourse
     */
    private $resource;

    /**
     * File Name
     * @var string
     */
    private $filename;

    /**
     * Process Export
     * @param null $filename
     * @param array $headers
     * @param array $rows
     * @return string
     */
    public function export($filename = null, $headers = array(), $rows = array())
    {
        return $this->startExport($filename)
            ->setHeaders($headers)
            ->appendRows($rows)
            ->completeExport();
    }

	/**
	 * @param string $filename
	 * @param array $headers
	 * @return $this
	 */
    public function initExport($filename, $headers)
	{
		return $this->startExport($filename)
			->setHeaders($headers);
	}

    /**
     * Start Export Process;
     * @param string $filename
     * @return $this
     */
    private function startExport($filename)
    {
        $this->resource = fopen($filename, 'w');
        $this->filename = $filename;
        return $this;
    }

    /**
     * Set File headers
     * @param array $headers
     * @return $this
     */
    private function setHeaders(array $headers)
    {
        fputcsv($this->resource, $headers, $this->delimeter);
        return $this;
    }

    /**
     * Append Row into Export File
     * @param array $row
     * @return $this
     */
    private function appendRow(array $row)
    {
        fputcsv($this->resource, $row, $this->delimeter);
        return $this;
    }

    /**
     * @param array $rows
     * @return $this
     */
    public function appendRows(array $rows)
    {
        foreach ($rows as $row) {
            $this->appendRow($row);
        }

        return $this;
    }

    /**
     * @return string
     */
    public function completeExport()
    {
        fclose($this->resource);
        return $this->filename;
    }

}