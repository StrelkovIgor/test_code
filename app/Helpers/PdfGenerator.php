<?php
/**
 * Created by PhpStorm.
 * User: gserg13
 * Date: 13.12.2018
 * Time: 0:26
 */

namespace App\Helpers;

use App\Models\Application;
use App\Models\ApplicationAgreement;
use App\Models\ApplicationDate;
use App\Models\ApplicationLoad;
use App\Models\ApplicationRoute;
use App\Models\IssuePlace;
use App\Models\PrivilegeStatus;
use App\Models\SpecialCondition;
use App\Models\User;
use App\Models\Vehicle;
use TCPDF;

class PdfGenerator
{
	/** @var int routes max symbols count int  */
	private $routeLengthLimit = 2000;

	private $fullHeight = 297;
	
	private $font = 'freeserif';
	private $fontSize = 12;
	private $unboldFontSize = 10;
	private $routeFontSize = 8;
	private $style = 'B';
	
	private $routeWidth = 180;
	private $routeHeight = 60;
	
	/** @var Application */
	private $application = null;
	/** @var ApplicationLoad */
	private $applicationLoad = null;
	/** @var ApplicationRoute */
	private $applicationRoute = null;
	/** @var PrivilegeStatus  */
	private $privilegeStatus = null;
	/** @var ApplicationDate */
	private $applicationDates = null;
	/** @var null IssuePlace */
	private $issuePlace = null;
	/** @var User */
	private $admin = null;
	/** @var Vehicle */
	private $vehicle = null;

	
	/** @var string */
	private $path = null;
	/** @var \TCPDF */
	private $pdf = null;
	
	private $fields = [
		[
			//page 1
			'id' => [93, 255],
			'year' => [139, 246],
			'type' => [72,246],
			'num' => [55,233],
			'start' => [108,233],
			'end' => [161,233],
			'route' => [14,216],
			'train' => [14,144],
			'owner' => [14,125.5],
			'cargo' => [14,116],
			'weight' => [55,97],
			'weight_ts' => [111,97],
			'weight_trailer' => [170,97],
			'distances' => [60,86],
			'loads' => [43,75],
			'length' => [71,64],
			'width' => [121,64],
			'height' => [171,64],
			'position' => [23,39],
			'name' => [146,39],
			'date' => [45,30],
			'executor' => [28,15],
		],
		[
			//page 2
			'cars' => [14, 290],
			'info' => [14, 269],
			'agreement' => [14, 217],
		]
	];
	
	/**
	 * Generate full permit by app
	 * @param Application $application
	 * @param string $path
	 * @return bool if file generated
	 */
	public function generatePermit($application, $path)
	{
		$this->application = $application;
		$this->path = $path;
		
		if (!$this->loadData()) {
			return false;
		}
		$this->initPermitPage();
		$this->renderFirstPage();
		
		$this->pdf->AddPage();
		$this->pdf->setPage(2);
		$this->renderSecondPage();
		
		$this->saveFile();
		return true;
	}
	
	/**
	 * Load all necessary data from db, return false if load failed
	 * @return bool
	 */
	private function loadData()
	{
		$this->applicationLoad = $this->application->application_load;
		$this->applicationRoute = $this->application->application_route;
		$this->privilegeStatus = $this->application->privilege_status;
		$this->applicationDates = $this->application->application_dates;
		$this->admin = $this->application->admin;
		$this->issuePlace = $this->applicationDates ? IssuePlace::find($this->applicationDates->issue_place_id) : null;
		$this->vehicle = $this->application->vehicle;
		
		return
			$this->applicationLoad &&
			($this->applicationRoute || $this->privilegeStatus) &&
			$this->applicationDates &&
			$this->admin &&
			$this->issuePlace &&
			$this->vehicle;
	}
	
	/**
	 * Init tcpdf element
	 * @return TCPDF
	 */
	private function initPermitPage()
	{
		$this->pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		
		$this->pdf->setFontSubsetting(true);
		$this->pdf->SetFont($this->font, $this->style, $this->fontSize);
		$this->pdf->SetPrintHeader(false);
		$this->pdf->SetPrintFooter(false);
		//bottom margings
		$this->pdf->SetAutoPageBreak(true, 0);
		
		$this->pdf->SetCreator(PDF_CREATOR);
		$this->pdf->SetAuthor('ГБУ БДД');
		$this->pdf->SetTitle('Тестовое разрешение');
		
		$this->pdf->SetAlpha(0);
		
		$this->pdf->AddPage();
		
		return $this->pdf;
	}
	
	/**
	 * Render all app info
	 */
	private function renderFirstPage()
	{
		$this->setFieldCoords('id');
		$this->write('№' . $this->application->getFormattedId());
		
		$this->setFieldCoords('type');
		$this->write('местная');
		
		$this->setFieldCoords('year');
		$this->write(date('Y'));
		
		$runs_count = $this->applicationDates->runs_count ?? 0;
		$this->setFieldCoords('num');
		$this->write($runs_count);
		
		$startDate = $this->application->startDateFormatted() ?? '';
		$this->setFieldCoords('start');
		$this->write($startDate);
		
		$finishDate = $this->application->finishDateFormatted() ?? '';
		$this->setFieldCoords('end');
		$this->write($finishDate);
		

		$vehiclesInfo = $this->application->getVehicleFullInfo();
		$vehiclesInfo .= $this->application->getTrailersFullInfo();

		$this->setFieldCoords('train');
		$this->write($vehiclesInfo);
		
		$this->setFieldCoords('owner');
		$name = $this->application->getUserName();
		$address = $this->application->getUserAddress();

		$this->pdf->MultiCell(180, 2, $name . ' ' . $address, 0, 'L', 0, 0, '', '', true, 0, false);
		
		$this->setFieldCoords('cargo');
		$loadInfo = $this->applicationLoad->getLoadTypeInfo() . ', ' . $this->applicationLoad->name;
		if($this->applicationLoad->load_weight){
			$loadInfo .= ', ' . $this->applicationLoad->getLoadWeight();
		}
		$this->write($loadInfo);
		
		
		
		$this->setFieldCoords('weight');
		$this->write($this->applicationLoad->weight);
		
		$this->setFieldCoords('weight_ts');
		$this->write($this->application->getVehicePtsWeightFormatted());
		
		$this->setFieldCoords('weight_trailer');
		$trailersWeight = $this->application->getTrailersTotalPtsWeight();
		$trailersWeightValue = $trailersWeight ? $trailersWeight : '';
		$this->write($trailersWeightValue);
		
		$this->setFieldCoords('distances');
		$this->write($this->applicationLoad->getAxleDistances());
		
		$this->setFieldCoords('loads');
		$this->write($this->applicationLoad->getAxleLoads());
		
		$this->setFieldCoords('length');
		$this->write($this->applicationLoad->length);
		
		$this->setFieldCoords('width');
		$this->write($this->applicationLoad->width);
		
		$this->setFieldCoords('height');
		$this->write($this->applicationLoad->height);
		
		$this->setFieldCoords('date');
		$this->write($this->application->getActivateDate($this->application->myDateFormat));
		
		//Route
		$this->renderRoute();
		$this->pdf->SetFont($this->font, '', $this->unboldFontSize);
		
		/** @var IssuePlace $issuePlace */
		$issuePlace = IssuePlace::find($this->applicationDates->issue_place_id);
		$this->setFieldCoords('position');
		$this->write($issuePlace->position);
		
		$this->setFieldCoords('name');
		$this->write($issuePlace->fio);
		
		$this->setFieldCoords('executor');
		$this->write($this->application->admin->name);
	}
	
	/**
	 * Render all app info
	 */
	private function renderSecondPage()
	{
		$this->pdf->SetFont($this->font, 'B', $this->fontSize);
		$this->setFieldCoords('cars', 2);
		$this->write($this->applicationLoad->getEscortTitle());
		$this->pdf->SetFont($this->font, '', $this->unboldFontSize);
		
		
		$this->setFieldCoords('info', 2);
		$infoText = 'При движении ч/з путепроводы, под ЛЭП и искусственные сооружения производить контрольные промеры габаритных размеров. Движение по мостам осуществлять строго в одиночном порядке со скоростью не более 15 км/ч. без остановок и ускорений. При введении временного ограничения в летний период при значениях дневной температуры воздуха свыше 32°C движение разрешается в период с 22.00 до 10.00 часов';

		$dc = SpecialCondition::find(SpecialCondition::SPRING_ID);
		if($dc){
            try {
                if(strtotime($this->application->start_date) < strtotime($dc->start_date) &&
                    strtotime($this->application->finish_date) >= strtotime($dc->finish_date)
                ){
                    $infoText .= "\r\n\r\n"."Данное разрешение не действует на период весеннего ограничения при условии превышения допустимых нагрузок в размере 6 тонн/ось";
                }
            }catch (\Exception $e){}
		}

        //$this->write($infoText);
        $this->pdf->MultiCell(180, 5, $infoText, 0, 'L', 1, 0, '', '', true);
		
		$agreementsList = '';
		$agreements = $this->application->application_agreements;
		if(!empty($agreements)){
			$i=0;
			foreach($agreements as $agreement){
				if($agreement->status === ApplicationAgreement::STATUS_ACCEPTED){
					$i++;
					$agreementsList .= '№ '. $i . ' ' . $agreement->department->title . ', ' . $agreement->accept_date . '<br />';
				}
			}
		}
		$this->setFieldCoords('agreement', 2);
		//$this->write($agreementsList);
		$this->pdf->MultiCell(180, 0, "<span>".$agreementsList."</span>", 0, 'L', 1, 0, '', '', true, 0, true);
	}
	
	/**
	 * Set pdf writer position
	 * @param string $key
	 * @param $page
	 */
	private function setFieldCoords($key, $page = 1)
	{
		$coords = array_get($this->fields[$page - 1], $key, [0, 0]);
		$x = $coords[0];
		$y = $this->fullHeight - $coords[1];
		$this->pdf->SetXY($x, $y);
	}
	
	/**
	 * Write text to pdf
	 * @param string $text
	 */
	private function write($text)
	{
		$this->pdf->Write($this->fontSize, $text);
	}
	
	/**
	 * Render route info
	 */
	private function renderRoute()
	{
		$this->pdf->SetFont($this->font, '', $this->routeFontSize);
		$routeCoords = $coords = array_get($this->fields[0], 'route', [0, 0]);
		
		$this->drawRouteLines($routeCoords[0], $this->fullHeight - $routeCoords[1], $this->routeWidth, $this->routeHeight, 2);
		$this->drawRouteLines($routeCoords[0], $this->fullHeight - $routeCoords[1], $this->routeWidth, $this->routeHeight, 2, true);
		
		$this->setFieldCoords('route');

		$routeText = '';
		if($this->privilegeStatus){
			$routeText = $this->privilegeStatus->route_info;
		}else{
			if($this->applicationRoute){
				$routeText = $this->applicationRoute->getRouteText() . ' (' . $this->applicationRoute->getRouteTypeInfo() . '. Движение по региональным дорогам)';
			}
		}

		//symbols limit
		$routeLength = mb_strlen($routeText, 'utf8');
		if($routeLength >= $this->routeLengthLimit){
			$routeText = mb_strimwidth($routeText, 0, $this->routeLengthLimit, null, 'utf8');
			$routeText .= '... продолжение маршрута см. на сайте https://aisktg.ru';
		}

		$routeText = '<div style="background-color: #fff">' . $routeText . '</div>';
		$this->pdf->SetFillColor(255, 255, 255);
		$this->pdf->MultiCell(180, 5, $routeText, 0, 'L', 1, 0, '', '', true, 0, true);
	}
	
	/**
	 * Draw route lines
	 * @param float $x
	 * @param float $y
	 * @param float $width
	 * @param float $height
	 * @param int $count
	 */
	private function drawRouteLines($x, $y, $width, $height, $count, $mirror = false)
	{
		$lineStyle = [
			'width' => 0.5,
			'cap' => 'butt',
			'join' => 'miter',
			'dash' => 0,
			'color' => [150, 150, 150]
		];
		$stepWidth = $width / $count / 2;
		
		for ($i = 0; $i < $count; $i++) {
			$startX = $x + $i * $stepWidth * 2;
			$startY = $y;
			$finishX = $startX + $stepWidth;
			$finishY = $y + $height;
			if($mirror){
				$swap = $startY;
				$startY = $finishY;
				$finishY = $swap;
			}
			$this->pdf->Line($startX, $startY, $finishX, $finishY, $lineStyle);
			
			$fin = $startY;
			$startX = $finishX;
			$startY = $finishY;
			$finishX = $startX + $stepWidth;
			$finishY = $fin;

			$this->pdf->Line($startX, $startY, $finishX, $finishY, $lineStyle);
		}
	}
	
	/**
	 * Save to file
	 */
	private function saveFile()
	{
		$this->pdf->Output($this->path, 'F');
	}
}