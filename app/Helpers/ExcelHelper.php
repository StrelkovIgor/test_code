<?php
/**
 * Created by PhpStorm.
 * User: gserg13
 * Date: 04.06.2018
 * Time: 11:57
 */

namespace App\Helpers;

use App\Models\Application;
use App\Models\ApplicationAgreement;
use App\Models\Audit\Audit;
use App\Models\Audit\Enum\Events;
use App\Models\Coefficient;
use App\Models\Role;
use App\Models\User;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class ExcelHelper
{
	private $activeReportSpreadSheet = null;
	private $activeReportWorksheet = null;
	private $activeReportCurrentRow = 2;
	private $activeReportKoeff = null;

	public $brandsListPath = 'templates/brands.xlsx';
	public $privilegeListPath = 'templates/privilege.xlsx';
	
	/**
	 * @var string invoice template path
	 */
	private $invoiceTemplatePath = 'templates/invoice.xlsx';

	/**
	 * @var string pay sheet template path
	 */
	private $paySheetTemplatePath = 'templates/paysheet.xlsx';

    public function initAllReportExport()
    {
        //empty file
        $this->activeReportSpreadSheet = new Spreadsheet();
        $this->activeReportWorksheet = $this->activeReportSpreadSheet->getActiveSheet();

        $this->fillAllReportTitles($this->activeReportWorksheet);

        $this->activeReportCurrentRow = 2;
        $this->activeReportKoeff = Coefficient::find(Coefficient::ID_GP);
    }

    public function addAllReportData($applications)
    {
        foreach($applications as $appData) {
            $this->addAllReportRowOptimized($this->activeReportWorksheet, $appData);
        }
    }


	public function initActiveReportExport()
	{
		//empty file
		$this->activeReportSpreadSheet = new Spreadsheet();
		$this->activeReportWorksheet = $this->activeReportSpreadSheet->getActiveSheet();

		$this->fillActiveReportTitles($this->activeReportWorksheet);

		$this->activeReportCurrentRow = 2;
		$this->activeReportKoeff = Coefficient::find(Coefficient::ID_GP);

	}

	public function addActiveReportData($applications)
	{
		foreach($applications as $appData) {
			$this->addActiveReportRowOptimized($this->activeReportWorksheet, $appData);
		}
	}

	public function finishActiveExport()
	{
		//save template, return link
		$writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($this->activeReportSpreadSheet);
		$pathInfo = $this->generateActiveReportPath();
		$writer->save($pathInfo['path']);

		return $pathInfo['url'];
	}

	/**
	 * Return new brands titles list
	 * @param string $path path to file
	 * @return array
	 */
	public function importVehicleBrands($path)
	{
		$titles = [];
		
		$reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader('Xlsx');
		
		//get rows count
		$worksheetData = $reader->listWorksheetInfo($path);
		$rowsCount = $worksheetData[0]['totalRows'] ?? 0;
		
		if ($rowsCount) {
			$spreadsheet = $reader->load($path);
			$worksheet = $spreadsheet->getActiveSheet();
			
			for ($i = 0; $i < $rowsCount; $i++) {
				$titles[] = $worksheet->getCell('A' . ($i + 1))->getValue();
			}
		}
		
		return $titles;
	}
	
	/**
	 * Return new privilege vehicles
	 * @param string $path path to file
	 * @return array
	 */
	public function importPrivilegeVehicles($path)
	{
		$data = [];
		
		$reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader('Xlsx');
		
		//get rows count
		$worksheetData = $reader->listWorksheetInfo($path);
		$rowsCount = $worksheetData[0]['totalRows'] ?? 0;

		if ($rowsCount) {
			$spreadsheet = $reader->load($path);
			$worksheet = $spreadsheet->getActiveSheet();
			
			for ($i = 2; $i <= $rowsCount; $i++) {
				$number = $worksheet->getCell('A' . $i)->getValue();
				$number = VehicleNumberHelper::formatNumber($number);
				$region = $worksheet->getCell('B' . $i)->getValue();
				$data[] = [
					'number' => $number,
					'region' => $region,
					'brand_title' => $worksheet->getCell('C' . $i)->getValue(),
					'model_title' => $worksheet->getCell('D' . $i)->getValue(),
					'real_number' => $number . ' ' . $region
				];
			}
		}
		
		return $data;
	}

	/**
	 * @param array $data
	 *
	 * @return string
	 */
	public function exportLogs($data)
	{
		//load template
		$spreadsheet = new Spreadsheet();

		//set invoice data
		$this->fillLogsData($spreadsheet, $data);

		//save template, return link
		$writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
		$pathInfo = $this->generateLogsPath();
		$writer->save($pathInfo['path']);

		return $pathInfo['url'];
	}

	/**
	 * @param Spreadsheet $spreadsheet
	 * @param array $data
	 */
	private function fillLogsData($spreadsheet, $data)
	{
		$worksheet = $spreadsheet->getActiveSheet();

		$this->fillAuditTitles($worksheet);

		$row = 2;
		foreach($data as $auditData) {
			$this->addAuditInfoRow($worksheet, $auditData, $row);
			$row++;
		}
	}

	/**
	 * @param Worksheet $worksheet
	 */
	private function fillAuditTitles($worksheet)
	{
		$worksheet->getCell('A1')->setValue('Дата и время события');
		$worksheet->getCell('B1')->setValue('Фио и роль пользователя');
		$worksheet->getCell('C1')->setValue('Тип действия');
		$worksheet->getCell('D1')->setValue('Объект действия');

		$worksheet->getColumnDimension('A')->setAutoSize(true);
		$worksheet->getColumnDimension('B')->setAutoSize(true);
		$worksheet->getColumnDimension('C')->setAutoSize(true);
		$worksheet->getColumnDimension('D')->setAutoSize(true);
	}

	/**
	 * @param Worksheet $worksheet
	 * @param array $auditData
	 * @param int $row
	 */
	private function addAuditInfoRow($worksheet, $auditData, $row)
	{
		$worksheet->getCell('A' . $row)->setValue($auditData['created_at']);
		$worksheet->getCell('B' . $row)->setValue($auditData['user_name'] . ' (' . Role::getNameById($auditData['user_role']) . ')');
		$worksheet->getCell('C' . $row)->setValue($auditData['event_title']);
		$worksheet->getCell('D' . $row)->setValue($this->auditObjectInfo($auditData));
	}

	/**
	 * @param array $auditData
	 * @return string
	 */
	private function auditObjectInfo ($auditData)
	{
		$auditObject = $auditData['audit_object'];
		unset($auditObject['id']);
		if(isset($auditObject['role_id'])) {
			$auditObject['role_id'] = Role::getNameById($auditObject['role_id']);
		}
		$objectInfo = implode(' / ' ,$auditObject);

		$result = $auditData['audit_type'] . ': ' . $objectInfo;

		return $result;
	}

	/**
	 * Generates save path and url for logs file
	 *
	 * @return array
	 */
	private function generateLogsPath()
	{
		$path = [
			'logs'
		];

		$filename = 'audit_' . time() . '.xlsx';
		$url = env('APP_URL') . '/storage/' . implode('/', $path) . '/' . $filename;


		$dirPath = 'public/' . implode('/', $path);
		\Storage::makeDirectory($dirPath);
		$path = storage_path('app/' . $dirPath . '/' . $filename);

		return [
			'path' => $path,
			'url' => $url
		];
	}

	/**
	 * Create excel file with active apps report
	 * @param Application[] $data
	 * @return string
	 */
	public function exportActiveReportsOptimized($data)
	{
		//empty file
		$spreadsheet = new Spreadsheet();

		//set invoice data
		$this->fillActiveReportDataOptimized($spreadsheet, $data);

		//save template, return link
		$writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
		$pathInfo = $this->generateActiveReportPath();
		$writer->save($pathInfo['path']);

		return $pathInfo['url'];
	}

	/**
	 * Create excel file with active apps report
	 * @param array $data
	 * @return string
	 */
	public function exportActiveReports($data)
	{
		//empty file
		$spreadsheet = new Spreadsheet();

		//set invoice data
		$this->fillActiveReportData($spreadsheet, $data);

		//save template, return link
		$writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
		$pathInfo = $this->generateActiveReportPath();
		$writer->save($pathInfo['path']);

		return $pathInfo['url'];
	}

	/**
	 * @param Spreadsheet $spreadsheet
	 * @param Application[] $data
	 */
	private function fillActiveReportDataOptimized($spreadsheet, $data)
	{
		$worksheet = $spreadsheet->getActiveSheet();

		$this->fillActiveReportTitles($worksheet);

		$koeff = Coefficient::find(Coefficient::ID_GP);
		$row = 2;
		foreach($data as $appData) {
			$this->addActiveReportRowOptimized($worksheet, $appData, $row, $koeff);
			$row++;
		}
	}

	/**
	 * @param Spreadsheet $spreadsheet
	 * @param array $data
	 */
	private function fillActiveReportData($spreadsheet, $data)
	{
		$worksheet = $spreadsheet->getActiveSheet();

		$this->fillActiveReportTitles($worksheet);

		$koeff = Coefficient::find(Coefficient::ID_GP);
		$row = 2;
		foreach($data as $appData) {
			$this->addActiveReportRow($worksheet, $appData, $row, $koeff);
			$row++;
		}
	}

    /**
     * @param Worksheet $worksheet
     */
    private function fillAllReportTitles($worksheet)
    {
        $worksheet->getCell('A1')->setValue('Порядковый номер');
        $worksheet->getCell('B1')->setValue('Номер разрешения');
        $worksheet->getCell('C1')->setValue('Территориальное управление');
        $worksheet->getCell('D1')->setValue('Дата начала действия разрешения');
        $worksheet->getCell('E1')->setValue('Дата окончания действия разрешения');
        $worksheet->getCell('F1')->setValue('Организация / ФИО заявителя');
        $worksheet->getCell('G1')->setValue('ИНН заявителя');
        $worksheet->getCell('H1')->setValue('ГРЗ ТС (основное)');
        $worksheet->getCell('I1')->setValue('Стоимость разрешения (руб)');
        $worksheet->getCell('J1')->setValue('Стоимость вреда');
        $worksheet->getCell('K1')->setValue('Дата и время подачи заявления');
        $worksheet->getCell('L1')->setValue('Ведомства для согласования');
        $worksheet->getCell('M1')->setValue('Дата и время согласования разрешения');
        $worksheet->getCell('N1')->setValue('Сотрудник, согласовавший разрешение');
        $worksheet->getCell('O1')->setValue('Дата и время выдачи разрешения');
        $worksheet->getCell('P1')->setValue('Сотрудник, выдавший (активировавший) разрешение');
        //new
        $worksheet->getCell('Q1')->setValue('Тип (быстрое / обычное)');
        $worksheet->getCell('R1')->setValue('Вид перевозки');//по маршруту с грузом, обратно без и т.п.
        $worksheet->getCell('S1')->setValue('Количество поездок');
        $worksheet->getCell('T1')->setValue('Маршрут');
        $worksheet->getCell('U1')->setValue('Общее расстояние');
        $worksheet->getCell('V1')->setValue('Марка, модель ГРЗ ТС');
        $worksheet->getCell('W1')->setValue('Марка, модель ГРЗ прицепа, если есть');
        $worksheet->getCell('X1')->setValue('Тип, наименование и масса груза');
        $worksheet->getCell('Y1')->setValue('Общая масса ТС');
        $worksheet->getCell('Z1')->setValue('Расстояние между осями');
        $worksheet->getCell('AA1')->setValue('Фактические нагрузки на оси');
        $worksheet->getCell('AB1')->setValue('Скатность');
        $worksheet->getCell('AC1')->setValue('Подвеска');
        $worksheet->getCell('AD1')->setValue('Количество колес');
        $worksheet->getCell('AE1')->setValue('Длина, ширина, высота автопоезда');
        $worksheet->getCell('AF1')->setValue('Статус');//privilege


        for ($char = 'A'; $char <= 'Z'; $char++) {
            $worksheet->getColumnDimension($char)->setAutoSize(true);
        }
        for ($char = 'A'; $char <= 'F'; $char++) {
            $worksheet->getColumnDimension('A' . $char)->setAutoSize(true);
        }
    }

	/**
	 * @param Worksheet $worksheet
	 */
	private function fillActiveReportTitles($worksheet)
	{
		$worksheet->getCell('A1')->setValue('Порядковый номер');
		$worksheet->getCell('B1')->setValue('Номер разрешения');
		$worksheet->getCell('C1')->setValue('Территориальное управление');
		$worksheet->getCell('D1')->setValue('Дата начала действия разрешения');
		$worksheet->getCell('E1')->setValue('Дата окончания действия разрешения');
		$worksheet->getCell('F1')->setValue('Организация / ФИО заявителя');
		$worksheet->getCell('G1')->setValue('ИНН заявителя');
		$worksheet->getCell('H1')->setValue('ГРЗ ТС (основное)');
		$worksheet->getCell('I1')->setValue('Стоимость разрешения (руб)');
		$worksheet->getCell('J1')->setValue('Стоимость вреда');
		$worksheet->getCell('K1')->setValue('Дата и время подачи заявления');
		$worksheet->getCell('L1')->setValue('Ведомства для согласования');
		$worksheet->getCell('M1')->setValue('Дата и время согласования разрешения');
		$worksheet->getCell('N1')->setValue('Сотрудник, согласовавший разрешение');
		$worksheet->getCell('O1')->setValue('Дата и время выдачи разрешения');
		$worksheet->getCell('P1')->setValue('Сотрудник, выдавший (активировавший) разрешение');

		for ($char = 'A'; $char <= 'P'; $char++) {
			$worksheet->getColumnDimension($char)->setAutoSize(true);
		}
	}

    /**
     * @param Worksheet $worksheet
     * @param Application $application
     */
    private function addAllReportRowOptimized ($worksheet, $application)
    {
        /** @var int $row */
        $row = $this->activeReportCurrentRow;
        /** @var Coefficient $koeff */
        $koeff = $this->activeReportKoeff;

        $worksheet->getCell('A' . $row)->setValue($row - 1);
        $worksheet->getCell('B' . $row)->setValue($application->getFormattedId());
        $worksheet->getCell('C' . $row)->setValue($application->getIssuePlaceTitle());
        $worksheet->getCell('D' . $row)->setValue($application->startDateFormatted());
        $worksheet->getCell('E' . $row)->setValue($application->finishDateFormatted());

        $worksheet->getCell('F' . $row)->setValue($application->username);
        $worksheet->getCell('G' . $row)->setValue($application->getUserInn());

        $worksheet->getCell('H' . $row)->setValue($application->getVehicleRealNumber());
        $priceFormatted = number_format($application->getRealPrice(), 2, ',', '');
        $worksheet->getCell('I' . $row)->setValue($priceFormatted);

        $blankPrice = 0;
        if($application->is_spring) {
            $blankPrice = $koeff->spring_value;
        }else{
            $blankPrice = $koeff->value;
        }
        $damage = $application->getRealPrice() - $blankPrice;
        $damage = max(0, $damage);
        $damageFormatted = number_format($damage, 2, ',', '');
        $worksheet->getCell('J' . $row)->setValue($damageFormatted);

        //events
        /** @var Audit $toReviewEvent */
        $toReviewEvent = null;
        /** @var Audit $acceptEvent */
        $acceptEvent = null;
        /** @var Audit $activateEvent */
        $activateEvent = null;
        foreach($application->audits as $audit) {
            if($audit->event_key === Events::APPLICANT_APP_TOREVIEW_KEY) {
                $toReviewEvent = $audit;
            }
            if($audit->event_key === Events::ADMINGBU_APP_ACCEPT_KEY) {
                $acceptEvent = $audit;
            }
            if($audit->event_key === Events::ADMINGBU_APP_ACTIVATE_KEY) {
                $activateEvent = $audit;
            }
        }

        $toReviewTime = $toReviewEvent ? $toReviewEvent->created_at : '';
        $worksheet->getCell('K' . $row)->setValue($toReviewTime);

        $worksheet->getCell('L' . $row)->setValue($this->getAppAgreementsOptimized($application));

        $acceptTime = $acceptEvent ? $acceptEvent->created_at : $application->accept_date;
        $acceptUsername = $acceptEvent && $acceptEvent->user ? $acceptEvent->user->name : '';
        if(!$acceptUsername){
            if($application->admin) {
                $acceptUsername = $application->admin->name;
            }
        }
        $worksheet->getCell('M' . $row)->setValue($acceptTime);
        $worksheet->getCell('N' . $row)->setValue($acceptUsername);

        $activateTime = $activateEvent ? $activateEvent->created_at : $application->activate_date;
        $activateUsername = $activateEvent && $activateEvent->user ? $activateEvent->user->name : '';
        if(!$activateUsername) {
            if($application->admin) {
                $activateUsername = $application->admin->name;
            }
        }


//		$worksheet->getCell('N' . $row)->setValue($application->activate_date);
        $worksheet->getCell('O' . $row)->setValue($activateTime);
        $worksheet->getCell('P' . $row)->setValue($activateUsername);

        //new
        //Тип заявления
        $isFastText = $application->isFast() ? 'Быстрое' : 'Обычное';
        $worksheet->getCell('Q' . $row)->setValue($isFastText);

        //Тип поездок
        $routeType = $application->application_route ? $application->application_route->getRouteTypeInfo() : '';
        $worksheet->getCell('R' . $row)->setValue($routeType);

        //число поездок
        $runsCount = $application->application_dates ? $application->application_dates->runs_count : '';
        $worksheet->getCell('S' . $row)->setValue($runsCount);

        //маршрут
        $routeText = '';
        if($application->privilege_status){
            $routeText = $application->privilege_status->route_info;
        }else{
            if($application->application_route){
                $routeText = $application->application_route->getRouteText();
            }
        }
        $worksheet->getCell('T' . $row)->setValue($routeText);

        //Общее расстояние
        $distance = $application->application_route ? $application->application_route->distance : 0;
        $worksheet->getCell('U' . $row)->setValue($distance);

        //Марка, модель, ГРЗ ТС
        $vehicleInfo = $application->getVehicleFullInfo();
        $worksheet->getCell('V' . $row)->setValue($vehicleInfo);

        //Марка, модель ГРЗ прицепа, если есть
        $trailersInfo = $application->getTrailersFullInfo();
        $worksheet->getCell('W' . $row)->setValue($trailersInfo);

        //Тип, наименование и масса груза
        $loadInfo = '';
        if ($application->application_load) {
            $loadInfo = $application->application_load->getLoadTypeInfo() . ', ' . $application->application_load->name;
            if($application->application_load->load_weight){
                $loadInfo .= ', ' . $application->application_load->getLoadWeight();
            }
        }
        $worksheet->getCell('X' . $row)->setValue($loadInfo);

        //Общая масса ТС
        $weight = $application->application_load ? $application->application_load->weight : 0;
        $worksheet->getCell('Y' . $row)->setValue($weight);//@todo check

        //Расстояние между осями
        $distances = $application->application_load ? $application->application_load->getAxleDistances() : '';
        $worksheet->getCell('Z' . $row)->setValue($distances);

        //Фактические нагрузки на оси
        $loads = $application->application_load ? $application->application_load->getAxleLoads() : '';
        $worksheet->getCell('AA' . $row)->setValue($loads);

        //Скатности осей
        $wheels = $application->application_load ? $application->application_load->getAxleWheelCount() : '';
        $worksheet->getCell('AB' . $row)->setValue($wheels);

        //Подвеска
        $types = $application->application_load ? $application->application_load->getAxleIsLifting() : '';
        $worksheet->getCell('AC' . $row)->setValue($types);

        //Число колес
        $wheels = $application->application_load ? $application->application_load->getAxleWheels() : '';
        $worksheet->getCell('AD' . $row)->setValue($wheels);

        //Длина, ширина, высота автопоезда
        $sizesVal = '';
        if ($application->application_load) {
            $sizes = [$application->application_load->length, $application->application_load->width, $application->application_load->height];
            $sizesVal = implode(' x ', $sizes);
        }
        $worksheet->getCell('AE' . $row)->setValue($sizesVal);

        //Длина, ширина, высота автопоезда
        $privilegeStr = '';
        if ($application->privilege_status) {
            $privilegeStr = $application->privilege_status->title;
        }
        $worksheet->getCell('AF' . $row)->setValue($privilegeStr);

        $this->activeReportCurrentRow++;
    }

	/**
	 * @param Worksheet $worksheet
	 * @param Application $application
	 */
	private function addActiveReportRowOptimized ($worksheet, $application)
	{
		/** @var int $row */
		$row = $this->activeReportCurrentRow;
		/** @var Coefficient $koeff */
		$koeff = $this->activeReportKoeff;

		$worksheet->getCell('A' . $row)->setValue($row - 1);
		$worksheet->getCell('B' . $row)->setValue($application->getFormattedId());
		$worksheet->getCell('C' . $row)->setValue($application->getIssuePlaceTitle());
		$worksheet->getCell('D' . $row)->setValue($application->startDateFormatted());
		$worksheet->getCell('E' . $row)->setValue($application->finishDateFormatted());

		$worksheet->getCell('F' . $row)->setValue($application->username);
		$worksheet->getCell('G' . $row)->setValue($application->getUserInn());

		$worksheet->getCell('H' . $row)->setValue($application->getVehicleRealNumber());
		$priceFormatted = number_format($application->getRealPrice(), 2, ',', '');
		$worksheet->getCell('I' . $row)->setValue($priceFormatted);

		$blankPrice = 0;
		if($application->is_spring) {
			$blankPrice = $koeff->spring_value;
		}else{
			$blankPrice = $koeff->value;
		}
		$damage = $application->getRealPrice() - $blankPrice;
		$damage = max(0, $damage);
		$damageFormatted = number_format($damage, 2, ',', '');
		$worksheet->getCell('J' . $row)->setValue($damageFormatted);

		//events
		/** @var Audit $toReviewEvent */
		$toReviewEvent = null;
		/** @var Audit $acceptEvent */
		$acceptEvent = null;
		/** @var Audit $activateEvent */
		$activateEvent = null;
		foreach($application->audits as $audit) {
			if($audit->event_key === Events::APPLICANT_APP_TOREVIEW_KEY) {
				$toReviewEvent = $audit;
			}
			if($audit->event_key === Events::ADMINGBU_APP_ACCEPT_KEY) {
				$acceptEvent = $audit;
			}
			if($audit->event_key === Events::ADMINGBU_APP_ACTIVATE_KEY) {
				$activateEvent = $audit;
			}
		}

		$toReviewTime = $toReviewEvent ? $toReviewEvent->created_at : '';
		$worksheet->getCell('K' . $row)->setValue($toReviewTime);

		$worksheet->getCell('L' . $row)->setValue($this->getAppAgreementsOptimized($application));

		$acceptTime = $acceptEvent ? $acceptEvent->created_at : $application->accept_date;
		$acceptUsername = $acceptEvent && $acceptEvent->user ? $acceptEvent->user->name : '';
		if(!$acceptUsername){
			if($application->admin) {
				$acceptUsername = $application->admin->name;
			}
		}
		$worksheet->getCell('M' . $row)->setValue($acceptTime);
		$worksheet->getCell('N' . $row)->setValue($acceptUsername);

		$activateTime = $activateEvent ? $activateEvent->created_at : $application->activate_date;
		$activateUsername = $activateEvent && $activateEvent->user ? $activateEvent->user->name : '';
		if(!$activateUsername) {
			if($application->admin) {
				$activateUsername = $application->admin->name;
			}
		}


//		$worksheet->getCell('N' . $row)->setValue($application->activate_date);
		$worksheet->getCell('O' . $row)->setValue($activateTime);
		$worksheet->getCell('P' . $row)->setValue($activateUsername);


		$this->activeReportCurrentRow++;
	}

	/**
	 * @param Worksheet $worksheet
	 * @param array $appData
	 * @param int $row
	 * @param Coefficient $koeff
	 */
	private function addActiveReportRow ($worksheet, $appData, $row, $koeff)
	{
		$isFast = intval($appData['is_fast']);

		$worksheet->getCell('A' . $row)->setValue($row - 1);
		$worksheet->getCell('B' . $row)->setValue(Application::idToFormattedId($appData['id'], $appData['created_at'], $isFast));
		$worksheet->getCell('C' . $row)->setValue($appData['dates']['issue_place']['title'] ?? null);
		$worksheet->getCell('D' . $row)->setValue($appData['start_date']);
		$worksheet->getCell('E' . $row)->setValue($appData['finish_date']);

		$name = $isFast ? $appData['fast']['name'] : $appData['user']['name'];
		$worksheet->getCell('F' . $row)->setValue($name);

		$inn = $isFast ? $appData['fast']['inn'] : $appData['user']['inn'];
		$worksheet->getCell('G' . $row)->setValue($inn);

		$worksheet->getCell('H' . $row)->setValue($appData['vehicle']['real_number']);
		$worksheet->getCell('I' . $row)->setValue($appData['real_price']);

		$toReviewEvent = $appData['events'][Events::APPLICANT_APP_TOREVIEW] ?? null;
		$toReviewTime = $toReviewEvent && $toReviewEvent['created_at'] ? $toReviewEvent['created_at'] : '';
		$worksheet->getCell('J' . $row)->setValue($toReviewTime);

		$worksheet->getCell('K' . $row)->setValue($this->getAppAgreements($appData));

		$acceptEvent = $appData['events'][Events::ADMINGBU_APP_ACCEPT] ?? null;
		$acceptTime = $acceptEvent ? $acceptEvent['created_at'] : '';
		$acceptUsername = $acceptEvent ? $acceptEvent['user_name'] : '';

		$worksheet->getCell('L' . $row)->setValue($acceptTime);
		$worksheet->getCell('M' . $row)->setValue($acceptUsername);

		$activateEvent = $appData['events'][Events::ADMINGBU_APP_ACTIVATE] ?? null;
		$activateTime = $activateEvent ? $activateEvent['created_at'] : '';
		$activateUsername =$activateEvent ? $activateEvent['user_name'] : '';

		$worksheet->getCell('N' . $row)->setValue($activateTime);
		$worksheet->getCell('O' . $row)->setValue($activateUsername);

		$damage = 0;
		$blankPrice = 0;
		if($appData['is_spring']) {
			$blankPrice = $koeff->spring_value;
		}else{
			$blankPrice = $koeff->value;
		}
		$damage = $appData['real_price'] - $blankPrice;
		$damage = max(0, $damage);
		$worksheet->getCell('P' . $row)->setValue($damage);
	}

	/**
	 * @param Application $application
	 * @return string
	 */
	private function getAppAgreementsOptimized($application)
	{
		$result = "";

		if(!empty($application->application_agreements)) {
			foreach($application->application_agreements as $agreement) {
				if($agreement->status !== ApplicationAgreement::STATUS_ACCEPTED) {
					continue;
				}

				$acceptEvent = null;
				foreach($agreement->audits_active as $audit) {
					if($audit->event_key === Events::DEPAGENT_APP_ACCEPT_KEY) {
						$acceptEvent = $audit;
					}
				}

				$username = '';
				if($acceptEvent && $acceptEvent->user) {
					$username = $acceptEvent->user->name;
				}
				if(!$username) {
					if($agreement->user) {
						$username = $agreement->user->name;
					}
				}
				$agreementRow =
					(isset($agreement->department) ? $agreement->department->title : '') . '/' .
					(isset($agreement->created_at) ? $agreement->created_at->toDateTimeString() : '') . '/' .
					($acceptEvent ? $acceptEvent->created_at : $agreement->accept_date) . '/' .
					$username;

				if($result) {
					$result .= ", ";
				}
				$result .= $agreementRow;
				//$result[] = implode('/', $data);
			}
		}
//		if($result) {
//			return implode(", ", $result);
//		}

		return $result;
	}

	/**
	 * @param array $appData
	 * @return string
	 */
	private function getAppAgreements($appData)
	{
		$result = [];
		if(!empty($appData['application_agreements'])) {
			foreach($appData['application_agreements'] as $applicationAgreement) {
				//only accepted agreements
				if($applicationAgreement['status'] !== ApplicationAgreement::STATUS_ACCEPTED) {
					continue;
				}
				$agrData = [];
				$agrData[] = $applicationAgreement['department']['title'] ?? '';
				$agrData[] = $applicationAgreement['created_at'] ?? '';

				$event = $applicationAgreement['events'][Events::DEPAGENT_APP_ACCEPT] ?? null;
				if($event) {
					$agrData[] = $event ? $event['created_at'] : '';
					$agrData[] = $event ? $event['user_name'] : '';
				}

				$result[] = implode('/', $agrData);
			}
		}

		return implode(", ", $result);
	}

	/**
	 * Generates save path and url for logs file
	 *
	 * @return array
	 */
	private function generateActiveReportPath()
	{
		$path = [
			'active-report'
		];

		$filename = 'audit_' . time() . '.xlsx';
		$url = env('APP_URL') . '/storage/' . implode('/', $path) . '/' . $filename;


		$dirPath = 'public/' . implode('/', $path);
		\Storage::makeDirectory($dirPath);
		$path = storage_path('app/' . $dirPath . '/' . $filename);

		return [
			'path' => $path,
			'url' => $url
		];
	}

	/**
	 * Generate invoice with application data by template
	 * @param Application $application
	 * @return string url
	 */
	public function generateInvoice($application)
	{
		//load template
		$spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load(storage_path($this->invoiceTemplatePath));
		
		//set invoice data
		$this->fillInvoiceData($spreadsheet, $application);
		
		//save template, return link
		$writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
		$pathInfo = $this->generateInvoicePath($application);
		$writer->save($pathInfo['path']);
		
		return $pathInfo['url'];
	}
	
	/**
	 * Spread
	 * @param Spreadsheet $spreadsheet
	 * @param Application $application
	 */
	private function fillInvoiceData($spreadsheet, $application)
	{
		$formatHelper = new FormatHelper();
		
		$worksheet = $spreadsheet->getActiveSheet();

		$name = $application->getUserName();
		$address = $application->getUserAddress();
		$inn = $application->getUserInn();
		
		//Название счета
		$worksheet->getCell('A20')->setValue('Счет № ' . $application->getFormattedId() . ' от ' . date('d.m.Y'));
		
		//Заказчик, наименование
		$worksheet->getCell('F21')->setValue($name);
		
		//Плательщик полная информация: Инн, наименование, адрес
		$worksheet->getCell('F22')->setValue(
			'Инн ' . $inn .
			', ' . $name,
			', адрес: ' . $address
		);
		
		//Суммы
		$price = $application->getRealPrice();
		$gpPrice = $this->getGpPrice();
		$price -= $gpPrice;
		
		$priceFormatted = $formatHelper->priceNumberFormat($price);
		
		$worksheet->getCell('Y27')->setValue($priceFormatted);
		$worksheet->getCell('AI27')->setValue($priceFormatted);
		$worksheet->getCell('AI28')->setValue($priceFormatted);
		$worksheet->getCell('AI30')->setValue($priceFormatted);
		
		//Сумма прописью
		$priceWordFormatted = $formatHelper->priceWordsFormat($price);
		$worksheet->getCell('A33')->setValue($priceWordFormatted);
	}
	
	/**
	 * @return float|int
	 */
	private function getGpPrice()
	{
		/** @var Coefficient $gpCoeff */
		$gpCoeff = Coefficient::find(Coefficient::ID_GP);
		return $gpCoeff->value ?? 0;
	}
	
	/**
	 * Generates save path and url for file
	 * @param Application $application
	 * @return array
	 */
	private function generateInvoicePath($application)
	{
		$path = [
			'user',
			$application->user_id,
			'application',
			$application->id,
		];
		
		$filename = 'invoice_' . time() . '.xlsx';
		$url = env('APP_URL') . '/storage/' . implode('/', $path) . '/' . $filename;
		
		
		$dirPath = 'public/' . implode('/', $path);
		//print_r($dirPath);die;
		\Storage::makeDirectory($dirPath);
		$path = storage_path('app/' . $dirPath . '/' . $filename);
		
		return [
			'path' => $path,
			'url' => $url
		];
	}

	/**
	 * Generate pay sheet file
	 * @param Application $application
	 * @return string
	 */
	public function generatePaySheet($application) {
		//load template
		$spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load(storage_path($this->paySheetTemplatePath));

		//set invoice data
		$this->fillPaySheetData($spreadsheet, $application);

		//save template, return link
		$writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
		$pathInfo = $this->generatePaySheetPath($application);
		$writer->save($pathInfo['path']);

		return $pathInfo['url'];
	}

	/**
	 * @param $spreadsheet
	 * @param Application $application
	 */
	private function fillPaySheetData($spreadsheet, $application){
		$worksheet = $spreadsheet->getActiveSheet();

		/** @var User $user */
		$user = $application->user;

		//Название счета
		$worksheet->getCell('A20')->setValue('Счет № ' . $application->getFormattedId() . ' от ' . date('d.m.Y'));
	}


	/**
	 * Generates save path and url for pay sheet file
	 * @param Application $application
	 * @return array
	 */
	private function generatePaySheetPath($application)
	{
		$path = [
			'user',
			$application->user_id,
			'application',
			$application->id,
		];

		$filename = 'paysheet_' . time() . '.xlsx';
		$url = env('APP_URL') . '/storage/' . implode('/', $path) . '/' . $filename;


		$dirPath = 'public/' . implode('/', $path);
		\Storage::makeDirectory($dirPath);
		$path = storage_path('app/' . $dirPath . '/' . $filename);

		return [
			'path' => $path,
			'url' => $url
		];
	}
}