<?php


namespace App\Helpers;


use phpDocumentor\Reflection\Types\Self_;

class Eputs
{
    /**
     * Class Eputs
     * @property int $code
     * @property bool $success
     * @property array $errors
     * @property array|object $data
     */
    const CODE_SCCESS = 200,
        CODE_BAD_REQUEST = 400,
        CODE_ERRORS = 500;

    public $code = self::CODE_SCCESS;
    public $success = true;
    public $errors = [];
    public $data = [];

    public function setModelResponse($object){
        $this->data = $object;
        return $this;
    }

    public function setError($message, $code = self::CODE_ERRORS){
        $this->code = $code;
        $this->success = false;
        if(is_array($message))
            $this->errors = $message;
        else
            $this->errors['message'] = $message;
        return $this;
    }


}