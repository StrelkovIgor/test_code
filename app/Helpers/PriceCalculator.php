<?php

namespace App\Helpers;

use App\Models\Application;
use App\Models\ApplicationDate;
use App\Models\ApplicationLoad;
use App\Models\ApplicationRoute;
use App\Models\ApplicationTrailer;
use App\Models\AxleLoad;
use App\Models\Coefficient;
use App\Models\PrivilegeStatus;

class PriceCalculator
{
	const GROUP_MAX_DISTANCE = 2.5;

	const PRICE_COEFF = 1.92;

	const ALLOWED_DEFAULT = 9;

	private static $singleWeightAllowed = [
		2 => 18,
		3 => 25,
		4 => 32,
		5 => 38,
	];
	private static $trailersWeightAllowed = [
		3 => 28,
		4 => 36,
		5 => 40,
		6 => 44,
	];//Безымянный коэффициент в формуле

	/** @var Application */
	private $application = null;

	/** @var  Coefficient[] */
	private $coefficients;

	/** @var  AxleLoad[] */
	private $axleLoads;

	/** @var  ApplicationLoad */
	private $application_load;

	/** @var  ApplicationRoute */
	private $application_route;

	/** @var  ApplicationDate */
	private $application_dates;

	/** @var  ApplicationTrailer[] */
	private $application_trailers;

	/** @var  array calculating info */
	private $info = ['legs' => []];

	/** @var array Override some application values: distance */
	private $overrides = [];

	/** @var AxleGroup[] */
	private $axlesGroups = [];

	/** @var int  */
	private $isSpring = 0;

    private $defaultRoadType = ApplicationRoute::DEFAULT_ROAD_TYPE;

    /** @var array Информация о цене по дистанции и коэффициентов */
    private $priceInfo = [];

	/**
	 * PriceCalculator constructor.
	 * @param Application $application
	 */
	public function __construct($application)
	{
		$this->application = $application;

		$this->loadCoefficients();
		$this->loadData();
		$this->loadAxleLoads();

		return $this->canCalculate();
	}

	/**
	 *
	 */
	private function loadCoefficients()
	{
		//$this->coefficients = Coefficient::all()->getDictionary();
        $roadTypes = ApplicationRoute::getRoadTypes();
        $this->coefficients = [];
        foreach($roadTypes as $roadType) {
            $this->coefficients[$roadType] = [];
        }

        $coefficients = Coefficient::all();
        foreach($coefficients as $coefficient) {
            if(!isset($this->coefficients[$coefficient->road_type])) {
                $this->coefficients[$coefficient->road_type] = [];
            }
            $this->coefficients[$coefficient->road_type][$coefficient->key] = $coefficient;
        }
	}

	private function loadAxleLoads()
	{
		$this->axleLoads = AxleLoad::all();
	}

	/**
	 * Load app resources
	 */
	private function loadData()
	{
		/** @var ApplicationLoad $applicationLoad */
		$this->application_load = $this->application->application_load ?? null;

		/** @var ApplicationRoute $applicationRoute */
		$this->application_route = $this->application->application_route ?? null;

		/** @var ApplicationDate $applicationDate */
		$this->application_dates = $this->application->application_dates ?? null;

		/** @var ApplicationDate $applicationDate */
		$this->application_trailers = $this->application->application_trailers ?? [];
	}

	/**
	 * Check if we can calculate app price
	 * @return bool
	 */
	private function canCalculate()
	{
		if(!$this->application){
			return false;
		}
		return
			$this->application->privilege_status_id ||
			(
				$this->application_load &&
				$this->application_route &&
				$this->application_dates
			);
	}

	public function setOverride($key, $value){
		$this->overrides[$key] = $value;
	}

	/**
	 * @return array
	 */
	public function getLastCalculationInfo()
	{
		return $this->info;
	}

	/**
	 * @return float|int
	 */
	public function calculatePrice($isSpring = 0)
	{
		if (!$this->canCalculate()) {
			return 0;
		}

		$this->isSpring = $isSpring;

		$price = 0;

		$Gp = $this->getCoefficient(Coefficient::ID_GP, $this->defaultRoadType);
		$Pb = $this->getCoefficient(Coefficient::ID_PB, $this->defaultRoadType);

		//check status
		$privilegeStatus = $this->application->privilege_status_id ? PrivilegeStatus::find($this->application->privilege_status_id) : null;

		$this->info['privilegeStatus'] = $privilegeStatus ? $privilegeStatus->id : null;
		if ($privilegeStatus && $privilegeStatus->isActive()) {
			$this->info['price'] = $Gp + $Pb;
            $this->priceInfo['duty'] = $Gp;
            $this->priceInfo['form'] = $Pb;
			return $Gp + $Pb;
		}

		//App data
		try {
			$n = $this->application_dates->runs_count;
            $distanceByType = [
                ApplicationRoute::ROAD_TYPE_REGIONAL => 0,
                ApplicationRoute::ROAD_TYPE_FEDERAL => 0,
                ApplicationRoute::ROAD_TYPE_LOCAL => 0
            ];
            $tracks = null;
//            if($this->application_route->tracks)
//                $tracks = \GuzzleHttp\json_decode($this->application_route->tracks, true);
//			$legs = $this->overrides['legs'] ?? $tracks ?? \GuzzleHttp\json_decode($this->application_route->legs, true);
			$legs = $this->overrides['legs'] ?? \GuzzleHttp\json_decode($this->application_route->legs, true);
			if (!$this->application->isSmev()){
                $price =  + $Pb + $Gp;
                $this->priceInfo['duty'] = $Gp;
                $this->priceInfo['form'] = $Pb;
            }
            $this->priceInfo['distance'] = [];
			if (!empty($legs)) {
                $distanceByType = self::splitLegsToTypes($legs);

                foreach(ApplicationRoute::getRoadTypes() as $roadType) {
                    $distance = ApplicationRoute::getFullRouteDistance($distanceByType[$roadType], $this->application_route->type_id);
                    $distanceByType[$roadType] = $distance;
                }
            } else {
                $roadType = ApplicationRoute::ROAD_TYPE_REGIONAL;
                $startFinishDistance = $this->overrides['distance'] ?? $this->application_route->distance;
                $fullRouteDistance = ApplicationRoute::getFullRouteDistance($startFinishDistance, $this->application_route->type_id);
                $distanceByType[$roadType] = $fullRouteDistance;
            }

			$priceInfoKey = $isSpring ? 'spring_price' : 'price';
            foreach($distanceByType as $roadType => $distance) {
                $distancePrice = $this->calculateLegPrice($roadType, $distance);
                $this->priceInfo[$priceInfoKey][$roadType] = $distancePrice;
                $this->priceInfo['distance'][$roadType] = $distance;
                $price += $distancePrice;
            }

		} catch (\Exception $e) {
			$price = 0;
			print_r($e->getTraceAsString());
			die;
		}

		$price = round($price, 2);

		if($this->application_route)  $this->application_route->setDistanceInfo($this->priceInfo['distance']);

        $this->info = array_merge($this->info, [
            'price' => $price,
            'priceInfo' => $this->priceInfo
        ]);

		return $price;
	}

	public static function splitLegsToTypes($legs) {
        $distanceByType = [
            ApplicationRoute::ROAD_TYPE_REGIONAL => 0,
            ApplicationRoute::ROAD_TYPE_FEDERAL => 0,
            ApplicationRoute::ROAD_TYPE_LOCAL => 0
        ];

        if (!empty($legs)) {
            //calculate distances by road type
            foreach($legs as $leg) {
                $roadType = array_get($leg, 't');
                if ($roadType !== null) {
                    $distance = floatval(array_get($leg, 'd',0));
                    $distanceByType[$roadType] += $distance / 1000;
                }else{
                    foreach ($leg as $newLeg){
                        $roadType = array_get($newLeg, 't');
                        if ($roadType !== null) {
                            $distance = floatval(array_get($newLeg, 'd',0));
                            $distanceByType[$roadType] += $distance / 1000;
                        }
                    }
                }
            }
        }

        return $distanceByType;
    }

    /**
     * @param int $roadType
     * @param float $distance kilometers
     *
     * @return float
     */
	private function calculateLegPrice($roadType, $distance) {
	    if (!$distance) {
	        return 0;
        }

        $Gp = $this->getCoefficient(Coefficient::ID_GP, $roadType);
        $Pb = $this->getCoefficient(Coefficient::ID_PB, $roadType);

        $Tpg = $this->getCoefficient(Coefficient::ID_TPG, $roadType);
        $Itg = $this->getCoefficient(Coefficient::ID_ITG, $roadType);
        $Ttg = $Tpg * $Itg;

        $n = $this->application_dates->runs_count;
        $S = $distance / 100;

        //tmp data
        $Rpm = $this->calculateWeightScathe($roadType);
        $Rpoms = $this->calculateAxlesScathes($roadType);
        $RpomsSum = array_sum($Rpoms);
        $price = floatval($n * ($Rpm + $RpomsSum) * $S * $Ttg);

        $debugInfo =  [
            'price' => $price,
            'n' => $n,
            'Rpm' => $Rpm,
            'RpomsSum' => $RpomsSum,
            'S' => $S,
            'Ttg' => $Ttg,
            'Pb' => $Pb,
            'Gp' => $Gp,
            'Rpoms' => $Rpoms
        ];
        $this->info['legs'][] = $debugInfo;

        return $price;
    }

	private function getCoefficient($id, $roadType, $default = 1)
	{
		/** @var Coefficient $coeff */
        $coeff = isset($this->coefficients[$roadType][$id]) ? $this->coefficients[$roadType][$id] : null;
		//$coeff = array_get($this->coefficients, $id, null);
		if (!empty($coeff)) {
			return $this->isSpring ? $coeff->spring_value : $coeff->value;
		}
		return $default;
	}

    /**
     * @param $roadType
     * @return float|int
     */
	private function calculateWeightScathe($roadType)
	{
		$Kkaprem = $this->getCoefficient(Coefficient::ID_KAP_REM, $roadType);
		$Kpm = $this->getCoefficient(Coefficient::ID_KPM, $roadType);
		$c = $this->getCoefficient(Coefficient::ID_C, $roadType);
		$d = $this->getCoefficient(Coefficient::ID_D, $roadType);
        $Klow = $this->getCoefficient(Coefficient::ID_KLOW, $roadType);


		$Ppm = $this->getPpm();
		$RpmParams = [
			'Kkaprem' => $Kkaprem,
			'Kpm' => $Kpm,
			'c' => $c,
			'd' => $d,
			'Ppm' => $Ppm
		];
		$this->info['RpmParams'] = $RpmParams;

		if ($Ppm == 0) {
			return 0;
		}

		$rpm = $Kkaprem * $Kpm * ($c + $d * $Ppm);

        $RpmParams['rpm'] = $rpm;

        if($Ppm >= 2 && $Ppm <=15){
            $rpm *= $Klow;
            $RpmParams['rpm_klow'] = $rpm;
        }

        $this->info['RpmParams'] = $RpmParams;

        return round($rpm);
	}

	private function getPpm()
	{
		/** @var ApplicationLoad $applicationLoad */
		$applicationLoad = $this->application_load;

		$maxAllowed = $this->getMassAllowed();
		$fact = $applicationLoad->weight;

		$diffPercent = $this->getOverLimitPercent($fact, $maxAllowed);
		$roundedPercent = $this->roundPpmPercent($diffPercent);

		$this->info['PpmParams'] = [
			'fact' => $fact,
			'allowed' => $maxAllowed,
			'diffPercent' => $diffPercent,
			'roundedPercent' => $roundedPercent
		];

		return $roundedPercent;
	}

	/**
	 * @return mixed
	 */
	private function getMassAllowed()
	{
		$axlesCount = $this->application_load->axles_count;
		$trailers = $this->application_trailers;
		$trailersCount = count($trailers);
		$this->info['trailers_count'] = $trailersCount;
		if ($trailersCount == 0) {
			$max = 5;
			$min = 2;

			$axlesCount = max($min, min($axlesCount, $max));
			return self::$singleWeightAllowed[$axlesCount];
		}

		$max = 6;
		$min = 3;

		$axlesCount = max($min, min($axlesCount, $max));
		return self::$trailersWeightAllowed[$axlesCount];
	}

	/**
	 * @param $fact
	 * @param $std
	 * @return integer
	 */
	private function getOverLimitPercent($fact, $std)
	{
		if($std == 0){
			return 0;
		}
		$factPerc = ($fact - $std) / $std * 100;
		$diffPercent = max(0, $factPerc);
        $diffPercent = $diffPercent > 2 ?$diffPercent:0;
		return $this->floor($diffPercent);
	}

	private function floor($val){
        preg_match('/([0-9]+)(\.[0-9]+)?/', strval($val), $preg);
        return array_get($preg, 1, null);
    }

	private function roundPpmPercent($percent)
	{
//		if ($percent > 60) {
//			return $percent;
//		}
//		return 10 * ceil($percent / 10);
        return intval($percent);
	}

	/**
     * @param int $roadType
     *
	 * @return array
	 */
	private function calculateAxlesScathes($roadType)
	{
		/** @var ApplicationLoad $applicationLoad */
		$applicationLoad = $this->application_load;

		$result = [];

		$axlesInfo = \GuzzleHttp\json_decode($applicationLoad->axles_info, true);
		if (!empty($axlesInfo)) {
			//split axles to groups
			$this->axlesGroups = AxleGroup::splitAxles($axlesInfo);
			$this->info['axleGroups'] = $this->axlesGroups;

			$index = 0;
			foreach ($axlesInfo as $axleInfo) {
				$result[] = $this->calculateAxleScathe($axleInfo, $index, $roadType);
				$index++;
			}
		}
		return $result;
	}

	/**
	 * Split axles to groups
	 * @param $axlesInfo
	 * @return AxleGroup[]
	 */
	private function splitAxles ($axlesInfo)
	{
		$distances = [];
		foreach($axlesInfo as $axleInfo) {
			$distances[] = $axleInfo['distance'];
		}

		/** @var AxleGroup[] $groups */
		$groups = [];
		$currentGroup = 0;

		$groups[0] = new AxleGroup();
		$groups[0]->setFirstIndex(0);
		$groups[0]->addAxle($axlesInfo[0]);
		for ($i = 0; $i < count($distances) - 1; $i++) {
			//if distance > max distance - start new group
			if ($distances[$i] > self::GROUP_MAX_DISTANCE) {
				$currentGroup++;
				$groups[$currentGroup] = new AxleGroup();
				$groups[$currentGroup]->setFirstIndex($i + 1);
			}

			//add next axle to group
			$groups[$currentGroup]->addAxle($axlesInfo[$i + 1]);
		}

		return $groups;
	}

	/**
	 * Fix pos value by axles group params
	 * @param float $pos value before fix
	 * @param int $index
	 * @return float
	 */
	private function fixByGroup($pos, $index) {
//		$axleGroup = $this->findGroup($index);
//		if(!$axleGroup) {
//			return $pos;
//		}
//
//		//if no overweight by group
//		if($axleGroup && !$axleGroup->hasOverweight()) {
//			$pos = 0;
//		}

		//

		return $pos;
	}

	private function fixAllowedLoadByGroup ($allowedLoad, $index, $roadType, $isSpring)
	{
		$axleGroup = $this->findGroup($index);
		if(!$axleGroup) {
			return $allowedLoad;
		}

		//if has overweight by group

		if(
			!$axleGroup->hasOverweight($roadType, $isSpring) &&
			(
				$axleGroup->getAxlesCount() === 2 ||
				$axleGroup->getAxlesCount() === 3
			) &&
			$axleGroup->getWheelCount() < AxleLoad::WHEELS_MORE_4
		) {
			//find by axleLoad
			$allowedLoad = $this->findSingleAxleAllowedLoad($roadType);
			//$allowedLoad = $axleGroup->getAllowedLoadForSingleAxle();
		}

/*
        if($axleGroup->hasOverweight()){
            var_dump("Перегруза есть");
            $allowedLoad = 1;
        }else{
            var_dump("Перегруза нету");
            if((
                    $axleGroup->getAxlesCount() === 2 ||
                    $axleGroup->getAxlesCount() === 3
                ) &&
                $axleGroup->getWheelCount() < AxleLoad::WHEELS_MORE_4
            ){
                $allowedLoad = $this->findSingleAxleAllowedLoad($roadType);
            }
        }
*/
		return $allowedLoad;
	}

	private function findSingleAxleAllowedLoad($roadType){
		$test = [];
		$load = AxleLoad::findByOptions(
			$this->axleLoads,
			1,
			AxleLoad::DISTANCE_INDEX_MORE_25,
			1,
			AxleLoad::WHEELS_LESS_4,
			AxleLoad::AXLE_TYPE_SPRING,
			$this->isSpring,
			$test
		);

		return $load ? $load->getValue($roadType) : self::ALLOWED_DEFAULT;
	}

	/**
	 * @param int $index
	 * @return AxleGroup|null
	 */
	private function findGroup($index)
	{
		for($i = 0; $i < count($this->axlesGroups); $i++) {
			$firstIndex = $this->axlesGroups[$i]->getFirstIndex();
			$lastIndex = $firstIndex + $this->axlesGroups[$i]->getAxlesCount();

			if($index >= $firstIndex && $index < $lastIndex){
				return $this->axlesGroups[$i];
			}
		}

		return null;
	}

	/**
	 * @param array $axleInfo
	 * @param int $index - axle's order index
	 * @param int $roadType - road type
	 * @return float|int
	 */
	private function calculateAxleScathe($axleInfo, $index, $roadType)
	{
		//coefficients
		$Kdkz = $this->getCoefficient(Coefficient::ID_KDKZ, $roadType);
		$Kkaprem = $this->getCoefficient(Coefficient::ID_KAP_REM, $roadType);
		$Ksez = $this->getCoefficient(Coefficient::ID_KSEZ, $roadType);
		$Rish = $this->getCoefficient(Coefficient::ID_RISH, $roadType);
		$a = $this->getCoefficient(Coefficient::ID_A, $roadType);
		$b = $this->getCoefficient(Coefficient::ID_B, $roadType);
		$H = $this->getCoefficient(Coefficient::ID_H, $roadType);

		//computed
		$factLoad = floatval($axleInfo['axle_load']);

		//$allowedLoad = floatval($axleInfo['permissible_load']);
		//changed - calculate permissible load by admin panel values
		$axleGroup = $this->findGroup($index);
		$allowedLoad = $axleGroup->calculatePermissibleLoad($this->axleLoads, $this->isSpring, $roadType);


		//Fix permissible load by group
		$allowedLoad = $this->fixAllowedLoadByGroup($allowedLoad, $index, $roadType, $this->isSpring);

		//@todo add road type
		$percent = $this->getOverLimitPercent($factLoad, $allowedLoad);
//        $pos = $this->calculatePos($percent, $roadType);
        $pos = $this->calculatePosH($percent, $H);


		//if group has no overweight - set pos = 0
		//$pos = $this->fixByGroup($pos, $index);

		$RpomsInfo = $this->info['RpomParams'] ?? [];
		$newRpom = [
			'Kdkz' => $Kdkz,
			'Kkaprem' => $Kkaprem,
			'Ksez' => $Ksez,
			'Rish' => $Rish,
			'a' => $a,
			'b' => $b,
			'H' => $H,

			'factLoad' => $factLoad,
			'allowedLoad' => $allowedLoad,
			'percent' => $percent,
			'Pos' => $pos,
		];
		$RpomsInfo[] = $newRpom;
		$this->info['RpomParams'][] = $newRpom;

		if ($pos == 0) {
			return 0;
		}

		$Rpom = $Kdkz * $Kkaprem * $Ksez * $Rish *
			(1 + 0.2 * pow($pos, 1.92) *
				($a / $H - $b));

		return max(0, $Rpom);
	}

	public function getPriceInfo () {
	    return $this->priceInfo;
    }

	private function roundPosPercent($percent)
	{
		$percent = floatval($percent);
		if ($percent > 60) {
			return $percent / 10;
		}
		return ceil($percent / 10);
	}

    private function calculatePos($percent, $roadType) {
        $pos = $this->roundPosPercentNew($percent, $roadType);
        $pos = max(0, $pos);
        return $pos;
    }

    private function calculatePosH($percent, $H){
        return max(0, $H * $percent / 100);
    }

    /**
     * @param float $percent
     * @param int $roadType
     */
    private function roundPosPercentNew($percent, $roadType)
    {
        $loadOfRoad = ApplicationRoute::ROAD_TO_LOAD[$roadType];
        return $loadOfRoad * $percent / 100;
    }
}