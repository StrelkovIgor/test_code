<?php
/**
 * Created by PhpStorm.
 * User: gserg13
 * Date: 09.07.2019
 * Time: 13:57
 */

namespace App\Helpers;

use App\Models\Application;
use App\Models\Audit\Enum\Events;

class DataFormatter {

	/**
	 * @param array $appsData
	 *
	 * @return array
	 */
	public static function applicationsToActiveExport($applicationsData)
	{
		$result = [];
		foreach($applicationsData as $appData) {
			$result[] = self::appDataToActiveReportData($appData);
		}
		return $result;
	}

	/**
	 * @param array $appData
	 * @return array
	 */
	private static function appDataToActiveReportData($appData)
	{
		$result = [];
		$isFast = intval($appData['is_fast']);
		$isSmev = intval($appData['is_smev']);

		$result['formatted_id'] = Application::idToFormattedId($appData['id'], $appData['created_at'], $isFast, $isSmev);
		$result['issue_place'] = $appData['dates']['issue_place']['title'] ?? null;
		$result['start_date'] = $appData['start_date'];

		$name = $isFast ? $appData['fast']['name'] : $appData['user']['name'];
		$result['name'] = $name;

		$inn = $isFast ? $appData['fast']['inn'] : $appData['user']['inn'];
		$result['inn'] = $inn;

		$result['real_number'] = $appData['vehicle']['real_number'];
		$result['price'] = $appData['price'];

		$toReviewEvent = $appData['events'][Events::APPLICANT_APP_TOREVIEW] ?? null;
		$toReviewTime = $toReviewEvent ? $toReviewEvent['created_at'] : '';
		/*
		$worksheet->getCell('J' . $row)->setValue($toReviewTime);

		$worksheet->getCell('K' . $row)->setValue($this->getAppAgreements($appData));//@todo

		$acceptEvent = $appData['events'][Events::ADMINGBU_APP_ACCEPT] ?? null;
		$acceptTime = $acceptEvent ? $acceptEvent['created_at'] : '';
		$acceptUsername = $acceptEvent ? $acceptEvent['user_name'] : '';

		$worksheet->getCell('L' . $row)->setValue($acceptTime);
		$worksheet->getCell('M' . $row)->setValue($acceptUsername);

		$activateEvent = $appData['events'][Events::ADMINGBU_APP_ACTIVATE] ?? null;
		$activateTime = $activateEvent ? $activateEvent['created_at'] : '';
		$activateUsername =$activateEvent ? $activateEvent['user_name'] : '';

		$worksheet->getCell('N' . $row)->setValue($activateTime);
		$worksheet->getCell('O' . $row)->setValue($activateUsername);
		*/
		return $result;
	}
}