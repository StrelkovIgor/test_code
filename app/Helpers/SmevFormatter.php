<?php
/**
 * Created by PhpStorm.
 * User: gserg13
 * Date: 04.06.2018
 * Time: 11:57
 */

namespace App\Helpers;

use App\Models\Application;
use App\Models\ApplicationDate;
use App\Models\ApplicationLoad;
use App\Models\ApplicationRoute;
use App\Models\ApplicationSmev;
use App\Models\ApplicationTrailer;
use App\Models\AxleLoad;
use App\Models\Vehicle;
use Illuminate\Support\Facades\DB;


class SmevFormatter {
	const LOCATION_LOCAL = 'International',
		LOCATION_REGIONAL = 'Interregional',
		LOCATION_INTERNATIONAL = 'Local';
	
	const MARK_WHEEL_COUNT = "\\d",
        MARK_AXLE_TYPE = "\\p",
        MARK_WHEELS = "\\e";
	
	private static $axleLoadRules = [
	    'wheel_count' => [
	        'symbol' => "\\d",
            'default_value' => 1,
            'custom_value' => 2
        ],
        'wheels' => [
            'symbol' => "\\e",
            'default_value' => 2,
            'custom_value' => 8
        ],
        'type_id' => [
            'symbol' => "\\p",
            'default_value' => AxleLoad::AXLE_TYPE_SPRING,
            'custom_value' => AxleLoad::AXLE_TYPE_NON_SPRING
        ],
    ];
	
	/**
	 * @param array $messageData
	 * @return bool
	 */
	public static function createSmevApplication($messageData)
	{
		$data = self::parseRegisterData($messageData);
		
		if(!isset($data['load_files'])) {
			$data['load_files'] = [];
		}
		if(!isset($data['markers'])) {
			$data['markers'] = [];
		}
		
		$application = null;
		$success = false;
		$admin = null;
		
		//@todo check if application by smev already exists
		
		DB::transaction(function() use ($data, $admin, &$success, &$application) {
			//create vehicle
			$vehicleData = array_get($data, 'vehicle');
			$vehicle = Vehicle::createSmev($vehicleData, $data, 0);
			if (!empty($vehicle)) {
				$data['vehicle'] = [
					'id' => $vehicle->id,
                    'is_smev' => 1
				];
			}

			//create trailers
			$trailers = [];
			$trailersData = array_get($data, 'trailers');
			
			if (!empty($trailersData)) {
				foreach ($trailersData as $trailerData) {
					if(!empty($trailerData['number']) && !empty($trailerData['title'])) {
                        $trailerData['pts_weight'] = array_get($data, 'trailers_pts_weight');
						$trailers[] = Vehicle::createSmev($trailerData, $data, 1);
					}
				}
			}

			//create app
			$application = Application::createByVehicles(null, $data, $admin);
			
			//bind  trailers to app
			$trailersData = array_get($data, 'trailers');
			if (!empty($trailersData)) {
				ApplicationTrailer::createFastApplicationBinds($application, $trailers);
			}
			
			//rename load_type_id to type_id to avoid  'type_id' field duplication
			$loadData = $data['load'];
			$load = ApplicationLoad::updateByData($application, $loadData);
   
            //save route
            $routeData = $data['route'];
            $route = ApplicationRoute::updateByData($application, $routeData);
        
			//save app dates
            $datesData = $data['dates'];
			$dates = ApplicationDate::updateByData($application, $datesData);
			$application->updateDates($data);
            
            $smev = ApplicationSmev::updateByData($application, $data);
            
			$application->save();
			
			$success = true;
		});
		
		return $success;
	}

    /**
     * @param $appData
     * @return bool
     */
    public static function sendFiles($appData) {
        $success = false;
        //@todo implement
        return $success;
    }

    /**
     * Check smev status by request data
     * @param array $appData
     * @return array
     */
	public static function checkApplication($appData) {
	    $result = [];
        $status = null;

	    $approvalId = array_get($appData, 'ApprovalID');
	    if ($approvalId) {
	        $application = Application::findByApprovalId($approvalId);
	        $status = $application->getSmevStatus();

	        if ($status) {
	            $result = self::getCheckResponseData($status, $application);
            }
        }

	    return $result;
    }

    /**
     * @param string $status
     * @param Application $application
     * @return array
     */
    private static function getCheckResponseData($status, $application) {
        $result = [
            'Status' => $status
        ];

	    switch($status) {
            case Application::SMEV_STATUS_WAITING_ATTACHMENTS:
                $result['PersonName'] = self::getCheckPersonName($application);
                $result['PersonJob'] = self::getCheckPersonJob($application);
                break;
            case Application::SMEV_STATUS_DECLINED:
                $result['PersonName'] = self::getCheckPersonName($application);
                $result['PersonJob'] = self::getCheckPersonJob($application);
                $result['Comment'] = self::getCheckComment($application);
                break;
            case Application::SMEV_STATUS_APPROVED:
                $result['PersonName'] = self::getCheckPersonName($application);
                $result['PersonJob'] = self::getCheckPersonJob($application);
                $result['OutputNumber'] = $application->getFormattedId();//@todo check
                $result['OutputDate'] = self::checkDateFormatToDateTime($application->getActivateDate('Y-m-d'), 'T00:00:00');
                $result['ApprovedPeriodFrom'] = self::checkDateFormatToDateTime($application->start_date, 'T00:00:00');
                $result['ApprovedPeriodTo'] = self::checkDateFormatToDateTime($application->finish_date, 'T23:59:59');
                $result['ApprovedRoute'] = $application->getRouteText();
                $result['Comment'] = self::getCheckComment($application);
        }

        return $result;
    }

    /**
     * @param Application $application
     * @return string
     */
    private static function getCheckPersonName($application) {
        return $application->getAdminName();
    }

    /**
     * @param Application $application
     * @return string
     */
    private static function getCheckPersonJob($application) {
        return $application->getAdminPosition();
    }

    /**
     * @param string $dateStr
     * @param string $postfix
     * @return string
     */
    private static function checkDateFormatToDateTime($dateStr, $postfix = '') {
        return $dateStr . $postfix;
    }

    /**
     * @param Application $application
     * @return string
     */
    private static function getCheckComment($application) {
        return $application->getComment();
    }

    /**
     * @param array $appData
     * @return bool
     * @throws \Exception
     */
    public static function cancelApplication($appData) {
        $success = false;
        $approvalId = array_get($appData, 'ApprovalID');

        if ($approvalId) {
            $application = Application::findByApprovalId($approvalId);
            if ($application) {
                $success = $application->delete();
            }
        }

        return $success;
    }

	/**
	 * @param array $appData
	 * @return array
	 */
	private static function parseRegisterData($appData)
	{
		$data = [
		    'is_smev' => 1,
			//main_application
			'username' => array_get($appData, 'TransporterInfo'),
			'start_date' => array_get($appData,'PeriodFrom'),
			'finish_date' => array_get($appData,'PeriodTo'),
			'location_type' => self::parseShippingType(array_get($appData, 'ShippingType')),
			
			//vehicles
			'vehicle' => [
				'owner_name' => array_get($appData, 'TransporterInfo'),
				'number' => self::parseNumberFromVehicleInfo(array_get($appData, 'TruckInfo')),
				'title' => self::parseTitleFromVehicleInfo(array_get($appData, 'TruckInfo')),
                'pts_weight' => (float) array_get($appData,'EmptyTruckWeight',0)
			],
			
			'trailers' => self::parseTrailerInfo(array_get($appData, 'TrailerInfo')),
            'trailers_pts_weight' => (float) array_get($appData,'EmptyTrailerWeight',0),
			
			//load
			'load' => [
				'name' => array_get($appData, 'FreightName'),
				'load_weight' => array_get($appData, 'FreightWeight'),
				'load_dimension' => array_get($appData, 'FreightDimension'),
				'length' => array_get($appData, 'TotalLength'),
				'width' => array_get($appData, 'TotalWidth'),
				'height' => array_get($appData, 'TotalHeight'),
				'weight' => array_get($appData, 'TotalWeight'),
				'radius' => array_get($appData, 'TurnRadius'),
				
				//axles
				'axles_count' => array_get($appData, 'AxlesCount'),
				'axles_info' => self::parseAxlesInfo($appData),
				
				'escort' => [
					'info' => array_get($appData, 'EscortInfo'),
					'speed' => array_get($appData, 'EstimatedSpeed'),
				]
			],
            
            //route
            'route' => [
                'text_route' => array_get($appData, 'RequestedRoute'),
                'type_id' => 4
            ],
			
			//dates
			'dates' => [
				'runs_count' => array_get($appData, 'TripCount')
			],
			
			'smev' => [
				'approval_id' => array_get($appData, 'ApprovalID'),
				'authority_name' => array_get($appData,'AuthorityName'),
				'output_number' => array_get($appData, 'OutputNumber'),
				'output_date' => array_get($appData, 'OutputDate'),
				
				//'freight_name' => array_get($appData, 'FreightName'),
				//'freight_dimension' => array_get($appData, 'FreightDimension'),
				//'freight_weight' => array_get($appData, 'FreightWeight'),
				
				//'route' => array_get($appData, 'RequestedRoute')
			]
		];
		
		return $data;
	}
	
	/**
     * Parse axles load info from smev
	 * @param array $appData
	 * @return array
	 */
	private static function parseAxlesInfo($appData)
	{
        $result = [];
        
	    $axlesCount = array_get($appData, 'AxlesCount', 0);
		if($axlesCount > 0) {
		    $axlesLoadsStr = array_get($appData, 'AxlesLoad');
		    $loadParts = explode('-', $axlesLoadsStr);
		    if(!empty($loadParts)) {
		        $axleInfo = [];
		        foreach($loadParts as $loadPart) {
                    //trim, replace , with .
		            $strVal = str_replace(',', '.', trim($loadPart));
              
		            $axleInfo = self::applyAxleRules($strVal, $axleInfo);
                    
                    $axleInfo['axle_load'] = floatval($strVal);
                    $axleInfo['distance'] = 0;
                    
                    $result[] = $axleInfo;
                }
            }
		    
		    $axlesIntervalsStr = array_get($appData, 'AxlesInterval');
            $intervalParts = explode('-', $axlesIntervalsStr);
		    if(!empty($intervalParts)) {
		        $i = 0;
		        foreach($intervalParts as $intervalPart) {
                    $strVal = str_replace(',', '.', trim($intervalPart));
                    $result[$i]['distance'] = floatval($strVal);
                    $i++;
                }
            }
        }
		
		return $result;
	}
    
    /**
     * Get wheel count, wheels, axle type info from axle load str
     * @param string $strVal
     * @param array $axleInfo
     * @return array
     */
	private static function applyAxleRules($strVal, $axleInfo)
    {
        foreach(self::$axleLoadRules as $key => $value) {
            $axleInfo[$key] = self::checkStrAxleMark($strVal, $value['symbol']) ? $value['custom_value'] : $value['default_value'];
        }
        
        return $axleInfo;
    }
    
    /**
     * Check mark exists in str. Remove mark from str after check
     * @param string $strVal
     * @param string $mark
     * @return bool
     */
	private static function checkStrAxleMark(&$strVal, $mark)
    {
        $result = false;
        if(stristr($strVal, $mark) !== false) {
            $result = true;
            //remove symbol
            $strVal = str_replace($mark, '', $strVal);
        }
        
        return $result;
    }
    
	/**
	 * @param $smevType
	 * @return int|null
	 */
	private static function parseShippingType ($smevType)
	{
		switch($smevType) {
			case self::LOCATION_LOCAL:
				return Application::LOCATION_LOCAL;
			case self::LOCATION_REGIONAL:
				return Application::LOCATION_REGIONAL;
			case self::LOCATION_INTERNATIONAL:
				return Application::LOCATION_INTERNATIONAL;
		}
		
		return null;
	}
	
	/**
	 * @param string $truckInfo
	 * @return string
	 */
	private static function parseTitleFromVehicleInfo($truckInfo)
	{
		$parts = explode('(', $truckInfo);
		return trim($parts[0]);
	}
	
	/**
	 * @param $truckInfo
	 * @return string
	 */
	private static function parseNumberFromVehicleInfo($truckInfo)
	{
		$parts = explode('(', $truckInfo);
		if(count($parts) <2) {
			return $parts[0];
		}
		
		$parts = explode(')', $parts[1]);
		return trim($parts[0]);
	}
	
	/**
	 * @param string $trailerInfo
	 * @return array
	 */
	private static function parseTrailerInfo ($trailerInfo)
	{
		$result = [];
		$parts = explode(',', $trailerInfo);
		
		foreach($parts as $part) {
			$result[] = [
				'owner_name' => '',
				'number' => self::parseNumberFromVehicleInfo($part),
				'title' => self::parseTitleFromVehicleInfo($part),
			];
		}
		
		return $result;
	}
}