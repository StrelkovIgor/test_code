<?php
/**
 * Created by PhpStorm.
 * User: Almaz G.
 * Date: 09.06.2018
 * Time: 17:57
 */

namespace App\Helpers;

use Illuminate\Http\UploadedFile;

class FileUploader
{
	public static $allowedExts = [
		'jpeg', 'jpg', 'png', 'pdf', 'xlsx', 'xls'
	];

	/**
	 * @param $fileSource
	 * @param array $params
	 * @return array
	 */
	public static function prepareUploadedFile($fileSource, array $params = [])
	{
        if($fileSource instanceof UploadedFile){
            return self::fileObject($fileSource, $params);
        }
		@list($type, $fileData) = explode(';', $fileSource);
		list(, $type) = explode(':', $type);
		$mimes = new \Mimey\MimeTypes;
		$extension = $mimes->getExtension($type);

		if(array_search($extension, self::$allowedExts) === false){
			return null;
		}

		$path = '';
		if (isset($params['path'])) {
			$path = implode('/', $params['path']) . '/';
		}
		$name = 'file_' . time() . "_" . rand(100, 10000) . '.' . $extension;
		$fileName = 'images/' . $path . $name;
		
		@list(, $fileData) = explode(',', $fileData);
		if ($fileData != "") {
			//Save to local storage
			\Storage::disk('public')->put($fileName, base64_decode($fileData));
		}
		
		return [
			'name' => isset($params['fileName']) ? $params['fileName'] : $name,
			'source' => $fileName,
			'url' => env('APP_URL') . '/storage/' . $fileName,
		];
	}

    public static function fileObject(UploadedFile $file, array $params = [])
    {
        $mimes = new \Mimey\MimeTypes;
        $extension = $mimes->getExtension($file->getMimeType());

        if(!$extension){
            $nameFile = $file->getClientOriginalName();
            $nameFile = explode('.',$nameFile);
            $extension = $nameFile[count($nameFile) - 1];
        }

        $params['fileName'] = $file->getClientOriginalName();

        $path = '';
        if (isset($params['path'])) {
            $path = implode('/', $params['path']) . '/';
        }

        $name = 'file_' . time() . "_" . rand(100, 10000) . '.' . $extension;
        $fileName = 'images/' . $path . $name;

        \Storage::disk('public')->put($fileName, file_get_contents($file->getRealPath()));

        return [
            'name' => isset($params['fileName']) ? $params['fileName'] : $name,
            'source' => $fileName,
            'url' => env('APP_URL') . '/storage/' . $fileName,
        ];
    }
}