<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RolesTableSeeder::class);
        //$this->call(LegalFormSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(CoefficientsTableSeeder::class);
        //$this->call(VehicleBrandsTableSeeder::class);
    }
}
