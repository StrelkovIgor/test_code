<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SpecialConditionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = Carbon::now();

        DB::table('special_conditions')->insert([
            [
                'id' => 1,
                'title' => 'Действует весеннее ограничение',
                'value' => 0,
                'created_at' => $now,
                'updated_at' => $now,
            ]
        ]);
    }
}
