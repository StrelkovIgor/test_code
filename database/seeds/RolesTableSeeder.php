<?php

use App\Models\Role;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            ['id' => Role::ROLE_ADMIN, 'name' => 'Администратор', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => Role::ROLE_OFFICER, 'name' => 'Сотрудник ГБУ БДД', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => Role::ROLE_INDIVIDUAL, 'name' => 'Заявитель (Физическое лицо)', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => Role::ROLE_FIRM, 'name' => 'Заявитель (Юридическое лицо)', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => Role::ROLE_FIRM_USER, 'name' => 'Сотрудник юр лица', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => Role::ROLE_DEPARTMENT_AGENT, 'name' => 'Сотрудник ведомства', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => Role::ROLE_WEIGHT_CONTROL, 'name' => 'Сотрудник ПВК', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
        ]);
    }
}
