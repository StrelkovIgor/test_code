<?php

use App\Models\Department;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DepartmentsTableSeeder extends Seeder
{
    const ID_SHIFT = 1000;
	/**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		//change ids of 1,2
		Department
			::withTrashed()
			->where('id', Department::DEPARTMENT_GIBDD)
			->update(['id' => Department::DEPARTMENT_GIBDD + self::ID_SHIFT]);
	
		Department
			::withTrashed()
			->where('id', Department::DEPARTMENT_GDS)
			->update(['id' => Department::DEPARTMENT_GDS + self::ID_SHIFT]);
    	
    	$now = Carbon::now();
		DB::table('departments')->insert([
			['id' => Department::DEPARTMENT_GIBDD, 'title' => 'Управление ГИБДД МВД по Республике Татарстан', 'short_title' => 'ГИБДД', 'created_at' => $now, 'updated_at' => $now],
			['id' => Department::DEPARTMENT_GDS, 'title' => 'Государственное казенное учреждение "Главтатдортранс" Республики Татарстан', 'short_title' => 'ГДС', 'created_at' => $now, 'updated_at' => $now],
		]);
    }
}
