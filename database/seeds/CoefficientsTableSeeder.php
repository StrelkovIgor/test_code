<?php

use App\Models\ApplicationRoute;
use App\Models\Coefficient;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CoefficientsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Coefficient::truncate();

        $now = Carbon::now();
        DB::table('coefficients')->insert([
            //Региональные
            ['key' => Coefficient::ID_GP, 'road_type' => ApplicationRoute::ROAD_TYPE_REGIONAL, 'value' => 1600, 'spring_value' => 1600, 'title' => 'Госпошлина (Gp)', 'short_title' => 'Gp',  'created_at' => $now, 'updated_at' => $now],
            ['key' => Coefficient::ID_PB, 'road_type' => ApplicationRoute::ROAD_TYPE_REGIONAL, 'value' => 20.05, 'spring_value' => 20.05, 'title' => 'Стоимость бланка (Pb)', 'short_title' => 'Pb',  'created_at' => $now, 'updated_at' => $now],
            ['key' => Coefficient::ID_TPG, 'road_type' => ApplicationRoute::ROAD_TYPE_REGIONAL, 'value' => 1, 'spring_value' => 1, 'title' => 'Базовый компенсационный индекс предыдущего года (Тпг)', 'short_title' => 'Тпг',  'created_at' => $now, 'updated_at' => $now],
            ['key' => Coefficient::ID_ITG, 'road_type' => ApplicationRoute::ROAD_TYPE_REGIONAL, 'value' => 1, 'spring_value' => 1, 'title' => 'Индекс-дефлятор инвестиций в основной капитал за счет всех источников финансирования в части капитального ремонта и ремонта автомобильных дорог на очередной финансовый год, разработанный для прогноза социально-экономического развития и учитываемый при формировании федерального бюджета на соответствующий финансовый год и плановый период (Iтг)', 'short_title' => 'Iтг',  'created_at' => $now, 'updated_at' => $now],
            ['key' => Coefficient::ID_KDKZ, 'road_type' => ApplicationRoute::ROAD_TYPE_REGIONAL, 'value' => 1.67, 'spring_value' => 1.67, 'title' => 'Коэффициент, учитывающий условия дорожно-климатических зон (Кдкз)', 'short_title' => 'Кдкз',  'created_at' => $now, 'updated_at' => $now],
            ['key' => Coefficient::ID_KAP_REM, 'road_type' => ApplicationRoute::ROAD_TYPE_REGIONAL, 'value' => 0.94, 'spring_value' => 0.94, 'title' => 'коэффициент, учитывающий относительную стоимость выполнения работ по капитальному ремонту и ремонту в зависимости от расположения автомобильной дороги на территории Российской Федерации (К кап.рем)', 'short_title' => 'К кап.рем',  'created_at' => $now, 'updated_at' => $now],
            ['key' => Coefficient::ID_KSEZ, 'road_type' => ApplicationRoute::ROAD_TYPE_REGIONAL, 'value' => 0.35, 'spring_value' => 0.35, 'title' => 'коэффициент, учитывающий природно-климатические условия (Ксез)', 'short_title' => 'Ксез',  'created_at' => $now, 'updated_at' => $now],

            ['key' => Coefficient::ID_RISH, 'road_type' => ApplicationRoute::ROAD_TYPE_REGIONAL, 'value' => 1840, 'spring_value' => 1840, 'title' => 'Исходное значение размера вреда, причиняемого транспортными средствами, при превышении допустимых осевых нагрузок для автомобильной дороги на 5 процентов(Р исх)', 'short_title' => 'Р исх',  'created_at' => $now, 'updated_at' => $now],
            ['key' => Coefficient::ID_KPM, 'road_type' => ApplicationRoute::ROAD_TYPE_REGIONAL, 'value' => 0.353, 'spring_value' => 0.353, 'title' => 'Коэффициент влияния массы транспортного средства в зависимости от расположения автомобильной дороги на территории Российской Федерации (Кпм)', 'short_title' => 'Кпм',  'created_at' => $now, 'updated_at' => $now],

            ['key' => Coefficient::ID_A, 'road_type' => ApplicationRoute::ROAD_TYPE_REGIONAL, 'value' => 37.7, 'spring_value' => 37.7, 'title' => 'a', 'short_title' => 'a',  'created_at' => $now, 'updated_at' => $now],
            ['key' => Coefficient::ID_B, 'road_type' => ApplicationRoute::ROAD_TYPE_REGIONAL, 'value' => 2.4, 'spring_value' => 2.4, 'title' => 'b', 'short_title' => 'b',  'created_at' => $now, 'updated_at' => $now],
            ['key' => Coefficient::ID_C, 'road_type' => ApplicationRoute::ROAD_TYPE_REGIONAL, 'value' => 7365, 'spring_value' => 7365, 'title' => 'c', 'short_title' => 'c',  'created_at' => $now, 'updated_at' => $now],
            ['key' => Coefficient::ID_D, 'road_type' => ApplicationRoute::ROAD_TYPE_REGIONAL, 'value' => 123.4, 'spring_value' => 123.4, 'title' => 'd', 'short_title' => 'd',  'created_at' => $now, 'updated_at' => $now],
            ['key' => Coefficient::ID_H, 'road_type' => ApplicationRoute::ROAD_TYPE_REGIONAL, 'value' => 10, 'spring_value' => 10, 'title' => 'нормативная (расчетная) осевая нагрузка для автомобильной дороги, тонн/ось', 'short_title' => 'H',  'created_at' => $now, 'updated_at' => $now],
            ['key' => Coefficient::ID_KLOW, 'road_type' => ApplicationRoute::ROAD_TYPE_REGIONAL, 'value' => 0.2, 'spring_value' => 0.2, 'title' => 'коэффициент при превышении значений допустимой массы от  2% до 15% включительно (Klow)', 'short_title' => 'Klow',  'created_at' => $now, 'updated_at' => $now],

            //Федеральные
            ['key' => Coefficient::ID_GP, 'road_type' => ApplicationRoute::ROAD_TYPE_FEDERAL, 'value' => 1600, 'spring_value' => 1600, 'title' => 'Госпошлина (Gp)', 'short_title' => 'Gp',  'created_at' => $now, 'updated_at' => $now],
            ['key' => Coefficient::ID_PB, 'road_type' => ApplicationRoute::ROAD_TYPE_FEDERAL, 'value' => 20.05, 'spring_value' => 20.05, 'title' => 'Стоимость бланка (Pb)', 'short_title' => 'Pb',  'created_at' => $now, 'updated_at' => $now],
            ['key' => Coefficient::ID_TPG, 'road_type' => ApplicationRoute::ROAD_TYPE_FEDERAL, 'value' => 1, 'spring_value' => 1, 'title' => 'Базовый компенсационный индекс предыдущего года (Тпг)', 'short_title' => 'Тпг',  'created_at' => $now, 'updated_at' => $now],
            ['key' => Coefficient::ID_ITG, 'road_type' => ApplicationRoute::ROAD_TYPE_FEDERAL, 'value' => 1, 'spring_value' => 1, 'title' => 'Индекс-дефлятор инвестиций в основной капитал за счет всех источников финансирования в части капитального ремонта и ремонта автомобильных дорог на очередной финансовый год, разработанный для прогноза социально-экономического развития и учитываемый при формировании федерального бюджета на соответствующий финансовый год и плановый период (Iтг)', 'short_title' => 'Iтг',  'created_at' => $now, 'updated_at' => $now],
            ['key' => Coefficient::ID_KDKZ, 'road_type' => ApplicationRoute::ROAD_TYPE_FEDERAL, 'value' => 1.67, 'spring_value' => 1.67, 'title' => 'Коэффициент, учитывающий условия дорожно-климатических зон (Кдкз)', 'short_title' => 'Кдкз',  'created_at' => $now, 'updated_at' => $now],
            ['key' => Coefficient::ID_KAP_REM, 'road_type' => ApplicationRoute::ROAD_TYPE_FEDERAL, 'value' => 0.94, 'spring_value' => 0.94, 'title' => 'коэффициент, учитывающий относительную стоимость выполнения работ по капитальному ремонту и ремонту в зависимости от расположения автомобильной дороги на территории Российской Федерации (К кап.рем)', 'short_title' => 'К кап.рем',  'created_at' => $now, 'updated_at' => $now],
            ['key' => Coefficient::ID_KSEZ, 'road_type' => ApplicationRoute::ROAD_TYPE_FEDERAL, 'value' => 0.35, 'spring_value' => 0.35, 'title' => 'коэффициент, учитывающий природно-климатические условия (Ксез)', 'short_title' => 'Ксез',  'created_at' => $now, 'updated_at' => $now],

            ['key' => Coefficient::ID_RISH, 'road_type' => ApplicationRoute::ROAD_TYPE_FEDERAL, 'value' => 840, 'spring_value' => 1840, 'title' => 'Исходное значение размера вреда, причиняемого транспортными средствами, при превышении допустимых осевых нагрузок для автомобильной дороги на 5 процентов(Р исх)', 'short_title' => 'Р исх',  'created_at' => $now, 'updated_at' => $now],
            ['key' => Coefficient::ID_KPM, 'road_type' => ApplicationRoute::ROAD_TYPE_FEDERAL, 'value' => 0.76, 'spring_value' => 0.353, 'title' => 'Коэффициент влияния массы транспортного средства в зависимости от расположения автомобильной дороги на территории Российской Федерации (Кпм)', 'short_title' => 'Кпм',  'created_at' => $now, 'updated_at' => $now],

            ['key' => Coefficient::ID_A, 'road_type' => ApplicationRoute::ROAD_TYPE_FEDERAL, 'value' => 39.5, 'spring_value' => 37.7, 'title' => 'a', 'short_title' => 'a',  'created_at' => $now, 'updated_at' => $now],
            ['key' => Coefficient::ID_B, 'road_type' => ApplicationRoute::ROAD_TYPE_FEDERAL, 'value' => 2.7, 'spring_value' => 2.4, 'title' => 'b', 'short_title' => 'b',  'created_at' => $now, 'updated_at' => $now],
            ['key' => Coefficient::ID_C, 'road_type' => ApplicationRoute::ROAD_TYPE_FEDERAL, 'value' => 7365, 'spring_value' => 7365, 'title' => 'c', 'short_title' => 'c',  'created_at' => $now, 'updated_at' => $now],
            ['key' => Coefficient::ID_D, 'road_type' => ApplicationRoute::ROAD_TYPE_FEDERAL, 'value' => 123.4, 'spring_value' => 123.4, 'title' => 'd', 'short_title' => 'd',  'created_at' => $now, 'updated_at' => $now],
            ['key' => Coefficient::ID_H, 'road_type' => ApplicationRoute::ROAD_TYPE_FEDERAL, 'value' => 11.5, 'spring_value' => 10, 'title' => 'нормативная (расчетная) осевая нагрузка для автомобильной дороги, тонн/ось', 'short_title' => 'H',  'created_at' => $now, 'updated_at' => $now],
            ['key' => Coefficient::ID_KLOW, 'road_type' => ApplicationRoute::ROAD_TYPE_FEDERAL, 'value' => 0.2, 'spring_value' => 0.2, 'title' => 'коэффициент при превышении значений допустимой массы от  2% до 15% включительно (Klow)', 'short_title' => 'Klow',  'created_at' => $now, 'updated_at' => $now],

            //Муниципальные
            ['key' => Coefficient::ID_GP, 'road_type' => ApplicationRoute::ROAD_TYPE_LOCAL, 'value' => 1600, 'spring_value' => 1600, 'title' => 'Госпошлина (Gp)', 'short_title' => 'Gp',  'created_at' => $now, 'updated_at' => $now],
            ['key' => Coefficient::ID_PB, 'road_type' => ApplicationRoute::ROAD_TYPE_LOCAL, 'value' => 20.05, 'spring_value' => 20.05, 'title' => 'Стоимость бланка (Pb)', 'short_title' => 'Pb',  'created_at' => $now, 'updated_at' => $now],
            ['key' => Coefficient::ID_TPG, 'road_type' => ApplicationRoute::ROAD_TYPE_LOCAL, 'value' => 1, 'spring_value' => 1, 'title' => 'Базовый компенсационный индекс предыдущего года (Тпг)', 'short_title' => 'Тпг',  'created_at' => $now, 'updated_at' => $now],
            ['key' => Coefficient::ID_ITG, 'road_type' => ApplicationRoute::ROAD_TYPE_LOCAL, 'value' => 1, 'spring_value' => 1, 'title' => 'Индекс-дефлятор инвестиций в основной капитал за счет всех источников финансирования в части капитального ремонта и ремонта автомобильных дорог на очередной финансовый год, разработанный для прогноза социально-экономического развития и учитываемый при формировании федерального бюджета на соответствующий финансовый год и плановый период (Iтг)', 'short_title' => 'Iтг',  'created_at' => $now, 'updated_at' => $now],
            ['key' => Coefficient::ID_KDKZ, 'road_type' => ApplicationRoute::ROAD_TYPE_LOCAL, 'value' => 1.67, 'spring_value' => 1.67, 'title' => 'Коэффициент, учитывающий условия дорожно-климатических зон (Кдкз)', 'short_title' => 'Кдкз',  'created_at' => $now, 'updated_at' => $now],
            ['key' => Coefficient::ID_KAP_REM, 'road_type' => ApplicationRoute::ROAD_TYPE_LOCAL, 'value' => 0.94, 'spring_value' => 0.94, 'title' => 'коэффициент, учитывающий относительную стоимость выполнения работ по капитальному ремонту и ремонту в зависимости от расположения автомобильной дороги на территории Российской Федерации (К кап.рем)', 'short_title' => 'К кап.рем',  'created_at' => $now, 'updated_at' => $now],
            ['key' => Coefficient::ID_KSEZ, 'road_type' => ApplicationRoute::ROAD_TYPE_LOCAL, 'value' => 0.35, 'spring_value' => 0.35, 'title' => 'коэффициент, учитывающий природно-климатические условия (Ксез)', 'short_title' => 'Ксез',  'created_at' => $now, 'updated_at' => $now],

            ['key' => Coefficient::ID_RISH, 'road_type' => ApplicationRoute::ROAD_TYPE_LOCAL, 'value' => 8500, 'spring_value' => 1840, 'title' => 'Исходное значение размера вреда, причиняемого транспортными средствами, при превышении допустимых осевых нагрузок для автомобильной дороги на 5 процентов(Р исх)', 'short_title' => 'Р исх',  'created_at' => $now, 'updated_at' => $now],
            ['key' => Coefficient::ID_KPM, 'road_type' => ApplicationRoute::ROAD_TYPE_LOCAL, 'value' => 0.353, 'spring_value' => 0.353, 'title' => 'Коэффициент влияния массы транспортного средства в зависимости от расположения автомобильной дороги на территории Российской Федерации (Кпм)', 'short_title' => 'Кпм',  'created_at' => $now, 'updated_at' => $now],

            ['key' => Coefficient::ID_A, 'road_type' => ApplicationRoute::ROAD_TYPE_LOCAL, 'value' => 7.3, 'spring_value' => 37.7, 'title' => 'a', 'short_title' => 'a',  'created_at' => $now, 'updated_at' => $now],
            ['key' => Coefficient::ID_B, 'road_type' => ApplicationRoute::ROAD_TYPE_LOCAL, 'value' => 0.27, 'spring_value' => 2.4, 'title' => 'b', 'short_title' => 'b',  'created_at' => $now, 'updated_at' => $now],
            ['key' => Coefficient::ID_C, 'road_type' => ApplicationRoute::ROAD_TYPE_LOCAL, 'value' => 7365, 'spring_value' => 7365, 'title' => 'c', 'short_title' => 'c',  'created_at' => $now, 'updated_at' => $now],
            ['key' => Coefficient::ID_D, 'road_type' => ApplicationRoute::ROAD_TYPE_LOCAL, 'value' => 123.4, 'spring_value' => 123.4, 'title' => 'd', 'short_title' => 'd',  'created_at' => $now, 'updated_at' => $now],
            ['key' => Coefficient::ID_H, 'road_type' => ApplicationRoute::ROAD_TYPE_LOCAL, 'value' => 6, 'spring_value' => 10, 'title' => 'нормативная (расчетная) осевая нагрузка для автомобильной дороги, тонн/ось', 'short_title' => 'H',  'created_at' => $now, 'updated_at' => $now],
            ['key' => Coefficient::ID_KLOW, 'road_type' => ApplicationRoute::ROAD_TYPE_LOCAL, 'value' => 0.2, 'spring_value' => 0.2, 'title' => 'коэффициент при превышении значений допустимой массы от  2% до 15% включительно (Klow)', 'short_title' => 'Klow',  'created_at' => $now, 'updated_at' => $now],
        ]);
    }
}
