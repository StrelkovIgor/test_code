<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    private $testPassword = 'qweqaz12';
    private $testPassword1 = 'pass12345';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = Carbon::now();

        DB::table('users')->insert([
            [
                'id' => 1,
                'name' => 'Тестовый администратор',
                'phone' => '+70000000001',
                'email' => 'user1@gmail.com',
                'role_id' => 1,
                'password' => bcrypt($this->testPassword),
                'created_at' => $now,
                'updated_at' => $now,
                'api_token' => str_random(60)
            ],
            /*
            [
                'id' => 2,
                'name' => 'Тестовый сотрудник',
                'phone' => '+70000000002',
                'email' => 'user2@gmail.com',
                'role_id' => 2,
                'password' => bcrypt($this->testPassword),
                'created_at' => $now,
                'updated_at' => $now,
                'api_token' => str_random(60)
            ],
            [
                'id' => 3,
                'name' => 'Тестовое физ лицо',
                'phone' => '+70000000003',
                'email' => 'user3@gmail.com',
                'role_id' => 3,
                'password' => bcrypt($this->testPassword),
                'created_at' => $now,
                'updated_at' => $now,
                'api_token' => str_random(60)
            ],
            [
                'id' => 4,
                'name' => 'Тестовое юр лиц',
                'phone' => '+70000000004',
                'email' => 'user4@gmail.com',
                'role_id' => 4,
                'password' => bcrypt($this->testPassword),
                'created_at' => $now,
                'updated_at' => $now,
                'api_token' => str_random(60)
            ],
            [
                'id' => 5,
                'name' => 'Физ лицо Намисофт',
                'phone' => '+70000000005',
                'email' => 'user@namisoft.ru',
                'role_id' => 3,
                'password' => bcrypt($this->testPassword1),
                'created_at' => $now,
                'updated_at' => $now,
                'api_token' => str_random(60)
            ],
            [
                'id' => 6,
                'name' => 'Админ с почтой',
                'phone' => '+70000000006',
                'email' => 'admin@namisoft.ru',
                'role_id' => 1,
                'password' => bcrypt($this->testPassword1),
                'created_at' => $now,
                'updated_at' => $now,
                'api_token' => str_random(60)
            ],
            [
                'id' => 7,
                'name' => 'Сотрудник намисофт',
                'phone' => '+70000000007',
                'email' => 'office@namisoft.ru',
                'role_id' => 2,
                'password' => bcrypt($this->testPassword1),
                'created_at' => $now,
                'updated_at' => $now,
                'api_token' => str_random(60)
            ],
            */
        ]);
/*
        DB::table('individuals')->insert([
            [
                'user_id' => 3,
                'inn' => '000000000001',
                'updated_at' => $now
            ],
            [
                'user_id' => 5,
                'inn' => '000000000005',
                'updated_at' => $now
            ],
        ]);

        DB::table('firms')->insert([
            [
                'id' => 1,
                'user_id' => 4,
                'legal_form_id' => 2,
                'address' => 'г. Казань, любая улица д. 1, кв. 1',
                'executive_fio' => 'Ответственный Юр Фирмы',
                'executive_position' => 'Директор',
                'reason_key' => 2,
                'contact_phone' => '+71000000001',
                'contact_fio' => 'Контактное Лицо Фирмы',
                'created_at' => $now,
                'updated_at' => $now,
            ],
        ]);

        DB::table('firm_payment_details')->insert([
            [
                'id' => 1,
                'firm_id' => 1,
                'bank_name' => 'Любой Банк',
                'bank_inn' => '0000000001',
                'correspondent_account' => '22222222222222222222',
                'bank_bik' => '333333333',
                'account' => '44444444444444444444',
                'created_at' => $now,
                'updated_at' => $now,
            ],
        ]);
*/
    }
}
