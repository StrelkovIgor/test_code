<?php

use App\Models\AxleLoad;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AxleLoadsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = Carbon::now();

        AxleLoad::truncate();
        DB::table('axle_load_limits')->insert([
            //Одиночные оси
            [
                'title' => 'Односкатная одиночная ось',
                'is_spring' => 0,
                'group_axle_count' => 1,
                'wheel_count' => 1,
                'distance_index' => AxleLoad::DISTANCE_INDEX_MORE_25,
                'axle_type' => AxleLoad::AXLE_TYPE_SPRING,
                'wheels' => AxleLoad::WHEELS_LESS_4,
                'value0' => 9, 'value1' => 10.5, 'value2' => 5.5,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'title' => 'Двускатная одиночная ось',
                'is_spring' => 0,
                'group_axle_count' => 1,
                'wheel_count' => 2,
                'distance_index' => AxleLoad::DISTANCE_INDEX_MORE_25,
                'axle_type' => AxleLoad::AXLE_TYPE_SPRING,
                'wheels' => AxleLoad::WHEELS_LESS_4,
                'value0' => 10, 'value1' => 11.5, 'value2' => 6,
                'created_at' => $now,
                'updated_at' => $now
            ],

            //Группы осей
            //Группы со скатностью = 1
            //Двухосные группы
            [
                'title' => 'Односкатная двухосная группа (расстояние до 1 м (включительно))',
                'is_spring' => 0,
                'group_axle_count' => 2,
                'wheel_count' => 1,
                'distance_index' => AxleLoad::DISTANCE_INDEX_LESS_1,
                'axle_type' => AxleLoad::AXLE_TYPE_SPRING,
                'wheels' => AxleLoad::WHEELS_LESS_4,
                'value0' => 5, 'value1' => 5.75, 'value2' => 4,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'title' => 'Односкатная двухосная группа (расстояние свыше 1 м до 1.3 м (включительно))',
                'is_spring' => 0,
                'group_axle_count' => 2,
                'wheel_count' => 1,
                'distance_index' => AxleLoad::DISTANCE_INDEX_1_13,
                'axle_type' => AxleLoad::AXLE_TYPE_SPRING,
                'wheels' => AxleLoad::WHEELS_LESS_4,
                'value0' => 6.5, 'value1' => 7, 'value2' => 4.5,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'title' => 'Односкатная двухосная группа (расстояние свыше 1.3 м до 1.8 м (включительно))',
                'is_spring' => 0,
                'group_axle_count' => 2,
                'wheel_count' => 1,
                'distance_index' => AxleLoad::DISTANCE_INDEX_13_18,
                'axle_type' => AxleLoad::AXLE_TYPE_SPRING,
                'wheels' => AxleLoad::WHEELS_LESS_4,
                'value0' => 7.5, 'value1' => 8.5, 'value2' => 5,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'title' => 'Односкатная двухосная группа (расстояние свыше 1.8 м до 2.5 м (включительно))',
                'is_spring' => 0,
                'group_axle_count' => 2,
                'wheel_count' => 1,
                'distance_index' => AxleLoad::DISTANCE_INDEX_18_25,
                'axle_type' => AxleLoad::AXLE_TYPE_SPRING,
                'wheels' => AxleLoad::WHEELS_LESS_4,
                'value0' => 8.5, 'value1' => 9, 'value2' => 5.5,
                'created_at' => $now,
                'updated_at' => $now
            ],

            //Трехосные группы
            [
                'title' => 'Односкатная трехосная группа (расстояние до 1 м (включительно))',
                'is_spring' => 0,
                'group_axle_count' => 3,
                'wheel_count' => 1,
                'distance_index' => AxleLoad::DISTANCE_INDEX_LESS_1,
                'axle_type' => AxleLoad::AXLE_TYPE_SPRING,
                'wheels' => AxleLoad::WHEELS_LESS_4,
                'value0' => 5, 'value1' => 5.66, 'value2' => 3.66,
                'created_at' => $now,
                'updated_at' => $now
            ],

            [
                'title' => 'Односкатная трехосная группа (расстояние свыше 1 м до 1.3 м (включительно)',
                'is_spring' => 0,
                'group_axle_count' => 3,
                'wheel_count' => 1,
                'distance_index' => AxleLoad::DISTANCE_INDEX_1_13,
                'axle_type' => AxleLoad::AXLE_TYPE_SPRING,
                'wheels' => AxleLoad::WHEELS_LESS_4,
                'value0' => 6, 'value1' => 6.66, 'value2' => 4,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'title' => 'Односкатная рессорная трехосная группа (расстояние свыше 1.3 м до 1.8 м (включительно))',
                'is_spring' => 0,
                'group_axle_count' => 3,
                'wheel_count' => 1,
                'distance_index' => AxleLoad::DISTANCE_INDEX_13_18,
                'axle_type' => AxleLoad::AXLE_TYPE_SPRING,
                'wheels' => AxleLoad::WHEELS_LESS_4,
                'value0' => 7, 'value1' => 7.83, 'value2' => 4.5,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'title' => 'Односкатная пневматическая трехосная группа (расстояние свыше 1.3 м до 1.8 м (включительно))',
                'is_spring' => 0,
                'group_axle_count' => 3,
                'wheel_count' => 1,
                'distance_index' => AxleLoad::DISTANCE_INDEX_13_18,
                'axle_type' => AxleLoad::AXLE_TYPE_NON_SPRING,
                'wheels' => AxleLoad::WHEELS_LESS_4,
                'value0' => 7.5, 'value1' => 7.83, 'value2' => 4.5,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'title' => 'Односкатная трехосная группа (расстояние свыше 1.8 м до 2.5 м (включительно))',
                'is_spring' => 0,
                'group_axle_count' => 3,
                'wheel_count' => 1,
                'distance_index' => AxleLoad::DISTANCE_INDEX_18_25,
                'axle_type' => AxleLoad::AXLE_TYPE_SPRING,
                'wheels' => AxleLoad::WHEELS_LESS_4,
                'value0' => 7.33, 'value1' => 8.33, 'value2' => 5,
                'created_at' => $now,
                'updated_at' => $now
            ],

            //4-х и более осная группа
            //Число колес меньше или равно 4
            [
                'title' => 'Односкатная 4-х и более осная группа, имеющая на каждой оси не более 4 колес (расстояние до 1 м (включительно))',
                'is_spring' => 0,
                'group_axle_count' => 4,
                'wheel_count' => 1,
                'distance_index' => AxleLoad::DISTANCE_INDEX_LESS_1,
                'axle_type' => AxleLoad::AXLE_TYPE_SPRING,
                'wheels' => AxleLoad::WHEELS_LESS_4,
                'value0' => 5, 'value1' => 5.5, 'value2' => 3.5,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'title' => 'Односкатная 4-х и более осная группа, имеющая на каждой оси не более 4 колес (расстояние свыше 1 м до 1.3 м (включительно))',
                'is_spring' => 0,
                'group_axle_count' => 4,
                'wheel_count' => 1,
                'distance_index' => AxleLoad::DISTANCE_INDEX_1_13,
                'axle_type' => AxleLoad::AXLE_TYPE_SPRING,
                'wheels' => AxleLoad::WHEELS_LESS_4,
                'value0' => 6, 'value1' => 6.5, 'value2' => 4,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'title' => 'Односкатная 4-х и более осная группа, имеющая на каждой оси не более 4 колес (расстояние свыше 1.3 м до 1.8 м (включительно))',
                'is_spring' => 0,
                'group_axle_count' => 4,
                'wheel_count' => 1,
                'distance_index' => AxleLoad::DISTANCE_INDEX_13_18,
                'axle_type' => AxleLoad::AXLE_TYPE_SPRING,
                'wheels' => AxleLoad::WHEELS_LESS_4,
                'value0' => 6.5, 'value1' => 7.5, 'value2' => 4.5,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'title' => 'Односкатная 4-х и более осная группа, имеющая на каждой оси не более 4 колес (расстояние свыше 1.8 м до 2.5 м (включительно))',
                'is_spring' => 0,
                'group_axle_count' => 4,
                'wheel_count' => 1,
                'distance_index' => AxleLoad::DISTANCE_INDEX_18_25,
                'axle_type' => AxleLoad::AXLE_TYPE_SPRING,
                'wheels' => AxleLoad::WHEELS_LESS_4,
                'value0' => 7, 'value1' => 8.5, 'value2' => 5,
                'created_at' => $now,
                'updated_at' => $now
            ],
            //Двускатные оси
            //Двухосные группы
            [
                'title' => 'Двускатная двухосная группа (расстояние до 1 м (включительно))',
                'is_spring' => 0,
                'group_axle_count' => 2,
                'wheel_count' => 2,
                'distance_index' => AxleLoad::DISTANCE_INDEX_LESS_1,
                'axle_type' => AxleLoad::AXLE_TYPE_SPRING,
                'wheels' => AxleLoad::WHEELS_LESS_4,
                'value0' => 5.5, 'value1' => 6.25, 'value2' => 4.5,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'title' => 'Двускатная двухосная группа (расстояние свыше 1 м до 1.3 м (включительно))',
                'is_spring' => 0,
                'group_axle_count' => 2,
                'wheel_count' => 2,
                'distance_index' => AxleLoad::DISTANCE_INDEX_1_13,
                'axle_type' => AxleLoad::AXLE_TYPE_SPRING,
                'wheels' => AxleLoad::WHEELS_LESS_4,
                'value0' => 7, 'value1' => 8, 'value2' => 5,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'title' => 'Двускатная двухосная группа  (расстояние свыше 1.3 м до 1.8 м (включительно))',
                'is_spring' => 0,
                'group_axle_count' => 2,
                'wheel_count' => 2,
                'distance_index' => AxleLoad::DISTANCE_INDEX_13_18,
                'axle_type' => AxleLoad::AXLE_TYPE_SPRING,
                'wheels' => AxleLoad::WHEELS_LESS_4,
                'value0' => 8, 'value1' => 9, 'value2' => 5.5,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'title' => 'Двускатная двухосная группа (расстояние свыше 1.8 м до 2.5 м (включительно))',
                'is_spring' => 0,
                'group_axle_count' => 2,
                'wheel_count' => 2,
                'distance_index' => AxleLoad::DISTANCE_INDEX_18_25,
                'axle_type' => AxleLoad::AXLE_TYPE_SPRING,
                'wheels' => AxleLoad::WHEELS_LESS_4,
                'value0' => 9, 'value1' => 10, 'value2' => 6,
                'created_at' => $now,
                'updated_at' => $now
            ],

            //Трехосные группы
            [
                'title' => 'Двускатная трехосная группа (расстояние до 1 м (включительно))',
                'is_spring' => 0,
                'group_axle_count' => 3,
                'wheel_count' => 2,
                'distance_index' => AxleLoad::DISTANCE_INDEX_LESS_1,
                'axle_type' => AxleLoad::AXLE_TYPE_SPRING,
                'wheels' => AxleLoad::WHEELS_LESS_4,
                'value0' => 5.5, 'value1' => 6, 'value2' => 4,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'title' => 'Двускатная трехосная группа (расстояние свыше 1 м до 1.3 м (включительно))',
                'is_spring' => 0,
                'group_axle_count' => 3,
                'wheel_count' => 2,
                'distance_index' => AxleLoad::DISTANCE_INDEX_1_13,
                'axle_type' => AxleLoad::AXLE_TYPE_SPRING,
                'wheels' => AxleLoad::WHEELS_LESS_4,
                'value0' => 6.5, 'value1' => 7, 'value2' => 4.33,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'title' => 'Двускатная рессорная трехосная группа  (расстояние свыше 1.3 м до 1.8 м (включительно))',
                'is_spring' => 0,
                'group_axle_count' => 3,
                'wheel_count' => 2,
                'distance_index' => AxleLoad::DISTANCE_INDEX_13_18,
                'axle_type' => AxleLoad::AXLE_TYPE_SPRING,
                'wheels' => AxleLoad::WHEELS_LESS_4,
                'value0' => 7.5, 'value1' => 8, 'value2' => 5,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'title' => 'Двускатная пневматическая трехосная группа  (расстояние свыше 1.3 м до 1.8 м (включительно))',
                'is_spring' => 0,
                'group_axle_count' => 3,
                'wheel_count' => 2,
                'distance_index' => AxleLoad::DISTANCE_INDEX_13_18,
                'axle_type' => AxleLoad::AXLE_TYPE_NON_SPRING,
                'wheels' => AxleLoad::WHEELS_LESS_4,
                'value0' => 7.5, 'value1' => 8, 'value2' => 5,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'title' => 'Двускатная трехосная группа (расстояние свыше 1.8 м до 2.5 м (включительно))',
                'is_spring' => 0,
                'group_axle_count' => 3,
                'wheel_count' => 2,
                'distance_index' => AxleLoad::DISTANCE_INDEX_18_25,
                'axle_type' => AxleLoad::AXLE_TYPE_SPRING,
                'wheels' => AxleLoad::WHEELS_LESS_4,
                'value0' => 7.66, 'value1' => 8.66, 'value2' => 5.33,
                'created_at' => $now,
                'updated_at' => $now
            ],
            //4-х и более осная группа
            [
                'title' => 'Двухскатная 4-х и более осная группа, имеющая на каждой оси не более 4 колес (расстояние до 1 м (включительно))',
                'is_spring' => 0,
                'group_axle_count' => 4,
                'wheel_count' => 2,
                'distance_index' => AxleLoad::DISTANCE_INDEX_LESS_1,
                'axle_type' => AxleLoad::AXLE_TYPE_SPRING,
                'wheels' => AxleLoad::WHEELS_LESS_4,
                'value0' => 5.5, 'value1' => 6, 'value2' => 4,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'title' => 'Двухскатная 4-х и более осная группа, имеющая на каждой оси не более 4 колес (расстояние свыше 1 м до 1.3 м (включительно))',
                'is_spring' => 0,
                'group_axle_count' => 4,
                'wheel_count' => 2,
                'distance_index' => AxleLoad::DISTANCE_INDEX_1_13,
                'axle_type' => AxleLoad::AXLE_TYPE_SPRING,
                'wheels' => AxleLoad::WHEELS_LESS_4,
                'value0' => 6.5, 'value1' => 7, 'value2' => 4.5,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'title' => 'Двухскатная 4-х и более осная группа, имеющая на каждой оси не более 4 колес (расстояние свыше 1.3 м до 1.8 м (включительно))',
                'is_spring' => 0,
                'group_axle_count' => 4,
                'wheel_count' => 2,
                'distance_index' => AxleLoad::DISTANCE_INDEX_13_18,
                'axle_type' => AxleLoad::AXLE_TYPE_SPRING,
                'wheels' => AxleLoad::WHEELS_LESS_4,
                'value0' => 7, 'value1' => 8, 'value2' => 5,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'title' => 'Двухскатная 4-х и более осная группа, имеющая на каждой оси не более 4 колес (расстояние свыше 1.8 м до 2.5 м (включительно))',
                'is_spring' => 0,
                'group_axle_count' => 4,
                'wheel_count' => 2,
                'distance_index' => AxleLoad::DISTANCE_INDEX_18_25,
                'axle_type' => AxleLoad::AXLE_TYPE_SPRING,
                'wheels' => AxleLoad::WHEELS_LESS_4,
                'value0' => 7.5, 'value1' => 9, 'value2' => 5.5,
                'created_at' => $now,
                'updated_at' => $now
            ],

            //Число колес больше или равно 4
            [
                'title' => 'Сближенные оси, имеющие на каждой оси по 8 и более колес (расстояние до 1 м (включительно))',
                'is_spring' => 0,
                'group_axle_count' => 2,
                'wheel_count' => 1,
                'distance_index' => AxleLoad::DISTANCE_INDEX_LESS_1,
                'axle_type' => AxleLoad::AXLE_TYPE_SPRING,
                'wheels' => AxleLoad::WHEELS_MORE_4,
                'value0' => 9.5, 'value1' => 11, 'value2' => 6,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'title' => 'Сближенные оси, имеющие на каждой оси по 8 и более колес (расстояние свыше 1 м до 1.3 м (включительно))',
                'is_spring' => 0,
                'group_axle_count' => 2,
                'wheel_count' => 1,
                'distance_index' => AxleLoad::DISTANCE_INDEX_1_13,
                'axle_type' => AxleLoad::AXLE_TYPE_SPRING,
                'wheels' => AxleLoad::WHEELS_MORE_4,
                'value0' => 10.5, 'value1' => 12, 'value2' => 6.5,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'title' => 'Сближенные оси, имеющие на каждой оси по 8 и более колес (расстояние свыше 1.3 м до 1.8 м (включительно))',
                'is_spring' => 0,
                'group_axle_count' => 2,
                'wheel_count' => 1,
                'distance_index' => AxleLoad::DISTANCE_INDEX_13_18,
                'axle_type' => AxleLoad::AXLE_TYPE_SPRING,
                'wheels' => AxleLoad::WHEELS_MORE_4,
                'value0' => 12, 'value1' => 14, 'value2' => 7.5,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'title' => 'Сближенные оси, имеющие на каждой оси по 8 и более колес (расстояние свыше 1.8 м до 2.5 м (включительно))',
                'is_spring' => 0,
                'group_axle_count' => 2,
                'wheel_count' => 1,
                'distance_index' => AxleLoad::DISTANCE_INDEX_18_25,
                'axle_type' => AxleLoad::AXLE_TYPE_SPRING,
                'wheels' => AxleLoad::WHEELS_MORE_4,
                'value0' => 13.5, 'value1' => 16, 'value2' => 8.5,
                'created_at' => $now,
                'updated_at' => $now
            ],

            //С ограничением
            [
                'title' => 'Односкатная одиночная ось',
                'is_spring' => 1,
                'group_axle_count' => 1,
                'wheel_count' => 1,
                'distance_index' => AxleLoad::DISTANCE_INDEX_MORE_25,
                'axle_type' => AxleLoad::AXLE_TYPE_SPRING,
                'wheels' => AxleLoad::WHEELS_LESS_4,
                'value0' => 6, 'value1' => 6, 'value2' => 6,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'title' => 'Двускатная одиночная ось',
                'is_spring' => 1,
                'group_axle_count' => 1,
                'wheel_count' => 2,
                'distance_index' => AxleLoad::DISTANCE_INDEX_MORE_25,
                'axle_type' => AxleLoad::AXLE_TYPE_SPRING,
                'wheels' => AxleLoad::WHEELS_LESS_4,
                'value0' => 6, 'value1' => 6, 'value2' => 6,
                'created_at' => $now,
                'updated_at' => $now
            ],

            //Группы осей
            //Группы со скатностью = 1
            //Двухосные группы
            [
                'title' => 'Односкатная двухосная группа (расстояние до 1 м (включительно))',
                'is_spring' => 1,
                'group_axle_count' => 2,
                'wheel_count' => 1,
                'distance_index' => AxleLoad::DISTANCE_INDEX_LESS_1,
                'axle_type' => AxleLoad::AXLE_TYPE_SPRING,
                'wheels' => AxleLoad::WHEELS_LESS_4,
                'value0' => 6, 'value1' => 6, 'value2' => 6,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'title' => 'Односкатная двухосная группа (расстояние свыше 1 м до 1.3 м (включительно))',
                'is_spring' => 1,
                'group_axle_count' => 2,
                'wheel_count' => 1,
                'distance_index' => AxleLoad::DISTANCE_INDEX_1_13,
                'axle_type' => AxleLoad::AXLE_TYPE_SPRING,
                'wheels' => AxleLoad::WHEELS_LESS_4,
                'value0' => 6, 'value1' => 6, 'value2' => 6,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'title' => 'Односкатная двухосная группа (расстояние свыше 1.3 м до 1.8 м (включительно))',
                'is_spring' => 1,
                'group_axle_count' => 2,
                'wheel_count' => 1,
                'distance_index' => AxleLoad::DISTANCE_INDEX_13_18,
                'axle_type' => AxleLoad::AXLE_TYPE_SPRING,
                'wheels' => AxleLoad::WHEELS_LESS_4,
                'value0' => 6, 'value1' => 6, 'value2' => 6,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'title' => 'Односкатная двухосная группа (расстояние свыше 1.8 м до 2.5 м (включительно))',
                'is_spring' => 1,
                'group_axle_count' => 2,
                'wheel_count' => 1,
                'distance_index' => AxleLoad::DISTANCE_INDEX_18_25,
                'axle_type' => AxleLoad::AXLE_TYPE_SPRING,
                'wheels' => AxleLoad::WHEELS_LESS_4,
                'value0' => 6, 'value1' => 6, 'value2' => 6,
                'created_at' => $now,
                'updated_at' => $now
            ],

            //Трехосные группы
            [
                'title' => 'Односкатная трехосная группа (расстояние до 1 м (включительно))',
                'is_spring' => 1,
                'group_axle_count' => 3,
                'wheel_count' => 1,
                'distance_index' => AxleLoad::DISTANCE_INDEX_LESS_1,
                'axle_type' => AxleLoad::AXLE_TYPE_SPRING,
                'wheels' => AxleLoad::WHEELS_LESS_4,
                'value0' => 6, 'value1' => 6, 'value2' => 6,
                'created_at' => $now,
                'updated_at' => $now
            ],

            [
                'title' => 'Односкатная трехосная группа (расстояние свыше 1 м до 1.3 м (включительно)',
                'is_spring' => 1,
                'group_axle_count' => 3,
                'wheel_count' => 1,
                'distance_index' => AxleLoad::DISTANCE_INDEX_1_13,
                'axle_type' => AxleLoad::AXLE_TYPE_SPRING,
                'wheels' => AxleLoad::WHEELS_LESS_4,
                'value0' => 6, 'value1' => 6, 'value2' => 6,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'title' => 'Односкатная рессорная трехосная группа (расстояние свыше 1.3 м до 1.8 м (включительно))',
                'is_spring' => 1,
                'group_axle_count' => 3,
                'wheel_count' => 1,
                'distance_index' => AxleLoad::DISTANCE_INDEX_13_18,
                'axle_type' => AxleLoad::AXLE_TYPE_SPRING,
                'wheels' => AxleLoad::WHEELS_LESS_4,
                'value0' => 6, 'value1' => 6, 'value2' => 6,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'title' => 'Односкатная пневматическая трехосная группа (расстояние свыше 1.3 м до 1.8 м (включительно))',
                'is_spring' => 1,
                'group_axle_count' => 3,
                'wheel_count' => 1,
                'distance_index' => AxleLoad::DISTANCE_INDEX_13_18,
                'axle_type' => AxleLoad::AXLE_TYPE_NON_SPRING,
                'wheels' => AxleLoad::WHEELS_LESS_4,
                'value0' => 6, 'value1' => 6, 'value2' => 6,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'title' => 'Односкатная трехосная группа (расстояние свыше 1.8 м до 2.5 м (включительно))',
                'is_spring' => 1,
                'group_axle_count' => 3,
                'wheel_count' => 1,
                'distance_index' => AxleLoad::DISTANCE_INDEX_18_25,
                'axle_type' => AxleLoad::AXLE_TYPE_SPRING,
                'wheels' => AxleLoad::WHEELS_LESS_4,
                'value0' => 6, 'value1' => 6, 'value2' => 6,
                'created_at' => $now,
                'updated_at' => $now
            ],

            //4-х и более осная группа
            //Число колес меньше или равно 4
            [
                'title' => 'Односкатная 4-х и более осная группа, имеющая на каждой оси не более 4 колес (расстояние до 1 м (включительно))',
                'is_spring' => 1,
                'group_axle_count' => 4,
                'wheel_count' => 1,
                'distance_index' => AxleLoad::DISTANCE_INDEX_LESS_1,
                'axle_type' => AxleLoad::AXLE_TYPE_SPRING,
                'wheels' => AxleLoad::WHEELS_LESS_4,
                'value0' => 6, 'value1' => 6, 'value2' => 6,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'title' => 'Односкатная 4-х и более осная группа, имеющая на каждой оси не более 4 колес (расстояние свыше 1 м до 1.3 м (включительно))',
                'is_spring' => 1,
                'group_axle_count' => 4,
                'wheel_count' => 1,
                'distance_index' => AxleLoad::DISTANCE_INDEX_1_13,
                'axle_type' => AxleLoad::AXLE_TYPE_SPRING,
                'wheels' => AxleLoad::WHEELS_LESS_4,
                'value0' => 6, 'value1' => 6, 'value2' => 6,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'title' => 'Односкатная 4-х и более осная группа, имеющая на каждой оси не более 4 колес (расстояние свыше 1.3 м до 1.8 м (включительно))',
                'is_spring' => 1,
                'group_axle_count' => 4,
                'wheel_count' => 1,
                'distance_index' => AxleLoad::DISTANCE_INDEX_13_18,
                'axle_type' => AxleLoad::AXLE_TYPE_SPRING,
                'wheels' => AxleLoad::WHEELS_LESS_4,
                'value0' => 6, 'value1' => 6, 'value2' => 6,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'title' => 'Односкатная 4-х и более осная группа, имеющая на каждой оси не более 4 колес (расстояние свыше 1.8 м до 2.5 м (включительно))',
                'is_spring' => 1,
                'group_axle_count' => 4,
                'wheel_count' => 1,
                'distance_index' => AxleLoad::DISTANCE_INDEX_18_25,
                'axle_type' => AxleLoad::AXLE_TYPE_SPRING,
                'wheels' => AxleLoad::WHEELS_LESS_4,
                'value0' => 6, 'value1' => 6, 'value2' => 6,
                'created_at' => $now,
                'updated_at' => $now
            ],
            //Двускатные оси
            //Двухосные группы
            [
                'title' => 'Двускатная двухосная группа (расстояние до 1 м (включительно))',
                'is_spring' => 1,
                'group_axle_count' => 2,
                'wheel_count' => 2,
                'distance_index' => AxleLoad::DISTANCE_INDEX_LESS_1,
                'axle_type' => AxleLoad::AXLE_TYPE_SPRING,
                'wheels' => AxleLoad::WHEELS_LESS_4,
                'value0' => 6, 'value1' => 6, 'value2' => 6,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'title' => 'Двускатная двухосная группа (расстояние свыше 1 м до 1.3 м (включительно))',
                'is_spring' => 1,
                'group_axle_count' => 2,
                'wheel_count' => 2,
                'distance_index' => AxleLoad::DISTANCE_INDEX_1_13,
                'axle_type' => AxleLoad::AXLE_TYPE_SPRING,
                'wheels' => AxleLoad::WHEELS_LESS_4,
                'value0' => 6, 'value1' => 6, 'value2' => 6,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'title' => 'Двускатная двухосная группа  (расстояние свыше 1.3 м до 1.8 м (включительно))',
                'is_spring' => 1,
                'group_axle_count' => 2,
                'wheel_count' => 2,
                'distance_index' => AxleLoad::DISTANCE_INDEX_13_18,
                'axle_type' => AxleLoad::AXLE_TYPE_SPRING,
                'wheels' => AxleLoad::WHEELS_LESS_4,
                'value0' => 6, 'value1' => 6, 'value2' => 6,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'title' => 'Двускатная двухосная группа (расстояние свыше 1.8 м до 2.5 м (включительно))',
                'is_spring' => 1,
                'group_axle_count' => 2,
                'wheel_count' => 2,
                'distance_index' => AxleLoad::DISTANCE_INDEX_18_25,
                'axle_type' => AxleLoad::AXLE_TYPE_SPRING,
                'wheels' => AxleLoad::WHEELS_LESS_4,
                'value0' => 6, 'value1' => 6, 'value2' => 6,
                'created_at' => $now,
                'updated_at' => $now
            ],

            //Трехосные группы
            [
                'title' => 'Двускатная трехосная группа (расстояние до 1 м (включительно))',
                'is_spring' => 1,
                'group_axle_count' => 3,
                'wheel_count' => 2,
                'distance_index' => AxleLoad::DISTANCE_INDEX_LESS_1,
                'axle_type' => AxleLoad::AXLE_TYPE_SPRING,
                'wheels' => AxleLoad::WHEELS_LESS_4,
                'value0' => 6, 'value1' => 6, 'value2' => 6,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'title' => 'Двускатная трехосная группа (расстояние свыше 1 м до 1.3 м (включительно))',
                'is_spring' => 1,
                'group_axle_count' => 3,
                'wheel_count' => 2,
                'distance_index' => AxleLoad::DISTANCE_INDEX_1_13,
                'axle_type' => AxleLoad::AXLE_TYPE_SPRING,
                'wheels' => AxleLoad::WHEELS_LESS_4,
                'value0' => 6, 'value1' => 6, 'value2' => 6,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'title' => 'Двускатная рессорная трехосная группа  (расстояние свыше 1.3 м до 1.8 м (включительно))',
                'is_spring' => 1,
                'group_axle_count' => 3,
                'wheel_count' => 2,
                'distance_index' => AxleLoad::DISTANCE_INDEX_13_18,
                'axle_type' => AxleLoad::AXLE_TYPE_SPRING,
                'wheels' => AxleLoad::WHEELS_LESS_4,
                'value0' => 6, 'value1' => 6, 'value2' => 6,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'title' => 'Двускатная пневматическая трехосная группа  (расстояние свыше 1.3 м до 1.8 м (включительно))',
                'is_spring' => 1,
                'group_axle_count' => 3,
                'wheel_count' => 2,
                'distance_index' => AxleLoad::DISTANCE_INDEX_13_18,
                'axle_type' => AxleLoad::AXLE_TYPE_NON_SPRING,
                'wheels' => AxleLoad::WHEELS_LESS_4,
                'value0' => 6, 'value1' => 6, 'value2' => 6,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'title' => 'Двускатная трехосная группа (расстояние свыше 1.8 м до 2.5 м (включительно))',
                'is_spring' => 1,
                'group_axle_count' => 3,
                'wheel_count' => 2,
                'distance_index' => AxleLoad::DISTANCE_INDEX_18_25,
                'axle_type' => AxleLoad::AXLE_TYPE_SPRING,
                'wheels' => AxleLoad::WHEELS_LESS_4,
                'value0' => 6, 'value1' => 6, 'value2' => 6,
                'created_at' => $now,
                'updated_at' => $now
            ],

            //4-х и более осная группа
            [
                'title' => 'Двухскатная 4-х и более осная группа, имеющая на каждой оси не более 4 колес (расстояние до 1 м (включительно))',
                'is_spring' => 1,
                'group_axle_count' => 4,
                'wheel_count' => 2,
                'distance_index' => AxleLoad::DISTANCE_INDEX_LESS_1,
                'axle_type' => AxleLoad::AXLE_TYPE_SPRING,
                'wheels' => AxleLoad::WHEELS_LESS_4,
                'value0' => 6, 'value1' => 6, 'value2' => 6,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'title' => 'Двухскатная 4-х и более осная группа, имеющая на каждой оси не более 4 колес (расстояние свыше 1 м до 1.3 м (включительно))',
                'is_spring' => 1,
                'group_axle_count' => 4,
                'wheel_count' => 2,
                'distance_index' => AxleLoad::DISTANCE_INDEX_1_13,
                'axle_type' => AxleLoad::AXLE_TYPE_SPRING,
                'wheels' => AxleLoad::WHEELS_LESS_4,
                'value0' => 6, 'value1' => 6, 'value2' => 6,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'title' => 'Двухскатная 4-х и более осная группа, имеющая на каждой оси не более 4 колес (расстояние свыше 1.3 м до 1.8 м (включительно))',
                'is_spring' => 1,
                'group_axle_count' => 4,
                'wheel_count' => 2,
                'distance_index' => AxleLoad::DISTANCE_INDEX_13_18,
                'axle_type' => AxleLoad::AXLE_TYPE_SPRING,
                'wheels' => AxleLoad::WHEELS_LESS_4,
                'value0' => 6, 'value1' => 6, 'value2' => 6,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'title' => 'Двухскатная 4-х и более осная группа, имеющая на каждой оси не более 4 колес (расстояние свыше 1.8 м до 2.5 м (включительно))',
                'is_spring' => 1,
                'group_axle_count' => 4,
                'wheel_count' => 2,
                'distance_index' => AxleLoad::DISTANCE_INDEX_18_25,
                'axle_type' => AxleLoad::AXLE_TYPE_SPRING,
                'wheels' => AxleLoad::WHEELS_LESS_4,
                'value0' => 6, 'value1' => 6, 'value2' => 6,
                'created_at' => $now,
                'updated_at' => $now
            ],

            //Число колес больше или равно 4
            [
                'title' => 'Сближенные оси, имеющие на каждой оси по 8 и более колес (расстояние до 1 м (включительно))',
                'is_spring' => 1,
                'group_axle_count' => 2,
                'wheel_count' => 1,
                'distance_index' => AxleLoad::DISTANCE_INDEX_LESS_1,
                'axle_type' => AxleLoad::AXLE_TYPE_SPRING,
                'wheels' => AxleLoad::WHEELS_MORE_4,
                'value0' => 6, 'value1' => 6, 'value2' => 6,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'title' => 'Сближенные оси, имеющие на каждой оси по 8 и более колес (расстояние свыше 1 м до 1.3 м (включительно))',
                'is_spring' => 1,
                'group_axle_count' => 2,
                'wheel_count' => 1,
                'distance_index' => AxleLoad::DISTANCE_INDEX_1_13,
                'axle_type' => AxleLoad::AXLE_TYPE_SPRING,
                'wheels' => AxleLoad::WHEELS_MORE_4,
                'value0' => 6, 'value1' => 6, 'value2' => 6,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'title' => 'Сближенные оси, имеющие на каждой оси по 8 и более колес (расстояние свыше 1.3 м до 1.8 м (включительно))',
                'is_spring' => 1,
                'group_axle_count' => 2,
                'wheel_count' => 1,
                'distance_index' => AxleLoad::DISTANCE_INDEX_13_18,
                'axle_type' => AxleLoad::AXLE_TYPE_SPRING,
                'wheels' => AxleLoad::WHEELS_MORE_4,
                'value0' => 6, 'value1' => 6, 'value2' => 6,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'title' => 'Сближенные оси, имеющие на каждой оси по 8 и более колес (расстояние свыше 1.8 м до 2.5 м (включительно))',
                'is_spring' => 1,
                'group_axle_count' => 2,
                'wheel_count' => 1,
                'distance_index' => AxleLoad::DISTANCE_INDEX_18_25,
                'axle_type' => AxleLoad::AXLE_TYPE_SPRING,
                'wheels' => AxleLoad::WHEELS_MORE_4,
                'value0' => 6, 'value1' => 6, 'value2' => 6,
                'created_at' => $now,
                'updated_at' => $now
            ]
        ]);
    }
}
