<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class IssuePlaceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$now = Carbon::now();
		DB::table('issue_places')->insert([
			['title' => 'г.Казань, Оренбургский тракт, 5', 'fio' => 'Анфимов С.Г.', 'position' => 'Начальник ТУ г. Казань', 'created_at' => $now, 'updated_at' => $now],
			['title' => 'г.Набережные Челны, проспект Казанский, 72', 'fio' => 'Карпов С.И.', 'position' => 'Начальник ТУ г. Набережные Челны', 'created_at' => $now, 'updated_at' => $now],
			['title' => 'г.Альметьевск, ул. Советская,184 «г»', 'fio' => 'Сураев С.Н.', 'position' => 'Начальник ТУ г. Альметьевск', 'created_at' => $now, 'updated_at' => $now],
			['title' => 'г.Нурлат, ул.Складская, 2', 'fio' => 'Гиниатуллин Р.Р.', 'position' => 'Начальник ТУ г. Нурлат', 'created_at' => $now, 'updated_at' => $now],
		]);
    }
}
