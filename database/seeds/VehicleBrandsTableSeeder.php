<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class VehicleBrandsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = Carbon::now();
        DB::table('vehicle_brands')->insert([
            ['title' => 'Mercedes Benz', 'created_at' => $now, 'updated_at' => $now],
            ['title' => 'Volvo', 'created_at' => $now, 'updated_at' => $now],
            ['title' => 'Renault', 'created_at' => $now, 'updated_at' => $now],
        ]);
    }
}
