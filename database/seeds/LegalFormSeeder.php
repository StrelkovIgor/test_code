<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LegalFormSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = Carbon::now();
        DB::table('legal_forms')->insert([
            ['title' => 'Полное товарищество', 'created_at' => $now, 'updated_at' => $now],
            ['title' => 'Товарищество на вере', 'created_at' => $now, 'updated_at' => $now],

            ['title' => 'Общество с ограниченной ответственностью', 'created_at' => $now, 'updated_at' => $now],
            ['title' => 'Публичное акционерное общество', 'created_at' => $now, 'updated_at' => $now],
            ['title' => 'Непубличное акционерное общество', 'created_at' => $now, 'updated_at' => $now],

            ['title' => 'Производственные кооперативы (артели)', 'created_at' => $now, 'updated_at' => $now],
            ['title' => 'Крестьянские (фермерские) хозяйства', 'created_at' => $now, 'updated_at' => $now],
            ['title' => 'Хозяйственные партнерства', 'created_at' => $now, 'updated_at' => $now],

            ['title' => 'Государственное унитарное предприятие (федеральное)', 'created_at' => $now, 'updated_at' => $now],
            ['title' => 'Государственное унитарное предприятие (субъекта РФ)', 'created_at' => $now, 'updated_at' => $now],
            ['title' => 'Муниципальное унитарное предприятие', 'created_at' => $now, 'updated_at' => $now],

            ['title' => 'Казеное предприятие (федеральное)', 'created_at' => $now, 'updated_at' => $now],
            ['title' => 'Казеное предприятие (субъетка РФ)', 'created_at' => $now, 'updated_at' => $now],
            ['title' => 'Казеное предприятие (муниципальное)', 'created_at' => $now, 'updated_at' => $now],
        ]);
    }
}
