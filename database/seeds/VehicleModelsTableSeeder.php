<?php

use App\Models\Vehicle;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class VehicleModelsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = Carbon::now();
        DB::table('vehicle_models')->insert([
            [
                'title' => 'Actros 1841LS',
                'brand_id' => 1,
                'is_trailer' => 0,
                'hidden' => 0,
                'vehicle_type_id' => Vehicle::TYPE_TRACTOR_1,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'title' => 'Actros 2541L 6х2',
                'brand_id' => 1,
                'is_trailer' => 0,
                'hidden' => 0,
                'vehicle_type_id' => Vehicle::TYPE_CAR_AXLE_2,
                'created_at' => $now,
                'updated_at' => $now
            ],

            [
                'title' => 'FH16',
                'brand_id' => 2,
                'hidden' => 0,
                'is_trailer' => 0,
                'vehicle_type_id' => Vehicle::TYPE_TRACTOR_1,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'title' => 'Ali Riza Usta',
                'brand_id' => 2,
                'hidden' => 0,
                'is_trailer' => 1,
                'vehicle_type_id' => Vehicle::TYPE_TRAILER_AXLE_2,
                'created_at' => $now,
                'updated_at' => $now
            ],
        ]);
    }
}
