FROM php:7.1-fpm-alpine


MAINTAINER alexander@namisoft.ru

# needed packages 
RUN apk add --update --no-cache --virtual .tools-deps supervisor \
            autoconf g++ libtool make libpng-dev libmcrypt-dev libxml2-dev imagemagick-dev\
 && docker-php-ext-install mbstring tokenizer mysqli pdo pdo_mysql gd zip soap \
 && (yes | pecl install imagick) \
 && rm -rf /var/cache/apk/*
 
 RUN set -ex \
    && mv "${PHP_INI_DIR}/php.ini-production" "${PHP_INI_DIR}/php.ini" \
    && sed -i "s|memory_limit =.*|memory_limit = 1024M|" ${PHP_INI_DIR}/php.ini \
    && sed -i "s|upload_max_filesize =.*|upload_max_filesize = 20M|" ${PHP_INI_DIR}/php.ini \
    && sed -i "s|max_execution_time =.*|max_execution_time = 120|" ${PHP_INI_DIR}/php.ini 


#Create Voumes and add data
VOLUME ["/var/www"]
ADD . /var/www/
WORKDIR /var/www/

# Install Composer.
COPY --from=composer /usr/bin/composer /usr/bin/composer

#enable supervisor
COPY apps.ini /etc/supervisor.d/aps.ini
CMD /usr/bin/supervisord --nodaemon 

