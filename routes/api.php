<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//version
Route::get('/version', function () {
    return 'version v2.4.5';
});

Route::any('cafap/server', 'CafapRestController@index')->middleware('request_logger');

Route::group(['prefix' => 'gradoservice', 'middleware' => ['auth:api','throttle:200,1']], function() {
    Route::get('auth', 'GradoserviceController@auth');
    Route::post('distance', 'GradoserviceController@distance');//@todo for testing, remove
});

Route::get('test-application/{application}/price', 'ApplicationController@testPrice');
Route::get('test-application/{application}/distance', 'ApplicationController@testDistance');
Route::get('test-application/{application}/loads', 'ApplicationController@testLoads');



Route::group(['prefix' => 'smev' ,'middleware' => ['request_logger','throttle:200,1']], function() {
    Route::post('register', 'SmevController@register');
    Route::post('send-files', 'SmevController@sendFiles');
    Route::post('check-url', 'SmevController@check');
//    Оставил функционал на тот случай если
//    Route::post('cancel', 'SmevController@cancel');
});

Route::any('smev/test', 'SmevTestController@registerTest');
Route::post('smev-test/register', 'SmevTestController@register');

Route::group(['middleware' => ['auth:api','throttle:200,1']], function() {
    Route::get('request-logs/search', 'RequestLogController@search');
});

//get file
Route::get('files/download/{file}/{name}', 'FilesController@download');

//registration:guests
Route::group(['middleware' => 'throttle:300,1'], function() {
    Route::post('register', 'Auth\RegisterController@register');
    Route::post('register/code-confirmation', 'Auth\RegisterController@codeConfirmation');
    //auth
    Route::post('login', 'Auth\LoginController@login');
    Route::post('logout', 'Auth\LoginController@logout');
    Route::post('email-confirmation', 'Auth\RegisterController@emailConfirmation');
    //restore password
    Route::post('restore-password/change-request', 'Auth\ForgotPasswordController@changeRequest');
    Route::post('restore-password/code-confirmation', 'Auth\ForgotPasswordController@codeConfirmation');
    Route::post('restore-password/new-password', 'Auth\ResetPasswordController@reset');
    Route::get('legal-forms', 'LegalFormController@index');
});

//Reg apps: admin, gbu
Route::group(['middleware' => ['auth:api','throttle:200,1', 'authAdminGbu']], function() {
    Route::get('reg-apps', 'RegAppController@index');
    Route::get('reg-apps/by-status/{status}/{role}', 'RegAppController@byStatus');
    
    Route::put('reg-apps/accept/{user}', 'RegAppController@accept');
    Route::put('reg-apps/decline/{user}', 'RegAppController@decline');
    
    Route::put('reg-apps/lock/{user}', 'RegAppController@lock');
    Route::put('reg-apps/unlock/{user}', 'RegAppController@unlock');
    
    Route::get('reg-apps/{user}', 'RegAppController@show');
    Route::post('reg-apps/admin-filter/{status}/{role}', 'RegAppController@adminFilter');
});

//manage users: all roles
Route::group(['middleware' => ['auth:api','throttle:200,1']], function() {
    Route::get('user/firm-users', 'UserController@firmUsers');
    Route::get('user/my-firm', 'UserController@myFirm');
    Route::get('user', 'UserController@me');
    Route::put('user', 'UserController@updateMe');
    Route::put('user/change-password', 'UserController@changePassword');
});


Route::group(['middleware' => ['auth:api','throttle:200,1', 'authAdmin']], function() {
    Route::post('users', 'UserController@index');
    Route::get('users/{user}', 'UserController@show');
    Route::put('users', 'UserController@store');
    Route::put('users/{user}', 'UserController@update');
    Route::delete('users/{user}', 'UserController@delete');
    
    Route::get('users/firm/{user}', 'UserController@adminFull');
});

//manage firm users
Route::group(['middleware' => ['auth:api','throttle:200,1', 'authFirm']], function() {
    Route::post('firm-users/page', 'FirmUserController@page');
    Route::put('firm-users', 'FirmUserController@store');
    Route::post('firm-users/access', 'FirmUserController@access');
    Route::get('firm-users/binds', 'FirmUserController@binds');
    Route::post('firm-users/{user}', 'FirmUserController@update');
    Route::delete('firm-users/{user}', 'FirmUserController@delete');
});

Route::group(['middleware' => ['auth:api','throttle:200,1', 'authClientEmployee']], function() {
    //user cabinet
    Route::post('vehicles/check-statuses', 'VehicleController@checkStatuses');
    Route::get('vehicles/user/all/{status}', 'VehicleController@userAll');
    Route::get('vehicles/user/{status}', 'VehicleController@user');
    Route::post('vehicles/user/{status}', 'VehicleController@user');
    Route::post('vehicles', 'VehicleController@store');
    Route::put('vehicles/{vehicle}', 'VehicleController@update');
    Route::delete('vehicles/{vehicle}', 'VehicleController@delete');
});
Route::group(['middleware' => ['auth:api','throttle:200,1', 'authAdminGbu']], function() {
    //admin cabinet
    Route::get('vehicles/status/{status}', 'VehicleController@admin');
    Route::post('vehicles/admin-filter/{status}', 'VehicleController@adminFilter');
    Route::post('vehicles/firm/{user}', 'VehicleController@adminFirm');
    Route::put('vehicles/to-work/{vehicle}', 'VehicleController@toWork');
    Route::put('vehicles/accept/{vehicle}', 'VehicleController@accept');
    Route::put('vehicles/decline/{vehicle}', 'VehicleController@decline');
});
//admin and user
Route::group(['middleware' => ['auth:api','throttle:200,1']], function() {
    //@todo change to client or admin
    Route::get('vehicles/{vehicle}', 'VehicleController@show');
});


Route::group(['middleware' => ['auth:api','throttle:200,1', 'authAdmin']], function() {
    Route::get('legal-forms/page', 'LegalFormController@page');
    Route::get('legal-forms/{legalForm}', 'LegalFormController@show');
    Route::post('legal-forms', 'LegalFormController@store');
    Route::put('legal-forms/{legalForm}', 'LegalFormController@update');
    Route::delete('legal-forms/{legalForm}', 'LegalFormController@delete');
});
Route::group(['middleware' => ['auth:api','throttle:200,1', 'authAdmin']], function() {
    Route::get('route-addresses/search/{search}', 'RouteAddressController@search');
    Route::get('route-addresses/page', 'RouteAddressController@page');
    Route::get('route-addresses/{routeAddress}', 'RouteAddressController@show');
    Route::post('route-addresses', 'RouteAddressController@store');
    Route::put('route-addresses/{routeAddress}', 'RouteAddressController@update');
    Route::delete('route-addresses/{routeAddress}', 'RouteAddressController@delete');
});
Route::group(['middleware' => ['auth:api','throttle:200,1', 'authAdmin']], function() {
    Route::post('vehicle-brands/export', 'VehicleBrandController@export');
    Route::get('vehicle-brands', 'VehicleBrandController@index');
    Route::get('vehicle-brands/page', 'VehicleBrandController@page');
    Route::get('vehicle-brands/{vehicleBrand}', 'VehicleBrandController@show');
});
Route::group(['middleware' => ['auth:api','throttle:200,1', 'authAdminGbu']], function() {
    Route::post('vehicle-brands', 'VehicleBrandController@store');
});
Route::group(['middleware' => ['auth:api','throttle:200,1', 'authAdmin']], function() {
    Route::put('vehicle-brands/{vehicleBrand}', 'VehicleBrandController@update');
    Route::delete('vehicle-brands/{vehicleBrand}', 'VehicleBrandController@delete');
    Route::post('vehicle-brands/accept/{vehicleBrand}', 'VehicleBrandController@accept');
    Route::post('vehicle-brands/hide/{vehicleBrand}', 'VehicleBrandController@hide');
});
Route::group(['middleware' => ['auth:api','throttle:200,1', 'authAdmin']], function() {
    Route::get('vehicle-models', 'VehicleModelController@index');
    Route::get('vehicle-models/page', 'VehicleModelController@page');
    Route::get('vehicle-models/{vehicleModel}', 'VehicleModelController@show');
});
Route::group(['middleware' => ['auth:api','throttle:200,1', 'authAdminGbu']], function() {
    Route::post('vehicle-models', 'VehicleModelController@store');
});
Route::group(['middleware' => ['auth:api','throttle:200,1', 'authAdmin']], function() {
    Route::put('vehicle-models/{vehicleModel}', 'VehicleModelController@update');
    Route::delete('vehicle-models/{vehicleModel}', 'VehicleModelController@delete');
    Route::post('vehicle-models/accept/{vehicleModel}', 'VehicleModelController@accept');
    Route::post('vehicle-models/hide/{vehicleModel}', 'VehicleModelController@hide');
});

Route::group(['middleware' => ['auth:api','throttle:200,1', 'authClient']], function() {
    Route::post('files/upload-inn', 'FilesController@uploadInn');
});

Route::group(['middleware' => ['auth:api','throttle:200,1', 'authClientEmployee']], function() {
    Route::post('files/upload-license', 'FilesController@uploadLicense');
    Route::post('files/upload-pts', 'FilesController@uploadPts');
    Route::post('files/upload-lease', 'FilesController@uploadLease');
    Route::post('files/upload-load', 'FilesController@uploadLoad');
    Route::post('files/upload-pay/{application}', 'FilesController@uploadPay');
    Route::post('files/upload-penalty/{application}', 'FilesController@uploadPenalty');
});
Route::group(['middleware' => ['auth:api','throttle:200,1', 'authAdmin']], function() {
    Route::post('files/upload-status', 'FilesController@uploadStatus');
});
Route::group(['middleware' => ['auth:api','throttle:200,1', 'authAdminGbu']], function() {
    Route::post('files/upload-fast-penalty', 'FilesController@uploadFastPenalty');
});
Route::group(['middleware' => ['auth:api','throttle:200,1']], function() {
    Route::post('files/remove/{file}', 'FilesController@remove');
});

Route::group(['middleware' => ['auth:api','throttle:200,1', 'authAdminGbuDepagent']], function() {
    Route::post('files/in-work/{application}', 'FilesController@inWork');
    Route::post('files/in-work-test/{application}', 'FilesController@test');
    Route::get('download/{file}', 'FilesController@downloadNew')->where('file', '[\w\.\/а-я]+');
    Route::delete('file/delete/{file}', 'FilesController@deleteNew')->where('file', '[\w\.\/а-я]+');
});

//departments
Route::group(['middleware' => ['auth:api','throttle:200,1', 'authAdmin']], function() {
    Route::get('departments', 'DepartmentController@index');
    Route::get('departments/page', 'DepartmentController@page');
    Route::get('departments/{department}', 'DepartmentController@show');
    Route::post('departments', 'DepartmentController@store');
    Route::put('departments/{department}', 'DepartmentController@update');
    Route::delete('departments/{department}', 'DepartmentController@delete');
});

//control-posts
Route::group(['middleware' => ['auth:api','throttle:200,1', 'authAdmin']], function() {
    Route::get('control-posts', 'ControlPostController@index');
    Route::get('control-posts/page', 'ControlPostController@page');
    Route::get('control-posts/{controlPost}', 'ControlPostController@show');
    Route::post('control-posts', 'ControlPostController@store');
    Route::put('control-posts/{controlPost}', 'ControlPostController@update');
    Route::delete('control-posts/{controlPost}', 'ControlPostController@delete');
});

//coefficients
Route::group(['middleware' => ['auth:api','throttle:200,1', 'authAdmin']], function() {
    Route::get('coefficients', 'CoefficientController@index');
    Route::get('coefficients/page', 'CoefficientController@page');
    Route::get('coefficients/{coefficient}', 'CoefficientController@show');
    Route::post('coefficients', 'CoefficientController@store');
    Route::put('coefficients/{coefficient}', 'CoefficientController@update');
    Route::delete('coefficients/{coefficient}', 'CoefficientController@delete');
});

//axle loads
Route::group(['middleware' => ['auth:api','throttle:200,1', 'authAdmin']], function() {
    Route::get('axle-loads', 'AxleLoadController@index');
    Route::get('axle-loads/page', 'AxleLoadController@page');
    Route::get('axle-loads/{axleLoad}', 'AxleLoadController@show');
    Route::put('axle-loads/{axleLoad}', 'AxleLoadController@update');
});

//special conditions
Route::group(['middleware' => ['auth:api','throttle:200,1', 'authAdmin']], function() {
    Route::get('special-conditions/{id}', 'SpecialConditionController@show');
    Route::put('special-conditions/{specialCondition}', 'SpecialConditionController@update');
});


//statuses
Route::group(['middleware' => ['auth:api','throttle:200,1', 'authClientEmployee']], function() {
    Route::get('privilege-statuses/check', 'PrivilegeStatusController@check');//client
});
Route::group(['middleware' => ['auth:api','throttle:200,1', 'authAdmin']], function() {
    Route::get('privilege-statuses', 'PrivilegeStatusController@index');
    Route::get('privilege-statuses/page', 'PrivilegeStatusController@page');
    Route::get('privilege-statuses/{privilegeStatus}', 'PrivilegeStatusController@show');
    Route::post('privilege-statuses', 'PrivilegeStatusController@store');
    Route::put('privilege-statuses/{privilegeStatus}', 'PrivilegeStatusController@update');
    Route::delete('privilege-statuses/{privilegeStatus}', 'PrivilegeStatusController@delete');
});

//status vehicles
Route::group(['middleware' => ['auth:api','throttle:200,1', 'authAdmin']], function() {
    Route::post('privilege-vehicles/export/{status}', 'PrivilegeVehicleController@export');
    
    Route::get('privilege-vehicles', 'PrivilegeVehicleController@index');
    Route::get('privilege-vehicles/page', 'PrivilegeVehicleController@page');
    Route::post('privilege-vehicles/page', 'PrivilegeVehicleController@page');
    Route::get('privilege-vehicles/{privilegeVehicle}', 'PrivilegeVehicleController@show');
    Route::post('privilege-vehicles', 'PrivilegeVehicleController@store');
    Route::put('privilege-vehicles/{privilegeVehicle}', 'PrivilegeVehicleController@update');
    Route::delete('privilege-vehicles/{privilegeVehicle}', 'PrivilegeVehicleController@delete');
});


//applications
Route::group(['middleware' => ['auth:api','throttle:200,1', 'authClientEmployee']], function() {
    Route::get('applications/templates', 'ApplicationController@templates');
    Route::post('applications/use-template/{application}', 'ApplicationController@useTemplate');
    Route::post('applications/user/status/{status}', 'ApplicationController@userFilter');
    
    //vehicle step
    Route::post('applications/vehicle', 'ApplicationController@create');
    Route::put('applications/{application}/vehicle', 'ApplicationController@vehicle');
    
    //load step
    Route::get('applications/{application}/load', 'ApplicationController@showLoad');
    Route::put('applications/{application}/load', 'ApplicationController@load');
    
    //route step
    Route::get('applications/{application}/route', 'ApplicationController@showRoute');
    Route::put('applications/{application}/route', 'ApplicationController@route');
    
    //dates step
    Route::get('applications/{application}/dates', 'ApplicationController@showDates');
    Route::put('applications/{application}/dates', 'ApplicationController@dates');
    
    //final
    Route::put('applications/{application}/withdraw', 'ApplicationController@withdraw');
    Route::put('applications/{application}/to-admin', 'ApplicationController@toAdmin');
    
    Route::delete('applications/{application}', 'ApplicationController@delete');
});
Route::group(['middleware' => ['auth:api','throttle:200,1', 'authAdminGbuDepagent']], function() {
    Route::post('applications/admin/{status}', 'ApplicationController@adminFilter');
    Route::put('applications/{application}/to-work', 'ApplicationController@toWork');
    Route::post('applications/to-work-multiple', 'ApplicationController@toWorkMultiple');
    Route::get('applications/{application}/reset-status', 'ApplicationController@reset_status');
});
Route::group(['prefix' => 'applications/{application}', 'middleware' => ['auth:api','throttle:200,1', 'authAdminGbuDepagent']], function() {
    //Adding Routes
//    Route::put('track/{save?}', 'ApplicationController@track')->where('save','save');
//    Route::DELETE('track/{timeId}', 'ApplicationController@trackRemove');
    Route::put('route-save', 'ApplicationController@routeSave');
});
Route::get('route-list', 'ApplicationController@getRouteList');
Route::group(['middleware' => ['auth:api','throttle:200,1', 'authAdminGbu']], function() {
    Route::post('applications/{application}/change-route-info', 'ApplicationController@changeRouteInfo');
    Route::put('applications/{application}/accept', 'ApplicationController@accept');
    Route::put('applications/{application}/activate', 'ApplicationController@activate');
    Route::put('applications/{application}/decline', 'ApplicationController@decline');
    Route::put('applications/{application}/need_attachments', 'ApplicationController@awaitingAttachments');

    Route::put('applications/{application}/lock', 'ApplicationController@lock');
    Route::put('applications/{application}/unlock', 'ApplicationController@unlock');
    
    Route::put('applications/{id}/restore', 'ApplicationController@restore');
    Route::post('applications/{application}/print-pdf', 'ApplicationController@printPdf');
    
    //@todo check only admin?
    Route::post('applications/{application}/send-to-department', 'ApplicationController@sendToDepartment');

    Route::get('applications/{application}/test-calculator','ApplicationController@test_calc');
});

Route::group(['middleware' => ['auth:api','throttle:200,1', 'authControl']], function() {
    Route::post('applications/control', 'ApplicationController@controlFilter');
    Route::post('applications/{application}/add-control-mark', 'ApplicationController@addControlMark');
});


//@todo
Route::group(['middleware' => ['auth:api','throttle:200,1']], function() {
    Route::get('applications/location', 'ApplicationController@location');
    
    Route::get('applications/{application}', 'ApplicationController@show');
    Route::get('applications/{application}/full', 'ApplicationController@showFull');
    
    Route::delete('applications/remove-agreement/{agreement}', 'ApplicationController@removeAgreement');
});


//fast applications
Route::group(['middleware' => ['auth:api','throttle:200,1', 'authAdminGbu']], function() {
    Route::post('fast-applications/create', 'FastApplicationController@store');
    Route::post('fast-applications/admin/{status}', 'FastApplicationController@adminFilter');
});

//agreements
Route::group(['middleware' => ['auth:api','throttle:200,1', 'authDepagent']], function() {
    Route::put('application-agreements/{application}/accept', 'ApplicationAgreementController@accept');
    Route::put('application-agreements/{application}/decline', 'ApplicationAgreementController@decline');
});

//audition
Route::group(['prefix' => 'audition', 'middleware' => ['auth:api','throttle:200,1', 'authAdmin']], function() {
    Route::post('list', 'AuditionController@list');
    Route::post('export', 'AuditionController@export');
    Route::get('directory', 'AuditionController@directory');
});

//reports
Route::group(['prefix' => 'reports', 'middleware' => ['auth:api','throttle:200,1', 'authAdmin']], function() {
    Route::get('statistic', 'ReportsController@statistic');
    
    Route::post('active-export', 'ReportsController@activeExport');
    Route::post('active', 'ReportsController@active');

	Route::get('statistic', 'ReportsController@statistic');

	Route::post('active-export', 'ReportsController@activeExport');
    Route::post('all-export', 'ReportsController@allExport');
	Route::post('active', 'ReportsController@active');
});

//reports
Route::group(['prefix' => 'processes', 'middleware' => ['auth:api','throttle:200,1', 'authAdmin']], function() {
    Route::get('list', 'ProcessController@list');
    Route::get('{process}/full', 'ProcessController@show');
});

//APVGK
Route::group(['prefix' => 'apvgk','middleware' => ['auth:api','throttle:200,1', 'authAdmin']], function() {
    Route::post('create', 'APVGKController@create');
    Route::get('page', 'APVGKController@page');
    Route::put('{apvgk}', 'APVGKController@update');
    Route::delete('{apvgk}', 'APVGKController@delete');
});

Route::group(['prefix' => 'eputs','middleware' => 'eputs','namespace' =>'Eputs'], function() {
    //auth
    Route::post('/authorize', 'EputsAuthController@login');
    Route::group(['middleware' => 'AuthEputs'], function() {
        Route::get('/application/list', 'EputsApplicationController@list');
        Route::get('/application/directory', 'EputsApplicationController@directory');
//        Route::get('/ramka/location/list', 'EputsApplicationController@locationList');
//        Route::get('/ramka/status', 'EputsApplicationController@locationList');
//        Route::get('/ramka/{application}/speed', 'EputsApplicationController@locationList');
    });

});