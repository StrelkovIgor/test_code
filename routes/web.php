<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	return 'version v2.4.5';
});

//Route::get('application/{application}/price', 'ApplicationController@testPrice');
//Route::get('application/{application}/loads', 'ApplicationController@testLoads');
//Route::get('application/{application}/distance', 'ApplicationController@testDistance');
//Route::get('application/{id}/invoice', 'ApplicationController@generateInvoice');
//Route::get('application/brands', 'ApplicationController@brands');
//Route::get('application/privilege', 'ApplicationController@privilege');
//Route::get('application/{application}/pdf', 'ApplicationController@pdf');
//Route::get('application/csv', 'ApplicationController@csv');
//Route::get('application/{application}/username-fix', 'ApplicationController@usernameFix');
//
//Route::get('vehicle/wheel_count', 'VehicleController@wheelCount');
//registration:guests

